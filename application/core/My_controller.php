<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_controller extends MX_Controller {

	function __construct(){
		parent::__construct();
	}


	public function index()
	{
		$this->load->view('welcome_message');
	}
}
