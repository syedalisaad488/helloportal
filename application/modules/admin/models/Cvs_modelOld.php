<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cvs_model extends CI_Model {
    function mask($record){

        if(count($record) > 0){

            // Check user
            $this->db->select('*');
            $this->db->where('IDmask', $record[0]);
            $this->db->where("mask_status != 'Deleted' ");

            $q = $this->db->get('t_mask');

            $this->db->where("mask_med_id",isset($record[13]) ? $record[13] : '');
            $med_id = $this->db->get("t_mask")->result_array();

            if(empty($med_id[0]['mask_med_id'])){

                $response = $q->result_array();

                if(count($response) == 0){
                    $newuser = array(

                        'mask_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                        'mask_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                        'mask_address'           => isset($record[2]) ? trim($record[2]) : '',
                        'mask_city'         => isset($record[3]) ? trim($record[3]) : '',
                        'mask_state'            => isset($record[4]) ? trim($record[4]) : '',
                        'mask_zip'           => isset($record[5]) ? trim($record[5]) : '',
                        'mask_dob'             => isset($record[6]) ? trim($record[6]) : '',
                        'mask_phone'          => isset($record[7]) ? trim($record[7]) : '',
                        'mask_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                        'mask_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                        'mask_doc_npi' 		  => isset($record[10]) ? trim($record[10]) : '',
                        'mask_doc_phone'   => isset($record[11]) ? trim($record[11]) : '',
                        'mask_doc_fax' => isset($record[12]) ? trim($record[12]) : '',
                        'mask_med_id'   => isset($record[13]) ? trim($record[13]) : '',
                        'mask_repcode' 		  => isset($record[14]) ? trim($record[14]) : '',
                        'mask_fax_back'   => isset($record[15]) ? trim($record[15]) : '',
                        'mask_call_back_num' 		  => isset($record[16]) ? trim($record[16]) : '',
                        'mask_data_entered' 	  => date("d-m-Y"),
                        'userID'          => $this->session->userdata('id')

                    );

                    $this->db->insert('t_mask', $newuser);
                }

            }else{
                $newuser = array(
                    'mask_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                    'mask_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                    'mask_address'           => isset($record[2]) ? trim($record[2]) : '',
                    'mask_city'         => isset($record[3]) ? trim($record[3]) : '',
                    'mask_state'            => isset($record[4]) ? trim($record[4]) : '',
                    'mask_zip'           => isset($record[5]) ? trim($record[5]) : '',
                    'mask_dob'             => isset($record[6]) ? trim($record[6]) : '',
                    'mask_phone'          => isset($record[7]) ? trim($record[7]) : '',
                    'mask_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                    'mask_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                    'mask_doc_npi' 		  => isset($record[10]) ? trim($record[10]) : '',
                    'mask_doc_phone'   => isset($record[11]) ? trim($record[11]) : '',
                    'mask_doc_fax' => isset($record[12]) ? trim($record[12]) : '',
                    'mask_med_id'   => isset($record[13]) ? trim($record[13]) : '',
                    'mask_call_back_num' 		  => isset($record[14]) ? trim($record[14]) : '',
                    'mask_fax_back'   => isset($record[15]) ? trim($record[15]) : '',
                    'mask_repcode' 		  => isset($record[16]) ? trim($record[16]) : '',
                    'mask_data_entered' 	  => date("d-m-Y"),
                    'userID'          => $this->session->userdata('id'),
                    'mask_status'          => 'dupe'

                );

                $this->db->insert('t_mask', $newuser);
            }

        }

    }
    function heatpad($record){


        if(count($record) > 0){

            // Check user
            $this->db->select('*');
            $this->db->where('IDheatpad', $record[0]);
            $this->db->where("heatpad_status != 'Deleted' ");

            $q = $this->db->get('t_heatpad');

            $this->db->where("heatpad_med_id",isset($record[17]) ? $record[17] : '');
            $med_id = $this->db->get("t_heatpad")->result_array();
            if(empty($med_id[0]['heatpad_med_id'])){
                $response = $q->result_array();
                if(count($response) == 0){
                    $newuser = array(
                        'heatpad_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                        'heatpad_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                        'heatpad_address'           => isset($record[2]) ? trim($record[2]) : '',
                        'heatpad_city'         => isset($record[3]) ? trim($record[3]) : '',
                        'heatpad_state'            => isset($record[4]) ? trim($record[4]) : '',
                        'heatpad_zip'           => isset($record[5]) ? trim($record[5]) : '',
                        'heatpad_dob'             => isset($record[6]) ? trim($record[6]) : '',
                        'heatpad_phone'          => isset($record[7]) ? trim($record[7]) : '',
                        'heatpad_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                        'heatpad_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                        'heatpad_doc_address' 	  => isset($record[10]) ? trim($record[10]) : '',
                        'heatpad_doc_city' 	      => isset($record[11]) ? trim($record[11]) : '',
                        'heatpad_doc_state' 	  => isset($record[12]) ? trim($record[12]) : '',
                        'heatpad_doc_zip' 		  => isset($record[13]) ? trim($record[13]) : '',
                        'heatpad_doc_npi' 		  => isset($record[14]) ? trim($record[14]) : '',
                        'heatpad_doc_phone'   => isset($record[15]) ? trim($record[15]) : '',
                        'heatpad_doc_fax' => isset($record[16]) ? trim($record[16]) : '',
                        'heatpad_med_id'   => isset($record[17]) ? trim($record[17]) : '',
                        'heatpad_call_back_num' 		  => isset($record[18]) ? trim($record[18]) : '',
                        'heatpad_fax_back'   => isset($record[19]) ? trim($record[19]) : '',
                        'heatpad_repcode' 		  => isset($record[20]) ? trim($record[20]) : '',
                        'heatpad_data_entered' 	  => date("d-m-Y"),
                        'userID'          => $this->session->userdata('id')
                    );
                    $this->db->insert('t_heatpad', $newuser);
                }

            }else{
                $newuser = array(
                    'heatpad_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                    'heatpad_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                    'heatpad_address'           => isset($record[2]) ? trim($record[2]) : '',
                    'heatpad_city'         => isset($record[3]) ? trim($record[3]) : '',
                    'heatpad_state'            => isset($record[4]) ? trim($record[4]) : '',
                    'heatpad_zip'           => isset($record[5]) ? trim($record[5]) : '',
                    'heatpad_dob'             => isset($record[6]) ? trim($record[6]) : '',
                    'heatpad_phone'          => isset($record[7]) ? trim($record[7]) : '',
                    'heatpad_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                    'heatpad_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                    'heatpad_doc_address' 	  => isset($record[10]) ? trim($record[10]) : '',
                    'heatpad_doc_city' 	      => isset($record[11]) ? trim($record[11]) : '',
                    'heatpad_doc_state' 	  => isset($record[12]) ? trim($record[12]) : '',
                    'heatpad_doc_zip' 		  => isset($record[13]) ? trim($record[13]) : '',
                    'heatpad_doc_npi' 		  => isset($record[14]) ? trim($record[14]) : '',
                    'heatpad_doc_phone'   => isset($record[15]) ? trim($record[15]) : '',
                    'heatpad_doc_fax' => isset($record[16]) ? trim($record[16]) : '',
                    'heatpad_med_id'   => isset($record[17]) ? trim($record[17]) : '',
                    'heatpad_call_back_num' 		  => isset($record[18]) ? trim($record[18]) : '',
                    'heatpad_fax_back'   => isset($record[19]) ? trim($record[19]) : '',
                    'heatpad_repcode' 		  => isset($record[20]) ? trim($record[20]) : '',
                    'heatpad_data_entered' 	  => date("d-m-Y"),
                    'userID'          => $this->session->userdata('id'),
                    'heatpad_status'          => 'dupe'

                );

                $this->db->insert('t_heatpad', $newuser);
            }

        }

    }
    function hipaa($record){


        if(count($record) > 0){

            // Check user
            $this->db->select('*');
            $this->db->where('IDhipaa', $record[0]);
            $this->db->where("hipaa_status != 'Deleted' ");

            $q = $this->db->get('t_hipaa');

            $this->db->where("hipaa_med_id",isset($record[17]) ? $record[17] : '');
            $med_id = $this->db->get("t_hipaa")->result_array();

            if(empty($med_id[0]['hipaa_med_id'])){

                $response = $q->result_array();

                if(count($response) == 0){
                    $newuser = array(
                        'hipaa_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                        'hipaa_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                        'hipaa_address'           => isset($record[2]) ? trim($record[2]) : '',
                        'hipaa_city'         => isset($record[3]) ? trim($record[3]) : '',
                        'hipaa_state'            => isset($record[4]) ? trim($record[4]) : '',
                        'hipaa_zip'           => isset($record[5]) ? trim($record[5]) : '',
                        'hipaa_dob'             => isset($record[6]) ? trim($record[6]) : '',
                        'hipaa_phone'          => isset($record[7]) ? trim($record[7]) : '',
                        'hipaa_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                        'hipaa_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                        'hipaa_doc_address' 	  => isset($record[10]) ? trim($record[10]) : '',
                        'hipaa_doc_city' 	      => isset($record[11]) ? trim($record[11]) : '',
                        'hipaa_doc_state' 	  => isset($record[12]) ? trim($record[12]) : '',
                        'hipaa_doc_zip' 		  => isset($record[13]) ? trim($record[13]) : '',
                        'hipaa_doc_phone'   => isset($record[14]) ? trim($record[14]) : '',
                        'hipaa_doc_fax' => isset($record[15]) ? trim($record[15]) : '',
                        'hipaa_doc_npi' => isset($record[16]) ? trim($record[16]) : '',
                        'hipaa_med_id'   => isset($record[17]) ? trim($record[17]) : '',
                        'hipaa_call_back_num' 		  => isset($record[18]) ? trim($record[18]) : '',
                        'hipaa_fax_back' 		  => isset($record[19]) ? trim($record[19]) : '',
                        'hipaa_repcode' 		  => isset($record[20]) ? trim($record[20]) : '',
                        'hipaa_data_entered' 	  => date("d-m-Y"),
                        'userID'          => $this->session->userdata('id'),
                       
                    );
                    $this->db->insert('t_hipaa', $newuser);
                }

            }else{
                $newuser = array(
                    'hipaa_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                    'hipaa_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                    'hipaa_address'           => isset($record[2]) ? trim($record[2]) : '',
                    'hipaa_city'         => isset($record[3]) ? trim($record[3]) : '',
                    'hipaa_state'            => isset($record[4]) ? trim($record[4]) : '',
                    'hipaa_zip'           => isset($record[5]) ? trim($record[5]) : '',
                    'hipaa_dob'             => isset($record[6]) ? trim($record[6]) : '',
                    'hipaa_phone'          => isset($record[7]) ? trim($record[7]) : '',
                    'hipaa_doc_firstname'     => isset($record[8])? trim($record[8]) : '',
                    'hipaa_doc_lastname'        => isset($record[9])? trim($record[9]) : '',
                    'hipaa_doc_address' 	  => isset($record[10]) ? trim($record[10]) : '',
                    'hipaa_doc_city' 	      => isset($record[11]) ? trim($record[11]) : '',
                    'hipaa_doc_state' 	  => isset($record[12]) ? trim($record[12]) : '',
                    'hipaa_doc_zip' 		  => isset($record[13]) ? trim($record[13]) : '',
                    'hipaa_doc_phone'   => isset($record[14]) ? trim($record[14]) : '',
                    'hipaa_doc_fax' => isset($record[15]) ? trim($record[15]) : '',
                    'hipaa_doc_npi' => isset($record[16]) ? trim($record[16]) : '',
                    'hipaa_med_id'   => isset($record[17]) ? trim($record[17]) : '',
                    'hipaa_call_back_num' 		  => isset($record[18]) ? trim($record[18]) : '',
                    'hipaa_fax_back' 		  => isset($record[19]) ? trim($record[19]) : '',
                    'hipaa_repcode' 		  => isset($record[20]) ? trim($record[20]) : '',
                    'hipaa_data_entered' 	  => date("d-m-Y"),
                    'userID'          => $this->session->userdata('id'),
                    'hipaa_status'          => 'dupe'

                );

                $this->db->insert('t_hipaa', $newuser);
            }

        }

    }



    function brace($record){

        if(count($record) > 0){

            // Check user
            $this->db->select('*');
            $this->db->where('IDaurbrac', $record[0]);
            $this->db->where("aurbrac_status != 'Deleted' ");

            $q = $this->db->get('t_aurtho_brace');

            $this->db->where("aurbrac_med_id",isset($record[35]) ? $record[35] : '');
            $med_id = $this->db->get("t_aurtho_brace")->result_array();

            if(empty($med_id[0]['aurbrac_med_id'])){
                $response = $q->result_array();
                if(count($response) == 0){
                    $newuser = array(
                        'aurbrac_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                        'aurbrac_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                        'aurbrac_address'           => isset($record[2]) ? trim($record[2]) : '',
                        'aurbrac_city'         => isset($record[3]) ? trim($record[3]) : '',
                        'aurbrac_state'            => isset($record[4]) ? trim($record[4]) : '',
                        'aurbrac_zip'           => isset($record[5]) ? trim($record[5]) : '',
                        'aurbrac_dob'             => isset($record[6]) ? trim($record[6]) : '',
                        'aurbrac_phone'          => isset($record[7]) ? trim($record[7]) : '',
                        'aurbrac_gender'            => isset($record[8]) ? trim($record[8]) : '',
                        'aurbrac_height'            => isset($record[9]) ? trim($record[9]) : '',
                        'aurbrac_weight'            => isset($record[10]) ? trim($record[10]) : '',
                        'aurbrac_pain_level'            => isset($record[11]) ? trim($record[11]) : '',
                        'aurbrac_back'      => isset($record[12]) ? (trim($record[12])== 'yes' OR trim($record[12])== 'Yes' OR trim($record[12])== 'YES' OR trim($record[12])== 'YeS' OR trim($record[12])== 'y' ? '1' : '0') : '0',
                        'aurbrac_l_knee'    => isset($record[13]) ? (trim($record[13])== 'yes' OR trim($record[13])== 'Yes' OR trim($record[13])== 'YES' OR trim($record[13])== 'YeS' OR trim($record[13])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_knee'    => isset($record[14]) ? (trim($record[14])== 'yes' OR trim($record[14])== 'Yes' OR trim($record[14])== 'YES' OR trim($record[14])== 'YeS' OR trim($record[14])== 'y' ? '1' : '0') : '0',
                        'aurbrac_l_wrist'   => isset($record[15]) ? (trim($record[15])== 'yes' OR trim($record[15])== 'Yes' OR trim($record[15])== 'YES' OR trim($record[15])== 'YeS' OR trim($record[15])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_wrist'   => isset($record[16]) ? (trim($record[16])== 'yes' OR trim($record[16])== 'Yes' OR trim($record[16])== 'YES' OR trim($record[16])== 'YeS' OR trim($record[16])== 'y' ? '1' : '0'): '0',
                        'aurbrac_l_ankle'   => isset($record[17]) ? (trim($record[17])== 'yes' OR trim($record[17])== 'Yes' OR trim($record[17])== 'YES' OR trim($record[17])== 'YeS' OR trim($record[17])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_ankle'   => isset($record[18]) ? (trim($record[18])== 'yes' OR trim($record[18])== 'Yes' OR trim($record[18])== 'YES' OR trim($record[18])== 'YeS' OR trim($record[18])== 'y' ? '1' : '0') : '0',
                        'aurbrac_l_shoulder' => isset($record[19]) ? (trim($record[19])== 'yes' OR trim($record[19])== 'Yes' OR trim($record[19])== 'YES' OR trim($record[19])== 'YeS' OR trim($record[19])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_shoulder' => isset($record[20]) ? (trim($record[20])== 'yes' OR trim($record[20])== 'Yes' OR trim($record[20])== 'YES' OR trim($record[20])== 'YeS' OR trim($record[20])== 'y' ? '1' : '0') : '0',
                        'aurbrac_l_elbow'    => isset($record[21]) ? (trim($record[21])== 'yes' OR trim($record[21])== 'Yes' OR trim($record[21])== 'YES' OR trim($record[21])== 'YeS' OR trim($record[21])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_elbow'   => isset($record[22]) ? (trim($record[22])== 'yes' OR trim($record[22])== 'Yes' OR trim($record[22])== 'YES' OR trim($record[22])== 'YeS' OR trim($record[22])== 'y' ? '1' : '0'): '0',
                        'aurbrac_l_hip'    => isset($record[23]) ? (trim($record[23])== 'yes' OR trim($record[23])== 'Yes' OR trim($record[23])== 'YES' OR trim($record[23])== 'YeS' OR trim($record[23])== 'y' ? '1' : '0') : '0',
                        'aurbrac_r_hip'    => isset($record[24]) ? (trim($record[24])== 'yes' OR trim($record[24])== 'Yes' OR trim($record[24])== 'YES' OR trim($record[24])== 'YeS' OR trim($record[24])== 'y' ? '1' : '0') : '0',
                        'aurbrac_neck'    => isset($record[25]) ? (trim($record[25])== 'yes' OR trim($record[25])== 'Yes' OR trim($record[25])== 'YES' OR trim($record[25])== 'YeS' OR trim($record[25])== 'y' ? '1' : '0') : '0',
                        'aurbrac_doc_firstname'     => isset($record[26])? trim($record[26]) : '',
                        'aurbrac_doc_lastname'        => isset($record[27])? trim($record[27]) : '',
                        'aurbrac_doc_address' 	  => isset($record[28]) ? trim($record[28]) : '',
                        'aurbrac_doc_city' 	      => isset($record[29]) ? trim($record[29]) : '',
                        'aurbrac_doc_state' 	  => isset($record[30]) ? trim($record[30]) : '',
                        'aurbrac_doc_zip' 		  => isset($record[31]) ? trim($record[31]) : '',
                        'aurbrac_doc_npi' 		  => isset($record[32]) ? trim($record[32]) : '',
                        'aurbrac_doc_phone'   => isset($record[33]) ? trim($record[33]) : '',
                        'aurbrac_doc_fax' => isset($record[34]) ? trim($record[34]) : '',
                        'aurbrac_med_id' 	  => isset($record[35]) ? trim($record[35]) : '',
                        'aurbrac_call_back_num'    => isset($record[36]) ? trim($record[36]) : '',
                        'aurbrac_fax_back'    => isset($record[37]) ? trim($record[37]) : '',
                        'aurbrac_repcode'     => isset($record[38]) ? trim($record[38]) : '',
                        'aurbrac_data_entered' 	  => date("m-d-Y"),
                        'userID'          => $this->session->userdata('id')



                    );

                    $this->db->insert('t_aurtho_brace', $newuser);
                }

            }else{
                $newuser = array(
                    'aurbrac_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                    'aurbrac_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                    'aurbrac_address'           => isset($record[2]) ? trim($record[2]) : '',
                    'aurbrac_city'         => isset($record[3]) ? trim($record[3]) : '',
                    'aurbrac_state'            => isset($record[4]) ? trim($record[4]) : '',
                    'aurbrac_zip'           => isset($record[5]) ? trim($record[5]) : '',
                    'aurbrac_dob'             => isset($record[6]) ? trim($record[6]) : '',
                    'aurbrac_phone'          => isset($record[7]) ? trim($record[7]) : '',
                    'aurbrac_gender'            => isset($record[8]) ? trim($record[8]) : '',
                    'aurbrac_height'            => isset($record[9]) ? trim($record[9]) : '',
                    'aurbrac_weight'            => isset($record[10]) ? trim($record[10]) : '',
                    'aurbrac_pain_level'            => isset($record[11]) ? trim($record[11]) : '',
                    'aurbrac_back'      => isset($record[12]) ? (trim($record[12])== 'yes' OR trim($record[12])== 'Yes' OR trim($record[12])== 'YES' OR trim($record[12])== 'YeS' OR trim($record[12])== 'y' ? '1' : '0') : '0',
                    'aurbrac_l_knee'    => isset($record[13]) ? (trim($record[13])== 'yes' OR trim($record[13])== 'Yes' OR trim($record[13])== 'YES' OR trim($record[13])== 'YeS' OR trim($record[13])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_knee'    => isset($record[14]) ? (trim($record[14])== 'yes' OR trim($record[14])== 'Yes' OR trim($record[14])== 'YES' OR trim($record[14])== 'YeS' OR trim($record[14])== 'y' ? '1' : '0') : '0',
                    'aurbrac_l_wrist'   => isset($record[15]) ? (trim($record[15])== 'yes' OR trim($record[15])== 'Yes' OR trim($record[15])== 'YES' OR trim($record[15])== 'YeS' OR trim($record[15])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_wrist'   => isset($record[16]) ? (trim($record[16])== 'yes' OR trim($record[16])== 'Yes' OR trim($record[16])== 'YES' OR trim($record[16])== 'YeS' OR trim($record[16])== 'y' ? '1' : '0'): '0',
                    'aurbrac_l_ankle'   => isset($record[17]) ? (trim($record[17])== 'yes' OR trim($record[17])== 'Yes' OR trim($record[17])== 'YES' OR trim($record[17])== 'YeS' OR trim($record[17])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_ankle'   => isset($record[18]) ? (trim($record[18])== 'yes' OR trim($record[18])== 'Yes' OR trim($record[18])== 'YES' OR trim($record[18])== 'YeS' OR trim($record[18])== 'y' ? '1' : '0') : '0',
                    'aurbrac_l_shoulder' => isset($record[19]) ? (trim($record[19])== 'yes' OR trim($record[19])== 'Yes' OR trim($record[19])== 'YES' OR trim($record[19])== 'YeS' OR trim($record[19])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_shoulder' => isset($record[20]) ? (trim($record[20])== 'yes' OR trim($record[20])== 'Yes' OR trim($record[20])== 'YES' OR trim($record[20])== 'YeS' OR trim($record[20])== 'y' ? '1' : '0') : '0',
                    'aurbrac_l_elbow'    => isset($record[21]) ? (trim($record[21])== 'yes' OR trim($record[21])== 'Yes' OR trim($record[21])== 'YES' OR trim($record[21])== 'YeS' OR trim($record[21])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_elbow'   => isset($record[22]) ? (trim($record[22])== 'yes' OR trim($record[22])== 'Yes' OR trim($record[22])== 'YES' OR trim($record[22])== 'YeS' OR trim($record[22])== 'y' ? '1' : '0'): '0',
                    'aurbrac_l_hip'    => isset($record[23]) ? (trim($record[23])== 'yes' OR trim($record[23])== 'Yes' OR trim($record[23])== 'YES' OR trim($record[23])== 'YeS' OR trim($record[23])== 'y' ? '1' : '0') : '0',
                    'aurbrac_r_hip'    => isset($record[24]) ? (trim($record[24])== 'yes' OR trim($record[24])== 'Yes' OR trim($record[24])== 'YES' OR trim($record[24])== 'YeS' OR trim($record[24])== 'y' ? '1' : '0') : '0',
                    'aurbrac_neck'    => isset($record[25]) ? (trim($record[25])== 'yes' OR trim($record[25])== 'Yes' OR trim($record[25])== 'YES' OR trim($record[25])== 'YeS' OR trim($record[25])== 'y' ? '1' : '0') : '0',
                    'aurbrac_doc_firstname'     => isset($record[26])? trim($record[26]) : '',
                    'aurbrac_doc_lastname'        => isset($record[27])? trim($record[27]) : '',
                    'aurbrac_doc_address' 	  => isset($record[28]) ? trim($record[28]) : '',
                    'aurbrac_doc_city' 	      => isset($record[29]) ? trim($record[29]) : '',
                    'aurbrac_doc_state' 	  => isset($record[30]) ? trim($record[30]) : '',
                    'aurbrac_doc_zip' 		  => isset($record[31]) ? trim($record[31]) : '',
                    'aurbrac_doc_npi' 		  => isset($record[32]) ? trim($record[32]) : '',
                    'aurbrac_doc_phone'   => isset($record[33]) ? trim($record[33]) : '',
                    'aurbrac_doc_fax' => isset($record[34]) ? trim($record[34]) : '',
                    'aurbrac_med_id' 	  => isset($record[35]) ? trim($record[35]) : '',
                    'aurbrac_call_back_num'    => isset($record[36]) ? trim($record[36]) : '',
                    'aurbrac_fax_back'    => isset($record[37]) ? trim($record[37]) : '',
                    'aurbrac_repcode'     => isset($record[38]) ? trim($record[38]) : '',
                    'aurbrac_data_entered' 	  => date("m-d-Y"),
                    'userID'          => $this->session->userdata('id'),
                    'aurbrac_status'          => 'dupe'

                );

                $this->db->insert('t_aurtho_brace', $newuser);
            }

        }

    }
    function cgm($record){

        if(count($record) > 0){

            // Check user
            $this->db->select('*');
            $this->db->where('IDcgmdiab', $record[0]);
            $q = $this->db->get('t_cgm_diabetic');

            $this->db->where("cgmdiab_medicare",isset($record[18]) ? $record[18] : '');
            $med_id = $this->db->get("t_cgm_diabetic")->result_array();


            if(empty($med_id[0]['cgmdiab_medicare'])){

                $response = $q->result_array();

                if(count($response) == 0){
                    $newuser = array(
                        'cgmdiab_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                        'cgmdiab_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                        'cgmdiab_address'           => isset($record[2]) ? trim($record[2]) : '',
                        'cgmdiab_city'         => isset($record[3]) ? trim($record[3]) : '',
                        'cgmdiab_state'            => isset($record[4]) ? trim($record[4]) : '',
                        'cgmdiab_zip'             => isset($record[5]) ? trim($record[5]) : '',
                        'cgmdiab_email'             => isset($record[6]) ? trim($record[6]) : '',
                        'cgmdiab_dob'          => isset($record[7]) ? trim($record[7]) : '',
                        'cgmdiab_phone'             => isset($record[8]) ? trim($record[8]) : '',
                        'cgmdiab_doc_firstname'    => isset($record[9]) ? trim($record[9]) : '',
                        'cgmdiab_doc_lastname'      => isset($record[10]) ? trim($record[10]) : '',
                        'cgmdiab_doc_address'      => isset($record[11]) ? trim($record[11]) : '',
                        'cgmdiab_doc_city'          => isset($record[12]) ? trim($record[12]) : '',
                        'cgmdiab_doc_state' 	  => isset($record[13]) ? trim($record[13]) : '',
                        'cgmdiab_doc_zip'          => isset($record[14]) ? trim($record[14]) : '',
                        'cgmdiab_doc_npi'   => isset($record[15]) ? trim($record[15]) : '',
                        'cgmdiab_doc_phone'    => isset($record[16]) ? trim($record[16]) : '',
                        'cgmdiab_doc_fax'     => isset($record[17]) ? trim($record[17]) : '',
                        'cgmdiab_medicare' 		  => isset($record[18]) ? trim($record[18]) : '',
                        'cgmdiab_insurance_com_id'   => isset($record[19]) ? trim($record[19]) : '',
                        'cgmdiab_insurance_com'   => isset($record[20]) ? trim($record[20]) : '',
                        'cgmdiab_call_back_num'          => isset($record[21]) ? trim($record[21]) : '',
                        'cgmdiab_fax_back'          => isset($record[22]) ? trim($record[22]) : '',
                        'cgmdiab_repcode'          => isset($record[23]) ? trim($record[23]) : '',
                        'cgmdiab_date_entered'          => date("d-m-Y"),
                        'userID'          => $this->session->userdata('id')

                    );

                    $this->db->insert('t_cgm_diabetic', $newuser);
                }

            }else {

                $newuser = array(
                    'cgmdiab_firstname'       => isset($record[0]) ? trim($record[0]) : '',
                    'cgmdiab_lastname'        => isset($record[1]) ? trim($record[1]) : '',
                    'cgmdiab_address'           => isset($record[2]) ? trim($record[2]) : '',
                    'cgmdiab_city'         => isset($record[3]) ? trim($record[3]) : '',
                    'cgmdiab_state'            => isset($record[4]) ? trim($record[4]) : '',
                    'cgmdiab_zip'             => isset($record[5]) ? trim($record[5]) : '',
                    'cgmdiab_email'             => isset($record[6]) ? trim($record[6]) : '',
                    'cgmdiab_dob'          => isset($record[7]) ? trim($record[7]) : '',
                    'cgmdiab_phone'             => isset($record[8]) ? trim($record[8]) : '',
                    'cgmdiab_doc_firstname'    => isset($record[9]) ? trim($record[9]) : '',
                    'cgmdiab_doc_lastname'      => isset($record[10]) ? trim($record[10]) : '',
                    'cgmdiab_doc_address'      => isset($record[11]) ? trim($record[11]) : '',
                    'cgmdiab_doc_city'          => isset($record[12]) ? trim($record[12]) : '',
                    'cgmdiab_doc_state' 	  => isset($record[13]) ? trim($record[13]) : '',
                    'cgmdiab_doc_zip'          => isset($record[14]) ? trim($record[14]) : '',
                    'cgmdiab_doc_npi'   => isset($record[15]) ? trim($record[15]) : '',
                    'cgmdiab_doc_phone'    => isset($record[16]) ? trim($record[16]) : '',
                    'cgmdiab_doc_fax'     => isset($record[17]) ? trim($record[17]) : '',
                    'cgmdiab_medicare' 		  => isset($record[18]) ? trim($record[18]) : '',
                    'cgmdiab_insurance_com_id'   => isset($record[19]) ? trim($record[19]) : '',
                    'cgmdiab_insurance_com'   => isset($record[20]) ? trim($record[20]) : '',
                    'cgmdiab_call_back_num'          => isset($record[21]) ? trim($record[21]) : '',
                    'cgmdiab_fax_back'          => isset($record[22]) ? trim($record[22]) : '',
                    'cgmdiab_repcode'          => isset($record[23]) ? trim($record[23]) : '',
                    'cgmdiab_date_entered'          => date("d-m-Y"),
                    'cgmdiab_status'          => 'dupe',
                    'userID'          => $this->session->userdata('id')

                );

                $this->db->insert('t_cgm_diabetic', $newuser);
            }


        }


    }

}
