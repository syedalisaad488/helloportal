<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_model
{

    function __construct()
    {
        parent::__construct();
    }

    public function rep_codes()
    {

        $result = $this->db->query("select * from ( select DISTINCT(aurbrac_repcode) from t_aurtho_brace
            union all select DISTINCT(cgmdiab_repcode) from t_cgm_diabetic ) alldata  where aurbrac_repcode!=''
        GROUP BY aurbrac_repcode")->result();
        return $result;


    }

    public function login($username, $pass)
    {

        $this->db->trans_start();
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('name', $username);
        $this->db->where('password', $pass);
        $query = $this->db->get()->result_array();
        $this->db->trans_complete();

        if (isset($query)) {
            return $query;
        } else {
            return false;

        }
    }


    public function save_client_data($data)
    {
        $table_data = array(
            "client_lead_process_date" => $data["client_lead_process_date"],
            "client_lead_posting_date" => $data["client_lead_posting_date"],
            "client_first_name" => $data["client_first_name"],
            "client_last_name" => $data["client_last_name"],
            "client_address" => $data["client_address"],
            "client_city" => $data["client_city"],
            "client_state" => $data["client_state"],
            "client_zip_code" => $data["client_zip_code"],
            "client_phone" => $data["client_phone"],
            "client_dob" => $data["client_dob"],
            "client_sex" => $data["client_sex"],
            "client_diabetes" => $data["client_diabetes"],
            "client_times_of_use_insul" => $data["client_times_of_use_insul"],
            "client_times_of_check_blood_sugar" => $data["client_times_of_check_blood_sugar"],
            "client_doc_first_name" => $data["client_doc_first_name"],
            "client_doc_last_name" => $data["client_doc_last_name"],
            "client_doc_address" => $data["client_doc_address"],
            "client_doc_city" => $data["client_doc_city"],
            "client_doc_state" => $data["client_doc_state"],
            "client_doc_zip_code" => $data["client_doc_zip_code"],
            "client_doc_npi" => $data["client_doc_npi"],
            "client_doc_phone" => $data["client_doc_phone"],
            "client_doc_fax" => $data["client_doc_fax"],
            "client_doc_medicare" => $data["client_doc_medicare"],
            "client_doc_id_member" => $data["client_doc_id_member"],
            "client_doc_recording" => $data["client_doc_recording"],
            "IDrepresentative" => $data["IDrepresentative"]
        );

        $this->db->trans_start();
        $this->db->insert("t_client_entry", $table_data);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        return $insert_id;

    }

    public function update_client_data($data, $IDclient)
    {
        $table_data = array(
            "client_lead_process_date" => $data["client_lead_process_date"],
            "client_lead_posting_date" => $data["client_lead_posting_date"],
            "client_first_name" => $data["client_first_name"],
            "client_last_name" => $data["client_last_name"],
            "client_address" => $data["client_address"],
            "client_city" => $data["client_city"],
            "client_state" => $data["client_state"],
            "client_zip_code" => $data["client_zip_code"],
            "client_phone" => $data["client_phone"],
            "client_dob" => $data["client_dob"],
            "client_sex" => $data["client_sex"],
            "client_diabetes" => $data["client_diabetes"],
            "client_times_of_use_insul" => $data["client_times_of_use_insul"],
            "client_times_of_check_blood_sugar" => $data["client_times_of_check_blood_sugar"],
            "client_doc_first_name" => $data["client_doc_first_name"],
            "client_doc_last_name" => $data["client_doc_last_name"],
            "client_doc_address" => $data["client_doc_address"],
            "client_doc_city" => $data["client_doc_city"],
            "client_doc_state" => $data["client_doc_state"],
            "client_doc_zip_code" => $data["client_doc_zip_code"],
            "client_doc_npi" => $data["client_doc_npi"],
            "client_doc_phone" => $data["client_doc_phone"],
            "client_doc_fax" => $data["client_doc_fax"],
            "client_doc_medicare" => $data["client_doc_medicare"],
            "client_doc_id_member" => $data["client_doc_id_member"],
            "client_doc_recording" => $data["client_doc_recording"]
        );
        $this->db->trans_start();
        $this->db->where("IDclient", $IDclient);
        $this->db->update("t_client_entry", $table_data);
        $this->db->trans_complete();

    }

    public function get_client_data_for_repren($IDrepresentative)
    {
        $this->db->trans_start();
        $this->db->where("IDrepresentative", $IDrepresentative);
        $result = $this->db->get("t_client_entry");
        $this->db->trans_complete();
        return $result;
    }

    public function get_client_data_for_agent($IDagent)
    {
        $this->db->select('login.*,t_client_entry.*');
        $this->db->from('t_client_entry');
        $this->db->join('login', 't_client_entry.IDrepresentative = login.id');
        $this->db->where("login.creator_id", $IDagent);
        $query = $this->db->get();
        return $query;

    }

    public function get_client_data_for_user($IDuser)
    {

        $query = array();
        $this->db->trans_start();
        $this->db->select("id");
        $this->db->where("creator_id", $IDuser);
        $agent_id = $this->db->get("login")->result();
        $i = 0;
        foreach ($agent_id as $key) {
            $this->db->select('login.*,t_client_entry.*');
            $this->db->from('t_client_entry');
            $this->db->join('login', 't_client_entry.IDrepresentative = login.id');
            $this->db->where("login.creator_id", $key->id);
            $query[$i] = $this->db->get()->result();
            $i++;
        }
        $this->db->trans_complete();
        return $query;
    }

    public function get_fax($IDuser, $prefix, $table_name)
    {
        $this->db->trans_start();
        $this->db->select("*");
        $this->db->where("ID$prefix", $IDuser);
        $this->db->from($table_name);
        $query = $this->db->get();
        $this->db->trans_complete();
        return $query->result();
    }


    public function get_pending_client_data_for_user($IDuser)
    {
        $query = array();
        $this->db->trans_start();
        $this->db->select("id");
        $this->db->where("creator_id", $IDuser);
        $agent_id = $this->db->get("login")->result();
        $i = 0;
        foreach ($agent_id as $key) {
            $this->db->select('login.*,t_client_entry.*');
            $this->db->from('t_client_entry');
            $this->db->join('login', 't_client_entry.IDrepresentative = login.id');
            $this->db->where("login.creator_id", $key->id);
            $this->db->where("t_client_entry.client_fax_sent_status = 0");
            $query[$i] = $this->db->get()->result();
            $i++;
        }
        $this->db->trans_complete();
        return $query;

    }


    public function get_sent_client_data_for_user($IDuser)
    {
        $query = array();
        $this->db->trans_start();
        $this->db->select("id");
        $this->db->where("creator_id", $IDuser);
        $agent_id = $this->db->get("login")->result();
        $i = 0;
        foreach ($agent_id as $key) {
            $this->db->select('login.*,t_client_entry.*');
            $this->db->from('t_client_entry');
            $this->db->join('login', 't_client_entry.IDrepresentative = login.id');
            $this->db->where("login.creator_id", $key->id);
            $this->db->where("t_client_entry.client_fax_sent_status = 1");
            $query[$i] = $this->db->get()->result();
            $i++;
        }
        $this->db->trans_complete();
        return $query;

    }

    public function get_specific_client_data($IDclient)
    {
        $this->db->trans_start();
        $this->db->where("IDclient", $IDclient);
        $this->db->limit(1);
        $result = $this->db->get("t_client_entry");
        $this->db->trans_complete();
        return $result;
    }


    public function save_user($name, $password, $role, $email, $comapny_name, $receive_num, $sent_num, $cgmdiab_tab, $aurbrac_tab, $mask_tab, $heatpad_tab, $hipaa_tab, $id = null, $repcode_json)
    {
        $this->db->trans_start();

        if ($id != null) {
            $data = array(
                'name' => $name,
                'password' => $password,
                'role' => $role,
                'email' => isset($email) ? $email : '',
                'company_name' => isset($comapny_name) ? $comapny_name : $name,
                'receive_num' => isset($receive_num) ? $receive_num : '',
                'sent_num' => isset($sent_num) ? $sent_num : '',
                'cgmdiab_tab' => isset($cgmdiab_tab) ? '1' : '0',
                'aurbrac_tab' => isset($aurbrac_tab) ? '1' : '0',
                'mask_tab' => isset($mask_tab) ? '1' : '0',
                'heatpad_tab' => isset($heatpad_tab) ? '1' : '0',
                'hipaa_tab' => isset($hipaa_tab) ? '1' : '0',
                'repcode' => isset($repcode_json) ? $repcode_json : '',
                'creator_id' => $this->session->userdata('id'));
            $this->db->where('id', $id);
            $this->db->update('login', $data);
        } else {
            $data = array(
                'name' => $name,
                'password' => $password,
                'role' => $role,
                'email' => isset($email) ? $email : '',
                'company_name' => isset($comapny_name) ? $comapny_name : $name,
                'receive_num' => isset($receive_num) ? $receive_num : '',
                'sent_num' => isset($sent_num) ? $sent_num : '',
                'cgmdiab_tab' => isset($cgmdiab_tab) ? '1' : '0',
                'aurbrac_tab' => isset($aurbrac_tab) ? '1' : '0',
                'mask_tab' => isset($mask_tab) ? '1' : '0',
                'heatpad_tab' => isset($heatpad_tab) ? '1' : '0',
                'hipaa_tab' => isset($hipaa_tab) ? '1' : '0',

                'repcode' => isset($repcode_json) ? $repcode_json : '',
                'creator_id' => $this->session->userdata('id'));
            $this->db->insert('login', $data);
        }
        $this->db->trans_complete();
    }

    public function get_agent_data($id = null)
    {
        $this->db->trans_start();
        $this->db->select('*');
        $this->db->from('login');
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $this->db->where('creator_id', $this->session->userdata('id'));
        $this->db->order_by('id', 'ASC');
        $result = $this->db->get();
        $this->db->trans_complete();
        return $result;
    }

    public function delete($table, $id = null)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        $this->db->delete($table);
        $this->db->trans_complete();

    }

    public function campaign_save_data($data, $prefix)
    {
        if ($prefix == "cgmdiab") {
            return $this->cmgdiab_save_data($data);
        } else if ($prefix == "aurbrac") {
            return $this->aurbrac_save_data($data);
        }
    }

    public function aurbrac_save_data($data)
    {
        $product = $data["aurbrac_product"];

        $table_data = array(
            "aurbrac_firstname" => $data["aurbrac_firstname"],
            "aurbrac_lastname" => $data["aurbrac_lastname"],
            "aurbrac_address" => $data["aurbrac_address"],
            "aurbrac_city" => $data["aurbrac_city"],
            "aurbrac_state" => $data["aurbrac_state"],
            "aurbrac_zip" => $data["aurbrac_zip"],
            "aurbrac_l_wrist" => in_array("l_wrist", $product) ? 1 : 0,
            "aurbrac_r_wrist" => in_array("r_wrist", $product) ? 1 : 0,
            "aurbrac_r_elbow" => in_array("r_elbow", $product) ? 1 : 0,
            "aurbrac_r_ankle" => in_array("r_ankle", $product) ? 1 : 0,
            "aurbrac_r_knee" => in_array("r_knee", $product) ? 1 : 0,
            "aurbrac_bi_ankle" => in_array("bi_ankle", $product) ? 1 : 0,
            "aurbrac_bi_elbow" => in_array("bi_elbow", $product) ? 1 : 0,
            "aurbrac_l_back" => in_array("l_back", $product) ? 1 : 0,
            "aurbrac_back" => in_array("back", $product) ? 1 : 0,
            "aurbrac_neck" => in_array("neck", $product) ? 1 : 0,
            "aurbrac_bi_wrist" => in_array("bi_wrist", $product) ? 1 : 0,
            "aurbrac_hip" => in_array("hip", $product) ? 1 : 0,
            "aurbrac_bl_knee" => in_array("bl_knee", $product) ? 1 : 0,
            "aurbrac_l_knee" => in_array("l_knee", $product) ? 1 : 0,
            "aurbrac_l_ankle" => in_array("l_ankle", $product) ? 1 : 0,
            "aurbrac_l_elbow" => in_array("l_elbow", $product) ? 1 : 0,
            "aurbrac_l_shoulder" => in_array("l_shoulder", $product) ? 1 : 0,
            "aurbrac_r_shoulder" => in_array("r_shoulder", $product) ? 1 : 0,
            "aurbrac_doc_firstname" => $data["aurbrac_doc_firstname"],
            "aurbrac_doc_lastname" => $data["aurbrac_doc_lastname"],
            "aurbrac_doc_address" => $data["aurbrac_doc_address"],
            "aurbrac_doc_city" => $data["aurbrac_doc_city"],
            "aurbrac_doc_state" => $data["aurbrac_doc_state"],
            "aurbrac_doc_zip" => $data["aurbrac_doc_zip"],
            "aurbrac_doc_phone" => $data["aurbrac_doc_phone"],
            "aurbrac_doc_fax" => $data["aurbrac_doc_fax"],
            "aurbrac_doc_npi" => $data["aurbrac_doc_npi"],
            "aurbrac_doc_pecos_cert" => $data["aurbrac_doc_pecos_cert"],
            "aurbrac_pcp_within_past" => $data["aurbrac_pcp_within_past"],
            "aurbrac_record_file" => $data["aurbrac_record_file"]
        );

        $this->db->trans_start();
        $this->db->insert("t_aurtho_brace", $table_data);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        return $insert_id;

    }

    public function cmgdiab_save_data($data)
    {

        $table_data = array(
            "cgmdiab_firstname" => $data["cgmdiab_firstname"],
            "cgmdiab_lastname" => $data["cgmdiab_lastname"],
            "cgmdiab_address" => $data["cgmdiab_address"],
            "cgmdiab_city" => $data["cgmdiab_city"],
            "cgmdiab_state" => $data["cgmdiab_state"],
            "cgmdiab_email" => $data["cgmdiab_email"],
            "cgmdiab_phone" => $data["cgmdiab_phone"],
            "cgmdiab_zip" => $data["cgmdiab_zip"],
            "cgmdiab_dob" => $data["cgmdiab_dob"],
            "cgmdiab_sex" => $data["cgmdiab_sex"],
            "cgmdiab_ssn" => $data["cgmdiab_ssn"],
            "cgmdiab_diabetes" => $data["cgmdiab_diabetes"],
            "cgmdiab_visit_doc" => $data["cgmdiab_visit_doc"],
            "cgmdiab_insulin_pum" => $data["cgmdiab_insulin_pum"],
            "cgmdiab_blood_sugar" => $data["cgmdiab_blood_sugar"],
            "cgmdiab_doc_firstname" => $data["cgmdiab_doc_firstname"],
            "cgmdiab_doc_lastname" => $data["cgmdiab_doc_lastname"],
            "cgmdiab_doc_address" => $data["cgmdiab_doc_address"],
            "cgmdiab_doc_city" => $data["cgmdiab_doc_city"],
            "cgmdiab_doc_state" => $data["cgmdiab_doc_state"],
            "cgmdiab_doc_zip" => $data["cgmdiab_doc_zip"],
            "cgmdiab_doc_npi" => $data["cgmdiab_doc_npi"],
            "cgmdiab_doc_phone" => $data["cgmdiab_doc_phone"],
            "cgmdiab_doc_fax" => $data["cgmdiab_doc_fax"],
            "cgmdiab_patient_ack" => $data["cgmdiab_patient_ack"],
            "cgmdiab_finan_aggrem" => $data["cgmdiab_finan_aggrem"],
            "cgmdiab_contact_me" => $data["cgmdiab_contact_me"],
            "cgmdiab_coinsurance" => $data["cgmdiab_coinsurance"],
            "cgmdiab_IDmedical" => $data["cgmdiab_IDmedical"],
            "cgmdiab_medicare" => $data["cgmdiab_medicare"],
            "cgmdiab_insurance_com" => $data["cgmdiab_insurance_com"],
            "cgmdiab_IDmember" => $data["cgmdiab_IDmember"],
            "cgmdiab_IDgroup" => $data["cgmdiab_IDgroup"],
            "cgmdiab_helpdisk" => $data["cgmdiab_helpdisk"],
            "cgmdiab_date_entered" => $data["cgmdiab_date_entered"],
            "cgmdiab_IDtrack" => $data["cgmdiab_IDtrack"],
            "cgmdiab_IDsite" => $data["cgmdiab_IDsite"],
            "cgmdiab_repcode" => $data["cgmdiab_repcode"],
            "cgmdiab_patient_url" => $data["cgmdiab_patient_url"],
            "cgmdiab_recording" => $data["cgmdiab_recording"]
        );

        $this->db->trans_start();
        $this->db->insert("t_cgm_diabetic", $table_data);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        return $insert_id;

    }


    public function get_specific_campaign_data($IDcampaign, $prefix)
    {
        $this->db->where("ID$prefix", $IDcampaign);
        if ($prefix == "cgmdiab") {
            $result = $this->db->get('t_cgm_diabetic');
        } else if ($prefix == "aurbrac") {
            $result = $this->db->get('t_aurtho_brace');
        }
        return $result->result_array();
    }

    public function get_aurbrac_data_for_fax($IDcampaign, $prefix)
    {
        $this->db->where("ID$prefix", $IDcampaign);
        $result = $this->db->get('t_aurtho_brace')->result_array();
        return $result;
        //  $products_array = array();
        //    array_push($products_array,$result[0]['aurbrac_back'],$result[0]['aurbrac_l_knee'],$result[0]['aurbrac_r_knee'],$result[0]['aurbrac_l_wrist'],$result[0]['aurbrac_r_wrist'],$result[0]['aurbrac_l_ankle'],$result[0]['aurbrac_r_ankle'],$result[0]['aurbrac_l_shoulder'],$result[0]['aurbrac_r_shoulder'],$result[0]['aurbrac_l_elbow'],$result[0]['aurbrac_r_elbow'],$result[0]['aurbrac_hip']);
        //  return $products_array;

    }

    public function campaign_update_data($data, $IDcampaign, $prefix)
    {
        if ($prefix == "cgmdiab") {
            $this->update_cgmdiab_data($data, $IDcampaign);
        } else if ($prefix == "aurbrac") {
            $this->update_aurbrac_data($data, $IDcampaign);

        }
    }


    public function update_aurbrac_data($data, $IDcampaign)
    {
        $product = array();
        $product = $data["aurbrac_product"];


        $table_data = array(
            "aurbrac_firstname" => $data["aurbrac_firstname"],
            "aurbrac_lastname" => $data["aurbrac_lastname"],
            "aurbrac_address" => $data["aurbrac_address"],
            "aurbrac_city" => $data["aurbrac_city"],
            "aurbrac_state" => $data["aurbrac_state"],
            "aurbrac_zip" => $data["aurbrac_zip"],
            "aurbrac_l_wrist" => in_array("l_wrist", $product) ? 1 : 0,
            "aurbrac_r_wrist" => in_array("r_wrist", $product) ? 1 : 0,
            "aurbrac_r_elbow" => in_array("r_elbow", $product) ? 1 : 0,
            "aurbrac_r_ankle" => in_array("r_ankle", $product) ? 1 : 0,
            "aurbrac_r_knee" => in_array("r_knee", $product) ? 1 : 0,
            "aurbrac_back" => in_array("back", $product) ? 1 : 0,
            "aurbrac_hip" => in_array("hip", $product) ? 1 : 0,
            "aurbrac_l_knee" => in_array("l_knee", $product) ? 1 : 0,
            "aurbrac_l_ankle" => in_array("l_ankle", $product) ? 1 : 0,
            "aurbrac_l_elbow" => in_array("l_elbow", $product) ? 1 : 0,
            "aurbrac_l_shoulder" => in_array("l_shoulder", $product) ? 1 : 0,
            "aurbrac_r_shoulder" => in_array("r_shoulder", $product) ? 1 : 0,
            "aurbrac_doc_firstname" => $data["aurbrac_doc_firstname"],
            "aurbrac_doc_lastname" => $data["aurbrac_doc_lastname"],
            "aurbrac_doc_address" => $data["aurbrac_doc_address"],
            "aurbrac_doc_city" => $data["aurbrac_doc_city"],
            "aurbrac_doc_state" => $data["aurbrac_doc_state"],
            "aurbrac_doc_zip" => $data["aurbrac_doc_zip"],
            "aurbrac_doc_phone" => $data["aurbrac_doc_phone"],
            "aurbrac_doc_fax" => $data["aurbrac_doc_fax"],
            "aurbrac_doc_npi" => $data["aurbrac_doc_npi"],
            "aurbrac_doc_pecos_cert" => $data["aurbrac_doc_pecos_cert"],
            "aurbrac_pcp_within_past" => $data["aurbrac_pcp_within_past"],
            "aurbrac_record_file" => $data["aurbrac_record_file"]
        );


        $this->db->trans_start();
        $this->db->where("IDaurbrac", $IDcampaign);
        $this->db->update("t_aurtho_brace", $table_data);
        $this->db->trans_complete();

    }

    public function update_cgmdiab_data($data, $IDcampaign)
    {


        $table_data = array(
            "cgmdiab_firstname" => $data["cgmdiab_firstname"],
            "cgmdiab_lastname" => $data["cgmdiab_lastname"],
            "cgmdiab_address" => $data["cgmdiab_address"],
            "cgmdiab_city" => $data["cgmdiab_city"],
            "cgmdiab_state" => $data["cgmdiab_state"],
            "cgmdiab_email" => $data["cgmdiab_email"],
            "cgmdiab_phone" => $data["cgmdiab_phone"],
            "cgmdiab_zip" => $data["cgmdiab_zip"],
            "cgmdiab_dob" => $data["cgmdiab_dob"],
            "cgmdiab_sex" => $data["cgmdiab_sex"],
            "cgmdiab_ssn" => $data["cgmdiab_ssn"],
            "cgmdiab_diabetes" => $data["cgmdiab_diabetes"],
            "cgmdiab_visit_doc" => $data["cgmdiab_visit_doc"],
            "cgmdiab_insulin_pum" => $data["cgmdiab_insulin_pum"],
            "cgmdiab_blood_sugar" => $data["cgmdiab_blood_sugar"],
            "cgmdiab_doc_firstname" => $data["cgmdiab_doc_firstname"],
            "cgmdiab_doc_lastname" => $data["cgmdiab_doc_lastname"],
            "cgmdiab_doc_address" => $data["cgmdiab_doc_address"],
            "cgmdiab_doc_city" => $data["cgmdiab_doc_city"],
            "cgmdiab_doc_state" => $data["cgmdiab_doc_state"],
            "cgmdiab_doc_npi" => $data["cgmdiab_doc_npi"],
            "cgmdiab_doc_zip" => $data["cgmdiab_doc_zip"],
            "cgmdiab_doc_npi" => $data["cgmdiab_doc_npi"],
            "cgmdiab_doc_fax" => $data["cgmdiab_doc_fax"],
            "cgmdiab_patient_ack" => $data["cgmdiab_patient_ack"],
            "cgmdiab_finan_aggrem" => $data["cgmdiab_finan_aggrem"],
            "cgmdiab_contact_me" => $data["cgmdiab_contact_me"],
            "cgmdiab_coinsurance" => $data["cgmdiab_coinsurance"],
            // "cgmdiab_IDmedical" => $data["cgmdiab_IDmedical"],
            // "cgmdiab_medicare" => $data["cgmdiab_medicare"],
            "cgmdiab_insurance_com" => $data["cgmdiab_insurance_com"],
            "cgmdiab_IDmember" => $data["cgmdiab_IDmember"],
            "cgmdiab_IDgroup" => $data["cgmdiab_IDgroup"],
            "cgmdiab_helpdisk" => $data["cgmdiab_helpdisk"],
            "cgmdiab_date_entered" => $data["cgmdiab_date_entered"],
            "cgmdiab_IDtrack" => $data["cgmdiab_IDtrack"],
            "cgmdiab_IDsite" => $data["cgmdiab_IDsite"],
            "cgmdiab_repcode" => $data["cgmdiab_repcode"],
            "cgmdiab_patient_url" => $data["cgmdiab_patient_url"],
            "cgmdiab_recording" => $data["cgmdiab_recording"]
        );

        $this->db->trans_start();
        $this->db->where("IDcgmdiab", $IDcampaign);
        $this->db->update("t_cgm_diabetic", $table_data);
        $this->db->trans_complete();

    }

    public function qa_campaign_update_data($data, $IDcampaign, $prefix)
    {

        if ($prefix == "aurbrac") {
            $this->qa_aurbrac_update_data($data, $IDcampaign);
        } else if ($prefix == "cgmdiab") {
            $this->qa_cgmdiab_update_data($data, $IDcampaign);
        }
    }

    public function qa_cgmdiab_update_data($data, $IDcampaign)
    {
        $table_data = array(
            "cgmdiab_firstname" => $data["cgmdiab_firstname"],
            "cgmdiab_lastname" => $data["cgmdiab_lastname"],
            "cgmdiab_address" => $data["cgmdiab_address"],
            "cgmdiab_city" => $data["cgmdiab_city"],
            "cgmdiab_state" => $data["cgmdiab_state"],
            "cgmdiab_email" => $data["cgmdiab_email"],
            "cgmdiab_phone" => $data["cgmdiab_phone"],
            "cgmdiab_zip" => $data["cgmdiab_zip"],
            "cgmdiab_dob" => $data["cgmdiab_dob"],
            "cgmdiab_sex" => $data["cgmdiab_sex"],
            "cgmdiab_ssn" => $data["cgmdiab_ssn"],
            "cgmdiab_diabetes" => $data["cgmdiab_diabetes"],
            "cgmdiab_visit_doc" => $data["cgmdiab_visit_doc"],
            "cgmdiab_insulin_pum" => $data["cgmdiab_insulin_pum"],
            "cgmdiab_blood_sugar" => $data["cgmdiab_blood_sugar"],
            "cgmdiab_doc_firstname" => $data["cgmdiab_doc_firstname"],
            "cgmdiab_doc_lastname" => $data["cgmdiab_doc_lastname"],
            "cgmdiab_doc_address" => $data["cgmdiab_doc_address"],
            "cgmdiab_doc_city" => $data["cgmdiab_doc_city"],
            "cgmdiab_doc_state" => $data["cgmdiab_doc_state"],
            "cgmdiab_doc_npi" => $data["cgmdiab_doc_npi"],
            "cgmdiab_doc_zip" => $data["cgmdiab_doc_zip"],
            "cgmdiab_doc_npi" => $data["cgmdiab_doc_npi"],
            "cgmdiab_doc_fax" => $data["cgmdiab_doc_fax"],
            "cgmdiab_patient_ack" => $data["cgmdiab_patient_ack"],
            "cgmdiab_finan_aggrem" => $data["cgmdiab_finan_aggrem"],
            "cgmdiab_contact_me" => $data["cgmdiab_contact_me"],
            "cgmdiab_coinsurance" => $data["cgmdiab_coinsurance"],
            // "cgmdiab_IDmedical" => $data["cgmdiab_IDmedical"],
            // "cgmdiab_medicare" => $data["cgmdiab_medicare"],
            "cgmdiab_insurance_com" => $data["cgmdiab_insurance_com"],
            "cgmdiab_IDmember" => $data["cgmdiab_IDmember"],
            "cgmdiab_IDgroup" => $data["cgmdiab_IDgroup"],
            "cgmdiab_helpdisk" => $data["cgmdiab_helpdisk"],
            "cgmdiab_date_entered" => $data["cgmdiab_date_entered"],
            "cgmdiab_IDtrack" => $data["cgmdiab_IDtrack"],
            "cgmdiab_IDsite" => $data["cgmdiab_IDsite"],
            "cgmdiab_repcode" => $data["cgmdiab_repcode"],
            "cgmdiab_patient_url" => $data["cgmdiab_patient_url"],
            "cgmdiab_recording" => $data["cgmdiab_recording"]

        );

        $this->db->trans_start();
        $this->db->where("IDcgmdiab", $IDcampaign);
        $this->db->update("t_cgm_diabetic", $table_data);
        $this->db->trans_complete();

    }

    public function qa_aurbrac_update_data($data, $IDcampaign)
    {
        $product = array();
        $product = $data["aurbrac_product"];


        $table_data = array(
            "aurbrac_firstname" => $data["aurbrac_firstname"],
            "aurbrac_lastname" => $data["aurbrac_lastname"],
            "aurbrac_address" => $data["aurbrac_address"],
            "aurbrac_city" => $data["aurbrac_city"],
            "aurbrac_state" => $data["aurbrac_state"],
            "aurbrac_zip" => $data["aurbrac_zip"],
            "aurbrac_l_wrist" => in_array("l_wrist", $product) ? 1 : 0,
            "aurbrac_r_wrist" => in_array("r_wrist", $product) ? 1 : 0,
            "aurbrac_r_elbow" => in_array("r_elbow", $product) ? 1 : 0,
            "aurbrac_r_ankle" => in_array("r_ankle", $product) ? 1 : 0,
            "aurbrac_r_knee" => in_array("r_knee", $product) ? 1 : 0,
            "aurbrac_bi_ankle" => in_array("bi_ankle", $product) ? 1 : 0,
            "aurbrac_bi_elbow" => in_array("bi_elbow", $product) ? 1 : 0,
            "aurbrac_back" => in_array("back", $product) ? 1 : 0,
            "aurbrac_u_back" => in_array("u_back", $product) ? 1 : 0,
            "aurbrac_neck" => in_array("neck", $product) ? 1 : 0,
            "aurbrac_bi_wrist" => in_array("bi_wrist", $product) ? 1 : 0,
            "aurbrac_hip" => in_array("hip", $product) ? 1 : 0,
            "aurbrac_bl_knee" => in_array("bl_knee", $product) ? 1 : 0,
            "aurbrac_l_knee" => in_array("l_knee", $product) ? 1 : 0,
            "aurbrac_l_ankle" => in_array("l_ankle", $product) ? 1 : 0,
            "aurbrac_l_elbow" => in_array("l_elbow", $product) ? 1 : 0,
            "aurbrac_l_shoulder" => in_array("l_shoulder", $product) ? 1 : 0,
            "aurbrac_r_shoulder" => in_array("r_shoulder", $product) ? 1 : 0,
            "aurbrac_doc_firstname" => $data["aurbrac_doc_firstname"],
            "aurbrac_doc_lastname" => $data["aurbrac_doc_lastname"],
            "aurbrac_doc_address" => $data["aurbrac_doc_address"],
            "aurbrac_doc_city" => $data["aurbrac_doc_city"],
            "aurbrac_doc_state" => $data["aurbrac_doc_state"],
            "aurbrac_doc_zip" => $data["aurbrac_doc_zip"],
            "aurbrac_doc_phone" => $data["aurbrac_doc_phone"],
            "aurbrac_doc_fax" => $data["aurbrac_doc_fax"],
            "aurbrac_doc_npi" => $data["aurbrac_doc_npi"],
            "aurbrac_doc_pecos_cert" => $data["aurbrac_doc_pecos_cert"],
            "aurbrac_pcp_within_past" => $data["aurbrac_pcp_within_past"],
            "aurbrac_record_file" => $data["aurbrac_record_file"]
        );


        $this->db->trans_start();
        $this->db->where("IDaurbrac", $IDcampaign);
        $this->db->update("t_aurtho_brace", $table_data);
        $this->db->trans_complete();

    }

    public function count_all_data($prefix, $type, $table, $group)
    {
        $calling = 0;
        $query = $this->get_data_list($prefix, $type, $table, $group, $calling);

        return $query->num_rows();


    }

    public function call_camp_list($prefix, $type, $table, $group)
    {
        $calling = 1;
        $query = $this->get_data_list($prefix, $type, $table, $group, $calling);

        return $query->result();

    }

    public function get_data_list($prefix, $type, $table, $group, $calling)
    {

        $this->db->select('*');


        if ($this->session->userdata("role") != 'Owner' and $this->session->userdata("role") != 'QA' and $this->session->userdata("role") != 'Viewer' and $this->session->userdata("role") != 'Agent') {
            $this->db->where("userID", $this->session->userdata("id"));
        }
        if ($this->session->userdata("role") == 'QA') {
            $this->db->where("IDqa", $this->session->userdata("id"));
        }
        if ($this->session->userdata("role") == 'Agent') {
            $this->db->where("agentID", $this->session->userdata("creator_id"));
        }
        if ($type != 'Sent' and $type != 'Rejected' and $type != 'Cleaning' and $type != 'Delivered') {
            $this->db->where($prefix . "_status", $type);
        }
        if ($type == 'Sent') {
            $this->db->where($prefix . "_status", $type);
            $this->db->where("twilio_status !=", 'delivered');
            $this->db->where("sent_repeat", '<=', 3);
        }
        if ($type == 'Delivered') {
            $this->db->where("twilio_status", strtolower($type));
        }
        if ($type == 'Not_sent') {
            $this->db->group_start();
            $this->db->Or_where("twilio_status !=", 'delivered');
            $this->db->where("sent_repeat", '>', 3);
            $this->db->group_end();

        }
        if ($type != 'Delete') {
            $this->db->where($prefix . "_status!='Delete'");
        }
        if ($type == 'Rejected') {
            $this->db->where("(" . $prefix . "_status", $type);
            $this->db->or_where($prefix . "_firstname = ''");
            $this->db->or_where($prefix . "_lastname = ''");
            $this->db->or_where($prefix . "_phone = ''");
            $this->db->or_where($prefix . "_address = ''");
            $this->db->or_where($prefix . "_city = ''");
            $this->db->or_where($prefix . "_state = ''");
            $this->db->or_where($prefix . "_zip = ''");
            $this->db->or_where($prefix . "_dob = ''");
            $this->db->or_where($prefix . "_doc_firstname = ''");
            $this->db->or_where($prefix . "_doc_lastname = ''");
            $this->db->or_where($prefix . "_doc_address = ''");
            $this->db->or_where($prefix . "_doc_city = ''");
            $this->db->or_where($prefix . "_doc_state = ''");
            $this->db->or_where($prefix . "_doc_zip = ''");
            $this->db->or_where($prefix . "_doc_address = ''");
            $this->db->or_where($prefix . "_doc_phone = ''");
            if ($prefix == "cgmdiab") {
                $this->db->or_where($prefix . "_medicare = ''");
            } else {
                $this->db->or_where($prefix . "_med_id = ''");
            }
            $this->db->or_where($prefix . "_doc_fax = '')");

        }
        if ($type == 'Cleaning') {
            $this->db->group_start();
            $this->db->where($prefix . "_status = '' ");
            $this->db->or_where($prefix . "_status = 'Cleaning' ");
            $this->db->Or_where($prefix . "_status = 'dupe' ");
            $this->db->group_end();
            $this->db->where($prefix . "_firstname != ''");
            $this->db->where($prefix . "_lastname != ''");
            $this->db->where($prefix . "_phone != ''");
            $this->db->where($prefix . "_address != ''");
            $this->db->where($prefix . "_city != ''");
            $this->db->where($prefix . "_state != ''");
            $this->db->where($prefix . "_zip != ''");
            $this->db->where($prefix . "_dob != ''");
            $this->db->where($prefix . "_doc_firstname != ''");
            $this->db->where($prefix . "_doc_lastname != ''");
            if ($prefix != 'mask') {
                $this->db->where($prefix . "_doc_address != ''");
                $this->db->where($prefix . "_doc_city != ''");
                $this->db->where($prefix . "_doc_state != ''");
                $this->db->where($prefix . "_doc_zip != ''");
                $this->db->where($prefix . "_doc_address != ''");
            }

            $this->db->where($prefix . "_doc_phone != ''");
            if ($prefix == "cgmdiab") {
                $this->db->where($prefix . "_medicare != ''");
            } else {
                $this->db->where($prefix . "_med_id != ''");
            }
            $this->db->where($prefix . "_doc_fax != ''");
        }
        if ($this->session->userdata("role") == 'Viewer') {
            $repcode = json_decode($this->session->userdata("repcode"));
            $count = count($repcode);
            $a = 0;

            for ($i = 0; $i < $count; $i++) {

                if ($a == 0) {
                    $this->db->where('(' . $prefix . "_repcode = '" . $repcode[$i] . "' ");
                    $a = 1;

                }
                $this->db->or_where($prefix . "_repcode = '" . $repcode[$i] . "' ");
                if ($i == $count - 1) {
                    $this->db->or_where($prefix . "_repcode = '" . $repcode[$i] . "' )");
                }

            }

        }
        if (!empty($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("ID$prefix", 'DESC');
        }

        $this->db->from($table);
        $this->db->join('login', $table . '.agentID = login.id', 'left');


        if ($calling == 1) {
            if ($_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }
        if (isset($_POST["search"]["value"])) {
            $az = 1;
            $fields2 = [];
            array_push($fields2, 'name');
            $fields1 = $this->db->list_fields($table);
            $fields = array_merge($fields2, $fields1);
            // $fields = $this->db->list_fields($table);
            $count = count($fields);

            foreach ($fields as $field) {
                if ($field != $prefix . "_status" && $field != "ID" . $prefix) {
                    if ($az == 1) {
                        $this->db->like("(" . $field, $_POST["search"]["value"]);
                    } else if ($az == $count - 2) {
                        $s_1 = $_POST["search"]["value"];
                        $this->db->where("$field LIKE  '%$s_1%' ESCAPE '!' )");
                    } else {
                        $this->db->or_like($field, $_POST["search"]["value"]);
                    }
                    $az++;
                }

            }
        }
        if ($type != 'Cleaning') {
//				$this->db->group_by($group);
        }
        $query = $this->db->get();
        //   echo $this->db->last_query();
        return $query;

    }


    //fax pdf list start
    public function count_all_data_faxpdf($type)
    {
        $calling = 0;
        $query = $this->get_data_list_faxpdf($type, $calling);
        return $query->num_rows();
    }

    public function call_camp_list_faxpdf($type)
    {
        $calling = 1;
        $query = $this->get_data_list_faxpdf($type, $calling);
        return $query->result();
    }

    public function get_data_list_faxpdf($type, $calling)
    {
        $this->db->select('*');

        if ($this->session->userdata("role") != 'Owner' and $this->session->userdata("role") != 'QA' and $this->session->userdata("role") != 'Viewer' and $this->session->userdata("role") != 'Agent') {
            $this->db->where("userID", $this->session->userdata("id"));
        }
        if ($this->session->userdata("role") == 'Agent') {
            $this->db->where("agentID", $this->session->userdata("creator_id"));
        }
        if ($this->session->userdata("role") == 'QA') {
            $this->db->where("IDqa", $this->session->userdata("id"));
        }
        if ($type != 'Sent' and $type != 'Rejected' and $type != 'Cleaning' and $type != 'Delivered') {
            $this->db->where("faxpdf_status", $type);
        }
        if ($type == 'Sent') {
            $this->db->where("faxpdf_status", $type);
            $this->db->where("twilio_status !=", 'delivered');
            $this->db->where("sent_repeat", '<=', 3);
        }
        if ($type == 'Delivered') {
            $this->db->where("twilio_status", strtolower($type));
        }
        if ($type == 'Not_sent') {
            $this->db->group_start();
            $this->db->where("twilio_status !=", 'delivered');
            $this->db->Or_where("sent_repeat", '>', 3);
            $this->db->group_end();

        }
        if ($type != 'Delete') {
            $this->db->where("faxpdf_status!='Delete'");
        }
        if ($type == 'Rejected') {
            $this->db->where("(" . "faxpdf_status", $type);
            $this->db->or_where("faxpdf_doc_fax = '')");

        }
        if ($type == 'Cleaning') {
            $this->db->group_start();

            $this->db->where("faxpdf_status = '' ");
            $this->db->or_where("faxpdf_status = 'Cleaning' ");
            $this->db->group_end();
            $this->db->where("faxpdf_doc_fax != ''");
        }
        if ($this->session->userdata("role") == 'Viewer') {
            $repcode = json_decode($this->session->userdata("repcode"));
            $count = count($repcode);
            $a = 0;

            for ($i = 0; $i < $count; $i++) {

                if ($a == 0) {
                    $this->db->where('(' . "faxpdf_repcode = '" . $repcode[$i] . "' ");
                    $a = 1;

                }
                $this->db->or_where("faxpdf_repcode = '" . $repcode[$i] . "' ");
                if ($i == $count - 1) {
                    $this->db->or_where("faxpdf_repcode = '" . $repcode[$i] . "' )");
                }

            }

        }
        if (!empty($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("IDfaxpdf", 'DESC');
        }

        $this->db->from('t_faxpdf');
        $this->db->join('login', 't_faxpdf.agentID = login.id', 'left');


        if ($calling == 1) {
            if ($_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }
        if (isset($_POST["search"]["value"])) {
            $az = 1;
            $az = 1;
            $fields2 = [];
            array_push($fields2, 'name');
            $fields1 = $this->db->list_fields('t_faxpdf');
            $fields = array_merge($fields2, $fields1);
            $count = count($fields);

            foreach ($fields as $field) {
                if ($field != "faxpdf_status" && $field != "IDfaxpdf") {
                    if ($az == 1) {
                        $this->db->like("(" . $field, $_POST["search"]["value"]);
                    } else if ($az == $count - 2) {
                        $s_1 = $_POST["search"]["value"];
                        $this->db->where("$field LIKE  '%$s_1%' ESCAPE '!' )");
                    } else {
                        $this->db->or_like($field, $_POST["search"]["value"]);
                    }
                    $az++;
                }

            }
        }
        if ($type != 'Cleaning') {
            //				$this->db->group_by($group);
        }
        $query = $this->db->get();
        //   echo $this->db->last_query();
        return $query;

    }

    public function uploadFaxpdf($post)
    {

    }

    //fax pdf list end

    //fax calling
    public function get_faxlist_data($camp)
    {
        $calling = 1;
        $query = $this->get_faxlist($camp, $calling);
        return $query->result();
    }

    public function faxlist_count($camp)
    {
        $calling = 0;
        $query = $this->get_faxlist($camp, $calling);

        return $query->num_rows();

    }

    public function get_faxlist($camp, $calling)
    {
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $receive_nu = str_replace(array('(', ')', '-', '+1', ' '), '', $this->session->userdata('receive_num'));
        if ($this->session->userdata('role') != "QA" && $this->session->userdata('role') != "Owner") {

            $this->db->where("faxinfo_sender_number", $receive_nu);
        }
        $this->db->select("faxinfo_med_id, login.company_name as companyname,faxinfo_return_fax_id,faxinfo_date_time,faxinfo_rec_number,faxinfo_sender_number,faxinfo_old_new");
        $this->db->from("t_return_fax_info");
        $this->db->join('login', 'login.receive_num = t_return_fax_info.faxinfo_sender_number', 'left');
        if ($camp == 'aurbrac_receive') {
            $this->db->where('login.aurbrac_tab', '1');
        } else if ($camp == 'cgmdiab_receive') {
            $this->db->where('login.cgmdiab_tab', '1');
        }

        $start_date = $this->input->post("start_date");
        if ($start_date != "") {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->where("DATE(faxinfo_date_time) BETWEEN '$start_date' AND '$end_date'");
        }

        $this->db->where("faxinfo_sender_number != '2092621413' ");

        if (isset($_POST["search"]["value"])) {
            $az = 1;

            $fields = array('faxinfo_med_id', 'login.company_name', 'faxinfo_return_fax_id', 'faxinfo_date_time', 'faxinfo_rec_number', 'faxinfo_sender_number');
            $count = count($fields);

            foreach ($fields as $field) {
                if ($az == 1) {
                    $this->db->like("(" . $field, $_POST["search"]["value"]);
                } else if ($az == $count) {
                    $s_1 = $_POST["search"]["value"];
                    $this->db->where("$field LIKE  '%$s_1%' ESCAPE '!' )");
                } else {
                    $this->db->or_like($field, $_POST["search"]["value"]);
                }
                $az++;
            }
        }

        if (isset($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("`t_return_fax_info`.`IDfaxinfo`", "DESC");
        }
        if ($calling == 1) {
            if ($_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }
        $this->db->group_by('faxinfo_return_fax_id');
        $result = $this->db->get();

        return $result;
    }

    public function campaign_update_fax_info($prefix, $IDcampaign, $IDfax, $aurbracSendCounter = null)
    {
        date_default_timezone_set("Asia/Karachi");
        $table = "";

        if ($prefix == "aurbrac") {
            $table = "t_aurtho_brace";
        } else if ($prefix == "cgmdiab") {
            $table = "t_cgm_diabetic";
        } else if ($prefix == "mask") {
            $table = "t_mask";
        } else if ($prefix == "heatpad") {
            $table = "t_heatpad";
        } else if ($prefix == "hipaa") {
            $table = "t_hipaa";
        } else if ($prefix == "faxpdf") {
            $table = "t_faxpdf";
        }
        $timestamp = date("Y-m-d H:i:s");

        $previousData = $this->get_data_for_fax_send($IDcampaign, $prefix);
        $no_of_sent_faxes = json_decode($previousData[0][$prefix . '_no_of_sent_faxes']);
        $json_no_of_sent_faxes = (!empty($no_of_sent_faxes)) ? json_encode($no_of_sent_faxes . ", $timestamp") : json_encode($timestamp);

        if ($this->session->userdata("role") == "Agent") {
            $IDsender = $this->session->userdata("creator_id");
        } else {
            $IDsender = $this->session->userdata("id");
        }
        $IDsSender = json_decode($previousData[0][$prefix . '_IDsSender']);
        $json_IDsSender = (!empty($IDsSender)) ? json_encode($IDsSender . ", $IDsender") : json_encode($IDsender);


        $num_pag = 0;

        $pdftext = file_get_contents($this->session->userdata("filePath"));
        $num_pag = preg_match_all("/\/Page\W/", $pdftext);


        $previousLimit = $this->getCurrentLimitSend();

        $this->db->where("IDsendlimit", $previousLimit->IDsendlimit);
        $updateDataLimit = array("send_dynamic" => $previousLimit->send_dynamic + $num_pag);
        $this->db->update("t_limit_send", $updateDataLimit);

        $previousLimitUser = $this->getCurrentLimitUser();
        $this->db->where("IDuserlimit", $previousLimitUser->IDuserlimit);
        $updateDataLimitUser = array("userlimit_send_dynamic" => $previousLimitUser->userlimit_send_dynamic + $num_pag);
        $this->db->update("t_user_limit", $updateDataLimitUser);

        $previousPageQuantity = $this->getCurrentPageQuantity($previousLimitUser->IDpgquan);
        $this->db->where("IDpgquan", $previousLimitUser->IDpgquan);
        $updateDataPageQuantity = array("pgquan_send_quan_dynamic" => $previousPageQuantity->pgquan_send_quan_dynamic + $num_pag);
        $this->db->update("t_page_quantity", $updateDataPageQuantity);

        $num_pag = $num_pag==0 ? 0 : $num_pag;

        if ($prefix == "aurbrac") {
            if ($aurbracSendCounter == 1) {
                $update_data = array("IDfax" => $IDfax, $prefix . "_status" => "Sent", $prefix . "_no_of_sent_faxes" => $json_no_of_sent_faxes, $prefix . "_fax_sent_date" => $timestamp, $prefix . "_IDsSender" => $json_IDsSender,$prefix."_no_of_pages"=>$num_pag,"twilio_status"=>"delivered","sent_repeat"=>0,"check_queue"=>0,"charge_back"=>2);
            } else if ($aurbracSendCounter == 0) {
                $update_data = array("IDfax" => $IDfax, $prefix . "_status" => "Sent", $prefix . "_fax_sent_date" => $timestamp);
            }
        } else {
            $update_data = array("IDfax" => $IDfax, $prefix . "_status" => "Sent", $prefix . "_no_of_sent_faxes" => $json_no_of_sent_faxes, $prefix . "_fax_sent_date" => $timestamp, $prefix . "_IDsSender" => $json_IDsSender,$prefix."_no_of_pages"=>$num_pag,"twilio_status"=>"delivered","sent_repeat"=>0,"check_queue"=>0,"charge_back"=>2);

        }

        $this->db->where("ID$prefix", $IDcampaign);
        $this->db->update($table, $update_data);
    }

    public function campaign_update_not_sent_fax_info($prefix, $IDcampaign)
    {
        $table = "";

        if ($prefix == "aurbrac") {
            $table = "t_aurtho_brace";
        } else if ($prefix == "cgmdiab") {
            $table = "t_cgm_diabetic";
        } else if ($prefix == "mask") {
            $table = "t_mask";
        } else if ($prefix == "heatpad") {
            $table = "t_heatpad";
        } else if ($prefix == "hipaa") {
            $table = "t_hipaa";
        } else if ($prefix == "faxpdf") {
            $table = "t_faxpdf";
        }
        $timestamp = date("Y-m-d H:i:s");
        $update_data = array($prefix . "_status" => "Not_sent", $prefix . "_fax_sent_date" => $timestamp);
        $this->db->where("ID$prefix", $IDcampaign);
        $this->db->update($table, $update_data);
    }


    public function update_qa_status($IDcampaign, $table, $prefix, $status, $reason)
    {


        $this->db->where("ID$prefix", $IDcampaign);
        $update_data = array(
            $prefix . '_status' => $status,
            $prefix . '_reason' => $reason

        );

        $this->db->update($table, $update_data);

    }


    public function get_data_for_fax_send($IDcampaign, $prefix)
    {
        $table = "";

        if ($prefix == "aurbrac") {
            $table = "t_aurtho_brace";
        } else if ($prefix == "cgmdiab") {
            $table = "t_cgm_diabetic";
        } else if ($prefix == "mask") {
            $table = "t_mask";
        } else if ($prefix == "heatpad") {
            $table = "t_heatpad";
        } else if ($prefix == "hipaa") {
            $table = "t_hipaa";
        } else if ($prefix == "faxpdf") {
            $table = "t_faxpdf";
        }

        if ($prefix == "faxpdf") {
            $this->db->select($prefix . '_doc_fax ,' . $prefix . '_no_of_sent_faxes,' . $prefix . '_file,' . $prefix . '_path_old_new,' . $prefix . '_path,' . $prefix . '_IDsSender');
        } else {
            $this->db->select($prefix . '_doc_fax ,' . $prefix . '_no_of_sent_faxes,' . $prefix . '_IDsSender');
        }


        $this->db->where("ID$prefix", $IDcampaign);
        $this->db->where("userID", $this->session->userdata("id")); // to check if the Lead-ID is present in the currently logged in user.
        return $this->db->get($table)->result_array();
    }

    public function qa_assign($where)
    {
        $this->db->select("*");
        $this->db->where("role", 'QA');
        $this->db->where($where);

        return $this->db->get('login')->result();
    }

    public function check_authencity($IDcampaign, $prefix)

    {
        $table = "";

        if ($prefix == "aurbrac") {
            $table = "t_aurtho_brace";
        } else if ($prefix == "cgmdiab") {
            $table = "t_cgm_diabetic";
        }

        $userID = $this->session->userdata("id");
        $this->db->where("ID$prefix", $IDcampaign);
        $this->db->where("userID", $userID);
        $result = $this->db->get($table)->result_array();
        if (!empty($result[0]['userID'])) {
            return 1;
        } else {
            return 0;
        }
    }

    public function check_authencity_for_qa($IDcampaign, $prefix)
    {
        $table = "";

        if ($prefix == "aurbrac") {
            $table = "t_aurtho_brace";
        } else if ($prefix == "cgmdiab") {
            $table = "t_cgm_diabetic";
        }

        $userID = $this->session->userdata("id");
        $this->db->where("ID$prefix", $IDcampaign);
        $this->db->where("IDqa", $userID);
        $result = $this->db->get($table)->result_array();
        if (!empty($result[0]['IDqa'])) {
            return 1;
        } else {
            return 0;
        }
    }


    public function get_twillo_faxlist_data()
    {
        $calling = 1;
        $query = $this->get_twillo_faxlist($calling);
        return $query->result();
    }

    public function twillo_faxlist_count()
    {
        $calling = 0;
        $query = $this->get_twillo_faxlist($calling);

        return $query->num_rows();

    }

    public function get_twillo_faxlist($calling)
    {

        $receive_nu = '+1' . $this->session->userdata('sent_num');

        $this->db->select("*");
        $this->db->from("received");
        if ($this->session->userdata('role') != "Owner") {

            $this->db->where("To", $receive_nu);
        }

        if ($this->session->userdata('username') == 'TLPsolution') {

            $this->db->where("is_edited", 1);

        }


        $start_date = $this->input->post("start_date");
        if (!empty($start_date) != "") {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->where("DATE(faxinfo_date_time) BETWEEN '$start_date' AND '$end_date'");
        }


        if (!empty($_POST["search"]["value"])) {

            $az = 1;
            $fields = array('faxinfo_med_id', 'companyname', 'FaxSid', 'faxinfo_date_time', 'to', 'from', 'show_data_time', 'show_send_no', 'show_receive_no');
            $count = count($fields);

            foreach ($fields as $field) {
                if ($az == 1) {
                    $this->db->like("(" . $field, $_POST["search"]["value"]);
                } else if ($az == $count) {
                    $s_1 = $_POST["search"]["value"];
                    $this->db->or_where("$field LIKE  '%$s_1%' ESCAPE '!' )");
                } else {
                    $this->db->or_like($field, $_POST["search"]["value"]);
                }
                $az++;
            }
        }

        if (!empty($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("id", "DESC");
        }
        if ($calling == 1) {
            if ($_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }

        $result = $this->db->get();

        return $result;
    }

    public function get_mfax_faxlist_data()
    {
        $calling = 1;
        $query = $this->get_mfax_faxlist($calling);
        return $query->result();
    }

    public function mfax_faxlist_count()
    {
        $calling = 0;
        $query = $this->get_mfax_faxlist($calling);

        return $query->num_rows();

    }

    public function get_mfax_faxlist($calling)
    {

        $receive_nu = $this->session->userdata('sent_num');
//        echo $receive_nu;
//        die;

        $this->db->select("*");
        $this->db->from("mfaxreceiving");
        if ($this->session->userdata('role') != "Owner") {

            $this->db->where("myfaxnumber",'+1'.trim($this->session->userdata('sent_num')));
        }

        $start_date = $this->input->post("start_date");
        if (!empty($start_date) != "") {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;
            $this->db->where("DATE(created_at) BETWEEN '$start_date' AND '$end_date'");
        }


        if (!empty($_POST["search"]["value"])) {

            $az = 1;
            $fields = array('faxinfo_med_id',  'faxid', 'created_at', 'senderfaxnumber', 'myfaxnumber');
            $count = count($fields);

            foreach ($fields as $field) {
                if ($az == 1) {
                    $this->db->like("(" . $field, $_POST["search"]["value"]);
                } else if ($az == $count) {
                    $s_1 = $_POST["search"]["value"];
                    $this->db->where("$field LIKE  '%$s_1%' ESCAPE '!' )");
                } else {
                    $this->db->or_like($field, $_POST["search"]["value"]);
                }
                $az++;
            }
        }

        if (!empty($_POST["order"])) {
            $this->db->order_by($_POST['order']['0']['column'], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by("id", "DESC");
        }
        if ($calling == 1) {
            if ($_POST["length"] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }


        $result = $this->db->get();

        return $result;
    }

    public function getAgentDataForUser($id = null)
    {
        if ($id) {
            $this->db->where("id", $id);
        }
        $this->db->where("creator_id", $this->session->userdata("id"));
        return $this->db->get("login");
    }

    public function save_agent($name, $password)
    {
        $data = array(
            "name" => trim($name),
            "password" => trim($password),
            "role" => "Agent",
            "creator_id" => $this->session->userdata("id"),
            "active" => 0,
            "company_name" => $this->session->userdata("company_name"),
            "receive_num" => $this->session->userdata("receive_num"),
            "sent_num" => $this->session->userdata("sent_num"),
            "cgmdiab_tab" => $this->session->userdata("cgmdiab_tab"),
            "aurbrac_tab" => $this->session->userdata("aurbrac_tab"),
            "mask_tab" => $this->session->userdata("mask_tab"),
            "heatpad_tab" => $this->session->userdata("heatpad_tab"),
            "hipaa_tab" => $this->session->userdata("hipaa_tab"),
            "faxpdf_tab" => $this->session->userdata("faxpdf_tab"),
            "email" => $this->session->userdata("email"),
            "repcode" => $this->session->userdata("repcode")
        );

        $this->db->trans_start();
        $this->db->insert("login", $data);
        $this->db->trans_complete();
    }

    public function agentAvailibility($updateData, $id)
    {
        $this->db->where('id', $id);
        $this->db->where('creator_id', $this->session->userdata("id"));
        $checkIfExist = $this->db->get("login")->row();
        if (isset($checkIfExist->id)) {
            $this->db->trans_start();
            $this->db->where("id", $id);
            $this->db->update("login", $updateData);
            $this->db->trans_complete();
        }
    }

    public function update_agent($name, $password, $id)
    {
        if (isset($id)) {
            $this->db->trans_start();
            $this->db->where("creator_id", $this->session->userdata("id"));
            $this->db->where("id", $id);
            $updateData = array(
                "name" => $name,
                "password" => $password
            );
            $this->db->update("login", $updateData);
            $this->db->trans_complete();
        }
    }

    public function search_agent($name)
    {
        $this->db->where('name', $name);
        $query = $this->db->get('login')->first_row();
        return $query;

    }

    public function userlimit_data()
    {
        //  ************ previouse query *******
        
        //  $this->db->where('is_activated', 1);
        // $this->db->where('userlimit_month', date('M-Y'));
        // $this->db->where('role', 'user');
        // $this->db->join('t_user_limit', 't_user_limit.userID = login.id', 'left');
        // $this->db->from('login');
        // $query = $this->db->get()->result();
        
        //************ previouse query *******
        
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->where('is_activated', 1);
        $this->db->where('userlimit_month', date('M-Y'));
        $this->db->where('role', 'user');
        $this->db->group_by("userID");
        $this->db->order_by("IDuserlimit","DESC");
        $this->db->join('t_user_limit', 't_user_limit.userID = login.id', 'left');
        $this->db->from('login');
        $query = $this->db->get()->result();
        return $query;
    }

    public function userlimit_total_fax()
    {
        $this->db->where('pgquan_date', date('M-Y'));
        $this->db->from('t_page_quantity')->limit(1);
        $query = $this->db->get()->result();
        return $query;
    }

    public function userlimit_validation()
    {
        $this->db->where('pgquan_date', date('M-Y'));
        $this->db->from('t_page_quantity')->limit(1);
        $query1 = $this->db->get()->first_row();
        $this->db->select_sum('userlimit_send_static');
        $this->db->where('userlimit_month', date('M-Y'));
        $query = $this->db->get('t_user_limit')->first_row();
        return $query1->pgquan_send_quan_static - $query->userlimit_send_static;

    }

    public function userlimit_select()
    {
        $this->db->where('is_activated', 1);
        $this->db->where('role', 'User');
        $query = $this->db->get('login')->result();
        return $query;
    }

    public function assgin_limit_user($data)
    {

        $this->db->where('userlimit_month', date('M-Y'));
        $this->db->where('userID', $data['userID']);
        $query = $this->db->get('t_user_limit')->first_row();
        if ($query != null) {
            $array = array(
                'userID' => $data['userID'],
                'IDpgquan' => $data['IDpgquan'],
                'userlimit_month' => $data['userlimit_month'],
                'userlimit_send_static' => $data['userlimit_send_static'] + $query->userlimit_send_static,
                'userlimit_send_dynamic' => $query->userlimit_send_dynamic

            );
            $this->db->where('IDuserlimit', $query->IDuserlimit);
            $this->db->update('t_user_limit', $array);
        } else {
            $array = array(
                'userID' => $data['userID'],
                'IDpgquan' => $data['IDpgquan'],
                'userlimit_month' => $data['userlimit_month'],
                'userlimit_send_static' => $data['userlimit_send_static'],
                'userlimit_send_dynamic' => 0

            );

            $this->db->insert('t_user_limit', $array);
        }
        $this->session->set_userdata("token", md5(uniqid(mt_rand(), true) . microtime(true)));

    }

    public function getLastIDLimitUser()
    {
        $this->db->select_max('IDuserlimit');
        return $this->db->get('t_user_limit')->row()->IDuserlimit;
    }

    public function sendlimit_data()
    {
       
    
        $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $this->db->where('is_activated', 1);
        $this->db->where('send_month', date('M-Y'));
        $this->db->group_start();
        $this->db->or_where('login.id', $this->session->userdata('id'));
        $this->db->or_where('creator_id', $this->session->userdata('id'));
        $this->db->group_end();
        $this->db->join('t_limit_send', 't_limit_send.sendID = login.id', 'left');
        
         $this->db->group_by("sendID");
        $this->db->order_by("IDsendlimit","DESC");
        
        $this->db->from('login');
        $query = $this->db->get()->result();
        return $query;
    }

    public function sendlimit_total_fax()
    {
        $this->db->where('userlimit_month', date('M-Y'));
        $this->db->where('userID', $this->session->userdata('id'));
        $this->db->from('t_user_limit')->limit(1);
        $query = $this->db->get()->result();
        return $query;
    }

    public function sendlimit_validation()
    {
        $this->db->where('userlimit_month', date('M-Y'));
        $this->db->where('userID', $this->session->userdata('id'));
        $this->db->from('t_user_limit')->limit(1);
        $query1 = $this->db->get()->first_row();
        $this->db->select_sum('send_static');
        $this->db->where('send_month', date('M-Y'));
        $query = $this->db->get(' t_limit_send')->first_row();
        return $query1->userlimit_send_static - $query->send_static;

    }

    public function sendlimit_select()
    {
        $this->db->where('is_activated', 1);
        $this->db->group_start();
        $this->db->or_where('login.id', $this->session->userdata('id'));
        $this->db->or_where('creator_id', $this->session->userdata('id'));
        $this->db->group_end();
        $query = $this->db->get('login')->result();
        return $query;
    }

    public function assgin_limit_send($data)
    {

        $this->db->where('send_month', date('M-Y'));
        $this->db->where('sendID', $data['userID']);
        $query = $this->db->get('t_limit_send')->first_row();
        if ($query != null) {
            $array = array(
                'sendID' => $data['userID'],
                'userID' => $this->session->userdata('id'),
                'send_month' => $data['userlimit_month'],
                'send_static' => $data['userlimit_send_static'] + $query->send_static,
                'send_dynamic' => $query->send_dynamic

            );
            $this->db->where('IDsendlimit', $query->IDsendlimit);
            $this->db->update('t_limit_send', $array);
        } else {
            $array = array(
                'sendID' => $data['userID'],
                'userID' => $this->session->userdata('id'),
                'send_month' => $data['userlimit_month'],
                'send_static' => $data['userlimit_send_static'],
                'send_dynamic' => 0

            );

            $this->db->insert('t_limit_send', $array);
        }
        $this->session->set_userdata("token", md5(uniqid(mt_rand(), true) . microtime(true)));

    }

    public function getLastIDLimitsend()
    {
        $this->db->select_max('IDuserlimit');
        return $this->db->get('t_user_limit')->row()->IDuserlimit;
    }

    public function getCurrentLimitSend()
    {
        $this->db->select("(send_static - send_dynamic) as remainingValue , send_dynamic , IDsendlimit");
        $this->db->where("send_month", date('M-Y'));
        if ($this->session->userdata("role") == "Agent") {
            $this->db->where("sendID", $this->session->userdata("creator_id"));
        } else {
            $this->db->where("sendID", $this->session->userdata("id"));
        }
        return $this->db->get("t_limit_send")->row();
    }

    public function getCurrentLimitUser()
    {
        $this->db->select("(userlimit_send_static - userlimit_send_dynamic) as remainingValue , userlimit_send_dynamic , IDuserlimit,IDpgquan");
        $this->db->where("userlimit_month", date('M-Y'));
        $this->db->where("userID", $this->session->userdata("id"));
        return $this->db->get("t_user_limit")->row();
    }

    public function getCurrentPageQuantity($IDpgquan)
    {
        $this->db->select("(pgquan_send_quan_static - pgquan_send_quan_dynamic) as remainingValue , pgquan_send_quan_dynamic ");
        $this->db->where("IDpgquan", $IDpgquan);
        return $this->db->get("t_page_quantity")->row();
    }
}
