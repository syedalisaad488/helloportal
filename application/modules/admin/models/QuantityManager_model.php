<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QuantityManager_model extends CI_model {

	function __construct(){
		parent::__construct();
	}

    public function getData()
    {
        return $this->db->get("t_dollar");
    }
    
    public function save_dollar()
    {
        $dollar_rs_value = $this->input->post("dollar_rs_value");
        $dollar_value_in_rs = $this->input->post("dollar_value_in_rs");
        $dollar_twillio_per_page = $this->input->post("dollar_twillio_per_page");
        $date = date('M-Y');

        $pageQuantity = round(($dollar_rs_value/$dollar_value_in_rs)/0.019);

        $dataArr = array(
                "dollar_rs_value" => $dollar_rs_value,
                "dollar_value_in_rs" => $dollar_value_in_rs,
                "dollar_date" => $date,
                "dollar_twillio_per_page" => $dollar_twillio_per_page,
                "dollar_value" => round($dollar_rs_value/$dollar_value_in_rs),
                "dollar_total_pages" => $pageQuantity,
                "dollar_twillio_per_page" => $dollar_twillio_per_page
            );
        $this->db->trans_start();
        $this->db->insert('t_dollar',$dataArr);
        $IDdollar = $this->db->insert_id();
        
        $this->db->where("IDdollar !=",$IDdollar);
        $updateData = array( "dollar_is_active" => 0);
        $this->db->update("t_dollar",$updateData);
        
        $this->db->where("pgquan_date",$date);
        $pgQuanMonthWiseData = $this->db->get("t_page_quantity");
        if($pgQuanMonthWiseData->num_rows()==0)
        {
            $dataArr = array(
                    "pgquan_quantity" => $pageQuantity,
                    "pgquan_date" => $date,
                    "IDdollar" => $IDdollar,
                    "pgquan_rec_quan_static" => round($pageQuantity*0.5),
                    "pgquan_send_quan_static" => round($pageQuantity*0.5),
                );
           
            $this->db->insert('t_page_quantity',$dataArr);   
            $IDLastPgQuan = $this->db->insert_id();
            $this->db->where("IDpgquan !=",$IDLastPgQuan);
            $updateDataPgQuan = array( "pgquan_is_active" => 0);
            $this->db->update("t_page_quantity",$updateDataPgQuan);
        }
        else
        {
            $pgQuanPrevious = $pgQuanMonthWiseData->row();
            
            $IDsDollarDecoded = json_decode($pgQuanPrevious->IDdollar);
            $json_IDsDollar= (!empty($IDsDollarDecoded)) ? json_encode($IDdollar . ", $IDsDollarDecoded") : json_encode($IDdollar) ;

            $this->db->where("IDpgquan",$pgQuanPrevious->IDpgquan);
            $pgQuanUpdateData =  array(
                    "pgquan_quantity" => $pgQuanPrevious->pgquan_quantity+$pageQuantity,
                    "pgquan_date" => $date,
                    "IDdollar" => $json_IDsDollar,
                    "pgquan_rec_quan_static" => $pgQuanPrevious->pgquan_rec_quan_static+(round($pageQuantity*0.5)),
                    "pgquan_send_quan_static" => $pgQuanPrevious->pgquan_send_quan_static+(round($pageQuantity*0.5)),
                );
            
            $this->db->update("t_page_quantity",$pgQuanUpdateData);
        }
     	$this->db->trans_complete();  
     	$this->session->set_userdata("token",md5(uniqid(mt_rand(), true).microtime(true)));
    }

    public function getLastIDdollar()
    {
        $this->db->select_max('IDdollar');
        return $this->db->get('t_dollar')->row()->IDdollar;
    }


}
