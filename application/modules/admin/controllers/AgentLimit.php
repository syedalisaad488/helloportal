<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AgentLimit extends My_controller {

    function __construct(){
        parent::__construct();
        $role = $this->session->userdata("role");
        if($role!="User")
        {
            redirect("admin/Dashboard");
        }
        $this->load->model("Admin_model");
    }
    public function index(){
        $randomString = uniqid(rand(), true);
        $this->session->set_userdata("token",$randomString);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $data['token'] = $randomString;
        $data['user'] = $this->Admin_model->sendlimit_data();
        $data['select_user'] = $this->Admin_model->sendlimit_select();
        $data['userlimit_total_fax'] = $this->Admin_model->sendlimit_total_fax();
        $this->load->view('User/agentLimit',$data);
    }
    public function assgin_limit_send(){
        $validation_number = $this->Admin_model->sendlimit_validation();
        $data = $this->input->post();
        if(count($data) != 5){
            if($validation_number-$data['userlimit_send_static'] <0){
                $this->session->set_flashdata("errorMsg",'<div class="alert alert-danger" role="alert" >Do not open Developer Tools !!</div>');
                redirect("admin/AgentLimit/");
            }
        }

        if($this->input->post("token") == $this->session->userdata("token"))
        {
            $this->Admin_model->assgin_limit_send($data);
        }
        redirect(base_url('admin/AgentLimit/'));
    }



}

