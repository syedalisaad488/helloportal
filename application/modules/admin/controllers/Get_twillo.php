<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Get_twillo extends My_controller {



    function __construct(){

        parent::__construct();
        if(!$this->session->userdata("id"))
        {
            redirect("admin/Dashboard/logout");
        }
        if($this->session->userdata("role") == "Agent")
        {
            redirect($this->session->userdata("login_redirect"));
        }
        passwordAuthentication();
        $this->load->model("Admin_model");

    }

    function get_new_fax(){
        $data['check'] = "";
        if($this->input->post())
        {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;

        }
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/recived_mfax',$data);
    }
    // GET REQUEST
    public function getList()
    {
        $data['check'] = "";
        if($this->input->post())
        {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;

        }
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/recived_twillo',$data);

    }
    public function  fetch_fax(){
        $callData = $this->Admin_model->get_twillo_faxlist_data();
       
        $totalData = $this->Admin_model->twillo_faxlist_count();
        $data = array();
        $id_counter = 1;

        foreach ($callData as $row) {
            $db_show_data_time = ($row->show_data_time == 0) ? '' : $row->show_data_time;
            $db_show_send_no = ($row->show_send_no == 0) ? '' : $row->show_send_no;
            $db_show_receive_no = ($row->show_receive_no == 0) ? '' : $row->show_receive_no;
            $is_edited = ($row->is_edited == 0) ? '' : 'checked';

            if(	$this->session->userdata('username')=='TLP Solution')
            {
                $show_data_time= $row->faxinfo_date_time."<input type='datetime-local' value='$db_show_data_time' id='show_data_time$row->id' name='show_data_time'>";
                $show_send_no=  $row->To."<input type='text'  id='show_send_no$row->id' value='$db_show_send_no' name='show_send_no'>";
                $show_receive_no= $row->From."<input type='text' id='show_receive_no$row->id' value='$db_show_receive_no' name='show_receive_no'>
                <input type='checkbox' class='pull-right' $is_edited id='is_edited$row->id'  name='is_edited'>
                            <a href='#' onclick='submitChangeReceive($row->id)'>Submit</a><br>";
            }
            else
            {
                $formstart="";
                $endform ='';
                $show_data_time= ($row->show_data_time != 0) ? date("Y-m-d h:i:sa",strtotime($row->show_data_time)) :  $row->faxinfo_date_time;
                $show_send_no=  ($row->show_send_no != 0) ? $row->show_send_no :  $row->To;
                $show_receive_no= ($row->show_receive_no != 0) ? $row->show_receive_no :  $row->From;
            }

            $callData = array();
            $callData[] = $row->faxinfo_med_id;
            $callData[] = $row->companyname;
            $callData[] = $row->downloaded == 1 ? '<span style="">'.$row->FaxSid.'</span>' : $row->FaxSid ;
            $callData[] = $show_data_time;
            $callData[] = $show_send_no;
            $callData[] =  $show_receive_no;
            $var = "old_new_".$id_counter;
            $num_pag="";
            if(file_exists('assets/documents/new_receiving/'.$row->FaxSid.".pdf"))
            {
              $pdftext = file_get_contents(base_url('assets/documents/new_receiving/').$row->FaxSid.".pdf");
              $num_pag = preg_match_all("/\/Page\W/", $pdftext);
            }
            $num_pag = $num_pag != "" ? 'Page(s): '.$num_pag : '';
            $status = ($row->faxinfo_old_new == 0 && $row->faxinfo_med_id == "") ? '<span style="color:red;"><b>New</b></span>' : '';
            $callData[] = '<a href="#"  class="medicalIdEntryBtn" data-id="'.$row->FaxSid.'">Manage Med-Id</a>
                        <form  action="'. base_url('/admin/Receiving/download/').$row->FaxSid.'" method="POST">
                            <input type="hidden" name="faxinfo_return_fax_id" value="'. $row->FaxSid.'">
                            <a href="#" onclick="change_status('. $id_counter.')" ><input type="submit" class="btn btn-primary" value="Download"></a>
                            <div id="'.$var.'"></span>'.$status.'</div><span>'.$num_pag.'</span>
                        </form>';
            $id_counter++;
            $data[] = $callData;
        }


        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);
    }


}


