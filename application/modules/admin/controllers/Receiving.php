<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;
class Receiving extends My_controller {

    private $sid    = "AC720fa9d520db09505609c091b9152f7f";
    private $token  = "2e11a5a6e537609375d68fa63d869bb3";
    private $security_id = "2486e9b3c97e";
    private $hipaaLayoutType = "";


    function __construct(){
//        ob_start();
        parent::__construct();
//        $role = $this->session->userdata("role");
//        if($role!="User")
//        {
//            redirect("admin/Dashboard");
//        }
        require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
        $this->load->model("Admin_model");
        $this->output->set_content_type('text/xml');





    }
    function download($id){

        $twilio = new Client($this->sid, $this->token);

        $fax = $twilio->fax->v1->faxes($id)
            ->fetch();

        $this->load->helper('download');
        $this->load->helper('file');
        $url= $fax->mediaUrl;
        $this->db->where("FaxSid",$id);
        $this->db->where("downloaded",1);
        $query = $this->db->get('received');
        $ret = $query->row();

        if(empty($ret)){
            $file_url = "https://s3-external-1.amazonaws.com/".str_replace("https://","",$url)."";

            $file_name = $id.'.pdf';

            $data = file_get_contents($file_url);
            $num_pag = preg_match_all("/\/Page\W/", $data);
            $num_pag = $num_pag != "" ? $num_pag : '0';
            $update_status = array(
                'pages' => $num_pag,
                'faxinfo_old_new' => 1,
                'downloaded' => 1
            );
            $this->db->where("FaxSid",$id);
            $this->db->update("received",$update_status);
            $this->db->where('pgquan_is_active',1);
            $query =$this->db->get('t_page_quantity')->first_row();

                   $t_page_quantity = array(
                       'pgquan_rec_quan_dynamic' => $num_pag+$query->pgquan_rec_quan_dynamic,
                   );
            $this->db->where('pgquan_is_active',1);
            $this->db->update("t_page_quantity",$t_page_quantity);

            file_put_contents(getcwd().'/assets/documents/new_receiving/'.$file_name, $data);

            force_download($file_name, $data);
        }else{
            $file_name = $id.'.pdf';
            $file_url = getcwd().'/assets/documents/new_receiving/'.$file_name;
            $data = file_get_contents($file_url);

            force_download($file_name, $data);

        }


    }

    public function sent()
    {
        $twimlResponse = new SimpleXMLElement("<Response></Response>");
        $receiveEl = $twimlResponse->addChild('Receive');
        $receiveEl->addAttribute('action', 'https://hello-portal.com/fax/admin/Receiving/received');

        $this->output->set_output($twimlResponse->asXML());
    }

    // Define a handler for when the fax is finished sending to us - if successful,
    // We will have a URL to the contents of the fax at this point
    public function received()
    {
        $send_nu = str_replace(array( '(', ')','-','+1' ,' '), '', $this->input->post("To"));
        $this->db->select('name');
        $this->db->where('sent_num',$send_nu);
         $this->db->where('role','User');
        $query = $this->db->get('login')->first_row();


        // log the URL of the PDF received in the fax
        log_message('info', $this->input->post("MediaUrl"));
        $data = array(
            'MediaUrl' =>  $this->input->post("MediaUrl"),
            'FaxSid' =>  $this->input->post("FaxSid"),
            'From' =>  $this->input->post("From"),
            'To' =>  $this->input->post("To"),
            'AccountSid' =>  $this->input->post("AccountSid"),
            'faxinfo_date_time' =>  date("Y-m-d h:I:s A"),
            'received_month' =>  date("M-Y"),
            'companyname' =>  isset($query->name) ? $query->name : '',
//            'companyname' =>  '',

        );

        $this->db->insert('received', $data);

        // Respond with empty 200/OK to Twilio
        $this->output->set_status_header(200);
        $this->output->get_output('');
    }




}

