<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class QA extends My_controller {

	function __construct(){
		parent::__construct();
		$role = $this->session->userdata("role");
		if($role!="QA")
		{
			redirect("admin/Dashboard");
		}
	    $this->load->library('upload');
	    $this->load->library('pdf');
		$this->load->model("Admin_model");
		$this->load->library('csvimport');
	}
	public function index()
	{
		redirect("admin/get/getlist");
	}



		public function edit_data($IDcampaign,$prefix)
		{


			$table = "";
		     
    	    if($prefix == "aurbrac")
    	    {
    	        $table = "t_aurtho_brace";
    	    }
    	    else if($prefix == "cgmdiab")
    	    {
    	        $table = "t_cgm_diabetic";
    	    }
    	    $data['IDcampaign'] = $IDcampaign;
    	    $data['prefix'] = $prefix;
			$this->load->view('include/head');
		    $this->load->view('include/nav');
    	    $this->load->view("admin/QA/update_status_entry",$data);

		}

		public function update_data($IDcampaign,$prefix,$status,$reason)
		{
		 
			$table = "";
		     
    	    if($prefix == "aurbrac")
    	    {
    	        $table = "t_aurtho_brace";
    	    }
    	    else if($prefix == "cgmdiab")
    	    {
    	        $table = "t_cgm_diabetic";
    	    }

    	    $this->Admin_model->update_qa_status($IDcampaign,$table,$prefix,$status,$reason);

    	    redirect("admin/QA/data_list/".$prefix."/QA_list");
		}

    public function campaign_edit_data($IDcampaign,$prefix)
    {
        $check_authen = $this->Admin_model->check_authencity_for_qa($IDcampaign,$prefix);
        if($check_authen==1)
        {
            $this->load->view('include/head');
            $data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
            $data['IDcampaign'] = $IDcampaign;
            $data['prefix'] = $prefix;
            $this->load->view("QA/".$prefix."_data_edit",$data);
        }

        else if($check_authen==0)
        {
            $this->load->view('include/head');
            $this->load->view('include/nav');
            $this->load->view("User/error");
        }
    }
	
	public function campaign_update_data($IDcampaign,$prefix)
	{
	     $table = "";
	    if($prefix == "aurbrac")
	    {
	        $table = "t_aurtho_brace";
	    }
	    else if($prefix = "cgmdiab")
	    {
	        $table = "t_cgm_diabetic";
	    }
		$data = $this->input->post();
	    $this->Admin_model->campaign_update_data($data,$IDcampaign,$prefix);

	    redirect("admin/User/campaign_all_list/".$prefix);
	}
    public function qa_campaign_update_data($IDcampaign,$prefix)
    {
        $table = "";
        if($prefix == "aurbrac")
        {
            $table = "t_aurtho_brace";
        }
        else if($prefix = "cgmdiab")
        {
            $table = "t_cgm_diabetic";
        }
        $check_authen = $this->Admin_model->check_authencity_for_qa($IDcampaign,$prefix);
        if($check_authen==1)
        {
            $data = $this->input->post();
            $this->Admin_model->qa_campaign_update_data($data,$IDcampaign,$prefix);
            $this->session->set_flashdata("data_saved","Data edited successfully");
            redirect($_SERVER['HTTP_REFERER']);
        }
        else if($check_authen==0)
        {
            $this->load->view('include/head');
            $this->load->view('include/nav');
            $this->load->view("User/error");
        }
    }   
		public function campaign_save_html_to_pdf($IDcampaign,$prefix,$table)
	{
	    $this->load->library('upload');
	    $this->load->library('pdf');
	    $this->campaign_write_fax($IDcampaign,$prefix,$table);
        $html = $this->output->get_output();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();
        $this->dompdf = null;
   		file_put_contents("assets/documents/faxes/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", $output);
	}
	
	public function campaign_write_fax($IDcampaign,$prefix,$table_name)
	{
    	$data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table_name);
		$this->load->view("admin/fax_temp_$prefix",$data);
	}
		public function qa_campaign_pending_list($prefix,$type)
		{
			if($prefix=='aurbrac'){
				$IDuser = $this->session->userdata("id");
				$table="t_aurtho_brace";
				$data['client_data'] = $this->Admin_model->get_data_list($prefix,$type,$table,'aurbrac_med_id');
								$this->load->view('include/qa_nav');

				$this->load->view('include/nav');
				$this->load->view('admin/QA/qa_aurbrac_data',$data);
			}
			else if($prefix=='cgmdiab'){
				$table="t_cgm_diabetic";
				$IDuser = $this->session->userdata("id");
				$data['client_data'] = $this->Admin_model->get_data_list($prefix,$type,$table,'cgmdiab_medicare');
								$this->load->view('include/qa_nav');

				$this->load->view('include/nav');
				$this->load->view('admin/QA/qa_cgmdiab_data',$data);
			}
		}
    public function campaign_list($prefix)
    {
        if($prefix=='aurbrac'){
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/QA/qa_aurbrac_data');
        }
        else if($prefix=='cgmdiab'){
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/QA/qa_cgmdiab_data');
        }
    }
    public function fetch_camp_data($prefix,$type){



        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'aurbrac_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');

        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'cgmdiab_medicare');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'cgmdiab_medicare');

        }



        $data = array();
        if($prefix=='aurbrac')
        {
            foreach ($callData as $row) {
                $callData = array();
                $status = "";
                if ($row->aurbrac_status == 'Good') {
                    $status = "<span class='btn btn-success'>GOOD</span>";
                } else if ($row->aurbrac_status == 'Deleted') {
                    $status = "<span class='btn btn-danger'>DELETED</span>";
                } else if ($row->aurbrac_status == 'Rejected') {
                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
                } else if ($row->aurbrac_status == 'Rxadditional') {
                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
                } else if ($row->aurbrac_status == 'Sent') {
                    $status = "<span class='btn btn-info' >SENT</span>";
                } else if ($row->aurbrac_status == 'Completed') {
                    $status = "<span class='btn btn-primary'  >Completed</span>";
                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDaurbrac . '" ></center>';
                $callData[] = $status;
                $callData[] = $row->aurbrac_reason;
                $callData[] = $row->aurbrac_sns;
                $callData[] = $row->aurbrac_firstname;
                $callData[] = $row->aurbrac_lastname;
                $callData[] = $row->aurbrac_address;
                $callData[] = $row->aurbrac_city;
                $callData[] = $row->aurbrac_state;
                $callData[] = $row->aurbrac_zip;
                $callData[] = $row->aurbrac_email;
                $callData[] = $row->aurbrac_dob;

                $callData[] = (strpos($row->aurbrac_phone, '+1') !== false) ? $row->aurbrac_phone : '+1' . $row->aurbrac_phone;
                $callData[] = $row->aurbrac_gender;
                $callData[] = $row->aurbrac_ssn;
                $callData[] = ($row->aurbrac_back == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_hip == 1 ? 'Yes' : 'No');
                $callData[] = $row->aurbrac_doc_firstname;
                $callData[] = $row->aurbrac_doc_lastname;
                $callData[] = $row->aurbrac_doc_address;
                $callData[] = $row->aurbrac_doc_city;
                $callData[] = $row->aurbrac_doc_state;
                $callData[] = $row->aurbrac_doc_zip;
                $callData[] = $row->aurbrac_doc_npi;
                $callData[] = (strpos($row->aurbrac_doc_phone, '+1') !== false) ? $row->aurbrac_doc_phone : '+1' . $row->aurbrac_doc_phone;
                $callData[] = (strpos($row->aurbrac_doc_fax, '+1') !== false) ? $row->aurbrac_doc_fax : '+1' . $row->aurbrac_doc_fax;
                $callData[] = $row->aurbrac_pcp_within_past;
                $callData[] = $row->aurbrac_med_id;
                $callData[] = $row->aurbrac_insurance_company;
                $callData[] = $row->aurbrac_IDmember;
                $callData[] = $row->aurbrac_IDgroup;
                $callData[] = $row->aurbrac_data_entered;
                $callData[] = $row->aurbrac_IDtrack;
                $callData[] = $row->aurbrac_IDsite;
                $callData[] = $row->aurbrac_repcode;
                $callData[] = $row->aurbrac_record_file;
                $callData[] = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDaurbrac) . '/aurbrac" >Edit</a>
									<div class="dropdown" >
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Change Status
                                            </button>
                                        <div class="dropdown-menu">
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>                                    
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MISSING ICD 10">MISSING ICD 10</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
                                    </div>
                                    </div>';
                $data[] = $callData;
            }
        }
        else if($prefix=='cgmdiab') {

            foreach ($callData as $row) {
                $callData = array();
                $status = "";
                if ($row->cgmdiab_status == 'Good') {
                    $status = "<span class='btn btn-success'>GOOD</span>";
                } else if ($row->cgmdiab_status == 'Deleted') {
                    $status = "<span class='btn btn-danger'>DELETED</span>";
                } else if ($row->cgmdiab_status == 'Rejected') {
                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
                } else if ($row->cgmdiab_status == 'Rxadditional') {
                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
                } else if ($row->cgmdiab_status == 'Sent') {
                    $status = "<span class='btn btn-info' >SENT</span>";
                } else if ($row->cgmdiab_status == 'Completed') {
                    $status = "<span class='btn btn-primary'  >Completed</span>";
                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDcgmdiab . '" ></center>';
                $callData[] = $status;
                $callData[] = $row->cgmdiab_reason;
                $callData[] = $row->cgmdiab_sns;
                $callData[] = $row->cgmdiab_firstname;
                $callData[] = $row->cgmdiab_lastname;
                $callData[] = $row->cgmdiab_address;
                $callData[] = $row->cgmdiab_city;
                $callData[] = $row->cgmdiab_state;
                $callData[] = $row->cgmdiab_zip;
                $callData[] = $row->cgmdiab_email;
                $callData[] = $row->cgmdiab_dob;

                $callData[] = (strpos($row->cgmdiab_phone, '+1') !== false) ? $row->cgmdiab_phone : '+1' . $row->cgmdiab_phone;
                $callData[] = $row->cgmdiab_sex;
                $callData[] = $row->cgmdiab_ssn;
                $callData[] = $row->cgmdiab_diabetes;
                $callData[] = $row->cgmdiab_visit_doc;
                $callData[] = $row->cgmdiab_insulin_pum;
                $callData[] = $row->cgmdiab_blood_sugar;
                $callData[] = $row->cgmdiab_doc_firstname;
                $callData[] = $row->cgmdiab_doc_lastname;
                $callData[] = $row->cgmdiab_doc_address;
                $callData[] = $row->cgmdiab_doc_city;
                $callData[] = $row->cgmdiab_doc_state;
                $callData[] = $row->cgmdiab_doc_zip;
                $callData[] = $row->cgmdiab_doc_npi;
                $callData[] = (strpos($row->cgmdiab_doc_phone, '+1') !== false) ? $row->cgmdiab_doc_phone : '+1' . $row->cgmdiab_doc_phone;
                $callData[] = (strpos($row->cgmdiab_doc_fax, '+1') !== false) ? $row->cgmdiab_doc_fax : '+1' . $row->cgmdiab_doc_fax;
                $callData[] = $row->cgmdiab_patient_ack;
                $callData[] = $row->cgmdiab_finan_aggrem;
                $callData[] = $row->cgmdiab_contact_me;
                $callData[] = $row->cgmdiab_coinsurance;
                $callData[] = $row->cgmdiab_medicare;
                $callData[] = $row->cgmdiab_insurance_com;
                $callData[] = $row->cgmdiab_IDmember;
                $callData[] = $row->cgmdiab_IDgroup;
                $callData[] = $row->cgmdiab_date_entered;
                $callData[] = $row->cgmdiab_IDtrack;
                $callData[] = $row->cgmdiab_IDsite;
                $callData[] = $row->cgmdiab_repcode;
                $callData[] = $row->cgmdiab_patient_url;
                $callData[] = $row->cgmdiab_recording;
                $callData[] = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDcgmdiab) . '/cgmdiab" >Edit</a>
									<div class="dropdown" >
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Change Status
                                            </button>
                                        <div class="dropdown-menu">
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>                                    
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING ICD 10">MISSING ICD 10</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
                                    </div>
                                    </div>';
                $data[] = $callData;
            }
        }

        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);

    }

}

