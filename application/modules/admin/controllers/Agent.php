<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends My_controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$role = $this->session->userdata("role");
		if($role!="Agent")
		{
			redirect("admin/Dashboard");
		}
		$this->load->library('upload');
	    $this->load->library('pdf');
		$this->load->model("Admin_model");
	}


		public function save_agent($id=null)
	{
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		if($id==null){
			if($name!=null){
				$this->Admin_model->save_agent($name,$password,$role,$id);		
			}
		$query['data']=$this->Admin_model->get_agent_data();
		$this->load->view('Agent/add_agent',$query);
		}else{
			if($name!=null){
				 $this->Admin_model->save_agent($name,$password,$role,$id);	
				 $reload='1';	
			}
			if(isset($reload)){
				$query['data']=$this->Admin_model->get_agent_data();
		$this->load->view('Agent/add_agent',$query);

			}else{
				$query['data']=$this->Admin_model->get_agent_data($id);
		$this->load->view('Agent/edit_agent',$query);

			}
					
		}
	}
public function delete($id=null)
	{
	$this->Admin_model->delete($table='login',$id);	
	$this->load->save_agent();		
	}


	public function data_list()
	{
		$IDagent = $this->session->userdata("id");
		$data['client_data'] = $this->Admin_model->get_client_data_for_agent($IDagent);
		$this->load->view('include/head');
		$this->load->view('include/nav');	
		$this->load->view('admin/Agent/client_data',$data);
	}

	public  function update_data($IDclient)
	{
	  $data = $this->input->post();
	  $this->Admin_model->update_client_data($data,$IDclient);
	  $this->save_html_to_pdf($IDclient);
	  redirect("admin/Agent/data_list");	
	}

	public function edit_client_data($IDclient)
	{
		$data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('admin/Agent/client_edit',$data);
	}


	public function show_client_data($IDclient)
	{
		$data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
		
		$this->load->view('repren/client_details',$data);
	}
	
		public function save_html_to_pdf($IDclient)
	{
	    $this->write_fax($IDclient);
        $html = $this->output->get_output();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();
   		file_put_contents("assets/documents/faxes/fax_of_sending$IDclient.pdf", $output);
	}
	
	public function write_fax($id)
	{
	    	$data['fax'] = $this->Admin_model->get_fax($id);
			$this->load->view('admin/fax_template',$data);
	}

}

