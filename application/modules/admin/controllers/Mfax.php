<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mfax extends My_controller {

    function __construct(){
        parent::__construct();

    }

    public function index()
    {
        $url = 'https://api.documo.com/v1/faxes';

        $file = new CURLFile('assets/pdf-new/user_103/Jun-2021/2NWbqxvKS5OnjBrwCTP3-6-4b48b9ba63b6234e30405cab53b0017325882673-1624298949_103dummy.pdf', 'application/pdf', 'sample');
        $body = array(
            'faxNumber' => '17862449574',
            'callerId' => '8669510781',
            'recipientName' => '',
            'coverPage' => '',
            'subject' => '',
            'notes' => '',
            'attachments' => $file
        );

        $headers = array(
            'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
            'Content-Type: multipart/form-data'
        );

        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($response);
        print_r($res);"<br><br>";
        if(isset($res->resultInfo))
        {
            echo "Fax has sent , ID: ".$res->messageId;
        }
        else
        {
            echo "something went wrong";
        }

        echo "<br><br><br><br><br>";



    }

    public function getStatus()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.documo.com/v1/fax/dbcb4199-9a54-4150-9225-6996a6d7f7ed/info',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
      $response = json_decode($response);
        echo $response->status;

    }
//
//    public function updateNumber()
//        {
//            $old = array(
//
//                '7865514352'
//                ,'7865514209'
//                ,'7865514427'
//                ,'7864184496'
//                ,'7865514139'
//                ,'7865514363'
//                ,'7862899977'
//                ,'7863861823'
//                ,'7865514192'
//                ,'7865514456'
//                ,'7865514367'
//                ,'7864421176'
//                ,'7865514266'
//                ,'7865514275'
//                ,'7865514334'
//                ,'7865514370'
//                ,'7865514029'
//                ,'7865514112'
//                ,'7865514030'
//                ,'7865514240'
//                ,'7865514335'
//                ,'7865514159'
//                ,'7865514230'
//                ,'7865514176'
//                ,'7865514171'
//                ,'7865514428'
//                ,'7865514331'
//                ,'7865514038'
//                ,'7865514227'
//                ,'7865514324'
//                ,'7865514416'
//                ,'7865514438'
//                ,'7865514043'
//                ,'7865514137'
//                ,'7865514259'
//                ,'7865514201'
//                ,'7865514429'
//                );
//
//
//            $new = array(
//                "7862449574",
//                "7865519432",
//                "7865519444",
//                "7865519954",
//                "7865519987",
//                "7865780033",
//                "7865780238",
//                "7865780433",
//                "7865910210",
//                "7865910267",
//                "7866541331",
//                "7866541436",
//                "7866870093",
//                "7866870435",
//                "7866870931",
//                "7866870936",
//                "7866870937",
//                "7866870938",
//                "7866870941",
//                "7866870943",
//                "7866870946",
//                "7866870947",
//                "7866870948",
//                "7866870953",
//                "7866870957",
//                "7866870958",
//                "7866870959",
//                "7866870965",
//                "7866870966",
//                "7866870969",
//                "7866870970",
//                "7866870971",
//                "7866870973",
//                "7866870975",
//                "7866870976",
//                "7866870978",
//                "7866870980",
//            );
//
//            for($i=0;$i<count($old);$i++)
//            {
//                $this->db->where("sent_num",$old[$i]);
//                $data = array("sent_num"=>$new[$i]);
//                $this->db->update("login",$data);
//            }
//        }
//
//        public function receive_sent_change()
//        {
//            foreach($this->db->get("login")->result() as $key)
//                {
//                    $this->db->where("id",$key->id);
//                    $data=array("receive_num"=>$key->sent_num);
//                    $this->db->update("login",$data);
//                }
//        }
}

