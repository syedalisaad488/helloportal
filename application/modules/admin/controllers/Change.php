<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Change extends My_controller {

	function __construct(){
		parent::__construct();


	}





	public function changetab($prefix)
	{

	   $status = $this->input->post('status');
	   $table = "";
		if($prefix == "aurbrac")
	    {
	        $table = "t_aurtho_brace";
	    }
	    else if($prefix == "cgmdiab")
	    {
	        $table = "t_cgm_diabetic";
	    }
		else if($prefix == "mask")
		{
			$table = "t_mask";
		}
		else if($prefix == "heatpad")
		{
			$table = "t_heatpad";
		}else if($prefix == "hipaa")
		{
			$table = "t_hipaa";
		}
        else if($prefix == "faxpdf")
        {
            $table = "t_faxpdf";
        }


		for($i=0;$i<count($this->input->post("send_fax_data[]"));$i++)
		{
			$IDcampaign = $this->input->post("send_fax_data[$i]");

		if($status=="SNS_checked")
        {
            $data = array(
            $prefix.'_sns' => "checked"

        );

        $this->db->where('ID'.$prefix, $IDcampaign);
        $this->db->update($table, $data);

        }
        else
        {
		    $data = array(
            $prefix.'_status' => $status

        );

        $this->db->where('ID'.$prefix, $IDcampaign);
        $this->db->update($table, $data);
        }
		}
		$previous_url = $this->session->userdata('previous_url');

		 if($prefix == "aurbrac")
	    {
	    	if($this->session->userdata('role')!='Owner'){

				redirect('admin/List_controller/campaign_list/aurbrac/Cleaning');
			}else{
				redirect('admin/Owner/campaign_list/aurbrac/Cleaning');


			}
	    }
	    else if($prefix == "cgmdiab")
	    {
			if($this->session->userdata('role')!='Owner') {

				redirect('admin/List_controller/campaign_list/cgmdiab/Cleaning');
			} else{

			}

		}
		redirect($_SERVER['HTTP_REFERER']);


	}




}

