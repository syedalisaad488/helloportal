<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;
class User extends My_controller {

    private $sid    = "AC720fa9d520db09505609c091b9152f7f";
    private $token  = "2e11a5a6e537609375d68fa63d869bb3";
    private $security_id = "2486e9b3c97e";
    private $LayoutType = "";
    private $currentLimit = 0 ;
    private $globalFaxData ;



//    private $sid    = "AC100b9cdf3bc43c1f10f4e91f554bf716";
//	private $token  = "80bc466098b22c0edfcd2486e9b3c97e";
//	private $this->security_id = "2486e9b3c97e";
//    private $LayoutType = "";


    function __construct(){
        parent::__construct();
        $role = $this->session->userdata("role");
		if($role=="User" || $role=="Agent")
		{
			
		}
		else
		{
		    redirect("admin/Dashboard");
		}
        passwordAuthentication();
        require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
        $this->load->model("Admin_model");
        $this->load->library('csvimport');
        $this->load->library('upload');
        $this->load->library('pdf');
        $this->load->library('form_validation');
        $root = base_url();



    }

    public function test_pad($IDcampaign,$prefix,$table)
    {
        $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
        $fax_back = $prefix."_fax_back";
        $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
        print_r($data['fax']);
    }

    public function received()
    {
        // log the URL of the PDF received in the fax
        log_message('info', $this->input->post("MediaUrl"));

        // Respond with empty 200/OK to Twilio
        $this->set_status_header(200);
        $this->output->set_output('');
    }
//    public function index()
//    {
//        $this->load->view('include/head');
//        $this->load->view('include/nav');
//        $this->load->view('User/index');
//
//    }
    
    public function agent()
    {
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $data['agent_data'] = $this->Admin_model->getAgentDataForUser();
        $this->load->view("User/add_agent",$data);
    }
    
    public function edit_agent($id)
    {
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $data['agent_data'] = $this->Admin_model->getAgentDataForUser($id)->row();
        $this->load->view("User/edit_agent",$data);
    }
    
    public function changeNum()
    {
        $this->db->trans_start();
        $this->db->where("role","Agent");
        foreach($this->db->get("login")->result() as $key )
        {
            
            $this->db->where("agentID",$key->id);
            $this->db->where("agentID != 0");

            $updateData = array("userID" => $key->creator_id);
            $this->db->update("t_mask",$updateData);
        }
        $this->db->trans_complete();
    }
    
    public function update_agent()
    {
        $name = $this->input->post('name');
        $id = $this->input->post('IDagent');
        $password = $this->input->post('password');
        $name_edit = $this->Admin_model->getAgentDataForUser($id)->row()->name;
        $this->form_validation->set_rules('name', 'Username', 'is_unique[login.name]');
        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            if($name==$name_edit)
            {
                if(strlen($password)>20)
                {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Maximum length is 15.</div>');
                    redirect("admin/User/edit_agent/".$id);
                }
                if(preg_match('/^[[:alnum:]]+$/', $password) === 0)
                {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Use Alphabets and Numbers only without space.</div>');
                    redirect("admin/User/edit_agent/".$id);
                }
                else
                {
                    $this->Admin_model->update_agent($name, $password, $id);
                    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert" >Successfully Edited.</div>');
                    redirect("admin/User/agent");
                }
            }
            else
            {
                if ($this->form_validation->run() == FALSE)
                {
                    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Agent name :  "'.$name.'" is taken. Try different username.</div>');
                    redirect("admin/User/edit_agent/".$id);
                }
                else
                {
                    if(strlen($name)>20 || strlen($password)>20)
                    {
                        $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Maximum length is 20.</div>');
                        redirect("admin/User/edit_agent/".$id);
                    }
                    if(preg_match('/^[[:alnum:]]+$/', $name) === 0 || preg_match('/^[[:alnum:]]+$/', $password) === 0)
                    {
                        $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Use Alphabets and Numbers only without space.</div>');
                        redirect("admin/User/edit_agent/".$id);
                    }
                    else
                    {
                        $this->Admin_model->update_agent($name, $password, $id);
                        $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert" >Successfully Edited.</div>');
                        redirect("admin/User/agent");
                    }
                }
            }
        }
    }
    
    public function save_agent()
    {
        $name = $this->input->post('name');
        $password = $this->input->post('password');
        $this->form_validation->set_rules('name', 'Username', 'is_unique[login.name]');

        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            if ($this->form_validation->run() == FALSE)
            {   
                $this->session->set_flashdata('name',$name);
                $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Agent name :  "'.$name.'" is taken. Try different username.</div>');
            }
            else
            {   
                if(strlen($name)>20 || strlen($password)>20)
                {
                    $this->session->set_flashdata('name',$name);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Maximum length is 20.</div>');
                    redirect("admin/User/agent");
                }
                if(preg_match('/^[[:alnum:]]+$/', $name) === 0 || preg_match('/^[[:alnum:]]+$/', $password) === 0)
                {
                    $this->session->set_flashdata('name',$name);
                    $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Use Alphabets and Numbers only without space.</div>');
                }
                else
                {
                    $this->Admin_model->save_agent($name, $password);
                    $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert" >Agent name :  "'.$name.'" is successfully added.</div>');
                }

            }
        }

        redirect("admin/User/agent");
    }
    
    public function Avalibility($par,$id)
    {
        $is_activated;
        if($par == "activate") { $is_activated = 1;} else if($par == "de-activate") { $is_activated = 0 ; }
        $updateData = array("is_activated" => $is_activated != null ? $is_activated : 0);
        $this->Admin_model->agentAvailibility($updateData,$id);
        redirect("admin/user/agent");
    }



    public function pending_list()
    {
        $IDuser = $this->session->userdata("id");
        $data['client_data'] = $this->Admin_model->get_pending_client_data_for_user($IDuser);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/client_data_pending',$data);
    }

    public function sent_list()
    {
        $IDuser = $this->session->userdata("id");
        $data['client_data'] = $this->Admin_model->get_sent_client_data_for_user($IDuser);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/client_data_sent',$data);
    }

    public function data_list()
    {
        $IDuser = $this->session->userdata("id");
        $data['client_data'] = $this->Admin_model->get_client_data_for_user($IDuser);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/client_data',$data);
    }

//      $sid    = "AC720fa9d520db09505609c091b9152f7f";
// 		$token  = "2e11a5a6e537609375d68fa63d869bb3";
    public function test()
    {
      echo $this->Admin_model->getCurrentLimitSend()->remainingValue;   
    }
    
    public function limitForBrace($product,$IDcampaign)
    {
        $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/aurbrac/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_aurbrac_".$IDcampaign.".pdf");
        $num_pag = preg_match_all("/\/Page\W/", $pdftext);
        if(!isset($num_pag) && $num_pag > $this->Admin_model->getCurrentLimitSend()->remainingValue)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

    private function faxSend($sender,$receiver,$path)
    {
        $url = 'https://api.documo.com/v1/faxes';

        $file = new CURLFile($path , 'application/pdf', 'sample');
        $body = array(
            'faxNumber' => $receiver,
            'callerId' => $sender,
            'recipientName' => '',
            'coverPage' => '',
            'subject' => '',
            'notes' => '',
            'attachments' => $file
        );

        $headers = array(
            'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
            'Content-Type: multipart/form-data'
        );

        $ch = curl_init();

        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    public function campaign_send_fax($prefix)
    {
        $stopper = 0;
        $this->currentLimit = $this->Admin_model->getCurrentLimitSend()->remainingValue;
        if(!isset($this->currentLimit) || $this->currentLimit<=5)
        {
            $stopper = 1;
        }

        $stopper = 0;
        if($stopper == 0)
        {
            $table = "";
            if($prefix == "aurbrac")
            {
                $table = "t_aurtho_brace";
            }
            else if($prefix == "cgmdiab")
            {
                $table = "t_cgm_diabetic";
            }
            else if($prefix == "mask")
            {
                $this->LayoutType = $this->input->post("maskLayoutType");
                $table = "t_mask";
            }
            else if($prefix == "heatpad")
            {
                $table = "t_heatpad";
            }
            else if($prefix == "hipaa")
            {
                $this->LayoutType = $this->input->post("hipaaLayoutType");
                $table = "t_hipaa";
            }
            else if($prefix == "faxpdf")
            {
                $table = "t_faxpdf";
            }
            
           
    
            $IDcampaign = $this->input->post("campaign_id");

            $fax_data = $this->Admin_model->get_data_for_fax_send($IDcampaign,$prefix);
      
            $result =  $this->fax_html($IDcampaign,$prefix,$table,$fax_data);
        
            if(empty($result))
            {
            
                //to check if fax-ID is present in the currently logged-in account , if it doesn't exist(this will only happen if someone tried in developers tools to mess
                // with the system)
                if(empty($fax_data[0][$prefix.'_doc_fax']))
                {
                    die;
                }
        
        
                // retrieving all number of sent faxes and checking whether the count is greater or equal to 3
                $DecodedTimestamp = json_decode($fax_data[0][$prefix.'_no_of_sent_faxes']);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
                $counter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
        
        
                if($counter>=3)
                {
                    //because the leads was already sent 3 times so it just changes its status to 'Sent' in database without sending it to doctor.
                    $update_data = array($prefix."_status" => "Sent");
                    $this->db->where("ID$prefix",$IDcampaign);
                    $this->db->update($table,$update_data);
                    die;
                }
        
                $doc_no = $fax_data[0][$prefix.'_doc_fax'];
                $send_no = $this->session->userdata("sent_num");
                $doc_fax_num =  str_replace(array( '(', ')','-','+1' , ' ' ), '',$doc_no);
                $doc_fax_num = "1".$doc_fax_num;
                $send_num =  $send_no;

                
                $num_pag=0;

                if($prefix == "cgmdiab")
                {
                    $res = $this->faxSend($send_num,$doc_fax_num,'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
                    if(isset($res->resultInfo))
                    {
                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
                    }
                    else
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }
//                    try
//                    {
//                        $twilio = new Client($this->sid, $this->token);
//                        $fax = $twilio->fax->v1->faxes
//                            ->create('+1'.$doc_fax_num, // to
//                                base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", // mediaUrl
//                                array("from" => '+1'.$send_num)
//                            );
//                        $this->session->set_userdata("filePath",base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
//                        $IDfax = $fax->sid;
//                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$IDfax);
//                    }
//                    catch(Exception $e)
//                    {
//                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
//                    }
                    // unlink(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
        
                }
        
                else if($prefix == "mask")
                {
                    $res = $this->faxSend($send_num,$doc_fax_num,'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
                    if(isset($res->resultInfo))
                    {
                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
                    }
                    else
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }

//                    try
//                    {
//                        $twilio = new Client($this->sid, $this->token);
//                        $fax = $twilio->fax->v1->faxes
//                            ->create('+1'.$doc_fax_num, // to
//                                base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", // mediaUrl
//                                array("from" => '+1'.$send_num)
//                            );
//                        $this->session->set_userdata("filePath",base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
//                        $IDfax = $fax->sid;
//                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$IDfax);
//                    }
//                    catch(Exception $e)
//                    {
//                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
//                    }
                    // unlink(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
        
                }
        
                else if($prefix == "heatpad")
                {
                    $res = $this->faxSend($send_num,$doc_fax_num,'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
                    if(isset($res->resultInfo))
                    {
                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
                    }
                    else
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }

//                    try
//                    {
//                        $twilio = new Client($this->sid, $this->token);
//                        $fax = $twilio->fax->v1->faxes
//                            ->create('+1'.$doc_fax_num, // to
//                                base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", // mediaUrl
//                                array("from" => '+1'.$send_num)
//                            );
//                        $this->session->set_userdata("filePath",base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
//                        $IDfax = $fax->sid;
//                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$IDfax);
//                    }
//                    catch(Exception $e)
//                    {
//                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
//                    }
                    // unlink(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
        
                }
        
                else if($prefix == "hipaa")
                {
                    $res = $this->faxSend($send_num,$doc_fax_num,'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
                    if(isset($res->resultInfo))
                    {
                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
                    }
                    else
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }
//                    try
//                    {
//                        $twilio = new Client($this->sid, $this->token);
//                        $fax = $twilio->fax->v1->faxes
//                            ->create('+1'.$doc_fax_num, // to
//                                base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", // mediaUrl
//                                array("from" => '+1'.$send_num)
//                            );
//                        $this->session->set_userdata("filePath",base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
//                        $IDfax = $fax->sid;
//                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$IDfax);
//                    }
//                    catch(Exception $e)
//                    {
//                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
//                    }
                    // unlink(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
        
                }
        
                else if($prefix == "faxpdf")
                {
                    $pathPDF = "";
                    if($fax_data[0][$prefix.'_path_old_new']==0)
                    {
                        $pathPDF = "assets/pdf/".$fax_data[0][$prefix.'_file'] ;
                    }
                    else if($fax_data[0][$prefix.'_path_old_new']==1)
                    {
                        $pathPDF = "assets/pdf-new/".$fax_data[0][$prefix.'_path']."/".$fax_data[0][$prefix.'_file'];
                    }
                    
                    $this->session->set_userdata("filePath",$pathPDF);

                    $res = $this->faxSend($send_num,$doc_fax_num,$pathPDF);
          

                    if(isset($res->resultInfo))
                    {
                        $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
                    }
                    else
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }
        
                }
        
                else if($prefix == "aurbrac")
                {
        
                    $product = "";
                    $aur = $this->Admin_model->get_aurbrac_data_for_fax($IDcampaign,$prefix);
                    $product = "";
                    $check_availibility = 0;
                    $aurbracSendCounter = 1;
        
        
        
                    if($aur[0]['aurbrac_back'] == 1 && $this->limitForBrace("back",$IDcampaign) !=0 ) { $product = "back"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
        
                    if($aur[0]['aurbrac_l_knee'] == 1 && $aur[0]['aurbrac_r_knee'] == 1 && $this->limitForBrace("knee",$IDcampaign) !=0 ) { $product = "knee"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    else
                    {
                        if($aur[0]['aurbrac_l_knee'] == 1 && $this->limitForBrace("left_knee",$IDcampaign) !=0 ) { $product = "left_knee"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                        if($aur[0]['aurbrac_r_knee'] == 1 && $this->limitForBrace("right_knee",$IDcampaign) !=0 ) { $product = "right_knee"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    }
        
                    if($aur[0]['aurbrac_l_wrist'] == 1 && $aur[0]['aurbrac_r_wrist'] == 1 && $this->limitForBrace("wrist",$IDcampaign) !=0 ) { $product = "wrist"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    else
                    {
                        if($aur[0]['aurbrac_l_wrist'] == 1 && $this->limitForBrace("left_wrist",$IDcampaign) !=0 ) { $product = "left_wrist"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                        if($aur[0]['aurbrac_r_wrist'] == 1 && $this->limitForBrace("right_wrist",$IDcampaign) !=0 ) { $product = "right_wrist"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    }
        
                    if($aur[0]['aurbrac_l_ankle'] == 1 && $aur[0]['aurbrac_r_ankle'] == 1 && $this->limitForBrace("ankle",$IDcampaign) !=0 ) { $product = "ankle"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    else
                    {
                        if($aur[0]['aurbrac_l_ankle'] == 1 && $this->limitForBrace("left_ankle",$IDcampaign) !=0 ) { $product = "left_ankle"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                        if($aur[0]['aurbrac_r_ankle'] == 1 && $this->limitForBrace("right_ankle",$IDcampaign) !=0 ) { $product = "right_ankle"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    }
        
                    if($aur[0]['aurbrac_l_hip'] == 1 && $aur[0]['aurbrac_r_hip'] == 1 && $this->limitForBrace("hip",$IDcampaign) !=0 ) { $product = "hip"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    else
                    {
                        if($aur[0]['aurbrac_l_hip'] == 1 && $this->limitForBrace("left_hip",$IDcampaign) !=0 ) { $product = "left_hip"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                        if($aur[0]['aurbrac_r_hip'] == 1 && $this->limitForBrace("right_hip",$IDcampaign) !=0 ) { $product = "right_hip"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    }
        
                    if($aur[0]['aurbrac_l_shoulder'] == 1 && $this->limitForBrace("left_shoulder",$IDcampaign) !=0 ) { $product = "left_shoulder"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    if($aur[0]['aurbrac_r_shoulder'] == 1 && $this->limitForBrace("right_shoulder",$IDcampaign) !=0 ) { $product = "right_shoulder"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    if($aur[0]['aurbrac_l_elbow'] == 1 && $this->limitForBrace("left_elbow",$IDcampaign) !=0 ) { $product = "left_elbow"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    if($aur[0]['aurbrac_r_elbow'] == 1 && $this->limitForBrace("right_elbow",$IDcampaign) !=0 ) { $product = "right_elbow"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
                    if($aur[0]['aurbrac_neck'] == 1 && $this->limitForBrace("neck",$IDcampaign) !=0 ) { $product = "neck"; $check_availibility = 1; $this->send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter); $aurbracSendCounter = 0; }
        
                    if( $check_availibility == 0)
                    {
                        $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
                    }
        
                }
            }
            else
            {
                $stopper = 1;
            }
        }
        echo $stopper;
        
    }

    public function send_fax_aurbrac($doc_fax_num,$send_num,$prefix,$IDcampaign,$product,$aurbracSendCounter)
    {


        $res = $this->faxSend($send_num,$doc_fax_num,'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf");
        if(isset($res->resultInfo))
        {
            $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$res->messageId);
        }
        else
        {
            $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
        }

//        $twilio = new Client($this->sid, $this->token);
//        try
//        {
//            $fax = $twilio->fax->v1->faxes
//                ->create('+1'.$doc_fax_num, // to
//                base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf", // mediaUrl
//                    array("from" => '+1'.$send_num)
//                );
//            $this->session->set_userdata("filePath",base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf");
//            $IDfax = $fax->sid;
//            $this->Admin_model->campaign_update_fax_info($prefix,$IDcampaign,$IDfax,$aurbracSendCounter);
//        }
//        catch(Exception $e)
//        {
//            $this->Admin_model->campaign_update_not_sent_fax_info($prefix,$IDcampaign);
//        }
        // unlink(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf");

    }

    public function fax_html($IDcampaign,$prefix,$table,$fax_data = null)
    {
        if(isset($fax_data[0][$prefix.'_doc_fax']))
        {
            $date = date('M-Y');
            if($prefix == "faxpdf")
            {
                $pathPDF = "";
                if($fax_data[0][$prefix.'_path_old_new']==0)
                {
                    $pathPDF = base_url()."assets/pdf/".$fax_data[0][$prefix.'_file'] ;
                }
                else if($fax_data[0][$prefix.'_path_old_new']==1)
                {
                    $pathPDF = base_url()."assets/pdf-new/".$fax_data[0][$prefix.'_path']."/".$fax_data[0][$prefix.'_file'];
                }
                $pdftext = file_get_contents($pathPDF);
                $num_pag = preg_match_all("/\/Page\W/", $pdftext);
                if($num_pag > $this->currentLimit)
                {
                    return 0;
                }
            }
        }
        else
        {
            return 0;
        }
        
        if($prefix=="cgmdiab")
        {
            $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
            $fax_back = $prefix."_fax_back";
            $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
            $dompdf = new $this->dompdf();
            $html = $this->output->get_output();
            $fax_page = $this->load->view('admin/templates/cgmdiab/fax_temp_cgmdiab',$data,true);
            $dompdf->loadHtml($fax_page);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();
           
            if (!is_dir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'))) 
            {
                mkdir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'), 0700, TRUE);
            }
            file_put_contents('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf",  $output);
            $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
            $num_pag = preg_match_all("/\/Page\W/", $pdftext);
            if($num_pag > $this->currentLimit)
            {
                return 0;
            }
        }
        else if($prefix=="mask")
        {
            $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
            $fax_back = $prefix."_fax_back";
            $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
            $dompdf = new $this->dompdf();
            $html = $this->output->get_output();
            $fax_page = $this->load->view('admin/templates/mask/'.$this->LayoutType,$data,true);
            $dompdf->loadHtml($fax_page);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();
            if (!is_dir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'))) 
            {
                mkdir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'), 0700, TRUE);
            }
            file_put_contents('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf",  $output);
            $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
            $num_pag = preg_match_all("/\/Page\W/", $pdftext);
            if($num_pag > $this->currentLimit)
            {
                return 0;
            }
        }
        else if($prefix=="heatpad")
        {
            $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
            $fax_back = $prefix."_fax_back";
            $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
            $dompdf = new $this->dompdf();
            $html = $this->output->get_output();
            $fax_page = $this->load->view('admin/templates/heatpad/fax_temp_heatpad',$data,true);
            $dompdf->loadHtml($fax_page);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();
            if (!is_dir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'))) 
            {
                mkdir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'), 0700, TRUE);
            }
            file_put_contents('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf",  $output);
            $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
            $num_pag = preg_match_all("/\/Page\W/", $pdftext);
            if($num_pag > $this->currentLimit)
            {
                return 0;
            }
        }
        else if($prefix=="hipaa")
        {
            $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
            $fax_back = $prefix."_fax_back";
            $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
            $dompdf = new $this->dompdf();
            $html = $this->output->get_output();
            $fax_page = $this->load->view('admin/templates/hipaa/'.$this->LayoutType,$data,true);
            $dompdf->loadHtml($fax_page);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $output = $dompdf->output();
            if (!is_dir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'))) 
            {
                mkdir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'), 0700, TRUE);
            }
            file_put_contents('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf",  $output);
            $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf");
            $num_pag = preg_match_all("/\/Page\W/", $pdftext);
            if($num_pag > $this->currentLimit)
            {
                return 0;
            }
        }
        else if($prefix=="aurbrac")
        {
            $aur = $this->Admin_model->get_aurbrac_data_for_fax($IDcampaign,$prefix);
            $product = "";

            if($aur[0]['aurbrac_back'] == 1) { $product = "back";  return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }

            if($aur[0]['aurbrac_l_knee'] == 1 && $aur[0]['aurbrac_r_knee'] == 1) { $product = "knee"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            else
            {
                if($aur[0]['aurbrac_l_knee'] == 1) { $product = "left_knee"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
                if($aur[0]['aurbrac_r_knee'] == 1) { $product = "right_knee"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            }

            if($aur[0]['aurbrac_l_wrist'] == 1 && $aur[0]['aurbrac_r_wrist'] == 1) { $product = "wrist"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            else
            {
                if($aur[0]['aurbrac_l_wrist'] == 1) { $product = "left_wrist"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
                if($aur[0]['aurbrac_r_wrist'] == 1) { $product = "right_wrist"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            }

            if($aur[0]['aurbrac_l_ankle'] == 1 && $aur[0]['aurbrac_r_ankle'] == 1) { $product = "ankle"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            else
            {
                if($aur[0]['aurbrac_l_ankle'] == 1) { $product = "left_ankle"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
                if($aur[0]['aurbrac_r_ankle'] == 1) { $product = "right_ankle"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            }

            if($aur[0]['aurbrac_l_hip'] == 1 && $aur[0]['aurbrac_r_hip'] == 1) { $product = "hip"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            else
            {
                if($aur[0]['aurbrac_l_hip'] == 1) { $product = "left_hip"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
                if($aur[0]['aurbrac_r_hip'] == 1) { $product = "right_hip"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            }

            if($aur[0]['aurbrac_l_shoulder'] == 1 ) { $product = "left_shoulder"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            if($aur[0]['aurbrac_r_shoulder'] == 1 ) { $product = "right_shoulder"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            if($aur[0]['aurbrac_l_elbow'] == 1 ) { $product = "left_elbow"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            if($aur[0]['aurbrac_r_elbow'] == 1 ) { $product = "right_elbow"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }
            if($aur[0]['aurbrac_neck'] == 1 ) { $product = "neck"; return  $this->aurbrac_save_pdf($IDcampaign,$prefix,$table,$product); }


        }

    }

    public function aurbrac_save_pdf($IDcampaign,$prefix,$table,$product)
    {
        $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
        $fax_back = $prefix."_fax_back";
        $data['fax'][0]->$fax_back = (empty($data['fax'][0]->$fax_back)) ? $this->session->userdata('sent_num') : $data['fax'][0]->$fax_back;
        $dompdf = new $this->dompdf();
        $html = $this->output->get_output();
        $fax_page = $this->load->view('admin/templates/aurbrac/fax_temp_'.$product.'_aurbrac',$data,true);
        $dompdf->loadHtml($fax_page);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        if (!is_dir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'))) 
        {
            mkdir('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id'), 0700, TRUE);
        }
        file_put_contents('assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf",  $output);
        $pdftext = file_get_contents(base_url().'assets/documents/faxes'.$this->security_id.'/'.$prefix.'/user_'.$this->session->userdata('id')."/fax_of_sending_".$product."_".$prefix."_".$IDcampaign.".pdf");
        $num_pag = preg_match_all("/\/Page\W/", $pdftext);
        if($num_pag > $this->currentLimit)
        {
            return 0;
        }
        else
        {
            return "";
        }
    }


    public function uploadFaxpdf()
    {
        $pdftext = file_get_contents($_FILES["file"]["tmp_name"]);
        $num_pag = preg_match_all("/\/Page\W/", $pdftext);
        if($num_pag > 5)
        {
            $this->session->set_flashdata('devError','<div class="alert alert-danger" role="alert" >Do not open developer tools!!!</div>');
            redirect("admin/List_controller/campaign_list/faxpdf/Cleaning");
        }
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
        $date = date('M-Y');
        if (!is_dir('assets/pdf-new/user_'.$this->session->userdata('id').'/'.$date)) 
        {
            mkdir('assets/pdf-new/user_'.$this->session->userdata('id').'/'.$date, 0700, TRUE);
        }
        $fileName = substr(basename($_FILES["file"]["name"]), 0, -4);
        $extensionType = substr(basename($_FILES["file"]["name"]), -4);
        if($extensionType==".pdf")
        {
            if(preg_match('/^[[:alnum:]]+$/', $fileName) === 0)
            {
                $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Use Alphabets and Numbers only without space for the File Name.</div>');
                redirect('admin/List_controller/campaign_list/faxpdf/Cleaning');
            }
            
            $target_dir = 'assets/pdf-new/user_'.$this->session->userdata('id').'/'.$date.'/';
            $imgName = $randomString."-".$this->session->userdata("creator_id")."-".session_id()."-".time().'_'.$this->session->userdata('id').basename($_FILES["file"]["name"]);
            $target_file = $target_dir.$imgName;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    
            move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
    
            $data = array (
                'faxpdf_doc_fax' => $this->input->post('faxpdf_doc_number'),
                'faxpdf_file' => $imgName,
                'faxpdf_no_of_pages' => $this->input->post("totalPages"),
                'faxpdf_path' => 'user_'.$this->session->userdata('id').'/'.$date,
                'faxpdf_path_old_new' => 1,
                'userID' => $this->session->userdata('id'),
                'agentID' => $this->session->userdata('creator_id'),
    
                'faxpdf_data_entered' 	  => date("d-m-Y"),
            );
            
            $this->db->trans_start();
            $this->db->insert('t_faxpdf',$data);
        	$this->db->trans_complete();
        }
        else
        {
            $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert" >Only PDF files are supported.</div>');
            redirect('admin/List_controller/campaign_list/faxpdf/Cleaning');            
        }

        redirect('admin/List_controller/campaign_list/faxpdf/Cleaning');


    }


    public function update_data($IDclient)
    {
        $data = $this->input->post();
        $this->Admin_model->update_client_data($data,$IDclient);
        $this->save_html_to_pdf($IDclient);
        redirect("admin/User/data_list");
    }

    public function edit_client_data($IDclient)
    {
        $data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/client_edit',$data);
    }

    public function show_client_data($IDclient)
    {
        $data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('user/client_details',$data);
    }

    public function campaign_save_html_to_pdf($IDcampaign,$prefix,$table)
    {
        //dump($this->dompdf);
        $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
        $this->load->view("admin/templates/$prefix/fax_temp_$prefix",$data);
        $dd  = $this->output->get_output();
        $this->dompdf->load_html($dd);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output =  $this->dompdf->output();

        file_put_contents("assets/documents/faxes".$this->security_id."/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", $output);



    }

    public function campaign_write_fax($IDcampaign,$prefix,$table_name)
    {
        $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table_name);
        $this->load->view("admin/fax_temp_$prefix",$data);
    }

    public function write_fax($id)
    {
        $data['fax'] = $this->Admin_model->get_fax($id);
        $this->load->view('admin/fax_template',$data);
    }



    public function campaign_data_entry($prefix)
    {
        $this->load->view('include/head');
        $data['prefix'] = $prefix;
        $this->load->view("User/data/".$prefix."_data_entry",$data);
    }

    public function campaign_edit_data($IDcampaign,$prefix)
    {
        $this->load->view('include/head');
        $data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
        $check_authen = $this->Admin_model->check_authencity($IDcampaign,$prefix);
        if($check_authen==1)
        {
            $data['IDcampaign'] = $IDcampaign;
            $data['prefix'] = $prefix;
            $this->load->view("User/data/".$prefix."_data_edit",$data);
        }
        else if($check_authen==0)
        {
            $this->load->view('include/head');
            $this->load->view('include/nav');
            $this->load->view("User/error");
        }
    }

    public function campaign_show_data($IDcampaign,$prefix)
    {
        $data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
        $data['IDcampaign'] = $IDcampaign;
        $data['prefix'] = $prefix;
        $this->load->view("User/data/".$prefix."_data_show",$data);
    }

    public function campaign_save_data($prefix)
    {

        $table = "";
        if($prefix == "aurbrac")
        {
            $table = "t_aurtho_brace";
        }
        else if($prefix == "cgmdiab")
        {
            $table = "t_cgm_diabetic";
        }
        $data = $this->input->post();
        $IDcampaign = $this->Admin_model->campaign_save_data($data,$prefix);


        $this->session->set_flashdata("data_saved","Data saved successfully");
        redirect($_SERVER['HTTP_REFERER']);
    }



    public function campaign_update_data($IDcampaign,$prefix)
    {
        $table = "";
        if($prefix == "aurbrac")
        {
            $table = "t_aurtho_brace";
        }
        else if($prefix = "cgmdiab")
        {
            $table = "t_cgm_diabetic";
        }
        $check_authen = $this->Admin_model->check_authencity($IDcampaign,$prefix);
        if($check_authen==1)
        {
            $data = $this->input->post();
            $IDcampaign = $this->Admin_model->campaign_update_data($data,$IDcampaign,$prefix);
            $this->session->set_flashdata("data_saved","Data edited successfully");
            redirect($_SERVER['HTTP_REFERER']);
        }
        else if($check_authen==0)
        {
            $this->load->view('include/head');
            $this->load->view('include/nav');
            $this->load->view("User/error");
        }
    }




    public function fetch_camp_data($prefix,$type){



        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'aurbrac_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');

        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'cgmdiab_medicare');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'cgmdiab_medicare');

        }



        $data = array();
        if($prefix=='aurbrac')
        {
            foreach ($callData as $row) {
                $callData = array();
//                $status = "";
//                if ($row->aurbrac_status == 'Good') {
//                    $status = "<span class='btn btn-success'>GOOD</span>";
//                } else if ($row->aurbrac_status == 'Deleted') {
//                    $status = "<span class='btn btn-danger'>DELETED</span>";
//                } else if ($row->aurbrac_status == 'Rejected') {
//                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->aurbrac_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->aurbrac_status == 'Sent') {
//                    $status = "<span class='btn btn-info' >SENT</span>";
//                } else if ($row->aurbrac_status == 'Completed') {
//                    $status = "<span class='btn btn-primary'  >Completed</span>";
//                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDaurbrac . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->aurbrac_reason);
                $callData[] = $row->aurbrac_firstname;
                $callData[] = $row->aurbrac_lastname;
                $callData[] = $row->aurbrac_address;
                $callData[] = $row->aurbrac_city;
                $callData[] = $row->aurbrac_state;
                $callData[] = $row->aurbrac_zip;
//                $callData[] = $row->aurbrac_email;
                $callData[] = $row->aurbrac_dob;

                $callData[] = (strpos($row->aurbrac_phone, '+1') !== false) ? $row->aurbrac_phone : '+1' . $row->aurbrac_phone;
                $callData[] = $row->aurbrac_gender;
                $callData[] = $row->aurbrac_back;
                $callData[] = ($row->aurbrac_l_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_hip == 1 ? 'Yes' : 'No');
                $callData[] = $row->aurbrac_doc_firstname;
                $callData[] = $row->aurbrac_doc_lastname;
                $callData[] = $row->aurbrac_doc_address;
                $callData[] = $row->aurbrac_doc_city;
                $callData[] = $row->aurbrac_doc_state;
                $callData[] = $row->aurbrac_doc_zip;
                $callData[] = $row->aurbrac_doc_npi;
                $callData[] = (strpos($row->aurbrac_doc_phone, '+1') !== false) ? $row->aurbrac_doc_phone : '+1' . $row->aurbrac_doc_phone;
                $callData[] = (strpos($row->aurbrac_doc_fax, '+1') !== false) ? $row->aurbrac_doc_fax : '+1' . $row->aurbrac_doc_fax;
                $callData[] = $row->aurbrac_pcp_within_past;
                $callData[] = $row->aurbrac_med_id;
                $callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
                $callData[] = $row->aurbrac_data_entered;
                $callData[] = $row->aurbrac_insurance_company;
                $callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
                $callData[] = $row->aurbrac_data_entered;
//                $callData[] = $row->aurbrac_IDtrack;
//                $callData[] = $row->aurbrac_IDsite;
                $callData[] = $row->aurbrac_repcode;
                $callData[] = $row->aurbrac_record_file;
                $callData[] = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDaurbrac) . '/aurbrac" >Edit</a>';
                $data[] = $callData;
            }
        }
        else if($prefix=='cgmdiab') {

            foreach ($callData as $row) {
                $callData = array();
//                $status = "";
//                if ($row->cgmdiab_status == 'Good') {
//                    $status = "<span class='btn btn-success'>GOOD</span>";
//                } else if ($row->cgmdiab_status == 'Deleted') {
//                    $status = "<span class='btn btn-danger'>DELETED</span>";
//                } else if ($row->cgmdiab_status == 'Rejected') {
//                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->cgmdiab_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->cgmdiab_status == 'Sent') {
//                    $status = "<span class='btn btn-info' >SENT</span>";
//                } else if ($row->cgmdiab_status == 'Completed') {
//                    $status = "<span class='btn btn-primary'  >Completed</span>";
//                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDcgmdiab . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->cgmdiab_reason);
                $callData[] = $row->cgmdiab_firstname;
                $callData[] = $row->cgmdiab_lastname;
                $callData[] = $row->cgmdiab_address;
                $callData[] = $row->cgmdiab_city;
                $callData[] = $row->cgmdiab_state;
                $callData[] = $row->cgmdiab_zip;
//                $callData[] = $row->cgmdiab_email;
                $callData[] = $row->cgmdiab_dob;

                $callData[] = (strpos($row->cgmdiab_phone, '+1') !== false) ? $row->cgmdiab_phone : '+1' . $row->cgmdiab_phone;
                $callData[] = $row->cgmdiab_sex;
                $callData[] = $row->cgmdiab_ssn;
                $callData[] = $row->cgmdiab_diabetes;
                $callData[] = $row->cgmdiab_visit_doc;
                $callData[] = $row->cgmdiab_insulin_pum;
                $callData[] = $row->cgmdiab_blood_sugar;
                $callData[] = $row->cgmdiab_doc_firstname;
                $callData[] = $row->cgmdiab_doc_lastname;
                $callData[] = $row->cgmdiab_doc_address;
                $callData[] = $row->cgmdiab_doc_city;
                $callData[] = $row->cgmdiab_doc_state;
                $callData[] = $row->cgmdiab_doc_zip;
                $callData[] = $row->cgmdiab_doc_npi;
                $callData[] = (strpos($row->cgmdiab_doc_phone, '+1') !== false) ? $row->cgmdiab_doc_phone : '+1' . $row->cgmdiab_doc_phone;
                $callData[] = (strpos($row->cgmdiab_doc_fax, '+1') !== false) ? $row->cgmdiab_doc_fax : '+1' . $row->cgmdiab_doc_fax;
                $callData[] = $row->cgmdiab_patient_ack;
                $callData[] = $row->cgmdiab_finan_aggrem;
                $callData[] = $row->cgmdiab_contact_me;
                $callData[] = $row->cgmdiab_coinsurance;
                $callData[] = $row->cgmdiab_medicare;
                $callData[] = $row->cgmdiab_insurance_com;
                $callData[] = $row->cgmdiab_IDmember;
//                $callData[] = $row->cgmdiab_IDgroup;
                $callData[] = $row->cgmdiab_date_entered;
//                $callData[] = $row->cgmdiab_IDtrack;
//                $callData[] = $row->cgmdiab_IDsite;
                $callData[] = $row->cgmdiab_repcode;
                $callData[] = $row->cgmdiab_patient_url;
                $callData[] = $row->cgmdiab_recording;
                $callData[] = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDcgmdiab) . '/cgmdiab" >Edit</a>
									<div class="dropdown" >
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Change Status
                                            </button>
                                        <div class="dropdown-menu">
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>                                    
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING ICD 10">MISSING ICD 10</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
                                    </div>
                                    </div>';
                $data[] = $callData;
            }
        }

        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);

    }


}

