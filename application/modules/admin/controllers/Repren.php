<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repren extends My_controller {

	function __construct(){
		parent::__construct();
		$role = $this->session->userdata("role");
		if($role!="Representative")
		{
			redirect("admin/Dashboard");
		}
		$this->load->library('upload');
	    $this->load->library('pdf');
		$this->load->model("Admin_model");
		}

	public function data_entry()
	{
		$this->load->view('repren/client_entry');
	}

	public function save_data()
	{
	  $data = $this->input->post();
	  $IDclient = $this->Admin_model->save_client_data($data);
	  $this->save_html_to_pdf($IDclient);
	  redirect("admin/Repren/data_list");
	}


	public function data_list()
	{
		$IDrepresentative = $this->session->userdata("id");
		$data['client_data'] = $this->Admin_model->get_client_data_for_repren($IDrepresentative);
		$this->load->view('repren/client_data',$data);
	}

	

	public function show_client_data($IDclient)
	{
		$data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
		$this->load->view('repren/client_details',$data);
	}

	public function save_html_to_pdf($IDclient)
	{
	    $this->write_fax($IDclient);
        $html = $this->output->get_output();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();
   		file_put_contents("assets/documents/faxes/fax_of_sending$IDclient.pdf", $output);
	}
	
	public function write_fax($id)
	{
	    	$data['fax'] = $this->Admin_model->get_fax($id);
			$this->load->view('admin/fax_template',$data);
	}

}

