<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cvs_controller extends CI_Controller {

	public function __construct(){

		parent::__construct();

		// load base_url
		$this->load->helper('url');

		// Load Model
		$this->load->model('Cvs_model');
		passwordAuthentication();
	}
	public function mask(){

		// Check form submit or not
		if($this->input->post('upload') != NULL ){
			$data = array();
			if(!empty($_FILES['file']['name'])){
				// Set preference
				$config['upload_path'] = 'assets/files/';
				$config['allowed_types'] = 'csv';
				$config['max_size'] = '1000'; // max_size in kb
				$config['file_name'] = $_FILES['file']['name'];

				// Load upload library
				$this->load->library('upload',$config);

				// File upload
				if($this->upload->do_upload('file')){
					// Get data about the file
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];

					// Reading file
					$file = fopen("assets/files/".$filename,"r");
					$i = 0;

					$importData_arr = array();

					while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
						$num = count($filedata);

						for ($c=0; $c < $num; $c++) {
							$importData_arr[$i][] = $filedata[$c];
						}
						$i++;
					}
					fclose($file);

					$skip = 0;

					// insert import data
					foreach($importData_arr as $userdata){
						if($skip != 0){
							$this->Cvs_model->mask($userdata);
						}
						$skip ++;
					}
					$data['response'] = 'successfully uploaded '.$filename;
				}else{
					$data['response'] = 'failed';
				}
			}else{
				$data['response'] = 'failed';
			}
			// load view
			redirect(base_url('admin/List_controller/campaign_list/mask/Cleaning'));

		}else{
			// load view
			echo 'sorry';
			//  redirect(base_url('admin/User/campaign_list/brace/Cleaning'));
		}

	}
	public function heatpad(){

		// Check form submit or not
		if($this->input->post('upload') != NULL ){
			$data = array();
			if(!empty($_FILES['file']['name'])){
				// Set preference
				$config['upload_path'] = 'assets/files/';
				$config['allowed_types'] = 'csv';
				$config['max_size'] = '1000'; // max_size in kb
				$config['file_name'] = $_FILES['file']['name'];

				// Load upload library
				$this->load->library('upload',$config);

				// File upload
				if($this->upload->do_upload('file')){
					// Get data about the file
					$uploadData = $this->upload->data();
					$filename = $uploadData['file_name'];

					// Reading file
					$file = fopen("assets/files/".$filename,"r");
					$i = 0;

					$importData_arr = array();

					while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
						$num = count($filedata);

						for ($c=0; $c < $num; $c++) {
							$importData_arr[$i][] = $filedata[$c];
						}
						$i++;
					}
					fclose($file);

					$skip = 0;

					// insert import data
					foreach($importData_arr as $userdata){
						if($skip != 0){
							$this->Cvs_model->heatpad($userdata);
						}
						$skip ++;
					}
					$data['response'] = 'successfully uploaded '.$filename;
				}else{
					$data['response'] = 'failed';
				}
			}else{
				$data['response'] = 'failed';
			}
			// load view
			redirect(base_url('admin/List_controller/campaign_list/heatpad/Cleaning'));

		}else{
			// load view
			echo 'sorry';
			//  redirect(base_url('admin/User/campaign_list/brace/Cleaning'));
		}

	}
    public function hipaa(){

        // Check form submit or not
        if($this->input->post('upload') != NULL ){
            $data = array();
            if(!empty($_FILES['file']['name'])){
                // Set preference
                $config['upload_path'] = 'assets/files/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '1000'; // max_size in kb
                $config['file_name'] = $_FILES['file']['name'];

                // Load upload library
                $this->load->library('upload',$config);

                // File upload
                if($this->upload->do_upload('file')){
                    // Get data about the file
                    $uploadData = $this->upload->data();
                    $filename = $uploadData['file_name'];

                    // Reading file
                    $file = fopen("assets/files/".$filename,"r");
                    $i = 0;

                    $importData_arr = array();

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);

                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach($importData_arr as $userdata){
                        if($skip != 0){
                            $this->Cvs_model->hipaa($userdata);
                        }
                        $skip ++;
                    }
                    $data['response'] = 'successfully uploaded '.$filename;
                }else{
                    $data['response'] = 'failed';
                }
            }else{
                $data['response'] = 'failed';
            }
            // load view
            redirect(base_url('admin/List_controller/campaign_list/hipaa/Cleaning'));

        }else{
            // load view
            echo 'sorry';
            //  redirect(base_url('admin/User/campaign_list/brace/Cleaning'));
        }

    }


    public function aurbrac(){

		// Check form submit or not
 		if($this->input->post('upload') != NULL ){
   			$data = array();
   			if(!empty($_FILES['file']['name'])){
    			// Set preference
    			$config['upload_path'] = 'assets/files/';
    			$config['allowed_types'] = 'csv';
    			$config['max_size'] = '1000'; // max_size in kb
    			$config['file_name'] = $_FILES['file']['name'];

    			// Load upload library
    			$this->load->library('upload',$config);

    			// File upload
    			if($this->upload->do_upload('file')){
     				// Get data about the file
     				$uploadData = $this->upload->data();
     				$filename = $uploadData['file_name'];

     				// Reading file
                    $file = fopen("assets/files/".$filename,"r");
                    $i = 0;

                    $importData_arr = array();

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);

                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach($importData_arr as $userdata){
                        if($skip != 0){
                            $this->Cvs_model->brace($userdata);
                        }
                        $skip ++;
                    }
     				$data['response'] = 'successfully uploaded '.$filename;
    			}else{
     				$data['response'] = 'failed';
    			}
   			}else{
    			$data['response'] = 'failed';
   			}
   			// load view
   			        redirect(base_url('admin/List_controller/campaign_list/aurbrac/Cleaning'));

  		}else{
   			// load view
   			echo 'sorry';
        //  redirect(base_url('admin/User/campaign_list/brace/Cleaning'));
  		}

	}
	public function cgmdiab(){


		// Check form submit or not
 		if($this->input->post('upload') != NULL ){
   			$data = array();
   			if(!empty($_FILES['file']['name'])){
    			// Set preference
    			$config['upload_path'] = 'assets/files/';
    			$config['allowed_types'] = 'csv';
    			$config['max_size'] = '1000'; // max_size in kb
    			$config['file_name'] = $_FILES['file']['name'];

    			// Load upload library
    			$this->load->library('upload',$config);

    			// File upload
    			if($this->upload->do_upload('file')){
     				// Get data about the file
     				$uploadData = $this->upload->data();
     				$filename = $uploadData['file_name'];

     				// Reading file
                    $file = fopen("assets/files/".$filename,"r");
                    $i = 0;

                    $importData_arr = array();

                    while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                        $num = count($filedata);

                        for ($c=0; $c < $num; $c++) {
                            $importData_arr[$i][] = $filedata[$c];
                        }
                        $i++;
                    }
                    fclose($file);

                    $skip = 0;

                    // insert import data
                    foreach($importData_arr as $userdata){
                        if($skip != 0){
                            $this->Cvs_model->cgm($userdata);
                        }
                        $skip ++;
                    }
     				$data['response'] = 'successfully uploaded '.$filename;
    			}else{
     				$data['response'] = 'failed';
    			}
   			}else{
    			$data['response'] = 'failed';
   			}
   			// load view
   			        redirect(base_url('/admin/List_controller/campaign_list/cgmdiab/Cleaning'));

  		}else{
   			// load view
   			echo 'sorry';
        //  redirect(base_url('admin/User/campaign_list/brace/Cleaning'));
  		}

	}

}
