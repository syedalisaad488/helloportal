<?php
//defined('BASEPATH') or exit('No direct script access allowed');
//
//use Twilio\Rest\Client;
//
//class CroneJob extends My_controller
//{
//
//
//    function __construct()
//    {
//        parent::__construct();
//        $this->load->model("Admin_model");
//    }
//
//
//
//
//    public function charge_back()
//    {
//        $array = array('t_mask','t_faxpdf', 't_heatpad', 't_hipaa',  't_cgm_diabetic', 't_aurtho_brace');
//        for ($i = 0; $i < count($array); $i++) {
//            if ($array[$i] == 't_faxpdf') {
//                $prefix = '';
//                $prefix = 'faxpdf';
//            } else if ($array[$i] == 't_heatpad') {
//                $prefix = 'heatpad';
//
//            } else if ($array[$i] == 't_hipaa') {
//                $prefix = '';
//                $prefix = 'hipaa';
//            } else if ($array[$i] == 't_mask') {
//                $prefix = 'mask';
//            } else if ($array[$i] == 't_cgm_diabetic') {
//                $prefix = 'cgmdiab';
//            } else {
//                $prefix = 'aurbrac';
//            }
//            $pages = $prefix.'_no_of_pages';
//            $table_id = 'ID'.$prefix;
//
//
//            $this->db->select("$table_id,userID,agentID,charge_back,$pages");
//            $this->db->where('sent_repeat >', 3);
//            $this->db->where('charge_back', 2);
//            $query = $this->db->get($array[$i])->result();
//            foreach ($query as $key) {
//                $sendID = $key->agentID != 0 ? $key->agentID : $key->userID;
//
//
//                $this->db->select('send_dynamic');
//                $this->db->where('sendID', $sendID);
//                $this->db->where('send_month', date('M-Y'));
//                $query1 = $this->db->get("t_limit_send")->first_row();
//
//
//                $this->db->trans_start();
//                $this->db->where('sendID', $sendID);
//                $this->db->where('send_month', date('M-Y'));
//                $updateData = array("send_dynamic" => $query1->send_dynamic - $key->$pages);
//                $this->db->update("t_limit_send", $updateData);
//                $this->db->trans_complete();
//
//                $this->db->select('userlimit_send_dynamic,userID,userlimit_month');
//                $this->db->where('userID', $key->userID);
//                $this->db->where('userlimit_month', date('M-Y'));
//                $query2 = $this->db->get("t_user_limit")->first_row();
//
//
//                $this->db->trans_start();
//                $this->db->where('userID', $key->userID);
//                $this->db->where('userlimit_month', date('M-Y'));
//                $updateData = array("userlimit_send_dynamic" => $query2->userlimit_send_dynamic - $key->$pages);
//                $this->db->update("t_user_limit", $updateData);
//                $this->db->trans_complete();
//                $this->db->trans_start();
//                $this->db->where($table_id, $key->$table_id);
//                $updateData = array("charge_back" =>  1,$prefix."_status"=>"Not_sent");
//                $this->db->update($array[$i] , $updateData);
//                $this->db->trans_complete();
//
//                $this->db->where('pgquan_date', date('M-Y'));
//                $query3 = $this->db->get("t_page_quantity")->first_row();
//
//
//                $this->db->trans_start();
//                $this->db->where('pgquan_date', date('M-Y'));
//                $updateData = array("pgquan_send_quan_dynamic" => $query3->pgquan_send_quan_dynamic - $key->$pages);
//                $this->db->update("t_page_quantity", $updateData);
//                $this->db->trans_complete();
//            }
//
//
//        }
//    }
//
//    public function faxpdfChangeStatus()
//    {
//        set_time_limit(10);
//        $this->db->select("IDfaxpdf,IDfax,faxpdf_status,twilio_status,sent_repeat,check_queue");
//        $this->db->where("faxpdf_status", "Sent");
//        $this->db->where("twilio_status !=", "delivered");
//        $this->db->order_by("check_queue", "asc");
//        $this->db->limit(20);
//        $query = $this->db->get("t_faxpdf")->result();
//
//        foreach ($query as $key) {
//
//            $curl = curl_init();
//
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => 'https://api.documo.com/v1/fax/'.$key->IDfax.'/info',
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => '',
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 0,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => 'GET',
//                CURLOPT_HTTPHEADER => array(
//                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
//                ),
//            ));
//
//            $fax = curl_exec($curl);
//            curl_close($curl);
//            $fax = json_decode($fax);
//
//
//
//
//            $this->db->trans_start();
//            $this->db->where("IDfaxpdf", $key->IDfaxpdf);
//            $faxStatus = $fax->status == "success" ? 'delivered' : $fax->status  ;
//            $updateData = array("twilio_status" => $faxStatus,
//                "check_queue" => $key->check_queue + 1,
//                "faxpdf_status" => $key->sent_repeat > 3 || $fax->status == "failed" ? "Not_sent" : "Sent");
//            $this->db->update("t_faxpdf", $updateData);
//            $this->db->trans_complete();
//        }
//    }
//
//    public function maskChangeStatus()
//    {
//        set_time_limit(10);
//        $this->db->select("IDmask,IDfax,mask_status,twilio_status,sent_repeat,check_queue");
//
//        $this->db->where("mask_status", "Sent");
//        $this->db->where("twilio_status !=", "delivered");
//        $this->db->order_by("check_queue", "asc");
//        $this->db->limit(20);
//        $query = $this->db->get("t_mask")->result();
//
//
//        foreach ($query as $key) {
//
//            $curl = curl_init();
//
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => 'https://api.documo.com/v1/fax/' . $key->IDfax . '/info',
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => '',
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 0,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => 'GET',
//                CURLOPT_HTTPHEADER => array(
//                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
//                ),
//            ));
//
//            $fax = curl_exec($curl);
//            curl_close($curl);
//            $fax = json_decode($fax);
//
//            $this->db->trans_start();
//            $this->db->where("IDmask", $key->IDmask);
//            $faxStatus = $fax->status == "success" ? 'delivered' : $fax->status;
//            $updateData = array("twilio_status" => $faxStatus,
//                "check_queue" => $key->check_queue + 1,
//                "mask_status" => $key->sent_repeat > 3 || $fax->status == "failed" ? "Not_sent" : "Sent");
//            $this->db->update("t_mask", $updateData);
//            $this->db->trans_complete();
//        }
//    }
//
//    public function heatpadChangeStatus()
//    {
//        set_time_limit(10);
//        $this->db->select("IDheatpad,IDfax,heatpad_status,twilio_status,sent_repeat,check_queue");
//
//        $this->db->where("heatpad_status", "Sent");
//        $this->db->where("twilio_status !=", "delivered");
//        $this->db->order_by("check_queue", "asc");
//        $this->db->limit(20);
//        $query = $this->db->get("t_heatpad")->result();
//
//
//        foreach ($query as $key) {
//
//            $curl = curl_init();
//
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => 'https://api.documo.com/v1/fax/' . $key->IDfax . '/info',
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => '',
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 0,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => 'GET',
//                CURLOPT_HTTPHEADER => array(
//                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
//                ),
//            ));
//
//            $fax = curl_exec($curl);
//            curl_close($curl);
//            $fax = json_decode($fax);
//
//            $this->db->trans_start();
//            $this->db->where("IDheatpad", $key->IDheatpad);
//            $faxStatus = $fax->status == "success" ? 'delivered' : $fax->status;
//            $updateData = array("twilio_status" => $faxStatus,
//                "check_queue" => $key->check_queue + 1,
//                "heatpad_status" => $key->sent_repeat > 3 || $fax->status == "failed" ? "Not_sent" : "Sent");
//            $this->db->update("t_heatpad", $updateData);
//            $this->db->trans_complete();
//        }
//    }
//
//    public function hipaaChangeStatus()
//    {
//        set_time_limit(10);
//        $this->db->select("IDhipaa,IDfax,hipaa_status,twilio_status,sent_repeat,check_queue");
//
//        $this->db->where("hipaa_status", "Sent");
//        $this->db->where("twilio_status !=", "delivered");
//        $this->db->order_by("check_queue", "asc");
//        $this->db->limit(20);
//        $query = $this->db->get("t_hipaa")->result();
//
//
//
//        foreach ($query as $key) {
//
//            $curl = curl_init();
//
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => 'https://api.documo.com/v1/fax/' . $key->IDfax . '/info',
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => '',
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 0,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => 'GET',
//                CURLOPT_HTTPHEADER => array(
//                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
//                ),
//            ));
//
//            $fax = curl_exec($curl);
//            curl_close($curl);
//            $fax = json_decode($fax);
//
//            $this->db->trans_start();
//            $this->db->where("IDhipaa", $key->IDhipaa);
//            $faxStatus = $fax->status == "success" ? 'delivered' : $fax->status;
//            $updateData = array("twilio_status" => $faxStatus,
//                "check_queue" => $key->check_queue + 1,
//                "hipaa_status" => $key->sent_repeat > 3 || $fax->status == "failed" ? "Not_sent" : "Sent");
//            $this->db->update("t_hipaa", $updateData);
//            $this->db->trans_complete();
//        }
//    }
//
//    public function cgmdiabChangeStatus()
//    {
//        set_time_limit(10);
//        $this->db->select("IDcgmdiab,IDfax,cgmdiab_status,twilio_status,sent_repeat,check_queue");
//
//        $this->db->where("cgmdiab_status", "Sent");
//        $this->db->where("twilio_status !=", "delivered");
//        $this->db->order_by("check_queue", "asc");
//        $this->db->limit(20);
//        $query = $this->db->get("t_cgm_diabetic")->result();
//
//
//        foreach ($query as $key) {
//
//            $curl = curl_init();
//
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => 'https://api.documo.com/v1/fax/' . $key->IDfax . '/info',
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => '',
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 0,
//                CURLOPT_FOLLOWLOCATION => true,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => 'GET',
//                CURLOPT_HTTPHEADER => array(
//                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0'
//                ),
//            ));
//
//            $fax = curl_exec($curl);
//            curl_close($curl);
//            $fax = json_decode($fax);
//
//            $this->db->trans_start();
//            $this->db->where("IDcgmdiab", $key->IDcgmdiab);
//            $faxStatus = $fax->status == "success" ? 'delivered' : $fax->status;
//            $updateData = array("twilio_status" => $faxStatus,
//                "check_queue" => $key->check_queue + 1,
//                "cgmdiab_status" => $key->sent_repeat > 3 || $fax->status == "failed" ? "Not_sent" : "Sent");
//            $this->db->update("t_cgm_diabetic", $updateData);
//            $this->db->trans_complete();
//        }
//    }
//
////    public function aurbracChangeStatus()
////    {
////        $sid = "AC720fa9d520db09505609c091b9152f7f";
////        $token = "2e11a5a6e537609375d68fa63d869bb3";
////        $security_id = "2486e9b3c97e";
////        require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
////
////        $this->db->select("IDaurbrac,IDfax,aurbrac_status,twilio_status,sent_repeat,check_queue");
////        $this->db->where("aurbrac_status", "Sent");
////        $this->db->group_start();
////        $this->db->where("twilio_status", "queued");
////        $this->db->Or_where("twilio_status", "processing");
////        $this->db->Or_where("twilio_status", "sending");
////        $this->db->Or_where("twilio_status", "receiving");
////        $this->db->Or_where("twilio_status", "received");
////
////        $this->db->group_end();
////        $this->db->order_by("check_queue", "asc");
////        $query = $this->db->get("t_aurtho_brace")->result();
////
////        foreach ($query as $key) {
////            $twilio = new Client($sid, $token);
////            $fax = $twilio->fax->v1->faxes($key->IDfax)->fetch();
////
////            $this->db->trans_start();
////            $this->db->where("IDaurbrac", $key->IDaurbrac);
////            $updateData = array("twilio_status" => $fax->status,
////                "check_queue" => $key->check_queue + 1,
////                "aurbrac_status" => $key->sent_repeat > 3 ? "Not_sent" : "Sent");
////            $this->db->update("t_aurbrac", $updateData);
////            $this->db->trans_complete();
////        }
////    }
////
////
////    public function cgmdiabResendFax()
////    {
////
////        $this->db->select("IDcgmdiab,IDfax,cgmdiab_doc_fax,userID,sent_repeat,cgmdiab_status,twilio_status");
////        $this->db->where("cgmdiab_status", "Sent");
////        $this->db->where("sent_repeat <=", 3);
////        $this->db->group_start();
////        $this->db->where("twilio_status =", "failed");
////        $this->db->or_where("twilio_status =", "");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_cgm_diabetic")->result();
////
////        foreach ($data as $key) {
////            $this->db->where("id", $key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////            try {
////
////                $url = 'https://api.documo.com/v1/faxes';
////
////                $file = new CURLFile('assets/documents/faxes2486e9b3c97e/cgmdiab/user_' . $key->userID . "/fax_of_sending_cgmdiab_" . $key->IDcgmdiab . ".pdf" , 'application/pdf', 'sample');
////                $body = array(
////                    'faxNumber' => "1".$key->cgmdiab_doc_fax,
////                    'callerId' => $send_num,
////                    'recipientName' => '',
////                    'coverPage' => '',
////                    'subject' => '',
////                    'notes' => '',
////                    'attachments' => $file
////                );
////
////                $headers = array(
////                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////                    'Content-Type: multipart/form-data'
////                );
////
////                $ch = curl_init();
////
////                $options = array(
////                    CURLOPT_URL => $url,
////                    CURLOPT_HTTPHEADER => $headers,
////                    CURLOPT_POST => true,
////                    CURLOPT_POSTFIELDS => $body,
////                    CURLOPT_RETURNTRANSFER => true
////                );
////                curl_setopt_array($ch, $options);
////
////                $fax = curl_exec($ch);
////                curl_close($ch);
////                $fax =  json_decode($fax);
////
////
////                $this->db->trans_start();
////                $this->db->where("IDcgmdiab", $key->IDcgmdiab);
////                $updateData = array("sent_repeat" => $key->sent_repeat + 1,
////                    "cgmdiab_status" => "Sent",
////                    "twilio_status" => "queued",
////                    "IDfax" => $fax->messageId);
////                $this->db->update("t_cgm_diabetic", $updateData);
////                $this->db->trans_complete();
////            } catch (Exception $e) {
////                $this->db->trans_start();
////                $this->db->where("IDcgmdiab", $key->IDfaxpdf);
////                $updateData = array(
////                    "cgmdiab_status" => "Not_sent",
////                    "twilio_status" => "canceled"
////                );
////                $this->db->update("t_cgm_diabetic", $updateData);
////                $this->db->trans_complete();
////            }
////        }
////
////    }
//
////    public function faxpdfResendFax()
////    {
//
////        $this->db->where("faxpdf_status", "Sent");
////        $this->db->where("sent_repeat <=", 3);
////        $this->db->group_start();
////        $this->db->where("twilio_status =", "failed");
////        $this->db->or_where("twilio_status =", "");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_faxpdf")->result();
////
////
////
////
////        foreach ($data as $key) {
////            $this->db->where("id", $key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////            try {
////                if ($key->faxpdf_path_old_new == 0) {
////                    $pathPDF =  "assets/pdf/" . $key->faxpdf_file;
////                } else if ($key->faxpdf_path_old_new == 1) {
////                    $pathPDF =  "assets/pdf-new/" . $key->faxpdf_path . "/" . $key->faxpdf_file;
////                }
////
////
////                $url = 'https://api.documo.com/v1/faxes';
////
////                $file = new CURLFile($pathPDF , 'application/pdf', 'sample');
////                $body = array(
////                    'faxNumber' => "1".$key->faxpdf_doc_fax,
////                    'callerId' => $send_num,
////                    'recipientName' => '',
////                    'coverPage' => '',
////                    'subject' => '',
////                    'notes' => '',
////                    'attachments' => $file
////                );
////
////                $headers = array(
////                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////                    'Content-Type: multipart/form-data'
////                );
////
////                $ch = curl_init();
////
////                $options = array(
////                    CURLOPT_URL => $url,
////                    CURLOPT_HTTPHEADER => $headers,
////                    CURLOPT_POST => true,
////                    CURLOPT_POSTFIELDS => $body,
////                    CURLOPT_RETURNTRANSFER => true
////                );
////                curl_setopt_array($ch, $options);
////
////                $fax = curl_exec($ch);
////                curl_close($ch);
////                $fax =  json_decode($fax);
////
////
////                $this->db->trans_start();
////                $this->db->where("IDfaxpdf", $key->IDfaxpdf);
////                $updateData = array("sent_repeat" => $key->sent_repeat + 1,
////                    "faxpdf_status" => "Sent",
////                    "twilio_status" => "queued",
////                    "IDfax" => $fax->messageId);
////                $this->db->update("t_faxpdf", $updateData);
////                $this->db->trans_complete();
////            } catch (Exception $e) {
////                $this->db->trans_start();
////                $this->db->where("IDfaxpdf", $key->IDfaxpdf);
////                $updateData = array(
////                    "faxpdf_status" => "Not_sent",
////                    "twilio_status" => "canceled"
////                );
////                $this->db->update("t_faxpdf", $updateData);
////                $this->db->trans_complete();
////            }
////        }
////    }
////
////    public function heatpadResendFax()
////    {
//
////        $this->db->select("IDheatpad,IDfax,heatpad_doc_fax,userID,sent_repeat,heatpad_status,twilio_status");
////        $this->db->where("heatpad_status", "Sent");
////        $this->db->where("sent_repeat <=", 3);
////        $this->db->group_start();
////        $this->db->where("twilio_status =", "failed");
////        $this->db->or_where("twilio_status =", "");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_heatpad")->result();
////
////        foreach ($data as $key) {
////            $this->db->where("id", $key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////            try {
////
////                $url = 'https://api.documo.com/v1/faxes';
////
////                $file = new CURLFile('assets/documents/faxes2486e9b3c97e/heatpad/user_' . $key->userID . "/fax_of_sending_heatpad_" . $key->IDheatpad . ".pdf" , 'application/pdf', 'sample');
////                $body = array(
////                    'faxNumber' => "1".$key->heatpad_doc_fax,
////                    'callerId' => $send_num,
////                    'recipientName' => '',
////                    'coverPage' => '',
////                    'subject' => '',
////                    'notes' => '',
////                    'attachments' => $file
////                );
////
////                $headers = array(
////                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////                    'Content-Type: multipart/form-data'
////                );
////
////                $ch = curl_init();
////
////                $options = array(
////                    CURLOPT_URL => $url,
////                    CURLOPT_HTTPHEADER => $headers,
////                    CURLOPT_POST => true,
////                    CURLOPT_POSTFIELDS => $body,
////                    CURLOPT_RETURNTRANSFER => true
////                );
////                curl_setopt_array($ch, $options);
////
////                $fax = curl_exec($ch);
////                curl_close($ch);
////                $fax =  json_decode($fax);
////
////
////                $this->db->trans_start();
////                $this->db->where("IDheatpad", $key->IDheatpad);
////                $updateData = array("sent_repeat" => $key->sent_repeat + 1,
////                    "heatpad_status" => "Sent",
////                    "twilio_status" => "queued",
////                    "IDfax" => $fax->messageId);
////                $this->db->update("t_heatpad", $updateData);
////                $this->db->trans_complete();
////            } catch (Exception $e) {
////                $this->db->trans_start();
////                $this->db->where("IDheatpad", $key->IDfaxpdf);
////                $updateData = array(
////                    "heatpad_status" => "Not_sent",
////                    "twilio_status" => "canceled"
////                );
////                $this->db->update("t_heatpad", $updateData);
////                $this->db->trans_complete();
////            }
////        }
//
////    }
////
////    public function hipaaResendFax()
////    {
//
////        $this->db->select("IDhipaa,IDfax,hipaa_doc_fax,userID,sent_repeat,hipaa_status,twilio_status");
////        $this->db->where("hipaa_status", "Sent");
////        $this->db->where("sent_repeat <=", 3);
////        $this->db->group_start();
////        $this->db->where("twilio_status =", "failed");
////        $this->db->or_where("twilio_status =", "");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_hipaa")->result();
////
////        foreach ($data as $key) {
////            $this->db->where("id", $key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////            try {
////
////                $url = 'https://api.documo.com/v1/faxes';
////
////                $file = new CURLFile('assets/documents/faxes2486e9b3c97e/hipaa/user_' . $key->userID . "/fax_of_sending_hipaa_" . $key->IDhipaa . ".pdf" , 'application/pdf', 'sample');
////                $body = array(
////                    'faxNumber' => "1".$key->hipaa_doc_fax,
////                    'callerId' => $send_num,
////                    'recipientName' => '',
////                    'coverPage' => '',
////                    'subject' => '',
////                    'notes' => '',
////                    'attachments' => $file
////                );
////
////                $headers = array(
////                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////                    'Content-Type: multipart/form-data'
////                );
////
////                $ch = curl_init();
////
////                $options = array(
////                    CURLOPT_URL => $url,
////                    CURLOPT_HTTPHEADER => $headers,
////                    CURLOPT_POST => true,
////                    CURLOPT_POSTFIELDS => $body,
////                    CURLOPT_RETURNTRANSFER => true
////                );
////                curl_setopt_array($ch, $options);
////
////                $fax = curl_exec($ch);
////                curl_close($ch);
////                $fax =  json_decode($fax);
////
////
////                $this->db->trans_start();
////                $this->db->where("IDhipaa", $key->IDhipaa);
////                $updateData = array("sent_repeat" => $key->sent_repeat + 1,
////                    "hipaa_status" => "Sent",
////                    "twilio_status" => "queued",
////                    "IDfax" => $fax->messageId);
////                $this->db->update("t_hipaa", $updateData);
////                $this->db->trans_complete();
////            } catch (Exception $e) {
////                $this->db->trans_start();
////                $this->db->where("IDhipaa", $key->IDfaxpdf);
////                $updateData = array(
////                    "hipaa_status" => "Not_sent",
////                    "twilio_status" => "canceled"
////                );
////                $this->db->update("t_hipaa", $updateData);
////                $this->db->trans_complete();
////            }
////        }
////    }
////
////    public function maskResendFax()
////    {
//
////        $this->db->select("IDmask,IDfax,mask_doc_fax,userID,sent_repeat,mask_status,twilio_status");
////        $this->db->where("mask_status", "Sent");
////        $this->db->where("sent_repeat <=", 3);
////        $this->db->group_start();
////        $this->db->where("twilio_status =", "failed");
////        $this->db->or_where("twilio_status =", "");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_mask")->result();
////
////        foreach ($data as $key) {
////            $this->db->where("id", $key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////            try {
////
////                $url = 'https://api.documo.com/v1/faxes';
////
////                $file = new CURLFile( 'assets/documents/faxes2486e9b3c97e/mask/user_' . $key->userID . "/fax_of_sending_mask_" . $key->IDmask . ".pdf" , 'application/pdf', 'sample');
////                $body = array(
////                    'faxNumber' => "1".$key->mask_doc_fax,
////                    'callerId' => $send_num,
////                    'recipientName' => '',
////                    'coverPage' => '',
////                    'subject' => '',
////                    'notes' => '',
////                    'attachments' => $file
////                );
////
////                $headers = array(
////                    'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////                    'Content-Type: multipart/form-data'
////                );
////
////                $ch = curl_init();
////
////                $options = array(
////                    CURLOPT_URL => $url,
////                    CURLOPT_HTTPHEADER => $headers,
////                    CURLOPT_POST => true,
////                    CURLOPT_POSTFIELDS => $body,
////                    CURLOPT_RETURNTRANSFER => true
////                );
////                curl_setopt_array($ch, $options);
////
////                $fax = curl_exec($ch);
////                curl_close($ch);
////                $fax =  json_decode($fax);
////
////
////                $this->db->trans_start();
////                $this->db->where("IDmask", $key->IDmask);
////                $updateData = array("sent_repeat" => $key->sent_repeat + 1,
////                    "mask_status" => "Sent",
////                    "twilio_status" => "queued",
////                    "IDfax" => $fax->messageId);
////                $this->db->update("t_mask", $updateData);
////                $this->db->trans_complete();
////            } catch (Exception $e) {
////                $this->db->trans_start();
////                $this->db->where("IDmask", $key->IDfaxpdf);
////                $updateData = array(
////                    "mask_status" => "Not_sent",
////                    "twilio_status" => "canceled"
////                );
////                $this->db->update("t_mask", $updateData);
////                $this->db->trans_complete();
////            }
////        }
////    }
//
////    public function aurbracResendFax(){
////        $sid    = "AC720fa9d520db09505609c091b9152f7f";
////        $token  = "2e11a5a6e537609375d68fa63d869bb3";
////
////        require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
////        $this->db->select("IDaurbrac,IDfax,aurbrac_doc_fax");
////        $this->db->where("aurbrac_status","Sent");
////        $this->db->where("sent_repeat <=",3);
////        $this->db->group_start();
////        $this->db->where("twilio_status !=","queued");
////        $this->db->or_where("twilio_status !=","delivered");
////        $this->db->or_where("twilio_status !=","processing");
////        $this->db->group_end();
////
////        $data = $this->db->get("t_aurtho_brace")->result();
////        foreach($data as $key)
////        {
////            $this->db->where("id",$key->userID);
////            $send_num = $this->db->get("login")->row()->sent_num;
////
////            $twilio = new Client($sid, $token);
////            $fax = $twilio->fax->v1->faxes
////                ->create('+1'.$key->aurbrac_doc_fax, // to
////                    base_url().'assets/documents/faxes2486e9b3c97e/aurbrac/user_'.$key->userID."/fax_of_sending_aurbrac_".$key->IDaurbrac.".pdf", // mediaUrl
////                    array("from" => '+1'.$send_num)
////                );
////
////            $this->db->where("IDaurbrac",$key->IDaurbrac);
////            $updateData = array("sent_repeat"=>$key->sent_repeat+1 ,
////                "aurbrac_status"=>"Sent",
////                "twilio_status"=>"queued",
////                "IDfax",$fax->sid);
////            $this->db->update("t_aurtho_brace",$updateData);
////
////        }
////    }
//
//    public function deleteFiles()
//    {
//        $this->db->select("faxpdf_path,faxpdf_path_old_new,faxpdf_file,faxpdf_no_of_sent_faxes,faxpdf_status,faxpdf_is_path_deleted,IDfaxpdf");
//        $this->db->group_start();
//        $this->db->where("LENGTH(faxpdf_no_of_sent_faxes)", 63);
//        $this->db->or_where("faxpdf_status", "Not_sent");
//        $this->db->or_where("faxpdf_status", "Delete");
//        $this->db->group_end();
//        $this->db->where("faxpdf_is_path_deleted", 0);
//        $data = $this->db->get("t_faxpdf")->result();
//
//        foreach ($data as $key) {
//            if ($key->faxpdf_path_old_new == 0) {
//                if (file_exists("assets/pdf/" . $key->faxpdf_file)) {
//                    unlink("assets/pdf/" . $key->faxpdf_file);
//                }
//            } else if ($key->faxpdf_path_old_new == 1) {
//                if (file_exists("assets/pdf-new/" . $key->faxpdf_path . "/" . $key->faxpdf_file)) {
//                    unlink("assets/pdf-new/" . $key->faxpdf_path . "/" . $key->faxpdf_file);
//                }
//            }
//
//            $this->db->where("IDfaxpdf", $key->IDfaxpdf);
//            $updateData = array("faxpdf_is_path_deleted" => 1);
//            $this->db->update("t_faxpdf", $updateData);
//        }
//
//    }
//
////    function database_backup()
////    {
////        $this->load->dbutil();
////        // $dbs = $this->dbutil->list_databases();
////
////        // foreach ($dbs as $db)
////        // {
////        //     echo $db.'<br>';
////        $tables = $this->db->list_tables();
////        // print_r($tables);
////
////        unset($tables[0]);
////        unset($tables[1]);
////        unset($tables[3]);
////        unset($tables[12]);
////        unset($tables[13]);
////        unset($tables[14]);
////        unset($tables[8]);
////        $array = array_values($tables);
////
////        print_r($array);
////
////        die;
////
////        // foreach ($tables as $table)
////        // {
////        //          echo $table."<br>";
////        $prefs = array(
////            'tables' => array($array),   // Array of tables to backup.
////            'ignore' => array(),                     // List of tables to omit from the backup
////            'format' => 'zip',                       // gzip, zip, txt
////            'filename' => 'backup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
////            'add_drop' => TRUE,                        // Whether to add DROP TABLE statements to backup file
////            'add_insert' => TRUE,                        // Whether to add INSERT data to backup file
////            'newline' => "\n"                         // Newline character used in backup file
////        );
////        $backup = $this->dbutil->backup($prefs);
////
////
////        // }
////        // Load the file helper and write the file to your server
////        $this->load->helper('file');
////        if (!is_dir('assets/documents/' . date("Y-m-d"))) {
////            mkdir('assets/documents/' . date("Y-m-d"), 0700, TRUE);
////        }
////        write_file(getcwd() . '/assets/documents/' . date("Y-m-d") . '/backup.zip', $backup);
////
////
////        echo "done";
////    }
//
////    private function faxSend($sender,$receiver,$path)
////    {
////        $url = 'https://api.documo.com/v1/faxes';
////
////        $file = new CURLFile($path , 'application/pdf', 'sample');
////        $body = array(
////            'faxNumber' => $receiver,
////            'callerId' => $sender,
////            'recipientName' => '',
////            'coverPage' => '',
////            'subject' => '',
////            'notes' => '',
////            'attachments' => $file
////        );
////
////        $headers = array(
////            'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
////            'Content-Type: multipart/form-data'
////        );
////
////        $ch = curl_init();
////
////        $options = array(
////            CURLOPT_URL => $url,
////            CURLOPT_HTTPHEADER => $headers,
////            CURLOPT_POST => true,
////            CURLOPT_POSTFIELDS => $body,
////            CURLOPT_RETURNTRANSFER => true
////        );
////        curl_setopt_array($ch, $options);
////
////        $response = curl_exec($ch);
////        curl_close($ch);
////        return json_decode($response);
////    }
//
//
//}
//
