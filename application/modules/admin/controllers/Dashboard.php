<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
	function __construct(){
		parent::__construct();
		$this->load->model('Admin_model');
		
	}


	public function index()
	{

		if(!$this->session->userdata("login_redirect")){
			$this->load->view('include/head');
			$this->load->view('login');
		}else{
			redirect($this->session->userdata("login_redirect"));
		}
	}


	public function login_process()
	{
	    $security_id = "2486e9b3c97e";
		$username = $this->input->post('username');
		$pass = $this->input->post('password');

		$query= $this->Admin_model->login($username,$pass);
		if($query==true)
		{
		   $role= $query[0]['role'];
		   	$this->session->set_userdata('repcode',$query[0]['repcode']);
  			$this->session->set_userdata('role',$query[0]['role']);
  			if($role=="Agent")
  			{
  			$this->session->set_userdata('id',$query[0]['creator_id']);
			$this->session->set_userdata('creator_id',$query[0]['id']);
  			}else{
  			$this->session->set_userdata('id',$query[0]['id']);
			$this->session->set_userdata('creator_id',$query[0]['creator_id']);
  			}
  			

			// $this->session->set_userdata('faxNo', $query[0]['faxNo']);
			$this->session->set_userdata('username', $query[0]['name']);
			$this->session->set_userdata('receive_num', $query[0]['receive_num']);
			$this->session->set_userdata('password', $query[0]['password']);
			$this->session->set_userdata('sent_num', $query[0]['sent_num']);
			$this->session->set_userdata('cgmdiab_tab', $query[0]['cgmdiab_tab']);
			$this->session->set_userdata('aurbrac_tab', $query[0]['aurbrac_tab']);
			$this->session->set_userdata('mask_tab', $query[0]['mask_tab']);
			$this->session->set_userdata('heatpad_tab', $query[0]['heatpad_tab']);
			$this->session->set_userdata('hipaa_tab', $query[0]['hipaa_tab']);
            $this->session->set_userdata('faxpdf_tab', $query[0]['faxpdf_tab']);
			$this->session->set_userdata('company_name', $query[0]['company_name']);
			$this->session->set_userdata('email', $query[0]['email']);

            try
            {
                if (!is_dir('assets/pdf-new/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/pdf-new/user_'.$query[0]['id'], 0700, TRUE);
                }
                if (!is_dir('assets/documents/faxes'.$security_id.'/aurbrac/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/documents/faxes'.$security_id.'/aurbrac/user_'.$query[0]['id'], 0700, TRUE);
                }
                if (!is_dir('assets/documents/faxes'.$security_id.'/cgmdiab/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/documents/faxes'.$security_id.'/cgmdiab/user_'.$query[0]['id'], 0700, TRUE);
                }
                if (!is_dir('assets/documents/faxes'.$security_id.'/heatpad/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/documents/faxes'.$security_id.'/heatpad/user_'.$query[0]['id'], 0700, TRUE);
                }
                if (!is_dir('assets/documents/faxes'.$security_id.'/hipaa/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/documents/faxes'.$security_id.'/hipaa/user_'.$query[0]['id'], 0700, TRUE);
                }   
                if (!is_dir('assets/documents/faxes'.$security_id.'/mask/user_'.$query[0]['id'])) 
                {
                    mkdir('assets/documents/faxes'.$security_id.'/mask/user_'.$query[0]['id'], 0700, TRUE);
                } 
            }
            catch(Exception $e)
            {
                redirect("admin/Dashboard/logout");
            }

  			if($query[0]['mask_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/mask/Cleaning";
  			}
  			else if($query[0]['hipaa_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/hipaa/Cleaning";
  			}
			else if($query[0]['heatpad_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/heatpad/Cleaning";
  			}
			else if($query[0]['aurbrac_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/aurbrac/Cleaning";
  			}
			else if($query[0]['cgmdiab_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/cgmdiab/Cleaning";
  			}
			else if($query[0]['faxpdf_tab'] == 1)
  			{
  			    $redirect = "admin/List_controller/campaign_list/faxpdf/Cleaning";
  			}
  			else
  			{
  			    if($role == "Agent" || $role == "User")
  			    {
  			        redirect("admin/Dashboard/logout");  
  			    }
  			}

		    if($role == "Owner"){
		    	$this->session->set_userdata("login_redirect","admin/Owner/save_agent");
			   redirect("admin/Owner/save_agent");
			}

		    else if($role == "User")
		    {
	     	    $this->session->set_userdata("login_redirect","admin/User/save_agent");
		        redirect($redirect);
			}

			else if($role == "Agent")
			{
			    if($query[0]['is_activated'] == 0)
			    {
		            $this->session->set_flashdata('authentication_error', 'Your account is currently deactivated!!');
	                redirect("admin/Dashboard");
			    }
		        $this->session->set_userdata("login_redirect",$redirect);
		        redirect($redirect);
			}

			else if($role == "QuantityManager")
			{
			    $this->session->set_userdata("login_redirect","admin/QuantityManager");
			    redirect("admin/QuantityManager");
			}
            else if($role == "userlimit")
            {
                $this->session->set_userdata("login_redirect","admin/Userlimit");
                redirect("admin/Userlimit");
            }
			
			else if($role == "Representative")
			{
			    $this->session->set_userdata("login_redirect","admin/Repren/campaign_list");
			    redirect("admin/Repren/data_list");
			}
			else if($role == "QA")
			{
			    $this->session->set_userdata("login_redirect","admin/QA/index");
			    redirect("admin/QA/index");
			}
			else if($role == "Viewer")
			{
			    $this->session->set_userdata("login_redirect","admin/Viewer/campaign_list/aurbrac/Cleaning");
			    redirect("admin/viewer/campaign_list/aurbrac/Cleaning");
			}

		    else
		    {
		        $this->session->set_flashdata('authentication_error', 'Wrong username or password.');
			    redirect("admin/Dashboard");
		    }

		}

		else
		{
		    $this->session->set_flashdata('authentication_error', 'Wrong username or password.');
		    redirect("admin/Dashboard");
	    }

    }

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("admin/Dashboard");
	}

}



