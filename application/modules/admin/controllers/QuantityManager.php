<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class QuantityManager extends My_controller {


    function __construct()
    {
        parent::__construct();
        $role = $this->session->userdata("role");
// 		if($role!="QuantityManager")
// 		{
// 			redirect("admin/Dashboard");
// 		}
		$this->load->model("QuantityManager_model");
        passwordAuthentication();
    }

    public function index()
    {
        
        $token = md5(uniqid(mt_rand(), true).microtime(true));
        $this->session->set_userdata("token",$token);
        $this->load->view('include/head');
        $this->load->view('include/nav');
       
        $data["token"] = $token;
        $data['dollar_data'] = $this->QuantityManager_model->getData();
        $this->load->view('QuantityManager',$data);
    }
    
    public function save_dollar()
    {
        $this->dollarValidation();
        if($this->input->post("token") == $this->session->userdata("token"))
        {
            $this->QuantityManager_model->save_dollar($this->input->post());
        }
        redirect("admin/QuantityManager");
    }

    private function dollarValidation()
    {
        $postArr = ['dollar_rs_value','dollar_value_in_rs','dollar_twillio_per_page','token'];
        
        if(count($postArr) != count($this->input->post()))
        {
            redirect("admin/Dashboard/logout");
        }
        
        foreach($postArr as $value)
        {
            if(!$this->input->post($value))
            {
                redirect("admin/Dashboard/logout");
            }
            if($this->input->post($value)[0] == ".")
            {
                $this->session->set_flashdata("errorMsg",'<div class="alert alert-danger" role="alert" >No value should be started with decimal point.</div>');
                redirect("admin/QuantityManager"); 
            }
            if($value !="token")
            {
                if(preg_match('/^([0-9.]+)$/', $this->input->post($value)) === 0) 
                {   
                    $this->session->set_flashdata("errorMsg",'<div class="alert alert-danger" role="alert" >Input values should only have numbers and decimal point.</div>');
                    redirect("admin/QuantityManager");            
                }
            }
        }
        
    }   

}

