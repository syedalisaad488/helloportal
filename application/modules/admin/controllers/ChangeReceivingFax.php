<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ChangeReceivingFax extends My_controller {

    function __construct(){
        parent::__construct();


    }

    function ChangeReceiveDetails($id)
    {
        $IDreceive = $id;
        $show_data_time = ($this->input->post("show_data_time_form")) ? $this->input->post("show_data_time_form") : 0;
        $show_send_no = ($this->input->post("show_send_no_form")) ? $this->input->post("show_send_no_form") : 0;
        $show_receive_no = ($this->input->post("show_receive_no_form")) ? $this->input->post("show_receive_no_form") : 0;


        $data = array(
            'show_data_time' => $show_data_time,
            'show_send_no' => $show_send_no,
            'show_receive_no' => $show_receive_no,
            'is_edited' => $this->input->post("show_is_edited_form"),
        );


        $this->db->where("id",$IDreceive);
        $this->db->update("received",$data);

        redirect("admin/Get_twillo/getlist");


    }

    function check()
    {
        $this->session->set_userdata("username","changeRec");
    }


}

