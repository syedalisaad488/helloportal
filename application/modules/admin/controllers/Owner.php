<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owner extends My_controller {

	function __construct(){
		parent::__construct();
				$this->load->helper('url');

		$role = $this->session->userdata("role");
		if($role!="Owner")
		{
			redirect("admin/Dashboard");
		}
		$this->load->library('upload');
	    $this->load->library('pdf');
	    $this->load->library('form_validation');
		$this->load->model("Admin_model");

	}

	public function index()
	{
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('owner/index');

	}

	public function save_agent($id=null)
	{
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		$email = $this->input->post('email');
		$comapny_name = $this->input->post('company_name');
		$rec_num = $this->input->post('receive_num');
		$snt_num = $this->input->post('sent_name');
		$cgmdiab_tab = $this->input->post('cgmdiab');
		$aurbrac_tab = $this->input->post('aurbrac');
		$mask_tab = $this->input->post('mask');
		$hippa_tab = $this->input->post('hipaa');
		$heatpad_tab = $this->input->post('heatpad');



		$receive_num = str_replace(array( '(', ')','-','+1',' ' ), '', $rec_num);
		$sent_num = str_replace(array( '(', ')','-','+1',' ' ), '', $snt_num);
		$repcode_json = json_encode($this->input->post("repcode"));




		$this->form_validation->set_rules('name', 'Username', 'is_unique[login.name]');


		if($id==null)
		{
			if($name!=null)
        			{

                	if ($this->form_validation->run() == FALSE)
                         {
                            $this->session->set_flashdata("unique_field","Enter a unique username.");
                            redirect("admin/Owner/save_agent");
                         }
				$this->Admin_model->save_user($name,$password,$role,$email,$comapny_name,$receive_num,$sent_num,$cgmdiab_tab,$aurbrac_tab,$mask_tab,$heatpad_tab,$hippa_tab,$id,$repcode_json);
		    }
		    	$query['rep_code'] = $this->Admin_model->rep_codes();
    		$query['data']=$this->Admin_model->get_agent_data();
    		$this->load->view('owner/add_agent',$query);
		}
		else
		{
			if($name!=null)
			{
				 $this->Admin_model->save_user($name,$password,$role,$email,$comapny_name,$receive_num,$sent_num,$cgmdiab_tab,$aurbrac_tab,$mask_tab,$heatpad_tab,$hippa_tab,$id,$repcode_json);
				 $reload='1';
			}
			if(isset($reload))
			{
				$query['data']=$this->Admin_model->get_agent_data();
		        $this->load->view('owner/add_agent',$query);
			}
			else
			{
				$query['data']=$this->Admin_model->get_agent_data($id);
		        $this->load->view('owner/edit_agent',$query);
			}

		}
	}

    public function delete($id)
	{
    	$this->Admin_model->delete($table='login',$id);
        redirect("admin/Owner/save_agent");
	}

	public function save_html_to_pdf($IDclient)
	{
	    $this->write_fax($IDclient);
        $html = $this->output->get_output();
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output = $this->dompdf->output();
   		file_put_contents("assets/documents/faxes/fax_of_sending$IDclient.pdf", $output);
	}

	public function write_fax($id)
	{
	    	$data['fax'] = $this->Admin_model->get_fax($id);
			$this->load->view('admin/fax_template',$data);
	}

		public function update_data($IDcampaign,$prefix,$status,$reason)
		{

			$table = "";

    	    if($prefix == "aurbrac")
    	    {
    	        $table = "t_aurtho_brace";
    	    }
    	    else if($prefix == "cgmdiab")
    	    {
    	        $table = "t_cgm_diabetic";
    	    }

    	    $this->Admin_model->update_qa_status($IDcampaign,$table,$prefix,$status,$reason);

    	    redirect("admin/Owner/campaign_list/".$prefix."/Cleaning");
		}

	public function assign_qa($prefix)
	{
		$idqa = $this->input->post('idqa');
		$table = "";
		if($prefix == "aurbrac")
		{
			$table = "t_aurtho_brace";
		}
		else if($prefix == "cgmdiab")
		{
			$table = "t_cgm_diabetic";
		}
		for($i=0;$i<count($this->input->post("send_fax_data[]"));$i++)
		{

			$IDcampaign = $this->input->post("send_fax_data[$i]");
			$data = array(
				'IDqa' => $idqa
			);
			$this->db->where('ID'.$prefix, $IDcampaign);
			$this->db->update($table, $data);
		}
		if($prefix == "aurbrac")
		{
			redirect('admin/Owner/campaign_list/aurbrac/Cleaning');
		}
		else if($prefix == "cgmdiab")
		{
			redirect('admin/Owner/campaign_list/cgmdiab/Cleaning');
		}
	}
    public function campaign_list($prefix,$type)
    {
        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $data['qa_assign'] = $this->Admin_model->qa_assign('aurbrac_tab="1"');
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/owner/aurbrac_data',$data);
        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $data['qa_assign'] = $this->Admin_model->qa_assign('cgmdiab_tab="1"');
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/owner/cgmdiab_data',$data);
        }
		else if($prefix=='mask'){
			$table="t_mask";
			$data['qa_assign'] = $this->Admin_model->qa_assign('mask_tab="1"');
			$this->load->view('include/qa_nav');
			$this->load->view('include/nav');
			$this->load->view('admin/owner/mask_data',$data);
		}
		else if($prefix=='heatpad'){
			$table="t_heatpad";
			$data['qa_assign'] = $this->Admin_model->qa_assign('heatpad_tab="1"');
			$this->load->view('include/qa_nav');
			$this->load->view('include/nav');
			$this->load->view('admin/owner/heatpad_data',$data);
		}
        else if($prefix=='hipaa'){
            $table="t_hipaa";
            $data['qa_assign'] = $this->Admin_model->qa_assign('hipaa_tab="1"');
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/owner/hipaa_data',$data);
        }
    }
    public function fetch_camp_data($prefix,$type){



        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'aurbrac_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');
            $get_all_data = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');
        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'cgmdiab_medicare');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'cgmdiab_medicare');

        }
		else if($prefix=='mask'){
			$table="t_mask";
			$callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'mask_med_id');
			$totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'mask_med_id');

		}
		else if($prefix=='heatpad'){
			$table="t_heatpad";
			$callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'heatpad_med_id');
			$totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'heatpad_med_id');

		}
        else if($prefix=='hipaa'){
            $table="t_hipaa";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'hipaa_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'hipaa_med_id');

        }


        $data = array();
        if($prefix=='aurbrac')
        {
            foreach ($callData as $row) {
                $callData = array();
//                $status = "";
//                if ($row->aurbrac_status == 'Good') {
//                    $status = "<span class='btn btn-sm btn-success'>GOOD</span>";
//                } else if ($row->aurbrac_status == 'Deleted') {
//                    $status = "<span class='btn btn-sm btn-danger'>DELETED</span>";
//                } else if ($row->aurbrac_status == 'Rejected') {
//                    $status = "<span class='btn btn-sm btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->aurbrac_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-sm btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->aurbrac_status == 'Sent') {
//                    $status = "<span class='btn btn-sm btn-info' >SENT</span>";
//                } else if ($row->aurbrac_status == 'Completed') {
//                    $status = "<span class='btn btn-sm btn-primary'  >Completed</span>";
//                }
//                else if ($row->aurbrac_status == 'Not_sent') {
//                    $status = "<span class='btn btn-sm btn-danger'  >Not sent</span>";
//                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDaurbrac . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->aurbrac_reason);
				$callData[] = strtoupper(isset($row->aurbrac_status) ? $row->aurbrac_status=='dupe' ? "DUPE" : '' : '');
				$callData[] = $row->aurbrac_firstname;
				$callData[] = $row->aurbrac_lastname;
				$callData[] = $row->aurbrac_address;
				$callData[] = $row->aurbrac_city;
				$callData[] = $row->aurbrac_state;
				$callData[] = $row->aurbrac_zip;
//                $callData[] = $row->aurbrac_email;
				$callData[] = $row->aurbrac_dob;
				$callData[] = (strpos($row->aurbrac_phone, '+1') !== false) ? $row->aurbrac_phone : '+1' . $row->aurbrac_phone;
				$callData[] = $row->aurbrac_gender;
				$callData[] = $row->aurbrac_height;
				$callData[] = $row->aurbrac_weight;
				$callData[] = $row->aurbrac_pain_level;
				$callData[] = ($row->aurbrac_back == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_knee == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_knee == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_wrist == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_wrist == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_ankle == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_ankle == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_shoulder == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_shoulder == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_elbow == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_elbow == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_hip == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_hip == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_neck == 1 ? 'Yes' : 'No');
				$callData[] = $row->aurbrac_doc_firstname;
				$callData[] = $row->aurbrac_doc_lastname;
				$callData[] = $row->aurbrac_doc_address;
				$callData[] = $row->aurbrac_doc_city;
				$callData[] = $row->aurbrac_doc_state;
				$callData[] = $row->aurbrac_doc_zip;
				$callData[] = $row->aurbrac_doc_npi;
				$callData[] = (strpos($row->aurbrac_doc_phone, '+1') !== false) ? $row->aurbrac_doc_phone : '+1' . $row->aurbrac_doc_phone;
				$callData[] = (strpos($row->aurbrac_doc_fax, '+1') !== false) ? $row->aurbrac_doc_fax : '+1' . $row->aurbrac_doc_fax;
				$callData[] = $row->aurbrac_med_id;
//				$callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
				$callData[] = $row->aurbrac_data_entered;
//				$callData[] = $row->aurbrac_insurance_company;
//				$callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
//                $callData[] = $row->aurbrac_IDtrack;
//                $callData[] = $row->aurbrac_IDsite;
				$callData[] = $row->aurbrac_call_back_num;
				$callData[] = $row->aurbrac_repcode;
//                $callData[] = '<a href="' . base_url('admin/Owner/campaign_edit_data/' . $row->IDaurbrac) . '/aurbrac" >Edit</a>
//									<div class="dropdown" >
//                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
//                                                Change Status
//                                            </button>
//                                        <div class="dropdown-menu">
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING ICD 10">MISSING ICD 10</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDaurbrac . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
//                                    </div>
//                                    </div>';
                $data[] = $callData;
            }
        }
		else if($prefix=='mask')
		{
			foreach ($callData as $row) {
				$callData = array();
				$callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDmask . '" ></center>';
				$callData[] = strtoupper(isset($row->mask_status) ? $row->mask_status=='dupe' ? 'DUPE': '': '');
				$callData[] = $row->mask_firstname;
				$callData[] = $row->mask_lastname;
				$callData[] = $row->mask_address;
				$callData[] = $row->mask_city;
				$callData[] = $row->mask_state;
				$callData[] = $row->mask_zip;
				$callData[] = $row->mask_dob;
				$callData[] = (strpos($row->mask_phone, '+1') !== false) ? $row->mask_phone : '+1' . $row->mask_phone;
				$callData[] = $row->mask_doc_firstname;
				$callData[] = $row->mask_doc_lastname;
				$callData[] = $row->mask_doc_npi;
				$callData[] = (strpos($row->mask_doc_phone, '+1') !== false) ? $row->mask_doc_phone : '+1' . $row->mask_doc_phone;
				$callData[] = (strpos($row->mask_doc_fax, '+1') !== false) ? $row->mask_doc_fax : '+1' . $row->mask_doc_fax;
				$callData[] = $row->mask_med_id;
				$callData[] = $row->mask_data_entered;
				$callData[] = $row->mask_call_back_num;
				$callData[] = $row->mask_repcode;
				$data[] = $callData;
			}
		}
		else if($prefix=='heatpad')
		{
			foreach ($callData as $row) {
				$callData = array();
//                $status = "";
//                if ($row->heatpad_status == 'Good') {
//                    $status = "<span class='btn btn-sm btn-success'>GOOD</span>";
//                } else if ($row->heatpad_status == 'Deleted') {
//                    $status = "<span class='btn btn-sm btn-danger'>DELETED</span>";
//                } else if ($row->heatpad_status == 'Rejected') {
//                    $status = "<span class='btn btn-sm btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->heatpad_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-sm btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->heatpad_status == 'Sent') {
//                    $status = "<span class='btn btn-sm btn-info' >SENT</span>";
//                } else if ($row->heatpad_status == 'Completed') {
//                    $status = "<span class='btn btn-sm btn-primary'  >Completed</span>";
//                }
//                else if ($row->heatpad_status == 'Not_sent') {
//                    $status = "<span class='btn btn-sm btn-danger'  >Not sent</span>";
//                }

				$callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDheatpad . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->heatpad_reason);
				$callData[] = strtoupper(isset($row->heatpad_status) ? $row->heatpad_status=='dupe' ? 'DUPE': '': '');
				$callData[] = $row->heatpad_firstname;
				$callData[] = $row->heatpad_lastname;
				$callData[] = $row->heatpad_address;
				$callData[] = $row->heatpad_city;
				$callData[] = $row->heatpad_state;
				$callData[] = $row->heatpad_zip;
				$callData[] = $row->heatpad_dob;
				$callData[] = (strpos($row->heatpad_phone, '+1') !== false) ? $row->heatpad_phone : '+1' . $row->heatpad_phone;
				$callData[] = $row->heatpad_doc_firstname;
				$callData[] = $row->heatpad_doc_lastname;
				$callData[] = $row->heatpad_doc_address;
				$callData[] = $row->heatpad_doc_city;
				$callData[] = $row->heatpad_doc_state;
				$callData[] = $row->heatpad_doc_zip;
				$callData[] = $row->heatpad_doc_npi;
				$callData[] = (strpos($row->heatpad_doc_phone, '+1') !== false) ? $row->heatpad_doc_phone : '+1' . $row->heatpad_doc_phone;
				$callData[] = (strpos($row->heatpad_doc_fax, '+1') !== false) ? $row->heatpad_doc_fax : '+1' . $row->heatpad_doc_fax;
				$callData[] = $row->heatpad_med_id;
				$callData[] = $row->heatpad_data_entered;
				$callData[] = $row->heatpad_call_back_num;
				$callData[] = $row->heatpad_repcode;
//                $callData[] = '<a href="' . base_url('admin/Owner/campaign_edit_data/' . $row->IDmask) . '/mask" >Edit</a>
//									<div class="dropdown" >
//                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
//                                                Change Status
//                                            </button>
//                                        <div class="dropdown-menu">
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING ICD 10">MISSING ICD 10</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDmask . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
//                                    </div>
//                                    </div>';
				$data[] = $callData;
			}
		}
        else if($prefix=='hipaa')
        {
            foreach ($callData as $row) {

                $used_products = "";
                $callData = array();
//                $status = "";
                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDhipaa . '" ></center>';
                $callData[] = strtoupper(isset($row->hipaa_status) ? $row->hipaa_status=='dupe' ? 'DUPE': '': '');
                $callData[] = $row->hipaa_firstname;
                $callData[] = $row->hipaa_lastname;
                $callData[] = $row->hipaa_address;
                $callData[] = $row->hipaa_city;
                $callData[] = $row->hipaa_state;
                $callData[] = $row->hipaa_zip;
                $callData[] = $row->hipaa_dob;
                $callData[] = (strpos($row->hipaa_phone, '+1') !== false) ? $row->hipaa_phone : '+1' . $row->hipaa_phone;
                $callData[] = $row->hipaa_doc_firstname;
                $callData[] = $row->hipaa_doc_lastname;
                $callData[] = $row->hipaa_doc_address;
                $callData[] = $row->hipaa_doc_city;
                $callData[] = $row->hipaa_doc_state;
                $callData[] = $row->hipaa_doc_zip;
                $callData[] = (strpos($row->hipaa_doc_phone, '+1') !== false) ? $row->hipaa_doc_phone : '+1' . $row->hipaa_doc_phone;
                $callData[] = (strpos($row->hipaa_doc_fax, '+1') !== false) ? $row->hipaa_doc_fax : '+1' . $row->hipaa_doc_fax;
                $callData[] = $row->hipaa_doc_npi;
                $callData[] = $row->hipaa_med_id;
                $callData[] = $row->hipaa_data_entered;
                $callData[] = $row->hipaa_call_back_num;
                $callData[] = $row->hipaa_repcode;
                $data[] = $callData;

                $counter++;
            }
        }
        else if($prefix=='cgmdiab') {

            foreach ($callData as $row) {
                $callData = array();
//                $status = "";
//                if ($row->cgmdiab_status == 'Good') {
//                    $status = "<span class='btn btn-sm btn-success'>GOOD</span>";
//                } else if ($row->cgmdiab_status == 'Deleted') {
//                    $status = "<span class='btn btn-sm btn-danger'>DELETED</span>";
//                } else if ($row->cgmdiab_status == 'Rejected') {
//                    $status = "<span class='btn btn-sm btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->cgmdiab_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-sm btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->cgmdiab_status == 'Sent') {
//                    $status = "<span class='btn btn-sm btn-info' >SENT</span>";
//                } else if ($row->cgmdiab_status == 'Completed') {
//                    $status = "<span class='btn btn-sm btn-primary'  >Completed</span>";
//                }
//                else if ($row->cgmdiab_status == 'Not_sent') {
//                    $status = "<span class='btn btn-sm btn-danger'  >Not sent</span>";
//                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDcgmdiab . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->cgmdiab_reason);
                $callData[] = strtoupper(isset($row->cgmdiab_status) ? "DUPE" : '');
                $callData[] = $row->cgmdiab_firstname;
                $callData[] = $row->cgmdiab_lastname;
                $callData[] = $row->cgmdiab_address;
                $callData[] = $row->cgmdiab_city;
                $callData[] = $row->cgmdiab_state;
                $callData[] = $row->cgmdiab_zip;
                $callData[] = $row->cgmdiab_email;
                $callData[] = $row->cgmdiab_dob;

                $callData[] = (strpos($row->cgmdiab_phone, '+1') !== false) ? $row->cgmdiab_phone : '+1' . $row->cgmdiab_phone;
//                $callData[] = $row->cgmdiab_diabetes;
//                $callData[] = $row->cgmdiab_visit_doc;
//                $callData[] = $row->cgmdiab_insulin_pum;
//                $callData[] = $row->cgmdiab_blood_sugar;
                $callData[] = $row->cgmdiab_doc_firstname;
                $callData[] = $row->cgmdiab_doc_lastname;
                $callData[] = $row->cgmdiab_doc_address;
                $callData[] = $row->cgmdiab_doc_city;
                $callData[] = $row->cgmdiab_doc_state;
                $callData[] = $row->cgmdiab_doc_zip;
                $callData[] = $row->cgmdiab_doc_npi;
                $callData[] = (strpos($row->cgmdiab_doc_phone, '+1') !== false) ? $row->cgmdiab_doc_phone : '+1' . $row->cgmdiab_doc_phone;
                $callData[] = (strpos($row->cgmdiab_doc_fax, '+1') !== false) ? $row->cgmdiab_doc_fax : '+1' . $row->cgmdiab_doc_fax;
//                $callData[] = $row->cgmdiab_patient_ack;
//                $callData[] = $row->cgmdiab_finan_aggrem;
//                $callData[] = $row->cgmdiab_contact_me;
//                $callData[] = $row->cgmdiab_coinsurance;
                $callData[] = $row->cgmdiab_medicare;
				$callData[] = $row->cgmdiab_insurance_com_id;
				$callData[] = $row->cgmdiab_insurance_com;
//                $callData[] = $row->cgmdiab_IDgroup;
                $callData[] = $row->cgmdiab_date_entered;
//                $callData[] = $row->cgmdiab_IDtrack;
//                $callData[] = $row->cgmdiab_IDsite;
				$callData[] = $row->cgmdiab_call_back_num;
				$callData[] = $row->cgmdiab_repcode;
//                $callData[] = $row->cgmdiab_recording;
//                $callData[] = '<a href="' . base_url('admin/Owner/campaign_edit_data/' . $row->IDcgmdiab) . '/cgmdiab" >Edit</a>
//									<div class="dropdown" >
//                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
//                                                Change Status
//                                            </button>
//                                        <div class="dropdown-menu">
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Good/GOOD">GOOD</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REJECTED">REJECTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Completed/COMPLETED">COMPLETED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/SENT">SENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/Rx Additional">Rx Additional</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX1">DR. VoicemailX1</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX2">DR. VoicemailX2</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Sent/DR. VoicemailX3">DR. VoicemailX3</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/Patient Not Diabetic">Patient Not Diabetic</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/SNS ON FILE">SNS ON FILE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO GOOD PPO INVALID">NO GOOD PPO INVALID</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/OTHER INSURANCE PRIMARY">OTHER INSURANCE PRIMARY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INSURANCE TERMINATED">INSURANCE TERMINATED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/HMO POLICY">HMO POLICY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO DME COVERAGE">NO DME COVERAGE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/NO MEDICAL BENEFITS">NO MEDICAL BENEFITS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RESIDES IN SKILLED NURSING FACILITY">RESIDES IN SKILLED NURSING FACILITY(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID INSURANCE">INVALID INSURANCE</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DOESN’T MEET INSURANCE CRITERIA">DOESN’T MEET INSURANCE CRITERIA</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/AUTHORIZATION REQUEST DENIED">AUTHORIZATION REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX REQUEST DENIED">RX REQUEST DENIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/REFUSED">REFUSED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Cannotreach/CAN’T REACH">CAN’T REACH</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT NEEDS APPOINTMENT">PATIENT NEEDS APPOINTMENT</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PT NOT INTERESTED">PT NOT INTERESTED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RX PENDING">RX PENDING</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MD NOT PECOS CERTIFIED">MD NOT PECOS CERTIFIED</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INVALID DOCTOR INFO">INVALID DOCTOR INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/INCORRECT DOCTOR">INCORRECT DOCTOR</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/DUPLICATE ENTRY">DUPLICATE ENTRY</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rxadditional/MISSING ICD 10">MISSING ICD 10</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING CLINICALS">MISSING CLINICALS</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/MISSING INSURANCE INFO">MISSING INSURANCE INFO</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/UNSUCCESSFUL FAX">UNSUCCESSFUL FAX(</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/RETURN TO SENDER">RETURN TO SENDER</a>
//                                        <a class="dropdown-item" href="' . base_url('admin/Owner/update_data/') . $row->IDcgmdiab . "/" . $this->uri->segment(4) . '/Rejected/PATIENT DECEASED">PATIENT DECEASED</a>
//                                    </div>
//                                    </div>';
                $data[] = $callData;
            }
        }

        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);

    }


   public function campaign_edit_data($IDcampaign,$prefix)
    {
        $this->load->view('include/head');
        $data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
        $check_authen = $this->Admin_model->check_authencity($IDcampaign,$prefix);

            $data['IDcampaign'] = $IDcampaign;
            $data['prefix'] = $prefix;
            $this->load->view("owner/".$prefix."_data_edit",$data);

    }

	 public function campaign_update_data($IDcampaign,$prefix)
    {
        $table = "";
        if($prefix == "aurbrac")
        {
            $table = "t_aurtho_brace";
        }
        else if($prefix = "cgmdiab")
        {
            $table = "t_cgm_diabetic";
        }
        $check_authen = $this->Admin_model->check_authencity($IDcampaign,$prefix);

            $data = $this->input->post();
            $IDcampaign = $this->Admin_model->campaign_update_data($data,$IDcampaign,$prefix);
            $this->session->set_flashdata("data_saved","Data edited successfully");
            redirect($_SERVER['HTTP_REFERER']);

    }


}

