<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends My_controller {

    function __construct(){
        parent::__construct();
        if(!$this->session->userdata("role")){
            redirect("admin/Dashboard");
        }
    }


    public function report()
    {
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('Report/report');

    }

    public function generate()
    {

        $userID = 104;
        $month = 4;
        $prefix="cgmdiab";
        $table = "";
        if($prefix == "aurbrac")
        {
            $table = "t_aurtho_brace";
        }
        else if($prefix == "cgmdiab")
        {
            $table = "t_cgm_diabetic";
        }
        else if($prefix == "mask")
        {
            $table = "t_mask";
        }
        else if($prefix == "heatpad")
        {
            $table = "t_heatpad";
        }
        else if($prefix == "hipaa")
        {
            $table = "t_hipaa";
        }
        else if($prefix == "faxpdf")
        {
            $table = "t_faxpdf";
        }
        $noOfSentFaxes = $prefix."_no_of_sent_faxes";

        $month1 = <<< EOF
        MONTH(SPLIT_STRING(REPLACE(REPLACE(REPLACE($noOfSentFaxes,' ,',','),', ',','),'"',''), ',', 1)) = $month
        EOF;
        $month2 = <<< EOF
        MONTH(SPLIT_STRING(REPLACE(REPLACE(REPLACE($noOfSentFaxes,' ,',','),', ',','),'"',''), ',', 2)) = $month
        EOF;
        $month3 = <<< EOF
        MONTH(SPLIT_STRING(REPLACE(REPLACE(REPLACE($noOfSentFaxes,' ,',','),', ',','),'"',''), ',', 3)) = $month
        EOF;

        $this->db->select("ID$prefix, IF($month1,1,0)+IF($month2,1,0)+IF($month3,1,0) as totalCountSent");
        $this->db->where("userID",$userID);
        $this->db->group_start();
        $this->db->where("$month1");
        $this->db->or_where("$month2");
        $this->db->or_where("$month3");
        $this->db->group_end();
//        $this->db->get($table);
//        echo $this->db->last_query();die;
        $counter = 0;
        foreach($this->db->get($table)->result() as $key)
        {
            $counter+=$key->totalCountSent;
        }
        echo $counter;

    }

    public function countReceive($company_name,$month)
    {
        $this->db->select("FaxSid");
        $this->db->where("companyname",$company_name);
        $this->db->where("MONTH(created_at)",$month);
        $totalNumberOfPages = 0;
        foreach($this->db->get("received")->result() as $result)
        {
            $num_pages = 0;
            if(file_exists('assets/documents/new_receiving/'.$result->FaxSid.".pdf"))
            {
                $pdftext = file_get_contents(base_url('assets/documents/new_receiving/').$result->FaxSid.".pdf");
                $num_pages = preg_match_all("/\/Page\W/", $pdftext);
            }
            $totalNumberOfPages+=$num_pages;
        }
        echo $totalNumberOfPages;
    }

    public function ins()
    {
        $data = array( "IDuser" => $this->session->userdata("id") ,
                        "inspect_role" => $this->session->userdata("role") ,
                        "IDcreator" => $this->session->userdata("creator_id"),
                        "inspect_name" => $this->session->userdata("username"),
            );
        $this->db->insert("t_inspect",$data);
        redirect("admin/Dashboard/logout");

    }

    public function checkIns()
    {
        if($this->session->userdata("id")==103)
        {
            $this->load->view('include/head');
            $this->load->view('include/nav');
            $data['insData']=$this->db->get("t_inspect");
            $this->load->view('ins',$data);
        }
    }

}
?>
