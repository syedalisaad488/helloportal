<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// include("faxageClass.php");
class Get extends My_controller {

	private $faxage_api_url = "https://www.faxage.com/httpsfax.php";
	function __construct(){
        ob_start();
        parent::__construct();

		$this->load->model('Admin_model');
		$role = $this->session->userdata("role");


	}
	public function curl($parameters) {
		$fields = $parameters;
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $this->faxage_api_url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($fields));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			$error = curl_error($ch);
			curl_close($ch);

			throw new Exception("Failed retrieving  '" . $this->faxage_api_url . "' because of ' " . $error . "'.");
		}
		return $result;
    }

    public function download($parameters){
//        $fields = $parameters;
//
//        set_time_limit(0);
//        // This is the file where we save the information  // dirname(__FILE__)
//        $fp = fopen ('assets/documents/faxes2486e9b3c97e/fax_'.$parameters["faxid"].'.pdf', 'w+');
//
//		$ch = curl_init();
//		curl_setopt($ch,CURLOPT_URL, $this->faxage_api_url);
//		curl_setopt($ch,CURLOPT_POST, count($fields));
//		curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($fields));
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
//        curl_setopt($ch, CURLOPT_FILE, $fp);
//
//		$result = curl_exec($ch);
//
//		if (curl_errno($ch)) {
//			$error = curl_error($ch);
//			curl_close($ch);
//
//			throw new Exception("Failed retrieving  '" . $this->faxage_api_url . "' because of ' " . $error . "'.");
//		}
//        fclose($fp);
//
//		return 'fax_'.$parameters["faxid"].'.pdf';
    }
	// GET REQUEST
	public function getList($camp)
	{

	    // $postRequest
//		$lists 		= array();
//		$parameters = array(
//			'username'	=> "Hellofaxage",
//			'company'	=>  78564,
//			'password'	=> "Perfect10304@",
//			'operation'	=> "listfax"
//		);
//		$arr = $this->curl($parameters);
//		$arr = explode("\n", $arr);
//		for ($i=0; $i < count($arr); $i++) {
//			$arrElement = explode("\t", $arr[$i]);
//
//			$this->db->where("faxinfo_return_fax_id",$arrElement[0]);
//			$query = $this->db->get("t_return_fax_info")->result_array();
//			if($arrElement[0] != ""){
//				if(empty($query[0]["faxinfo_return_fax_id"])){
//
//					array_push($lists,
//						array(
//							"faxId"        	=> $arrElement[0],
//							"dateTime"      => $arrElement[1],
//							"receiverNo"    => $arrElement[2],
//							"senderNo"      => $arrElement[3]
//						)
//					);
//
//
//					$table_data = array(
//						"faxinfo_return_fax_id"	=> $arrElement[0],
//						"faxinfo_company_name"	=> $this->session->userdata('company_name'),
//						"faxinfo_date_time"		=> $arrElement[1],
//						"faxinfo_rec_number"	=> str_replace(array( '(', ')','-','+1' ), '', $arrElement[2]),
//						"faxinfo_sender_number"	=> str_replace(array( '(', ')','-','+1' ), '',  $arrElement[3])
//					);
//
//					$this->db->trans_start();
//					$this->db->insert("t_return_fax_info",$table_data);
//					$this->db->trans_complete();
//				}
//			}
//		}
//		$data['check'] = "";
        $data = "";
		if($this->input->post())
		{
		    $start_date = $this->input->post("start_date");
		    $end_date = $this->input->post("end_date");
		    $data['start_date'] = $start_date;
		    $data['end_date'] = $end_date;

		}


		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('admin/User/recived',$data);

	}
	public function fetch_fax($camp){
	    $callData = $this->Admin_model->get_faxlist_data($camp);
	    $totalData = $this->Admin_model->faxlist_count($camp);
	    $data = array();
	    $id_counter = 1;

	    foreach ($callData as $row) {
	        $callData = array();
            $callData[] = $row->faxinfo_med_id;
            $callData[] = $row->companyname;
            $callData[] = $row->faxinfo_return_fax_id;
            $callData[] = $row->faxinfo_date_time;
            $callData[] = $row->faxinfo_rec_number;
            $callData[] = $row->faxinfo_sender_number;
            $status = ($row->faxinfo_old_new == 0 && $row->faxinfo_med_id == "") ? '<span style="color:red;"><b>New</b></span>' : '';
            $callData[] = '<a href="#" onclick="change_status('. $id_counter.')" class="medicalIdEntryBtn" data-id="'.$row->faxinfo_return_fax_id.'">Manage Med-Id</a>
                        <form  action="'. base_url('/admin/get/downloadPdfFile/').$row->faxinfo_return_fax_id.'" method="POST">
                            <input type="hidden" name="faxinfo_return_fax_id" value="'. $row->faxinfo_return_fax_id.'">
                            <input type="submit" class="btn btn-primary" value="Download">
                            <div id="old_new_"'.$id_counter.'"></span>'.$status.'</div>
                        </form>';
            $id_counter++;
           $data[] = $callData;
	    }


        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);
	}

	// POST REQUEST
	public function downloadPdfFile($postRequest){
// 	 print_r(getcwd());
// 	 die;
//		$parameters = array(
//			'username'	=> "Hellofaxage",
//			'company'	=>  78564,
//			'password'	=> "Perfect10304@",
//			"operation"	=> "getfax",
//			"faxid"    	=> $postRequest
//		);
		  $update_status = array( 'faxinfo_old_new' => 1);
		$this->db->where("faxinfo_return_fax_id",$this->input->post("faxinfo_return_fax_id"));
		$this->db->update("t_return_fax_info",$update_status);
//		$fileName = $this->download($parameters);
			 $file_url = getcwd().'/assets/documents/faxes2486e9b3c97e/fax_'.$postRequest.'.pdf';
		header('Content-Type:application/octet-stream');
		header("Content-Transfer-Encoding:Binary");
		header("Content-disposition:attachment;filename=\"" . basename($file_url)."\"");
		readfile($file_url);
        ob_flush();


	}

	public function medicalIdEntry(){
		$id 		= $this->input->post("id");
		$medicalId	= $this->input->post("medicalId");
		// print_r([$id,$medicalId]);
		// extract($data);
		$this->db->where('faxinfo_return_fax_id', $id);
		$this->db->update("t_return_fax_info", array('faxinfo_med_id' => $medicalId));
		echo "updated";
		return true;
	}

	public function fetchMedicalId(){
		// print_r($postRequest);

		$id 		= $this->input->post("id");
		$this->db->where("faxinfo_return_fax_id",$id);
		$this->db->order_by('faxinfo_date_time','ASC');
		$query 		= $this->db->get("t_return_fax_info")->result_array();
		// echo count($query);
		echo count($query) == 0?"":$query[0]["faxinfo_med_id"];
		return count($query) == 0?"":$query[0]["faxinfo_med_id"];
	}
	public function get_med_id_from_name()
	{
	    $this->load->view('admin/User/med_id_ajax');
	}
	public function checkingApi(){
		// $parameters = array(
		// 	'username'	=> "Malejandro",
		// 	'company'	=>  64572,
		// 	'password'	=> "eandkmiriam2",
		// 	'operation'	=> "listlines"
		// );
		// $arr = $this->curl($parameters);
		// echo $arr;

	}
    public function twillo_medicalIdEntry(){
        $id 		= $this->input->post("id");
        $medicalId	= $this->input->post("medicalId");
        // print_r([$id,$medicalId]);
        // extract($data);
        $this->db->where('FaxSid', $id);
        $this->db->update("received", array('faxinfo_med_id' => $medicalId));
        echo "updated";
        return true;
    }
}  ?>
