<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viewer extends My_controller {

	function __construct(){
		parent::__construct();
				$this->load->helper('url');

		$role = $this->session->userdata("role");
		if($role!="Viewer")
		{
			redirect("admin/Dashboard");
		}
		$this->load->library('upload');
	    $this->load->library('pdf');
	    $this->load->library('form_validation');
		$this->load->model("Admin_model");
		
	}
	public function index( )
	{
            $table="t_aurtho_brace";
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/viewer/aurbrac_data');
	}
	
    public function campaign_list($prefix,$type)
    {
        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/viewer/aurbrac_data');
        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $this->load->view('include/qa_nav');
            $this->load->view('include/nav');
            $this->load->view('admin/viewer/cgmdiab_data');
        }
    }
    public function fetch_camp_data($prefix,$type){



        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'aurbrac_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');

        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'cgmdiab_medicare');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'cgmdiab_medicare');

        }



        $data = array();
        if($prefix=='aurbrac')
        {
            foreach ($callData as $row) {
                $callData = array();
                $status = "";
                if ($row->aurbrac_status == 'Good') {
                    $status = "<span class='btn btn-success'>GOOD</span>";
                } else if ($row->aurbrac_status == 'Deleted') {
                    $status = "<span class='btn btn-danger'>DELETED</span>";
                } else if ($row->aurbrac_status == 'Rejected') {
                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
                } else if ($row->aurbrac_status == 'Rxadditional') {
                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
                } else if ($row->aurbrac_status == 'Sent') {
                    $status = "<span class='btn btn-info' >SENT</span>";
                } else if ($row->aurbrac_status == 'Completed') {
                    $status = "<span class='btn btn-primary'  >Completed</span>";
                }

             
                $callData[] = $status;
                $callData[] = urldecode($row->aurbrac_reason);
                $callData[] = $row->aurbrac_sns;

                $callData[] = $row->aurbrac_firstname;
                $callData[] = $row->aurbrac_lastname;
                $callData[] = $row->aurbrac_address;
                $callData[] = $row->aurbrac_city;
                $callData[] = $row->aurbrac_state;
                $callData[] = $row->aurbrac_zip;
                $callData[] = $row->aurbrac_email;
                $callData[] = $row->aurbrac_dob;
                $callData[] = (strpos($row->aurbrac_phone, '+1') !== false) ? $row->aurbrac_phone : '+1' . $row->aurbrac_phone;
                $callData[] = $row->aurbrac_gender;
                $callData[] = $row->aurbrac_ssn;
                $callData[] = ($row->aurbrac_back == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_knee == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_wrist == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_ankle == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_shoulder == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_l_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_r_elbow == 1 ? 'Yes' : 'No');
                $callData[] = ($row->aurbrac_hip == 1 ? 'Yes' : 'No');
                $callData[] = $row->aurbrac_doc_firstname;
                $callData[] = $row->aurbrac_doc_lastname;
                $callData[] = $row->aurbrac_doc_address;
                $callData[] = $row->aurbrac_doc_city;
                $callData[] = $row->aurbrac_doc_state;
                $callData[] = $row->aurbrac_doc_zip;
                $callData[] = $row->aurbrac_doc_npi;
                $callData[] = (strpos($row->aurbrac_doc_phone, '+1') !== false) ? $row->aurbrac_doc_phone : '+1' . $row->aurbrac_doc_phone;
                $callData[] = (strpos($row->aurbrac_doc_fax, '+1') !== false) ? $row->aurbrac_doc_fax : '+1' . $row->aurbrac_doc_fax;
                $callData[] = $row->aurbrac_pcp_within_past;
                $callData[] = $row->aurbrac_med_id;
                $callData[] = $row->aurbrac_insurance_company;
                $callData[] = $row->aurbrac_IDmember;
                $callData[] = $row->aurbrac_IDgroup;
                $callData[] = $row->aurbrac_data_entered;
                $callData[] = $row->aurbrac_IDtrack;
                $callData[] = $row->aurbrac_IDsite;
                $callData[] = $row->aurbrac_repcode;
                $callData[] = $row->aurbrac_record_file;
                $callData[] = "";
                
                $data[] = $callData;
            }
        }
        else if($prefix=='cgmdiab') {

            foreach ($callData as $row) {
                $callData = array();
                $status = "";
                if ($row->cgmdiab_status == 'Good') {
                    $status = "<span class='btn btn-success'>GOOD</span>";
                } else if ($row->cgmdiab_status == 'Deleted') {
                    $status = "<span class='btn btn-danger'>DELETED</span>";
                } else if ($row->cgmdiab_status == 'Rejected') {
                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
                } else if ($row->cgmdiab_status == 'Rxadditional') {
                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
                } else if ($row->cgmdiab_status == 'Sent') {
                    $status = "<span class='btn btn-info' >SENT</span>";
                } else if ($row->cgmdiab_status == 'Completed') {
                    $status = "<span class='btn btn-primary'  >Completed</span>";
                }

          
                $callData[] = $status;
                $callData[] = urldecode($row->cgmdiab_reason);
                $callData[] = $row->cgmdiab_sns;
                $callData[] = $row->cgmdiab_firstname;
                $callData[] = $row->cgmdiab_lastname;
                $callData[] = $row->cgmdiab_address;
                $callData[] = $row->cgmdiab_city;
                $callData[] = $row->cgmdiab_state;
                $callData[] = $row->cgmdiab_zip;
                $callData[] = $row->cgmdiab_email;
                $callData[] = $row->cgmdiab_dob;

                $callData[] = (strpos($row->cgmdiab_phone, '+1') !== false) ? $row->cgmdiab_phone : '+1' . $row->cgmdiab_phone;
                $callData[] = $row->cgmdiab_sex;
                $callData[] = $row->cgmdiab_ssn;
                $callData[] = $row->cgmdiab_diabetes;
                $callData[] = $row->cgmdiab_visit_doc;
                $callData[] = $row->cgmdiab_insulin_pum;
                $callData[] = $row->cgmdiab_blood_sugar;
                $callData[] = $row->cgmdiab_doc_firstname;
                $callData[] = $row->cgmdiab_doc_lastname;
                $callData[] = $row->cgmdiab_doc_address;
                $callData[] = $row->cgmdiab_doc_city;
                $callData[] = $row->cgmdiab_doc_state;
                $callData[] = $row->cgmdiab_doc_zip;
                $callData[] = $row->cgmdiab_doc_npi;
                $callData[] = (strpos($row->cgmdiab_doc_phone, '+1') !== false) ? $row->cgmdiab_doc_phone : '+1' . $row->cgmdiab_doc_phone;
                $callData[] = (strpos($row->cgmdiab_doc_fax, '+1') !== false) ? $row->cgmdiab_doc_fax : '+1' . $row->cgmdiab_doc_fax;
                $callData[] = $row->cgmdiab_patient_ack;
                $callData[] = $row->cgmdiab_finan_aggrem;
                $callData[] = $row->cgmdiab_contact_me;
                $callData[] = $row->cgmdiab_coinsurance;
                $callData[] = $row->cgmdiab_medicare;
                $callData[] = $row->cgmdiab_insurance_com;
                $callData[] = $row->cgmdiab_IDmember;
                $callData[] = $row->cgmdiab_IDgroup;
                $callData[] = $row->cgmdiab_date_entered;
                $callData[] = $row->cgmdiab_IDtrack;
                $callData[] = $row->cgmdiab_IDsite;
                $callData[] = $row->cgmdiab_repcode;
                $callData[] = $row->cgmdiab_patient_url;
                $callData[] = $row->cgmdiab_recording;
                $callData[] = "";
            
                $data[] = $callData;
            }
        }

        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);

    }
	




}

