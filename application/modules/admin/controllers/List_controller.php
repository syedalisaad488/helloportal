<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;
class List_controller extends My_controller {

	function __construct(){
		parent::__construct();
		$role = $this->session->userdata("role");
		if($role=="User" || $role=="Agent")
		{
			
		}else{
		    redirect("admin/Dashboard");
		}
		passwordAuthentication();
		require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
		$this->load->model("Admin_model");
		$this->load->library('csvimport');
		$this->load->library('upload');
		$this->load->library('pdf');



	}

	public function received()
    {
        // log the URL of the PDF received in the fax
        log_message('info', $this->input->post("MediaUrl"));

        // Respond with empty 200/OK to Twilio
        $this->set_status_header(200);
        $this->output->set_output('');
    }
	public function index()
	{
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('User/index');

	}
		public function save_agent($id=null)
	{
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$name = $this->input->post('name');
		$password = $this->input->post('password');
		$role = $this->input->post('role');
		if($id==null){
			if($name!=null){
				$this->Admin_model->save_agent($name,$password,$role,$id);
			}
		$query['data']=$this->Admin_model->get_agent_data();
		$this->load->view('User/add_agent',$query);
		}else{
			if($name!=null){
				 $this->Admin_model->save_agent($name,$password,$role,$id);
				 $reload='1';
			}
			if(isset($reload)){
				$query['data']=$this->Admin_model->get_agent_data();
		$this->load->view('User/add_agent',$query);

			}else{
				$query['data']=$this->Admin_model->get_agent_data($id);
		$this->load->view('User/edit_agent',$query);

			}

		}
	}

    public function delete($id=null)
	{
	$this->Admin_model->delete($table='login',$id);
	$this->load->save_agent();
	}


//      $sid    = "AC720fa9d520db09505609c091b9152f7f";
// 		$token  = "2e11a5a6e537609375d68fa63d869bb3";


	public function update_data($IDclient)
	{
	    $data = $this->input->post();
	    $this->Admin_model->update_client_data($data,$IDclient);
        $this->save_html_to_pdf($IDclient);
	    redirect("admin/User/data_list");
	}

	public function edit_client_data($IDclient)
	{
		$data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('admin/User/client_edit',$data);
	}

	public function show_client_data($IDclient)
	{
		$data['client_data'] = $this->Admin_model->get_specific_client_data($IDclient);
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('user/client_details',$data);
	}

	public function campaign_save_html_to_pdf($IDcampaign,$prefix,$table)
	{
	   //dump($this->dompdf);
	    $data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table);
	    $this->load->view("admin/templates/$prefix/fax_temp_$prefix",$data);
	   	$dd  = $this->output->get_output();
        $this->dompdf->load_html($dd);
        $this->dompdf->setPaper('A4', 'portrait');
        $this->dompdf->render();
        $output =  $this->dompdf->output();

   		file_put_contents("assets/documents/faxes/fax_of_sending_".$prefix."_".$IDcampaign.".pdf", $output);



	}

	public function campaign_write_fax($IDcampaign,$prefix,$table_name)
	{
    	$data['fax'] = $this->Admin_model->get_fax($IDcampaign,$prefix,$table_name);
		$this->load->view("admin/fax_temp_$prefix",$data);
	}

	public function write_fax($id)
	{
	    	$data['fax'] = $this->Admin_model->get_fax($id);
			$this->load->view('admin/fax_template',$data);
	}

	public function cgmdiab_data_list()
	{
		$IDuser = $this->session->userdata("id");
		$data['client_data'] = $this->Admin_model->get_client_cgmdiab_data_for_user($IDuser);
		$this->load->view('include/head');
		$this->load->view('include/nav');
		$this->load->view('admin/User/data_list/cgmdiab_data',$data);
	}





	public function campaign_data_entry($prefix)
	{
    	$this->load->view('include/head');
		$data['prefix'] = $prefix;
		$this->load->view("User/data/".$prefix."_data_entry",$data);
	}

	public function campaign_edit_data($IDcampaign,$prefix)
	{
	    $this->load->view('include/head');

		$data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
		$data['IDcampaign'] = $IDcampaign;
		$data['prefix'] = $prefix;
		$this->load->view("User/data/".$prefix."_data_edit",$data);
	}

	public function campaign_show_data($IDcampaign,$prefix)
	{
		$data['campaign_data'] = $this->Admin_model->get_specific_campaign_data($IDcampaign,$prefix);
		$data['IDcampaign'] = $IDcampaign;
		$data['prefix'] = $prefix;
		$this->load->view("User/data/".$prefix."_data_show",$data);
	}

	public function campaign_save_data($prefix)
	{

	    $table = "";
	    if($prefix == "aurbrac")
	    {
	        $table = "t_aurtho_brace";
	    }
	    else if($prefix == "cgmdiab")
	    {
	        $table = "t_cgm_diabetic";
	    }
		$data = $this->input->post();
	    $IDcampaign = $this->Admin_model->campaign_save_data($data,$prefix);

	    $this->session->set_flashdata("data_saved","Data saved successfully");
	    redirect($_SERVER['HTTP_REFERER']);
	}


	public function campaign_update_data($IDcampaign,$prefix)
	{
	     $table = "";
	    if($prefix == "aurbrac")
	    {
	        $table = "t_aurtho_brace";
	    }
	    else if($prefix = "cgmdiab")
	    {
	        $table = "t_cgm_diabetic";
	    }
		$data = $this->input->post();
         $IDcampaign = $this->Admin_model->campaign_update_data($data,$IDcampaign,$prefix);
	   $this->session->set_flashdata("data_saved","Data edited successfully");
	    redirect($_SERVER['HTTP_REFERER']);
	}


	public function campaign_list($prefix,$type)
		{
			if($prefix=='aurbrac'){
				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/aurbrac_data');
			}
			else if($prefix=='cgmdiab'){

				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/cgmdiab_data');
			}
			else if($prefix=='mask'){

				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/mask_data');
			}
			else if($prefix=='heatpad'){

				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/heatpad_data');
			}else if($prefix=='hipaa'){

				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/hipaa_data');
			}
            else if($prefix=='faxpdf'){

                $this->load->view('include/head');
                $this->load->view('include/nav');
                $this->load->view('admin/User/data/faxpdf_data');
            }
		}





    public function campaign_qa_list($type)
		{
			if($type=='aurbrac'){
				$IDuser = $this->session->userdata("id");
				$table="t_aurtho_brace";
				$data['client_data'] = $this->Admin_model->get_data_list($type,$table,'aurbrac_med_id');
				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/aurbrac_data',$data);
			}
			else if($type=='cgmdiab'){
				$table="t_cgm_diabetic";
				$IDuser = $this->session->userdata("id");
				$data['client_data'] = $this->Admin_model->get_data_list($type,$table,'cgmdiab_medicare');
				$this->load->view('include/head');
				$this->load->view('include/nav');
				$this->load->view('admin/User/data/cgmdiab_data',$data);
			}
		}

    public function fetch_camp_data($prefix,$type){



        if($prefix=='aurbrac'){
            $table="t_aurtho_brace";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'aurbrac_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'aurbrac_med_id');

        }
        else  if($prefix=='mask'){
			$table="t_mask";
			$callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'mask_med_id');
			$totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'mask_med_id');

		}
        else  if($prefix=='heatpad'){
			$table="t_heatpad";
			$callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'heatpad_med_id');
			$totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'heatpad_med_id');

		}
        else  if($prefix=='hipaa'){
            $table="t_hipaa";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'hipaa_med_id');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'hipaa_med_id');

        }
        else if($prefix=='cgmdiab'){
            $table="t_cgm_diabetic";
            $callData = $this->Admin_model->call_camp_list($prefix,$type,$table,'cgmdiab_medicare');
            $totalData = $this->Admin_model->count_all_data($prefix,$type,$table,'cgmdiab_medicare');

        }
        else if($prefix=='faxpdf'){
            $callData = $this->Admin_model->call_camp_list_faxpdf($type);
            $totalData = $this->Admin_model->count_all_data_faxpdf($type);
        }



        $data = array();
        if($prefix=='aurbrac')
        {
            $counter = 1;
            foreach ($callData as $row) {
                $agentID= $this->agent_name($row->agentID);

                $used_products = "";
                $callData = array();
//                $status = "";

                if($row->aurbrac_back == 1){ $used_products .= "Back/"; }
                if($row->aurbrac_l_knee == 1){ $used_products .= "Left Knee/"; }
                if($row->aurbrac_r_knee == 1){ $used_products .= "Right Knee/"; }
                if($row->aurbrac_l_wrist == 1){ $used_products .= "Left Wrist/"; }
                if($row->aurbrac_r_wrist == 1){ $used_products .= "Right Wrist/"; }
                if($row->aurbrac_l_ankle == 1){ $used_products .= "Left Ankle/"; }
                if($row->aurbrac_r_ankle == 1){ $used_products .= "Right Ankle/"; }
                if($row->aurbrac_l_shoulder == 1){ $used_products .= "Left Shoulder/"; }
                if($row->aurbrac_r_shoulder == 1){ $used_products .= "Right Shoulder/"; }
                if($row->aurbrac_l_elbow == 1){ $used_products .= "Left Elbow/"; }
                if($row->aurbrac_r_elbow == 1){ $used_products .= "Right Elbow/"; }
                if($row->aurbrac_r_hip == 1){ $used_products .= "Hip/"; }
                if($row->aurbrac_l_hip == 1){ $used_products .= "Hip/"; }


//                $edit_btn = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDaurbrac) . '/aurbrac" >Edit</a>';
//                $sent_fax_btn = '<a tabindex="1" style="text-decoration: underline;" href="#" onclick=show_sent_fax('.$counter.') >Show Sent Fax</a>
//                                 <input type="hidden" id="IDcampaign_sent_'.$counter.'"  value="'.$row->IDaurbrac.'" >
//                                 <input type="hidden" id="used_products_'.$counter.'"  value="'.$used_products.'">';


//                if ($row->aurbrac_status == 'Good') {
//                    $status = "<span class='btn btn-success'>GOOD</span>";
//                } else if ($row->aurbrac_status == 'Deleted') {
//                    $status = "<span class='btn btn-danger'>DELETED</span>";
//                } else if ($row->aurbrac_status == 'Rejected') {
//                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->aurbrac_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->aurbrac_status == 'Sent') {
//                    $status = "<span class='btn btn-info' >SENT</span>";
//                } else if ($row->aurbrac_status == 'Completed') {
//                    $status = "<span class='btn btn-primary'  >Completed</span>";
//                }
//                else if ($row->aurbrac_status == 'Not_sent') {
//                    $status = "<span class='btn btn-danger'  >Not sent</span>";
//                }



                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDaurbrac . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->aurbrac_reason);
				$callData[] = strtoupper(isset($row->aurbrac_status) ? $row->aurbrac_status=='dupe' ? "DUPE" : '' : '');
				
                $DecodedTimestamp = json_decode($row->aurbrac_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
               
                $DecodedIDsSender = json_decode($row->aurbrac_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);

                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';
                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }
                
                
				$callData[] = $row->aurbrac_firstname;
				$callData[] = $row->aurbrac_lastname;
				$callData[] = $row->aurbrac_address;
				$callData[] = $row->aurbrac_city;
				$callData[] = $row->aurbrac_state;
				$callData[] = $row->aurbrac_zip;
//                $callData[] = $row->aurbrac_email;
				$callData[] = $row->aurbrac_dob;

				$callData[] = (strpos($row->aurbrac_phone, '+1') !== false) ? $row->aurbrac_phone : '+1' . $row->aurbrac_phone;
				$callData[] = $row->aurbrac_gender;
				$callData[] = $row->aurbrac_height;
				$callData[] = $row->aurbrac_weight;
				$callData[] = $row->aurbrac_pain_level;

				$callData[] = ($row->aurbrac_back == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_knee == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_knee == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_wrist == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_wrist == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_ankle == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_ankle == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_shoulder == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_shoulder == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_elbow == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_elbow == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_l_hip == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_r_hip == 1 ? 'Yes' : 'No');
				$callData[] = ($row->aurbrac_neck == 1 ? 'Yes' : 'No');
				$callData[] = $row->aurbrac_doc_firstname;
				$callData[] = $row->aurbrac_doc_lastname;
				$callData[] = $row->aurbrac_doc_address;
				$callData[] = $row->aurbrac_doc_city;
				$callData[] = $row->aurbrac_doc_state;
				$callData[] = $row->aurbrac_doc_zip;
				$callData[] = $row->aurbrac_doc_npi;
				$callData[] = (strpos($row->aurbrac_doc_phone, '+1') !== false) ? $row->aurbrac_doc_phone : '+1' . $row->aurbrac_doc_phone;
				$callData[] = (strpos($row->aurbrac_doc_fax, '+1') !== false) ? $row->aurbrac_doc_fax : '+1' . $row->aurbrac_doc_fax;
				$callData[] = $row->aurbrac_med_id;
//				$callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
				$callData[] = $row->aurbrac_data_entered;
				$callData[] = $agentID;
//				$callData[] = $row->aurbrac_insurance_company;
//				$callData[] = $row->aurbrac_IDmember;
//                $callData[] = $row->aurbrac_IDgroup;
//                $callData[] = $row->aurbrac_IDtrack;
//                $callData[] = $row->aurbrac_IDsite;
				$callData[] = $row->aurbrac_call_back_num;
				$callData[] = $row->aurbrac_fax_back;
				$callData[] = $row->aurbrac_repcode;
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";
//                $callData[] = $edit_btn;
                $data[] = $callData;

                $counter++;
            }
        }
        else if($prefix=='mask')
		{
			$counter = 1;
			foreach ($callData as $row) {
			    $agentID= $this->agent_name($row->agentID);
				$callData = array();
				$callData[] = '<center><input type="checkbox" id="checkMain" name="send_fax_data[]" value="' . $row->IDmask . '" ></center>';
				$callData[] = strtoupper(isset($row->mask_status) ? $row->mask_status=='dupe' ? 'DUPE': '': '');
				
                $DecodedTimestamp = json_decode($row->mask_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
               
                $DecodedIDsSender = json_decode($row->mask_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);

                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';
                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }
                
				$callData[] = $row->mask_firstname;
				$callData[] = $row->mask_lastname;
				$callData[] = $row->mask_address;
				$callData[] = $row->mask_city;
				$callData[] = $row->mask_state;
				$callData[] = $row->mask_zip;
				$callData[] = $row->mask_dob;
				$callData[] = (strpos($row->mask_phone, '+1') !== false) ? $row->mask_phone : '+1' . $row->mask_phone;
				$callData[] = $row->mask_doc_firstname;
				$callData[] = $row->mask_doc_lastname;
				$callData[] = $row->mask_doc_npi;
                $callData[] = $row->mask_doc_address;
                $callData[] = $row->mask_doc_city;
                $callData[] = $row->mask_doc_state;
                $callData[] = $row->mask_doc_zip;
				$callData[] = (strpos($row->mask_doc_phone, '+1') !== false) ? $row->mask_doc_phone : '+1' . $row->mask_doc_phone;
				$callData[] = (strpos($row->mask_doc_fax, '+1') !== false) ? $row->mask_doc_fax : '+1' . $row->mask_doc_fax;
				$callData[] = $row->mask_med_id;
				$callData[] = $row->mask_data_entered;
				$callData[] = $agentID;
				$callData[] = $row->mask_call_back_num;
				$callData[] = $row->mask_fax_back;
				$callData[] = $row->mask_repcode;
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";
				$data[] = $callData;
				$counter++;
			}
		}
        else if($prefix=='heatpad')
		{
			$counter = 1;
			foreach ($callData as $row) {
			    $agentID= $this->agent_name($row->agentID);

				$used_products = "";
				$callData = array();
//                $status = "";

				if($row->heatpad_back == 1){ $used_products .= "Back/"; }
				if($row->heatpad_l_knee == 1){ $used_products .= "Left Knee/"; }
				if($row->heatpad_r_knee == 1){ $used_products .= "Right Knee/"; }
				if($row->heatpad_l_wrist == 1){ $used_products .= "Left Wrist/"; }
				if($row->heatpad_r_wrist == 1){ $used_products .= "Right Wrist/"; }
				if($row->heatpad_l_ankle == 1){ $used_products .= "Left Ankle/"; }
				if($row->heatpad_r_ankle == 1){ $used_products .= "Right Ankle/"; }
				if($row->heatpad_l_shoulder == 1){ $used_products .= "Left Shoulder/"; }
				if($row->heatpad_r_shoulder == 1){ $used_products .= "Right Shoulder/"; }
				if($row->heatpad_l_elbow == 1){ $used_products .= "Left Elbow/"; }
				if($row->heatpad_r_elbow == 1){ $used_products .= "Right Elbow/"; }
				if($row->heatpad_hip == 1){ $used_products .= "Hip/"; }




				$callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDheatpad . '" ></center>';
				$callData[] = strtoupper(isset($row->heatpad_status) ? $row->heatpad_status=='dupe' ? 'DUPE': '': '');
				
                $DecodedTimestamp = json_decode($row->heatpad_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
               
                $DecodedIDsSender = json_decode($row->heatpad_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);

                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';
                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }
                
				$callData[] = $row->heatpad_firstname;
				$callData[] = $row->heatpad_lastname;
				$callData[] = $row->heatpad_address;
				$callData[] = $row->heatpad_city;
				$callData[] = $row->heatpad_state;
				$callData[] = $row->heatpad_zip;
				$callData[] = $row->heatpad_dob;
				$callData[] = (strpos($row->heatpad_phone, '+1') !== false) ? $row->heatpad_phone : '+1' . $row->heatpad_phone;
				$callData[] = $row->heatpad_doc_firstname;
				$callData[] = $row->heatpad_doc_lastname;
				$callData[] = $row->heatpad_doc_address;
				$callData[] = $row->heatpad_doc_city;
				$callData[] = $row->heatpad_doc_state;
				$callData[] = $row->heatpad_doc_zip;
				$callData[] = $row->heatpad_doc_npi;
				$callData[] = (strpos($row->heatpad_doc_phone, '+1') !== false) ? $row->heatpad_doc_phone : '+1' . $row->heatpad_doc_phone;
				$callData[] = (strpos($row->heatpad_doc_fax, '+1') !== false) ? $row->heatpad_doc_fax : '+1' . $row->heatpad_doc_fax;
				$callData[] = $row->heatpad_med_id;
				$callData[] = $row->heatpad_data_entered;
				$callData[] = $agentID;
				$callData[] = $row->heatpad_call_back_num;
				$callData[] = $row->heatpad_fax_back;	
				$callData[] = $row->heatpad_repcode;
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";
				$data[] = $callData;

				$counter++;
			}
		}
        else if($prefix=='hipaa')
        {
            $counter = 1;
            foreach ($callData as $row) {
                                $agentID= $this->agent_name($row->agentID);


                $used_products = "";
                $callData = array();
//                $status = "";






                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDhipaa . '" ></center>';
                $callData[] = strtoupper(isset($row->hipaa_status) ? $row->hipaa_status=='dupe' ? 'DUPE': '': '');
                
                $DecodedTimestamp = json_decode($row->hipaa_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
               
                $DecodedIDsSender = json_decode($row->hipaa_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);

                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';
                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }
                
                
                $callData[] = $row->hipaa_firstname;
                $callData[] = $row->hipaa_lastname;
                $callData[] = $row->hipaa_address;
                $callData[] = $row->hipaa_city;
                $callData[] = $row->hipaa_state;
                $callData[] = $row->hipaa_zip;
                $callData[] = $row->hipaa_dob;
                $callData[] = (strpos($row->hipaa_phone, '+1') !== false) ? $row->hipaa_phone : '+1' . $row->hipaa_phone;
                $callData[] = $row->hipaa_doc_firstname;
                $callData[] = $row->hipaa_doc_lastname;
                $callData[] = $row->hipaa_doc_address;
                $callData[] = $row->hipaa_doc_city;
                $callData[] = $row->hipaa_doc_state;
                $callData[] = $row->hipaa_doc_zip;
                $callData[] = (strpos($row->hipaa_doc_phone, '+1') !== false) ? $row->hipaa_doc_phone : '+1' . $row->hipaa_doc_phone;
                $callData[] = (strpos($row->hipaa_doc_fax, '+1') !== false) ? $row->hipaa_doc_fax : '+1' . $row->hipaa_doc_fax;
                $callData[] = $row->hipaa_doc_npi;
                $callData[] = $row->hipaa_med_id;
                $callData[] = $row->hipaa_data_entered;
                $callData[] = $agentID;
                $callData[] = $row->hipaa_call_back_num;
                $callData[] = $row->hipaa_fax_back;    
                $callData[] = $row->hipaa_repcode;
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";
                $data[] = $callData;

                $counter++;
            }
        }

        else if($prefix=='cgmdiab') {

            foreach ($callData as $row) {
                $agentID= $this->agent_name($row->agentID);

                $callData = array();
//                $status = "";
//                $edit_btn = '<a href="' . base_url('admin/User/campaign_edit_data/' . $row->IDcgmdiab) . '/cgmdiab" >Edit</a>';
                $sent_fax_btn = '<a tabindex="1" style="text-decoration: underline;" href="http://betapandaproject.com/fax/assets/documents/faxes/fax_of_sending_cgmdiab_'.$row->IDcgmdiab.'.pdf" target="_blank">Show Sent Fax</a>';
//                if ($row->cgmdiab_status == 'Good') {
//                    $status = "<span class='btn btn-success'>GOOD</span>";
//                } else if ($row->cgmdiab_status == 'Deleted') {
//                    $status = "<span class='btn btn-danger'>DELETED</span>";
//                } else if ($row->cgmdiab_status == 'Rejected') {
//                    $status = "<span class='btn btn-danger' style='background: darkorange '>REJECTED</span>";
//                } else if ($row->cgmdiab_status == 'Rxadditional') {
//                    $status = "<span class='btn btn-info' style='background: goldenrod ' >RX Additional</span>";
//                } else if ($row->cgmdiab_status == 'Sent') {
//                    $status = "<span class='btn btn-info' >SENT</span>";
//                } else if ($row->cgmdiab_status == 'Completed') {
//                    $status = "<span class='btn btn-primary'  >Completed</span>";
//                } else if ($row->cgmdiab_status == 'Not_sent') {
//                    $status = "<span class='btn btn-danger'  >Not sent</span>";
//                }

                $callData[] = '<center><input type="checkbox" name="send_fax_data[]" value="' . $row->IDcgmdiab . '" ></center>';
//                $callData[] = $status;
//                $callData[] = urldecode($row->cgmdiab_reason);
                $callData[] = strtoupper(isset($row->cgmdiab_status) ? $row->cgmdiab_status== 'dupe'? 'DUPE' :'' : '');
                
                $DecodedTimestamp = json_decode($row->cgmdiab_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
               
                $DecodedIDsSender = json_decode($row->cgmdiab_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);

                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';
                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }
                
                $callData[] = $row->cgmdiab_firstname;
                $callData[] = $row->cgmdiab_lastname;
                $callData[] = $row->cgmdiab_address;
                $callData[] = $row->cgmdiab_city;
                $callData[] = $row->cgmdiab_state;
                $callData[] = $row->cgmdiab_zip;
                $callData[] = $row->cgmdiab_email;
                $callData[] = $row->cgmdiab_dob;

                $callData[] = (strpos($row->cgmdiab_phone, '+1') !== false) ? $row->cgmdiab_phone : '+1' . $row->cgmdiab_phone;
//                $callData[] = $row->cgmdiab_sex;
//                $callData[] = $row->cgmdiab_diabetes;
//                $callData[] = $row->cgmdiab_visit_doc;
//                $callData[] = $row->cgmdiab_insulin_pum;
//                $callData[] = $row->cgmdiab_blood_sugar;
                $callData[] = $row->cgmdiab_doc_firstname;
                $callData[] = $row->cgmdiab_doc_lastname;
                $callData[] = $row->cgmdiab_doc_address;
                $callData[] = $row->cgmdiab_doc_city;
                $callData[] = $row->cgmdiab_doc_state;
                $callData[] = $row->cgmdiab_doc_zip;
                $callData[] = $row->cgmdiab_doc_npi;
                $callData[] = (strpos($row->cgmdiab_doc_phone, '+1') !== false) ? $row->cgmdiab_doc_phone : '+1' . $row->cgmdiab_doc_phone;
                $callData[] = (strpos($row->cgmdiab_doc_fax, '+1') !== false) ? $row->cgmdiab_doc_fax : '+1' . $row->cgmdiab_doc_fax;
//                $callData[] = $row->cgmdiab_patient_ack;
//                $callData[] = $row->cgmdiab_finan_aggrem;
//                $callData[] = $row->cgmdiab_contact_me;
//                $callData[] = $row->cgmdiab_coinsurance;
				$callData[] = $row->cgmdiab_medicare;

				$callData[] = $row->cgmdiab_insurance_com_id;
				$callData[] = $row->cgmdiab_insurance_com;

//                $callData[] = $row->cgmdiab_IDgroup;
                $callData[] = $row->cgmdiab_date_entered;
                $callData[] = $agentID;
//                $callData[] = $row->cgmdiab_IDtrack;
//                $callData[] = $row->cgmdiab_IDsite;
				$callData[] = $row->cgmdiab_call_back_num;
				$callData[] = $row->cgmdiab_fax_back;
				$callData[] = $row->cgmdiab_repcode;
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";
//                $callData[] = $row->cgmdiab_patient_url;
//                $callData[] = $row->cgmdiab_recording;
//                $callData[] = $edit_btn;
                $data[] = $callData;
            }
        }

        else if($prefix=='faxpdf')
        {
            $counter = 1;
            foreach ($callData as $row) {
               $agentID= $this->agent_name($row->agentID);

                $used_products = "";
                $callData = array();
//                $status = "";

                $callData[] = '<input type="checkbox" name="send_fax_data[]" value="' . $row->IDfaxpdf . '" >';

                $DecodedTimestamp = json_decode($row->faxpdf_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
                
                $DecodedIDsSender = json_decode($row->faxpdf_IDsSender);
                $IDsSenderNames = explode(',',$DecodedIDsSender);
                
                $LoopCounter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
                $SentTimeWithNumbers = '<div style="font-size: 50px;">';

                for($i=0;$i<$LoopCounter;$i++)
                {
                    $IDsender = isset($IDsSenderNames[$i]) ? $IDsSenderNames[$i] : '';
                    $SentTimeWithNumbers .= $i+1 . ".  " .$totalSentFaxes[$i] . " : ". $this->agent_name($IDsender) ."</br>";
                }
                $SentTimeWithNumbers .= '</div>';
                if(!empty($DecodedTimestamp))
                {
                    $callData[] = "<a data-html='true'  data-toggle='tooltip' data-placement='right' title='$SentTimeWithNumbers'>".$LoopCounter."</a>";
                }
                else
                {
                    $callData[] = "0";
                }


                $callData[] = (strpos($row->faxpdf_doc_fax, '+1') !== false) ? $row->faxpdf_doc_fax : '+1' . $row->faxpdf_doc_fax;
                
                $DecodedTimestamp = json_decode($row->faxpdf_no_of_sent_faxes);
                $totalSentFaxes = explode(',',$DecodedTimestamp);
                $counter = (!empty($DecodedTimestamp)) ? count($totalSentFaxes) : 0 ;
               
                if($counter < 3 && $row->faxpdf_status!="Not_sent" && $row->faxpdf_status!="Delete")
                {
                    $num_pag="";
                    if($row->faxpdf_path_old_new == 0)
                    {
                        if(file_exists("assets/pdf/".$row->faxpdf_file))
                        {
                          $pdftext = file_get_contents(base_url()."assets/pdf/".$row->faxpdf_file);
                          $num_pag = preg_match_all("/\/Page\W/", $pdftext);
                        }
                        $num_pag = $num_pag != "" ? $num_pag : '';
                        $callData[] = "<a target='_blank' href=".base_url()."assets/pdf/".$row->faxpdf_file."><button type='button' class='btn btn-primary'>Show ($num_pag)</button></a>";
                    }
                    else if($row->faxpdf_path_old_new == 1)
                    {
                        if(file_exists("assets/pdf-new/".$row->faxpdf_path.'/'.$row->faxpdf_file))
                        {
                          $pdftext = file_get_contents(base_url()."assets/pdf-new/".$row->faxpdf_path.'/'.$row->faxpdf_file);
                          $num_pag = preg_match_all("/\/Page\W/", $pdftext);
                        }
                        $num_pag = $num_pag != "" ? $num_pag : '';                       
                        $callData[] = "<a target='_blank' href=".base_url()."assets/pdf-new/".$row->faxpdf_path.'/'.$row->faxpdf_file."><button type='button' class='btn btn-primary'>Show ($num_pag)</button></a>";
                    }
                }
                else
                {
                    $callData[] = "<a  href='#'><button type='button' class='btn btn-primary'>Show</button></a>";
                }
               
                $callData[] = $row->faxpdf_data_entered;
                $callData[] = $agentID;
                $str = $row->faxpdf_file;
                $str_arr = explode("_".$this->session->userdata("id"),$str);
                $callData[] = isset($str_arr[1]) ? $str_arr[1] : '';
                $callData[] = $type=="Not_sent" ? $row->twilio_status : "";


                $data[] = $callData;

                $counter++;
            }

        }

        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);
//        echo $this->db->last_query();

    }
    
    function agent_name($id){
        $id==0 || $id==6 ? $id=$this->session->userdata('id'): $id =$id;
        $this->db->where('id',$id);
        $query = $this->db->get('login')->first_row();
        return $query->name;
        
    }

}

