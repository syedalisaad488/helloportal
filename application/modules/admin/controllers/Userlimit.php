<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userlimit extends My_controller {

    function __construct(){
        parent::__construct();
        $role = $this->session->userdata("role");
        if($role!="userlimit")
        {
            redirect("admin/Dashboard");
        }

        $this->load->model("Admin_model");
    }
    public function index(){
      
      
        $randomString = md5(uniqid(mt_rand(), true).microtime(true));
        $this->session->set_userdata("token",$randomString);

        $this->load->view('include/head');
        $data['token'] = $randomString;
        $data['user'] = $this->Admin_model->userlimit_data();
      
        $data['select_user'] = $this->Admin_model->userlimit_select();
        $data['userlimit_total_fax'] = $this->Admin_model->userlimit_total_fax();
        $this->load->view('userlimit/assignUserLimit',$data);
    }
    public function assgin_limit_user(){
        $validation_number = $this->Admin_model->userlimit_validation();
        $data = $this->input->post();
        if(count($data) != 5){
            if($validation_number-$data['userlimit_send_static'] <0){
                $this->session->set_flashdata("errorMsg",'<div class="alert alert-danger" role="alert" >Do not open Developer Tools !!</div>');
                redirect("admin/Userlimit");
            }
        }

        if($this->input->post("token") == $this->session->userdata("token"))
        {
            $this->Admin_model->assgin_limit_user($data);
        }
        redirect(base_url('admin/Userlimit/'));
    }



}

