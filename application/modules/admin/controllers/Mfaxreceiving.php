<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mfaxreceiving extends My_controller {

    function __construct(){
        parent::__construct();
                $this->load->model("Admin_model");


    }

    public function cron()
    {
        set_time_limit(10);

        $url = 'https://api.documo.com/v1/fax/history';

        $query = http_build_query(array(
  'offset' => '0',
  'limit' => '20',
  'direction' => 'inbound',
  'status' => 'all',
  'include' => 'tags,contacts',
  'query' => ''
));

$headers = array(
  'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
  'Content-Type: application/json'
);

$ch = curl_init();

$options = array(
  CURLOPT_URL => "{$url}?{$query}",
  CURLOPT_HTTPHEADER => $headers
);
curl_setopt_array($ch, $options);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

$response = curl_exec($ch);
curl_close($ch);

$response = json_decode($response);

$data = $response->rows;


$count = count($data)-1;


for($i=0; $i<=$count;$i++){

    // echo $data[$i]->messageId;
    $this->db->where("faxid",$data[$i]->messageId);
    $result = $this->db->get("mfaxreceiving")->row();

    if(empty($result))
    {
        $data6 = array(
        'faxid' => $data[$i]->messageId,
        'myfaxnumber' => $data[$i]->faxNumber,
        'senderfaxnumber' => $data[$i]->faxCallerId,
        'created_at' => $data[$i]->createdAt
        );

        $this->db->insert('mfaxreceiving',$data6);
    }



}


    }

    
        // GET REQUEST
    public function getList()
    {
        $data['check'] = "";
        if($this->input->post())
        {
            $start_date = $this->input->post("start_date");
            $end_date = $this->input->post("end_date");
            $data['start_date'] = $start_date;
            $data['end_date'] = $end_date;

        }
        $this->load->view('include/head');
        $this->load->view('include/nav');
        $this->load->view('admin/User/recived_mfax',$data);

    }
 
    public function  fetch_fax(){
        $callData = $this->Admin_model->get_mfax_faxlist_data();
//        echo $this->db->last_query();
//
//        die;
        $totalData = $this->Admin_model->mfax_faxlist_count();
        $data = array();
        $id_counter = 1;



        foreach ($callData as $row) {
            $callData = array();
            $callData[] = $row->faxinfo_med_id;
            $callData[] = '';
            $callData[] = $row->faxid;
            $callData[] = $row->created_at;
            $callData[] = $row->senderfaxnumber == "mFax" ? "" : "+1".$row->senderfaxnumber;
            $callData[] = $row->myfaxnumber;
            $var = "old_new_".$id_counter;
            $status = ($row->faxinfo_old_new == 0 && $row->faxinfo_med_id == "") ? '<span style="color:red;"><b>New</b></span>' : '';
            $callData[] = '<a href="#"  class="medicalIdEntryBtn" data-id="'.$row->faxid.'">Manage Med-Id</a>
                        <form  action="'. base_url('/admin/Mfaxreceiving/download/').$row->faxid.'" method="POST">
                            <input type="hidden" name="faxinfo_return_fax_id" value="'. $row->faxid.'">
                            <a href="#" onclick="change_status('. $id_counter.')" ><input type="submit" class="btn btn-primary" value="Download"></a>
                            <div id="'.$var.'"></span>'.$status.'</div>
                        </form>';
            $id_counter++;
            $data[] = $callData;
        }


        $output = array(
            "draw"              =>  intval($this->input->post('draw')),
            "recordsTotal"      =>  intval($totalData),
            "recordsFiltered"   =>  intval($totalData),
            "data"              =>  $data
        );
        echo json_encode($output);
    }
     function download($id){


      $url = 'https://api.documo.com/v1/fax/'.$id.'/download';

$headers = array(
  'Authorization: Basic eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjMTdmZDg3Ny1jMjU4LTRhZDQtODBlMy1hN2RhODRkYTZjNTciLCJhY2NvdW50SWQiOiJlNzM1OTA2ZS05Y2VjLTQ2YWQtOWFlYi0wYjZhZGZiMDIyMGQiLCJpYXQiOjE2MTA1NTU5Nzl9.NFTs8r_Et5kGRwfG8twk4ziiqNHSij8C94ukrHJZlZ0',
);

         $update_status = array(

             'faxinfo_old_new' => 1,

         );
         $this->db->where("faxid",$id);
         $this->db->update("mfaxreceiving",$update_status);

$ch = curl_init();

$options = array(
  CURLOPT_URL => "{$url}",
  CURLOPT_HTTPHEADER => $headers
);
curl_setopt_array($ch, $options);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);


$response = curl_exec($ch);
curl_close($ch);


        $this->load->helper('download');
        $this->load->helper('file');
   
  
        $name = $id.'.pdf';
        force_download($name, $response);


    }
    





}

