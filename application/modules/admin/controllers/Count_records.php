<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Count_records extends My_controller
{

    public function index()
    {
        $this->db->where("date(faxpdf_fax_sent_date)","2021-01-13");

        $dateArr = array("2021-01-14");
        foreach($dateArr as $value)
        {
            $this->db->or_where("date(faxpdf_fax_sent_date)",$value);
        }

       $result = $this->db->get("t_faxpdf")->result();

       $counter = 0;
       $counterJson = 0;

       foreach($result as $key)
       {
           $temp = $key->faxpdf_no_of_sent_faxes;
           $temp = explode(",",$temp);
           foreach($temp as $value)
           {
               foreach($dateArr as $valueDate)
               {
                   if ($value == $valueDate)
                   {
                       $counterJson++; break;
                   }
               }
               $counterJson++;
           }
           $counter++;
       }
       echo "Json Counter: ".$counterJson."<br>";
        echo "Counter: ".$counter."<br>";
    }

}