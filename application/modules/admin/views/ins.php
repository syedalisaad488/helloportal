<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="az-content az-content-dashboard-five">
    <div class="az-header">
        <div class="container-fluid">
            <div class="az-header-left">
                <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
            </div><!-- az-header-left -->
            <div class="az-header-right">


                <div class="">
                    <button class="btn btn-primary">Edit Profile</button>
                    <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
                </div>
            </div>
        </div><!-- container -->
    </div><!-- az-header -->
    <div class="az-content-header d-block d-md-flex">

    </div><!-- az-content-header -->
    <div class="az-content-body">

        <div style="background: white; padding: 10px;">
            <br>
            <table id="example" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Time</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($insData->result() as $index => $key){ ?>
                    <tr>
                        <td><?= $index+1;?></td>
                        <td><?= $key->inspect_name;?></td>
                        <td><?= $key->inspect_role;?></td>
                        <td><?= $key->created_at;?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div><!-- row -->
    </div><!-- az-content-body -->

    <?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script>
    $(document).ready(function() {



        $('#example').DataTable( {
            pageLength: 10,
            lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500]
        } );
    } );


</script>

</html>



