<style>
	div.dt-buttons {
		float: right;
		margin-left:10px;
	}
	div.dataTables_wrapper {
		width:1045px;
		margin: 0 ;
	}
	th, td { white-space: nowrap; }
	th{
		background:white;
	}
	.DTFC_RightBodyWrapper{
		left: 20px!important;
	}
	.DTFC_RightHeadWrapper{
		left: 20px!important;

	}
 .dataTables_processing {
        top: 64px !important;
        z-index: 11000 !important;
        font-size : 40px !important;
    }
    	.dataTables_scrollBody{
	      min-height: 200px;

	}


</style>
<?php

$prefix_segment = $this->uri->segment(4);



?>
<div class="az-content az-content-dashboard-five">
	<div class="az-header">
		<div class="container-fluid">
			<div class="az-header-left">
				<a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
			</div><!-- az-header-left -->
			<div class="az-header-right">


				<div class="">
					<a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
				</div>
			</div>
		</div><!-- container -->
	</div><!-- az-header -->




	</form>
	<div class="az-content-body">


		<div style="background: white; padding: 10px;">
			<div id="checkboxlist">
			
						<table id="user_data" class="stripe row-border order-column" style="width:100%">
							<thead>
								<tr>
							
								<th>status</th>
								<th>Reason</th>
								<th>SNS checked</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>address</th>
								<th>city</th>
								<th>state</th>
								<th>zip</th>
								<th>email</th>
								<th>dob</th>
								<th>phone</th>
								<th>sex</th>
								<th>ssn</th>
								<th>diabetes</th>
								<th>visit_doc</th>
								<th>insulin pump</th>
								<th>blood sugar</th>
								<th>doc firstname</th>
								<th>doc lastname</th>
								<th>doc address</th>
								<th>doc city</th>
								<th>doc state</th>
								<th>doc zip</th>
								<th>doc npi</th>
								<th>doc phone</th>
								<th>doc fax</th>
								<th>patient ack</th>
								<th>finan_aggrem</th>
								<th>contact_me</th>
								<th>coinsurance</th>
								<th>medicare</th>
								<th>insurance company</th>
								<th>ID member</th>
								<th>IDg roup</th>
								<th>date entered</th>
								<th>ID track</th>
								<th>ID site</th>
								<th>repcode</th>
								<th>patient url</th>
								<th>recording</th>
								<th>Action</th>
						

							</tr>
							</thead>
							<tbody>
							<div class='row pd-10 px-2'>
								<div class="col-md-6">
									<?php
                                    echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cleaning</p>" : '';
									echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Can't Reach</p>" : '';
									echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
									echo ($this->uri->segment(5))=='Sented' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
									echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
									echo ($this->uri->segment(5))=='Senttover' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
									echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
									echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
									echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
									echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
									echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
									echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
									?>
								</div>
						


								<tr>

								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							
							</tr>
							
						

						

							</tbody>
							<tfoot>

							</tfoot>
						</table>
			</div>

			</form>

		</div><!-- row -->
	</div><!-- az-content-body -->

	<?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>


<script>
	$('.selectall').click(function() {
		if ($(this).is(':checked')) {
			$('div input').attr('checked', true);
		} else {
			$('div input').attr('checked', false);
		}
	});
	$(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            dom: 'lBfrtip',
            scrollX: true,
            info: false,
            processing:true,
            serverSide:true,
            order:[],
            fixedColumns:{
				leftColumns: false,
				rightColumns: 1
			},   columnDefs: [ {
            "targets": [],
            "orderable": false
            } ],


            ajax:{
                url:"<?php echo base_url() . 'admin/Viewer/fetch_camp_data/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>",
                type:"POST",
                error:function()
                {
                   alert("Something went wrong. Please refresh the page."); 
                   location.reload();
                }
            },


			pageLength: 10,
			lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000],




			buttons: [
				{
					extend:    'copyHtml5',
					text:      '<i class="fa fa-files-o"></i>',
					titleAttr: 'Copy',
					className: 'btn btn-outline-primary btn-xs'
				},
				{
					extend:    'excelHtml5',
					text:      '<i class="fa fa-file-excel-o"></i>',
					titleAttr: 'Excel',
					className: 'btn btn-outline-primary btn-xs'

				},
				{
					extend:    'csvHtml5',
					text:      '<i class="fa fa-file-text-o"></i>',
					titleAttr: 'CSV',
					className: 'btn btn-outline-primary btn-xs'

				},
				{
					extend:    'pdfHtml5',
					text:      '<i class="fa fa-file-pdf-o"></i>',
					titleAttr: 'PDF',
					className: 'btn btn-outline-primary btn-xs'

				}
			]
		} );
	} );


</script>
<script>
	$("#customFile").change(function () {
		var fileExtension = ['csv'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			alert("Only formats are allowed : "+fileExtension.join(', '));
		}
	});
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
