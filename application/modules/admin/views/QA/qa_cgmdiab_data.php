<style>
	div.dt-buttons {
		float: right;
		margin-left:10px;
	}
	div.dataTables_wrapper {
		width:1045px;
		margin: 0 ;
	}
	th, td { white-space: nowrap; }
	th{
		background:white;
	}
	.DTFC_RightBodyWrapper{
		left: 20px!important;
	}
	.DTFC_RightHeadWrapper{
		left: 20px!important;

	}
		.dataTables_scrollBody{
	      min-height: 200px;

	}


</style>
<?php

$prefix_segment = $this->uri->segment(4);


?>
<div class="az-content az-content-dashboard-five">
	<div class="az-header">
<div class="container-fluid">
			<div class="az-header-left" >
				<a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
			</div><!-- az-header-left -->
			<div class="az-header-right" style="width: 950px!important;">


			</div>
							<div class="">
					<a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
				</div>

		</div><!-- container -->
	</div><!-- az-header -->




	</form>
	<div class="az-content-body">
		<?php if(($this->uri->segment(5))=='Cleaning'){?>

		<!--<form method='post' action='<?php echo base_url().'admin/Cvs_controller/cgmdiab'; ?>' enctype="multipart/form-data">-->
		<!--<div class='row '>-->
		<!--	<div class="col-md-6">-->
		<!--		<div class="custom-file mb-3">-->
		<!--			<input  class="custom-file-input" id="customFile" type='file' name='file' accept=".csv" id="">-->
		<!--			<label class="custom-file-label" for="customFile">Choose file</label>-->
		<!--		</div>-->

		<!--	</div>-->
		<!--	<div class="col-md-6">-->
		<!--		<input type='submit' class="btn btn-outline-primary" value='Upload' name='upload'>-->
		<!--	</div>-->

		<!--</div>-->
		<!--</form>-->

		<?php } ?>

		<div style="background: white; padding: 10px;">
			<div id="checkboxlist">
				<?php if(($this->uri->segment(5))=='Staging'){?>
				<!--<form action="<?php echo base_url().'admin/User/campaign_send_fax/'.$prefix_segment; ?>" method="POST">-->
					<?php  } if(($this->uri->segment(5))!='Staging') { ?>
					<form action="<?php echo base_url().'admin/Change/changetab/'.$prefix_segment; ?>" method="POST">


						<?php } ?>
						<table id="user_data" class="stripe row-border order-column" style="width:100%">
							<thead>
                            <tr>
                                <th>Select&nbsp;&nbsp;<input type="checkbox" name="sample" class="selectall"/></th>
                                <th>status</th>
                                <th>Reason</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>address</th>
                                <th>city</th>
                                <th>state</th>
                                <th>zip</th>
                                <th>email</th>
                                <th>dob</th>
                                <th>phone</th>
                                <th>sex</th>
                                <th>ssn</th>
                                <th>diabetes</th>
                                <th>visit_doc</th>
                                <th>insulin pump</th>
                                <th>blood sugar</th>
                                <th>doc firstname</th>
                                <th>doc lastname</th>
                                <th>doc address</th>
                                <th>doc city</th>
                                <th>doc state</th>
                                <th>doc zip</th>
                                <th>doc npi</th>
                                <th>doc phone</th>
                                <th>doc fax</th>
                                <th>patient ack</th>
                                <th>finan_aggrem</th>
                                <th>contact_me</th>
                                <th>coinsurance</th>
                                <th>medicare</th>
                                <th>insurance company</th>
                                <th>ID member</th>
                                <th>IDg roup</th>
                                <th>date entered</th>
                                <th>ID track</th>
                                <th>ID site</th>
                                <th>repcode</th>
                                <th>patient url</th>
                                <th>recording</th>
                                <th style="width:70px;">Action</th>

                            </tr>

                            </thead>
							<tbody>
							<div class='row pd-10 px-2'>
								<div class="col-md-6">
									<?php
                                    echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cleaning</p>" : '';
									echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Can't Reach</p>" : '';
									echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
									echo ($this->uri->segment(5))=='Rxrequest' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
									echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
									echo ($this->uri->segment(5))=='Sented' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
									echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
									echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
									echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
									echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
									echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
									echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
									?>
								</div>
								<?php if(($this->uri->segment(5))=='Staging'){?>
								<div class="col-md-6 ">
									<!--<input type="submit" value="Send FAX" class="btn btn-success pull-right" >-->
								</div>
								<?php  } if(($this->uri->segment(5))!='Staging') { ?>
								<div class="col-md-4">
									<select class="form-control" name="status">
											<option value="Good">GOOD</option>
											<option value="Rejected">REJECTED</option>
											<option value="Deleted">DELETED</option>
											<option value="Sented">RX Request</option>
											<option value="Rxadditional">RX Additional</option>
											<option value="Rejected">SENT To Verifications</option>
											<option value="Cannotreach">Can't Reach</option>
											<option value="Completed">Completed</option>
											<option value="Archive">Archive</option>
										</select>
								</div>
								<div class="col-md-2">
									<input type="submit" value="Move To Tab" class="btn btn-success pull-right" >

								</div>
							</div>
							<?php } ?>

                            <tr>

                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td> </td>
                                <td></td>
                                <td></td>
                            </tr>

						

							</tbody>
							<tfoot>

							</tfoot>
						</table>
			</div>

			</form>

		</div><!-- row -->
	</div><!-- az-content-body -->

	<?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>


<script>
	$('.selectall').click(function() {
		if ($(this).is(':checked')) {
			$('div input').attr('checked', true);
		} else {
			$('div input').attr('checked', false);
		}
	});
	$(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            dom: 'lfrtip',
            scrollX: true,
            info: false,
            processing:true,
            serverSide:true,
            order:[],
            fixedColumns:{
                leftColumns: false,
                rightColumns: 1
            },

            ajax:{
                url:"<?php echo base_url() . 'admin/QA/fetch_camp_data/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>",
                type:"POST"
            },

			pageLength: 10,
			lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000]




// 			buttons: [
// 				{
// 					extend:    'copyHtml5',
// 					text:      '<i class="fa fa-files-o"></i>',
// 					titleAttr: 'Copy',
// 					className: 'btn btn-outline-primary btn-xs'
// 				},
// 				{
// 					extend:    'excelHtml5',
// 					text:      '<i class="fa fa-file-excel-o"></i>',
// 					titleAttr: 'Excel',
// 					className: 'btn btn-outline-primary btn-xs'

// 				},
// 				{
// 					extend:    'csvHtml5',
// 					text:      '<i class="fa fa-file-text-o"></i>',
// 					titleAttr: 'CSV',
// 					className: 'btn btn-outline-primary btn-xs'

// 				},
// 				{
// 					extend:    'pdfHtml5',
// 					text:      '<i class="fa fa-file-pdf-o"></i>',
// 					titleAttr: 'PDF',
// 					className: 'btn btn-outline-primary btn-xs'

// 				}
// 			]
		} );
	} );


</script>
<script>
	$("#customFile").change(function () {
		var fileExtension = ['csv'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			alert("Only formats are allowed : "+fileExtension.join(', '));
		}
	});
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
