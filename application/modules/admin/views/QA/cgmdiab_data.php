<head></head>
<style>
    div.dt-buttons {
        float: right;
        margin-left:10px;
    }
    div.dataTables_wrapper {
        width:1045px;
        margin: 0 ;
    }
    th, td { white-space: nowrap; }
    th{
        background:white;
    }
    	.DTFC_RightBodyWrapper{
		left: 20px!important;
	}
	.DTFC_RightHeadWrapper{
		left: 20px!important;

	}
		.dataTables_scrollBody{
	      min-height: 200px;

	}


</style>
<div class="az-content az-content-dashboard-five">
	<div class="az-header">
	<div class="az-header">
		<div class="container-fluid">
			<div class="az-header-left" >
				<a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
			</div><!-- az-header-left -->
			<div class="az-header-right" style="width: 950px!important;">


			</div>
							<div class="">
					<a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
				</div>

		</div><!-- container -->
	</div><!-- az-header -->

	</div><!-- az-header -->


    <div class="az-content-body">


        <div style="background: white; padding: 10px;">
            <div id="checkboxlist">
                <table id="example" class="stripe row-border order-column" style="width:100%">
                    <thead>
                    <tr>
                        <th>No</th>
          			<th>status</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>address</th>
								<th>city</th>
								<th>state</th>
								<th>zip</th>
								<th>email</th>
								<th>dob</th>
								<th>phone</th>
								<th>sex</th>
								<th>ssn</th>
								<th>diabetes</th>
								<th>visit_doc</th>
								<th>insulin pump</th>
								<th>blood sugar</th>
								<th>doc firstname</th>
								<th>doc lastname</th>
								<th>doc address</th>
								<th>doc city</th>
								<th>doc state</th>
								<th>doc zip</th>
								<th>doc npi</th>
								<th>doc phone</th>
								<th>doc fax</th>
								<th>patient ack</th>
								<th>finan_aggrem</th>
								<th>contact_me</th>
								<th>coinsurance</th>
								<th>medicare</th>
								<th>insurance company</th>
								<th>ID member</th>
								<th>IDg roup</th>
								<th>date entered</th>
								<th>ID track</th>
								<th>ID site</th>
								<th>repcode</th>
								<th>patient url</th>
								<th>Record File</th>
                        <th >Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <div class='row pd-10 px-2'>
                        <div class="col-md-6">
                            <?php
                            echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cleaning</p>" : '';
                            echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cann't Reach</p>" : '';
                            echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
                            echo ($this->uri->segment(5))=='Rxrequest' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
                            echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
                            echo ($this->uri->segment(5))=='Sented' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
                            echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
                            echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
                            echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
                            echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
                            echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
                            echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
                            ?>
                        </div>

                    </div>

                    <?php
                    $no=1;


                    foreach($client_data as $key) {
 ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                            <td>
									<?php echo ($key->cgmdiab_status)=='Good' ?  '<span class="btn btn-success">GOOD</span>' : ''; ?>
									<?php echo ($key->cgmdiab_status)=='Deleted' ?  '<span class="btn btn-danger">DELETED</span>' : ''; ?>
									<?php echo ($key->cgmdiab_status)=='Rejected' ?  '<span class="btn btn-danger" style="background: darkorange ">REJECTED</span>' : ''; ?>
									<?php echo ($key->cgmdiab_status)=='Sented' ?  '<span class="btn btn-info" >SENT</span>' : ''; ?>
								</td>
								<td><?php echo $key->cgmdiab_firstname; ?></td>
								<td> <?php echo $key->cgmdiab_lastname; ?></td>
								<td><?php echo $key->cgmdiab_address; ?></td>
								<td><?php echo $key->cgmdiab_city?></td>
								<td><?php echo $key->cgmdiab_state; ?></td>
								<td><?php echo $key->cgmdiab_zip;?></td>
								<td><?php echo $key->cgmdiab_email ?></td>
								<td><?php echo $key->cgmdiab_dob; ?></td>
								<td><?php    if(strpos($key->cgmdiab_phone, '+1') !== false){   echo $key->cgmdiab_phone;   } else{      echo '+1'.$key->cgmdiab_phone;                                             } ?></td>
								<td><?php echo $key->cgmdiab_sex ?></td>
								<td><?php echo $key->cgmdiab_ssn;?></td>
								<td><?php echo $key->cgmdiab_diabetes;?></td>
								<td><?php echo $key->cgmdiab_visit_doc; ?></td>
								<td><?php echo $key->cgmdiab_insulin_pum ?></td>
								<td><?php echo $key->cgmdiab_blood_sugar;?></td>
								<td><?php echo $key->cgmdiab_doc_firstname;?></td>
								<td><?php echo $key->cgmdiab_doc_lastname; ?></td>
								<td><?php echo $key->cgmdiab_doc_address ?></td>
								<td><?php echo $key->cgmdiab_doc_city;?></td>
								<td><?php echo $key->cgmdiab_doc_state;?></td>
								<td><?php echo $key->cgmdiab_doc_zip; ?></td>
								<td><?php echo $key->cgmdiab_doc_npi ?></td>
								<td><?php 										    if(strpos($key->cgmdiab_doc_phone, '+1') !== false){                                                 echo $key->cgmdiab_doc_phone;                                             } else{                                                 echo '+1'.$key->cgmdiab_doc_phone;                                             } ?></td>

								<td><?php 										    if(strpos($key->cgmdiab_doc_fax, '+1') !== false){                                                 echo $key->cgmdiab_doc_fax;                                             } else{                                                 echo '+1'.$key->cgmdiab_doc_fax;                                             } ?></td>

								<td><?php echo $key->cgmdiab_patient_ack; ?></td>
								<td><?php echo $key->cgmdiab_finan_aggrem ?></td>
								<td><?php echo $key->cgmdiab_contact_me;?></td>
								<td><?php echo $key->cgmdiab_coinsurance;?></td>
								<td><?php echo $key->cgmdiab_medicare ?></td>
								<td><?php echo $key->cgmdiab_insurance_com;?></td>
								<td><?php echo $key->cgmdiab_IDmember;?></td>
								<td><?php echo $key->cgmdiab_IDgroup;?></td>
								<td><?php echo $key->cgmdiab_helpdisk;?></td>
								<td><?php echo $key->cgmdiab_IDtrack ?></td>
								<td><?php echo $key->cgmdiab_IDsite;?></td>
								<td><?php echo $key->cgmdiab_repcode;?></td>
								<td><?php echo $key->cgmdiab_patient_url; ?></td>
								<td><?php echo $key->cgmdiab_recording ?></td>

                                    <td>
                                        <a class="btn btn-info" href="<?php echo base_url('admin/QA/campaign_edit_data/'.$key->IDcgmdiab).'/'.$this->uri->segment(4); ?>" >Edit</a>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Change Status
                                            </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Good/GOOD";?>">GOOD</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/REJECTED";?>">REJECTED</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Completed/COMPLETED";?>">COMPLETED</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Sented/SENT";?>">SENT</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rxadditional/Rx Additional";?>">Rx Additional</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/SNS ON FILE";?>">SNS ON FILE</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/NO GOOD PPO INVALID";?>">NO GOOD PPO INVALID</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/OTHER INSURANCE PRIMARY";?>">OTHER INSURANCE PRIMARY</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/INSURANCE TERMINATED";?>">INSURANCE TERMINATED</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/HMO POLICY";?>">HMO POLICY</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/NO DME COVERAGE";?>">NO DME COVERAGE</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/NO MEDICAL BENEFITS";?>">NO MEDICAL BENEFITS</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/RESIDES IN SKILLED NURSING FACILITY";?>">RESIDES IN SKILLED NURSING FACILITY(</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/INVALID INSURANCE";?>">INVALID INSURANCE</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/DOESN’T MEET INSURANCE CRITERIA";?>">DOESN’T MEET INSURANCE CRITERIA</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/AUTHORIZATION REQUEST DENIED";?>">AUTHORIZATION REQUEST DENIED</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/RX REQUEST DENIED";?>">RX REQUEST DENIED</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/REFUSED";?>">REFUSED</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Cannotreach/CAN’T REACH";?>">CAN’T REACH</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/PATIENT NEEDS APPOINTMENT";?>">PATIENT NEEDS APPOINTMENT</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/PT NOT INTERESTED";?>">PT NOT INTERESTED</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/RX PENDING";?>">RX PENDING</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/MD NOT PECOS CERTIFIED";?>">MD NOT PECOS CERTIFIED</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/INVALID DOCTOR INFO";?>">INVALID DOCTOR INFO</a>
                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/INCORRECT DOCTOR";?>">INCORRECT DOCTOR</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/DUPLICATE ENTRY";?>">DUPLICATE ENTRY</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/MISSING ICD 10";?>">MISSING ICD 10</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/MISSING CLINICALS";?>">MISSING CLINICALS</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/MISSING INSURANCE INFO";?>">MISSING INSURANCE INFO</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/UNSUCCESSFUL FAX";?>">UNSUCCESSFUL FAX(</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/RETURN TO SENDER";?>">RETURN TO SENDER</a>

                                        <a class="dropdown-item" href="<?php echo base_url('admin/QA/update_data/').$key->IDcgmdiab."/".$this->uri->segment(4)."/"."Rejected/PATIENT DECEASED";?>">PATIENT DECEASED</a>



                                    </div>
                                        </div>

                                </tr>

           

                    <?php }

                    ?>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>


        </div><!-- row -->
    </div><!-- az-content-body -->

    <?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>


<script>
    $('.selectall').click(function() {
        if ($(this).is(':checked')) {
            $('div input').attr('checked', true);
        } else {
            $('div input').attr('checked', false);
        }
    });
    $(document).ready(function() {
        $('#example').DataTable( {
            scrollX: true,
            info: false,
            fixedColumns:{
                rightColumns: 1
            },

            dom: 'lfrtip',
            pageLength: 10,
            lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000],



        } );
    } );


</script>
<script>
    $("#customFile").change(function () {
        var fileExtension = ['csv'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
        }
    });
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
