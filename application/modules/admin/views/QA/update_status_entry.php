<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
       
      </div><!-- az-content-header -->
      <div class="az-content-body">

        <div style="background: white; padding: 10px;">
          <form action="<?php echo base_url('admin/QA/update_data/').$IDcampaign."/".$prefix;?>" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control" required="required" name="status">
                          <option value="Rejected">Reject</option>
                          <option value="Good">Good</option>
                          <option value="Deleted">Delete</option>
                        </select>
                    </div>
                   
                    <div class="col-md-1">
                        <input class="btn btn-success" type="submit" value="Update">
                    </div>                
                </div>
            </form>
            <br>
         
          </div><!-- row -->
      </div><!-- az-content-body -->

 <?php $this->load->view('include/footer');?>
    </div><!-- az-content -->

  </body>
  <script>
 $(document).ready(function() {
    $('#example').DataTable( {


        dom: 'Bfrtip',
        pageLength: 10,
        lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500],
        buttons: [
            {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                className: 'btn btn-outline-primary btn-xs' 
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-outline-primary btn-xs' 

            }
        ]
    } );
} );
  </script>

</html>



