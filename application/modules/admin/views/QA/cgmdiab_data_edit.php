<?php $this->load->view("include/head"); ?>
      <?php $this->load->view("include/nav"); ?>

    
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
         
           <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href='<?php echo base_url("admin/Dashboard/logout/").$prefix; ?>'> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
          
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
        <h3>Client Entry Form</h3>
      </div><!-- az-content-header -->
      <div class="az-content-body">
      <form method="post" action="<?php echo base_url("admin/QA/qa_campaign_update_data/").$IDcampaign."/".$prefix; ?>"  enctype="multipart/form-data">
        <div style="background: white; padding: 10px;">
         <h4>Client Details</h4>
          <div class="row">
            <div class="col-md-3" class="form-group"><br>
              <label>First Name</label>
              <input type="text" name="cgmdiab_firstname" value="<?php echo $campaign_data[0]['cgmdiab_firstname']; ?>" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>last Name</label>
              <input type="text" name="cgmdiab_lastname" value="<?php echo $campaign_data[0]['cgmdiab_lastname']; ?>" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>City</label>
                  <input type="text" name="cgmdiab_city" value="<?php echo $campaign_data[0]['cgmdiab_city']; ?>" class="form-control">
            </div>
            <div class="col-md-3" class="form-group"><br>
                <label>State</label>
                <input type="text" name="cgmdiab_state" value="<?php echo $campaign_data[0]['cgmdiab_state']; ?>" class="form-control">
            </div>  
              <div class="col-md-12" class="form-group"><br>
                <label>Address</label>
                <input type="text" name="cgmdiab_address" value="<?php echo $campaign_data[0]['cgmdiab_address']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Email</label>
                  <input type="text" name="cgmdiab_email" value="<?php echo $campaign_data[0]['cgmdiab_email']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Phone</label>
                  <input type="text" name="cgmdiab_phone" value="<?php echo $campaign_data[0]['cgmdiab_phone']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Zip</label>
                <input type="text" name="cgmdiab_zip" value="<?php echo $campaign_data[0]['cgmdiab_zip']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Date of birth</label>
                <input type="text" name="cgmdiab_dob" value="<?php echo $campaign_data[0]['cgmdiab_dob']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Sex</label>
                  <select name="cgmdiab_sex" class="form-control">
                  <option <?=  $campaign_data[0]['cgmdiab_sex']=="Male" ? 'Selected' : ''; ?> value="Male">Male</option>
                  <option <?=  $campaign_data[0]['cgmdiab_sex']=="Female" ? 'Selected' : ''; ?> value="Female">Female</option>
                  <option  <?=  $campaign_data[0]['cgmdiab_sex']=="Other" ? 'Selected' : ''; ?> value="Other">Other</option>
                </select>
              </div>
              <div class="col-md-4"><br>
                <label>SSN</label>
               <input type="text" name="cgmdiab_ssn" value="<?php echo $campaign_data[0]['cgmdiab_ssn']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Diabetes</label>
              <input type="text" name="cgmdiab_diabetes" value="<?php echo $campaign_data[0]['cgmdiab_diabetes']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Visit Doctor</label>
                <input type="text" name="cgmdiab_visit_doc" value="<?php echo $campaign_data[0]['cgmdiab_visit_doc']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Isulin Pump</label>
                <input type="text" name="cgmdiab_insulin_pum" value="<?php echo $campaign_data[0]['cgmdiab_insulin_pum']; ?>" class="form-control">
              </div>
          </div>
         
              <div class="row">
               <div class="col-md-4"><br>
              <label>Blood Sugar</label>
                  <input type="text" name="cgmdiab_blood_sugar" value="<?php echo $campaign_data[0]['cgmdiab_blood_sugar']; ?>" class="form-control">
            </div>
              <div class="col-md-4"><br>
              <label>Doctor First Name</label>
                  <input type="text" name="cgmdiab_doc_firstname" value="<?php echo $campaign_data[0]['cgmdiab_doc_firstname']; ?>" class="form-control">
            </div>
            <div class="col-md-4"><br>
                <label>Doctor Last Name</label>
                <input type="text" name="cgmdiab_doc_lastname" value="<?php echo $campaign_data[0]['cgmdiab_doc_lastname']; ?>" class="form-control">
            </div>
              <div class="col-md-4"><br>
              <label>Doctor Address</label>
                  <input type="text" name="cgmdiab_doc_address" value="<?php echo $campaign_data[0]['cgmdiab_doc_address']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor City</label>
                  <input type="text" name="cgmdiab_doc_city" value="<?php echo $campaign_data[0]['cgmdiab_doc_city']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Doctor State</label>
                <input type="text" name="cgmdiab_doc_state" value="<?php echo $campaign_data[0]['cgmdiab_doc_state']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor ZIP</label>
                  <input type="text" name="cgmdiab_doc_zip" value="<?php echo $campaign_data[0]['cgmdiab_doc_zip']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor Phone</label>
                  <input type="text" name="cgmdiab_doc_phone" value="<?php echo $campaign_data[0]['cgmdiab_doc_phone']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor NPI</label>
                  <input type="text" name="cgmdiab_doc_npi" value="<?php echo $campaign_data[0]['cgmdiab_doc_npi']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor Fax</label>
                  <input type="text" name="cgmdiab_doc_fax" value="<?php echo $campaign_data[0]['cgmdiab_doc_fax']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Patient Acknowledgement</label>
                <input type="text" name="cgmdiab_patient_ack" value="<?php echo $campaign_data[0]['cgmdiab_patient_ack']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Financial Agreement</label>
                  <input type="text" name="cgmdiab_finan_aggrem" value="<?php echo $campaign_data[0]['cgmdiab_finan_aggrem']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Contact Me</label>
                  <input type="text" name="cgmdiab_contact_me" value="<?php echo $campaign_data[0]['cgmdiab_contact_me']; ?>" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Co Insurance</label>
                  <input type="text" name="cgmdiab_coinsurance" value="<?php echo $campaign_data[0]['cgmdiab_coinsurance']; ?>" class="form-control">
              </div>
              
                <div class="col-md-4"><br>
                <label>Insurance Company</label>
                    <input type="text" name="cgmdiab_insurance_com" value="<?php echo $campaign_data[0]['cgmdiab_insurance_com']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Member ID</label>
                    <input type="text" name="cgmdiab_IDmember" value="<?php echo $campaign_data[0]['cgmdiab_IDmember']; ?>" class="form-control">
              </div>
                <div class="col-md-4"><br>
                <label>Group ID</label>
                    <input type="text" name="cgmdiab_IDgroup" value="<?php echo $campaign_data[0]['cgmdiab_IDgroup']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Help Disk</label>
                    <input type="text" name="cgmdiab_helpdisk" value="<?php echo $campaign_data[0]['cgmdiab_helpdisk']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Data Entered</label>
                    <input type="text" name="cgmdiab_date_entered" value="<?php echo $campaign_data[0]['cgmdiab_date_entered']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Tracking ID</label>
                    <input type="text" name="cgmdiab_IDtrack" value="<?php echo $campaign_data[0]['cgmdiab_IDtrack']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Site ID</label>
                    <input type="text" name="cgmdiab_IDsite" value="<?php echo $campaign_data[0]['cgmdiab_IDsite']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Rep Code</label>
                    <input type="text" name="cgmdiab_repcode" value="<?php echo $campaign_data[0]['cgmdiab_repcode']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Patient URL Address</label>
                    <input type="text" name="cgmdiab_patient_url" value="<?php echo $campaign_data[0]['cgmdiab_patient_url']; ?>" class="form-control">
              </div>
               <div class="col-md-4"><br>
                <label>Recording</label>
                    <input type="text" name="cgmdiab_recording" value="<?php echo $campaign_data[0]['cgmdiab_recording']; ?>" class="form-control">
              </div>

              <br><br> <br><br><br><br>
              <div class="col-md-12">
              <button  style="float:right;" class="btn btn-success">Submit</button>
              </div>

          </div>
         </form>
          </div><!-- row -->
      </div><!-- az-content-body -->

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Ucored </span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->


  
    <script>
      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })

        /* ----------------------------------- */
        /* Dashboard content */

        $.plot('#flotChart1', [{
            data: flotSampleData5,
            color: '#8039f4'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.12 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart2', [{
            data: flotSampleData2,
            color: '#007bff'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart3', [{
            data: flotSampleData5,
            color: '#00cccc'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0.2 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotPie', [
          { label: 'Very Satisfied', data: [[1,25]], color: '#6f42c1'},
          { label: 'Satisfied', data: [[1,38]], color: '#007bff'},
          { label: 'Not Satisfied', data: [[1,20]], color: '#00cccc'},
          { label: 'Very Unsatisfied', data: [[1,15]], color: '#969dab'}
        ], {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 3/4,
                formatter: labelFormatter
              }
            }
          },
          legend: { show: false }
        });

        function labelFormatter(label, series) {
          return '<div style="font-size:11px; font-weight:500; text-align:center; padding:2px; color:white;">' + Math.round(series.percent) + '%</div>';
        }

        var ctx6 = document.getElementById('chartStacked1');
        new Chart(ctx6, {
          type: 'bar',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
              data: [10, 24, 20, 25, 35, 50, 20, 30, 28, 33, 45, 65],
              backgroundColor: '#6610f2',
              borderWidth: 1,
              fill: true
            },{
              data: [20, 30, 28, 33, 45, 65, 25, 35, 50, 20, 30, 28],
              backgroundColor: '#00cccc',
              borderWidth: 1,
              fill: true
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false,
                labels: {
                  display: false
                }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true,
                  fontSize: 11
                }
              }],
              xAxes: [{
                barPercentage: 0.4,
                ticks: {
                  fontSize: 11
                }
              }]
            }
          }
        });
      });
    </script>
  </body>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
