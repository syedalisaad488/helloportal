<body class="az-body az-body-sidebar az-light">
<div class="az-sidebar"><br>
    <div class="az-sidebar-header">
        <a href="index.html" class="az-logo"><img src="<?php echo base_url();?>img/PandaProject.png" alt="" width="200px" style="margin-top:30px;"></a>
    </div><!-- az-sidebar-header -->
    <br>
    <div class="az-sidebar-loggedin">
        <div class="az-img-user online"><img src="<?php echo base_url();?>img/img1.jpg" alt=""></div>
        <div class="media-body">
            <h6><?php echo $this->session->userdata("username"); ?></h6>
            <span style="font-size: 11px"><?php echo $this->session->userdata("role"); ?></span>
            <?php if($this->session->userdata("role")=="Agent"){ ?>
                <span style="font-size: 11px">Total Assgin:<?php echo isset(show_limit()->send_static) ? show_limit()->send_static : '' ?> </span>
                <span style="font-size: 11px">Total Used:<?php echo isset(show_limit()->send_dynamic) ? show_limit()->send_dynamic : '' ?></span>
                <span style="font-size: 11px">Remaining:<?php echo isset(show_limit()->send_static) && isset(show_limit()->send_dynamic) ? (show_limit()->send_static)-(show_limit()->send_dynamic) : '' ?> </span>
            <?php } ?>
        </div><!-- media-body -->
    </div><!-- az-sidebar-loggedin -->
    <div class="az-sidebar-body">
        <ul class="nav">
            <li class="nav-label">Main Menu</li>
            <?php $nav_active =  $this->uri->segment(3); ?>
            <?php if($this->session->userdata("role")== "Representative") { ?>


                <li class="nav-item ">
                    <a href="index.html" class="nav-link with-sub"><i class="typcn typcn-clipboard"></i>Client Data</a>
                    <ul class="nav-sub">

                        <li class="nav-sub-item <?php echo $nav_active=="data_entry" ? "active" : '' ;?>"><a href="<?php echo base_url('admin/Repren/data_entry'); ?>" class="nav-sub-link">Data Entry</a></li>
                        <li class="nav-sub-item <?php echo $nav_active=="data_list" ? "active" : '' ;?>"><a href="<?php echo base_url('admin/Repren/data_list'); ?>" class="nav-sub-link">Data List</a></li>

                    </ul>
                </li>
            <?php } ?>




            <?php if($this->session->userdata("role")== "User" || $this->session->userdata("role")== "Agent") { ?>


                <?php if($this->session->userdata("role")!= "Agent"){
                    ?>
                    <li class="nav-item <?php echo ($this->uri->segment(3))=='agent' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Agents</a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(3))=='agent' ? (($this->uri->segment(3))=='agent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/User/agent'); ?>" class="nav-sub-link show">Agents</a></li>
                        </ul>
                    </li>
                    <li class="nav-item <?php echo ($this->uri->segment(2))=='AgentLimit' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Limit Assign</a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(2))=='AgentLimit' ? (($this->uri->segment(2))=='AgentLimit' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/AgentLimit'); ?>" class="nav-sub-link show">Limit</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if($this->session->userdata("mask_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='mask' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Mask Data  </a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/mask/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/mask/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/mask/Sent'); ?><!--" class="nav-sub-link">Processing </a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/mask/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/mask/Not_sent'); ?>" class="nav-sub-link">Sent Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/mask/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        </ul>
                    </li>
                <?php } if($this->session->userdata("hipaa_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='hipaa' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Patient Verfication  </a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/hipaa/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/hipaa/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/hipaa/Sent'); ?><!--" class="nav-sub-link">Processing</a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/hipaa/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/hipaa/Not_sent'); ?>" class="nav-sub-link">Sent Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/hipaa/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        </ul>
                    </li>
                <?php }  if($this->session->userdata("heatpad_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='heatpad' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Heating Pad Data  </a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/heatpad/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/heatpad/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/heatpad/Sent'); ?><!--" class="nav-sub-link">Processing</a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/heatpad/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/heatpad/Not_sent'); ?>" class="nav-sub-link">Sent Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='eatpad' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/heatpad/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        </ul>
                    </li>
                <?php } if($this->session->userdata("aurbrac_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='aurbrac' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Brace Data  </a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/aurbrac/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/aurbrac/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/aurbrac/Sent'); ?><!--" class="nav-sub-link">Processing</a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/aurbrac/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/aurbrac/Not_sent'); ?>" class="nav-sub-link">Sent Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/aurbrac/Delete'); ?>" class="nav-sub-link">Delete</a></li>


                        </ul>
                    </li>
                <?php } if($this->session->userdata("cgmdiab_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub ">
                            <i class="typcn typcn-clipboard"></i>CGM Data</a>
                        <ul class="nav-sub">

                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/List_controller/campaign_list/cgmdiab/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/cgmdiab/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/cgmdiab/Sent'); ?><!--" class="nav-sub-link">Processing</a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/cgmdiab/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/cgmdiab/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/cgmdiab/Delete'); ?>" class="nav-sub-link">Delete</a></li>


                        </ul>
                    </li>
                <?php } if($this->session->userdata("faxpdf_tab")=='1'){?>
                    <li class="nav-item <?php echo ($this->uri->segment(4))=='faxpdf' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub ">
                            <i class="typcn typcn-clipboard"></i>PDF List</a>
                        <ul class="nav-sub">

                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/List_controller/campaign_list/faxpdf/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/faxpdf/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
<!--                            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/List_controller/campaign_list/faxpdf/Sent'); ?><!--" class="nav-sub-link">Processing</a></li>-->
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/faxpdf/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/faxpdf/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxdf' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/List_controller/campaign_list/faxpdf/Delete'); ?>" class="nav-sub-link">Delete</a></li>


                        </ul>
                    </li>
                <?php }       ?>
                <?php if($this->session->userdata("role")!= "Agent"){
                    ?>
                    <li class="nav-item <?php echo ($this->uri->segment(2)=='get_new_fax') ? 'active' : ''?>">
                        <a href="<?php echo base_url('admin/Get_twillo/get_new_fax'); ?>" class="nav-link "><i class="typcn typcn-clipboard"></i>New Received Fax</a>

                    </li>
<!--                    <li class="nav-item --><?php //echo ($this->uri->segment(3))=='getlist' ? 'active' : ''?><!--">-->
<!--                        <a href="--><?php //echo base_url('admin/get/getlist/All'); ?><!--" class="nav-link "><i class="typcn typcn-clipboard"></i>Old Received Fax</a>-->
<!---->
<!--                    </li>-->
<!--                    <li class="nav-item --><?php //echo ($this->uri->segment(2)=='Get_twillo') ? 'active' : ''?><!--">-->
<!--                        <a href="--><?php //echo base_url('admin/Get_twillo/getlist'); ?><!--" class="nav-link "><i class="typcn typcn-clipboard"></i>New Received Fax</a>-->
<!---->
<!--                    </li>-->
                <?php } ?>

            <?php  } ?>


            <?php if($this->session->userdata("role")== "Owner") { ?>

                <!--               <li class="nav-item --><?php //echo ($this->uri->segment(2)=='Report') ? 'active' : ''?><!--">-->
                <!--                   <a href="--><?php //echo base_url('admin/Report/report'); ?><!--" class="nav-link "><i class="typcn typcn-clipboard"></i>Report</a>-->
                <!---->
                <!--               </li>-->
                <li class="nav-item <?php echo ($this->uri->segment(2)=='get_new_fax') ? 'active' : ''?>">
                    <a href="<?php echo base_url('admin/Get_twillo/get_new_fax'); ?>" class="nav-link "><i class="typcn typcn-clipboard"></i>New Received Fax</a>

                </li>
<!--                <li class="nav-item --><?php //echo ($this->uri->segment(4)=='All') ? 'active' : ''?><!--">-->
<!--                    <a href="--><?php //echo base_url('admin/get/getlist/All'); ?><!--" class="nav-link "><i class="typcn typcn-clipboard"></i>Old Received Fax</a>-->
<!---->
<!--                </li>-->
<!--                <li class="nav-item --><?php //echo ($this->uri->segment(2)=='Get_twillo') ? 'active' : ''?><!--">-->
<!--                    <a href="--><?php //echo base_url('admin/Get_twillo/getlist'); ?><!--" class="nav-link "><i class="typcn typcn-clipboard"></i>New Received Fax</a>-->
<!---->
<!--                </li>-->

                <li class="nav-item <?php echo ($this->uri->segment(4))=='hipaa' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>HIPAA Data  </a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='hipaa' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/hipaa/Delete'); ?>" class="nav-sub-link">Delete</a></li>

                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='mask' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Mask Data  </a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/mask/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='mask' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/mask/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent_to_verify' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Sent_to_verify'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					   	<li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected_by_client' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected_by_client'); ?><!--" class="nav-sub-link">Rejected By Client</a></li>-->

                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='heatpad' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Heating Pad Data  </a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/heatpad/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='heatpad' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='eatpad' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/heatpad/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent_to_verify' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Sent_to_verify'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					   	<li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected_by_client' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected_by_client'); ?><!--" class="nav-sub-link">Rejected By Client</a></li>-->

                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='aurbrac' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Brace Data  </a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/aurbrac/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent_to_verify' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Sent_to_verify'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					   	<li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected_by_client' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Rejected_by_client'); ?><!--" class="nav-sub-link">Rejected By Client</a></li>-->

                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/aurbrac/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub ">
                        <i class="typcn typcn-clipboard"></i>CGM Data</a>
                    <ul class="nav-sub">

                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/cgmdiab/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent_to_verify' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Sent_to_verify'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					  					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected_by_client' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Rejected_by_client'); ?><!--" class="nav-sub-link">Rejected By Client</a></li>-->

                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='faxpdf' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub ">
                        <i class="typcn typcn-clipboard"></i>PDF List</a>
                    <ul class="nav-sub">

                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/faxpdf/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Sent'); ?>" class="nav-sub-link">Processing</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Delivered' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Delivered'); ?>" class="nav-sub-link">Sent Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Not_sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Not_sent'); ?>" class="nav-sub-link">Send Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/Owner/campaign_list/faxpdf/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent_to_verify' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Sent_to_verify'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					  					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected_by_client' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Rejected_by_client'); ?><!--" class="nav-sub-link">Rejected By Client</a></li>-->

                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/Owner/campaign_list/cgmdiab/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item ">
                    <a href="<?php echo base_url('admin/Owner/save_agent'); ?>" class="nav-link"><i class="typcn typcn-clipboard"></i>Add User</a>
                </li>
            <?php } ?>
            <?php if($this->session->userdata("role")== "QuantityManager") { ?>

                <li class="nav-item <?php echo ($this->uri->segment(2))=='QuantityManager' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Pages Manager</a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(2))=='QuantityManager' ? (($this->uri->segment(2))=='QuantityManager' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QuantityManager'); ?>" class="nav-sub-link">Monthly Form</a></li>
                    </ul>
                </li>
            <?php } ?>
            <?php if($this->session->userdata("role")== "Viewer") { ?>

                <li class="nav-item <?php echo ($this->uri->segment(4))=='aurbrac' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Brace Data  </a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/aurbrac/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/aurbrac/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/aurbrac/Sent'); ?>" class="nav-sub-link">Sent Failure</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/aurbrac/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Sent'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/aurbrac/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>
                <li class="nav-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? 'show' : ''?>">
                    <a href="index.html" class="nav-link with-sub ">
                        <i class="typcn typcn-clipboard"></i>CGM Data</a>
                    <ul class="nav-sub">

                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/viewer/campaign_list/cgmdiab/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cannotreach' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Cannotreach'); ?><!--" class="nav-sub-link">Can’t Reach</a></li>-->
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/cgmdiab/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/cgmdiab/Sent'); ?>" class="nav-sub-link">RX Requested</a></li>
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/viewer/campaign_list/cgmdiab/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Sent'); ?><!--" class="nav-sub-link">Sent to Verifications</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Pending' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Pending'); ?><!--" class="nav-sub-link">Pending</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->
                        <!--					   <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Archive' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/viewer/campaign_list/cgmdiab/Archive'); ?><!--" class="nav-sub-link">Archive</a></li>-->


                    </ul>
                </li>

            <?php } ?>

            <?php if($this->session->userdata("role")== "QA") { ?>

                <li class="nav-item <?php echo ($this->uri->segment(3))=='getlist' ? 'show' : ''?>">
                    <a href="All" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Received Fax</a>
                    <ul class="nav-sub">
                        <li class="nav-sub-item <?php echo ($this->uri->segment(4)=='All') ? 'active' : ''?> "><a href="<?php echo base_url('admin/get/getlist/All'); ?>" class="nav-sub-link">All</a></li>
                        <?php if($this->session->userdata("cgmdiab_tab")=='1'){ ?>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4)=='cgmdiab_receive') ? 'active' : ''?> "><a href="<?php echo base_url('admin/get/getlist/cgmdiab_receive'); ?>" class="nav-sub-link">CGM</a></li>
                        <?php } if($this->session->userdata("aurbrac_tab")=='1'){ ?>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4)=='aurbrac_receive') ? 'active' : ''?> "><a href="<?php echo base_url('admin/get/getlist/aurbrac_receive'); ?>" class="nav-sub-link">Brace</a></li>
                        <?php } ?>
                    </ul>

                </li>
                <?php if($this->session->userdata("aurbrac_tab")=='1'){?>

                    <li class="nav-item <?php echo ($this->uri->segment(4))=='aurbrac' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub "><i class="typcn typcn-clipboard"></i>Brace Data  </a>
                        <ul class="nav-sub">
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/aurbrac/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/aurbrac/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/aurbrac/Sent'); ?>" class="nav-sub-link">RX Requested</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/aurbrac/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/aurbrac/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/aurbrac/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='aurbrac' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/aurbrac/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->


                        </ul>
                    </li>
                <?php } if($this->session->userdata("cgmdiab_tab")=='1'){?>


                    <li class="nav-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub ">
                            <i class="typcn typcn-clipboard"></i>CGM Data</a>
                        <ul class="nav-sub">

                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/QA/campaign_list/cgmdiab/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/cgmdiab/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/cgmdiab/Sent'); ?>" class="nav-sub-link">RX Requested</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/cgmdiab/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->


                        </ul>
                    </li>

                <?php  } if($this->session->userdata("faxpdf_tab")=='1'){?>


                    <li class="nav-item <?php echo ($this->uri->segment(4))=='faxpdf' ? 'show' : ''?>">
                        <a href="index.html" class="nav-link with-sub ">
                            <i class="typcn typcn-clipboard"></i>PDF List</a>
                        <ul class="nav-sub">

                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Cleaning' ? 'active' : '') : ''?>"><a href="<?php echo base_url('admin/QA/campaign_list/faxpdf/Cleaning'); ?>" class="nav-sub-link">Leads</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Good' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/faxpdf/Good'); ?>" class="nav-sub-link">Ready To Fax</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Sent' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/faxpdf/Sent'); ?>" class="nav-sub-link">RX Requested</a></li>
                            <li class="nav-sub-item <?php echo ($this->uri->segment(4))=='faxpdf' ? (($this->uri->segment(5))=='Delete' ? 'active' : '') : ''?> "><a href="<?php echo base_url('admin/QA/campaign_list/faxpdf/Delete'); ?>" class="nav-sub-link">Delete</a></li>
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Rejected' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Rejected'); ?><!--" class="nav-sub-link">Rejected</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Deleted' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Deleted'); ?><!--" class="nav-sub-link">Deleted</a></li>-->
                            <!--            <li class="nav-sub-item --><?php //echo ($this->uri->segment(4))=='cgmdiab' ? (($this->uri->segment(5))=='Completed' ? 'active' : '') : ''?><!-- "><a href="--><?php //echo base_url('admin/QA/campaign_list/cgmdiab/Completed'); ?><!--" class="nav-sub-link">Successful</a></li>-->


                        </ul>
                    </li>
                <?php } ?>





            <?php } ?>

        </ul><!-- nav -->
    </div><!-- az-sidebar-body -->
</div><!-- az-sidebar -->

