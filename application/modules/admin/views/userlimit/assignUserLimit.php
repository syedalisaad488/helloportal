<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="az-content az-content-dashboard-five">
    <div class="az-header">
        <div class="container-fluid">
            <div class="az-header-left">
                <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
            </div><!-- az-header-left -->

            <div class="az-header-right">

                <div class="">
                    <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
                </div>

            </div><!-- az-header-right -->
        </div><!-- container -->
        <?php $faxremaing =0; foreach($user as $users){
            if($users->userlimit_month == date('M-Y') OR $users->userlimit_month ==  null){  $faxremaing +=$users->userlimit_send_static; } }?>

    </div><!-- az-header -->
    <div class="az-content-header d-block d-md-flex">

    </div><!-- az-content-header -->
    <div class="az-content-body">

        <div style="background: white; padding: 10px;">
            <form action="<?= base_url('admin/Userlimit/assgin_limit_user') ?>" method="post">
                <input type="hidden" name="token" value="<?= $token; ?>">
                <?= $this->session->flashdata("errorMsg"); ?>
                <div class="row">

                    <div class="col-md-2">
                        <label  class="label"> Total pages of faxes</label><br>
                        <label  class="label"> <?= isset($userlimit_total_fax[0]->pgquan_send_quan_static)? $userlimit_total_fax[0]->pgquan_send_quan_static  :'0' ?></label>
                    </div>
                    <div class="col-md-2">
                        <label  class="label"> Pages of faxes assigned</label><br>
                        <label  class="label"> <?= isset($faxremaing)? $faxremaing :'0' ?></label>
                    </div>
                    <div class="col-md-2">
                        <label  class="label"> Pages of faxes remain</label><br>
                        <label  class="label"> <?= isset($userlimit_total_fax[0]->pgquan_send_quan_static)? $userlimit_total_fax[0]->pgquan_send_quan_static -$faxremaing :'0' ?></label>
                    </div>
                    <div class="col-md-3">
                        <label  class="label">Number of value to assign</label>

                        <input class="form-control" name="userlimit_send_static" min="1" max="<?= isset($userlimit_total_fax[0]->pgquan_send_quan_static)? $userlimit_total_fax[0]->pgquan_send_quan_static  -$faxremaing :'0' ?>" type="number" placeholder="number of value to assign" required>
                        <input class="form-control" name="userlimit_month" value="<?= date('M-Y') ?>" hidden>
                        <input class="form-control" name="IDpgquan" value="<?= isset($userlimit_total_fax[0]->IDpgquan)? $userlimit_total_fax[0]->IDpgquan :'0' ?>" hidden>
                    </div>
                    <div class="col-md-2">
                        <label>User Name</label>
                        <select class="form-control" name="userID" required>
                            <?php foreach($select_user as $select_users) {?>
                            <option value="<?=$select_users->id ?>"><?=$select_users->name ?></option>
                            <?php  }  ?>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <br>
                        <input class="btn btn-success" type="submit" value="Assign">
                    </div>
                </div>
            </form>
            <br>
            <table id="example" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>User Name</th>
                    <th>Total Assign</th>
                    <th>total Used</th>
                    <th>Remaining</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $counting = array("total_assign"=>0,"total_used"=>0,"total_remain"=>0);
                $counter =0; foreach($user as $users){ ?>
                        <?php if($users->userlimit_month == date('M-Y') OR $users->userlimit_month ==  null){
                        $counting['total_assign']+=$users->userlimit_send_static;
                        $counting['total_used']+=$users->userlimit_send_dynamic;
                        $counting['total_remain']+=$users->userlimit_send_static-$users->userlimit_send_dynamic;
                        ?>
                <tr>
                    <td><?echo  $counter+=1;  ?></td>
                    <td><?=$users->name; ?></td>
                    <td><?=$users->userlimit_send_static; ?></td>
                    <td><?=$users->userlimit_send_dynamic; ?></td>
                    <td><?=$users->userlimit_send_static-$users->userlimit_send_dynamic; ?></td>


                </tr>
                <?php } }  ?>

                </tbody>
                <tfoot>
                <tr>
                      <th>ID </th>
                    <th>User Name</th>
                    <th>Total Assign</th>
                    <th>Total Used</th>
                    <th>Total Remaining</th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><?= $counting['total_assign'] ?></td>
                    <td><?= $counting['total_used'] ?></td>
                    <td><?= $counting['total_remain'] ?></td>
                </tr>
                </tfoot>
            </table>
        </div><!-- row -->
    </div><!-- az-content-body -->

    <?php $this->load->view('include/footer');?>
</div><!-- az-content -->



<script>
    $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
            e.preventDefault();
            $(this).parent().toggleClass('show');
            $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
            e.stopPropagation();

            // closing of sidebar menu when clicking outside of it
            if(!$(e.target).closest('.az-header-menu-icon').length) {
                var sidebarTarg = $(e.target).closest('.az-sidebar').length;
                if(!sidebarTarg) {
                    $('body').removeClass('az-sidebar-show');
                }
            }
        });


        $('#azSidebarToggle').on('click', function(e){
            e.preventDefault();

            if(window.matchMedia('(min-width: 992px)').matches) {
                $('.az-sidebar').toggle();
            } else {
                $('body').toggleClass('az-sidebar-show');
            }
        })

        /* ----------------------------------- */
        /* Dashboard content */

        $.plot('#flotChart1', [{
            data: flotSampleData5,
            color: '#8039f4'
        }], {
            series: {
                shadowSize: 0,
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: { colors: [ { opacity: 0 }, { opacity: 0.12 } ] }
                }
            },
            grid: {
                borderWidth: 0,
                labelMargin: 10,
                markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
            },
            yaxis: { show: false },
            xaxis: {
                show: true,
                position: 'top',
                color: 'rgba(102,16,242,.1)',
                reserveSpace: false,
                ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
                font: {
                    size: 10,
                    weight: '500',
                    family: 'Roboto, sans-serif',
                    color: '#999'
                }
            }
        });

        $.plot('#flotChart2', [{
            data: flotSampleData2,
            color: '#007bff'
        }], {
            series: {
                shadowSize: 0,
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: { colors: [ { opacity: 0 }, { opacity: 0.5 } ] }
                }
            },
            grid: {
                borderWidth: 0,
                labelMargin: 10,
                markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
            },
            yaxis: { show: false },
            xaxis: {
                show: true,
                position: 'top',
                color: 'rgba(102,16,242,.1)',
                reserveSpace: false,
                ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
                font: {
                    size: 10,
                    weight: '500',
                    family: 'Roboto, sans-serif',
                    color: '#999'
                }
            }
        });

        $.plot('#flotChart3', [{
            data: flotSampleData5,
            color: '#00cccc'
        }], {
            series: {
                shadowSize: 0,
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: { colors: [ { opacity: 0.2 }, { opacity: 0.5 } ] }
                }
            },
            grid: {
                borderWidth: 0,
                labelMargin: 10,
                markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
            },
            yaxis: { show: false },
            xaxis: {
                show: true,
                position: 'top',
                color: 'rgba(102,16,242,.1)',
                reserveSpace: false,
                ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
                font: {
                    size: 10,
                    weight: '500',
                    family: 'Roboto, sans-serif',
                    color: '#999'
                }
            }
        });

        $.plot('#flotPie', [
            { label: 'Very Satisfied', data: [[1,25]], color: '#6f42c1'},
            { label: 'Satisfied', data: [[1,38]], color: '#007bff'},
            { label: 'Not Satisfied', data: [[1,20]], color: '#00cccc'},
            { label: 'Very Unsatisfied', data: [[1,15]], color: '#969dab'}
        ], {
            series: {
                pie: {
                    show: true,
                    radius: 1,
                    innerRadius: 0.5,
                    label: {
                        show: true,
                        radius: 3/4,
                        formatter: labelFormatter
                    }
                }
            },
            legend: { show: false }
        });

        function labelFormatter(label, series) {
            return '<div style="font-size:11px; font-weight:500; text-align:center; padding:2px; color:white;">' + Math.round(series.percent) + '%</div>';
        }

        var ctx6 = document.getElementById('chartStacked1');
        new Chart(ctx6, {
            type: 'bar',
            data: {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [{
                    data: [10, 24, 20, 25, 35, 50, 20, 30, 28, 33, 45, 65],
                    backgroundColor: '#6610f2',
                    borderWidth: 1,
                    fill: true
                },{
                    data: [20, 30, 28, 33, 45, 65, 25, 35, 50, 20, 30, 28],
                    backgroundColor: '#00cccc',
                    borderWidth: 1,
                    fill: true
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    labels: {
                        display: false
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            fontSize: 11
                        }
                    }],
                    xAxes: [{
                        barPercentage: 0.4,
                        ticks: {
                            fontSize: 11
                        }
                    }]
                }
            }
        });
    });
</script>
</body>
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "order": [[ 3, "desc" ]]
        } );
    } );
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
