<!DOCTYPE html>
<html><head>
	<title></title>

</head><body>
	<style type="text/css">
		p{
			font-size: 11px;
		}
		h5{
			padding: 0px;
			margin: 0px;

		}
	</style>
		<?php foreach($fax as $key){ ?>
	<div>
		<div style=" width: 100%;">
			<h4 style="text-align: right">
				Balli Medical FAX BACK TO:<br>
			DETAILED WRITTEN ORDER - CERTIFICATE OF MEDICAL NECESSITY<br>
			</h4>
			
		</div>
		<hr>
		<div style="width: 100%;">
			<h5 style="text-decoration: underline;">INSTRUCTIONS</h5>
			<p>
			1. Complete all fields on this Detailed Written Order. 1. Complete all fields on this Detailed Written Order. <br>
			2. Use the Noridian November 2017 Physician Resource Letter (Continuous Glucose Monitors) to confirm coverage criteria and medical necessity documentation requirements are met. <br>
			3. Fax both this order and the patient’s most recent medical records that demonstrate coverage criteria are met to a DME supplier that provides the FreeStyle Libre 14 day system.<br>
			</p>
		</div>
	
		<div style="width: 100%; float: left; margin-bottom:10px; ">
			<div style="width: 49%; float:left; border-style: solid; border-width: 1px;">
				<h5 style="text-decoration: underline;">PATIENT INFORMATION </h5>
				<p style="padding: 5px;">
				NAME:<?=$key->client_first_name.''.$key->client_last_name;?>     <br>
				DATE OF BIRTH DATE OF:<br>
				PHONE: <?=$key->client_phone;?><br>
				<!--EMAIL: <?=$key->email_phone;?>     <br>-->
				ADDRESS:<?=$key->client_address;?> <br>
				PLEASE PROVIDE PATIENT INSURANCE:<br>
				MEDICARE #__________________________________ <br>
				INSURANCE: __________________________________ <br>
				GROUP: _____________________________________ <br>
				ID: _________________________________________ <br>					
				</p>

				
				
			</div>
			<div  style="width: 49%; float:right; border-style: solid; border-width: 1px;">
				<h5 style="text-decoration: underline;">PHYSICIAN INFORMATION</h5>
				<p style="padding: 5px;">
					PHYSICIAN NAME:<?=$key->client_doc_first_name.''.$key->client_doc_last_name;?> <br>
					PHONE:<?=$key->client_doc_phone;?>    <br>
					FAX:<?=$key->client_doc_fax;?><br>
					NOTES:<br>
					__________________________________ <br>
					__________________________________<br>    
					__________________________________<br>
					__________________________________ <br>
					__________________________________<br>    

				</p>			
				
			</div>
		</div>


		<div >			
			<div  style="width: 49%; float:left; border-style: solid; border-width: 1px;">
				<h5 style="border-style: solid; border-width: 1px;">K0554 (Freestyle Libre 14 day Reader)</h5>
				<p style="padding: 5px;">					
					1 Reader / 1095 days / No Refills <br> 
					Length of Need: Lifetime-unless specified otherwise: <br>
				</p>
			</div>
			<div  style="width: 49%; float:left; border-style: solid;   border-width: 1px;">
				<h5 style="border-style: solid; border-width: 1px;">K0554 (Freestyle Libre 14 day Reader)</h5>
				<p style="padding: 5px;">				
					2 Units / 28 days (2 Units = 1 month of sensors ) / 12 Refills <br>
					Length of Need: Lifetime-unless specified otherwise:      <br>
				</p>
			</div>
	</div>
	<div>
		<h5 style="text-decoration: underline;">DIAGNOSIS (ICD-10) & SUPPORTING CLINICAL INDICATIONS <span style="text-decoration: none; font-style: none; font-weight: 0px;">- Please check all that apply</span></h5> 
		<p>
			<b>				
				[    ] E10.9       [    ] E11.65       [    ] E10.65       [   ] E11.8       [    ] E11.9       [    ] OTHER: __________________ 
			</b>			
		</p>
		<div  style="width: 50%; float:left;">
			<p>				
				[  ]  History of hypoglycemia unawareness <br>
				[  ]  History of nocturnal hypoglycemia <br> 
				[  ]  Evidence of unexplained severe hypoglycemia requiring externalassistance for recovery external assistance for recovery <br>
				[  ]  DawnPhenomenom - fasting Dawn Phenomenom - fasting blood glucose often exceeds 200mg/dl<br>
				[  ]  Poor glycemic control as evidenced by 72 hour CGMS sensing trial sensing trial <br>
				[  ]  Patient has completed comprehensive diabetes education <br>
			</p>
		</div>

		<div  style="width: 50%; float:left;">
			<p>
				[  ]  History of glycemic excursions <br> 
				[  ]  Recurring  Recurring episodes of severe hypoglycemia <br>
				[  ]  Patient hospitalized or has required paramedical treatment for low blood sugar for low blood sugar <br>
				[  ]  History of suboptimal glycemic control before or during pregnancy pregnancy <br>
				[  ]  Multiple alterations in self-monitoring and insuin administrations to optimize care administrations to optimize care <br>
				[  ]  Patient motivated to achieve and maintain glycemic control<br>				
			</p>
			
		</div>
		
	</div>
	<div>
		<h5 style="text-decoration: underline;">CURRENT INSULIN REGIMEN:</h5>
		<p>
			[   ] INSULIN PUMP  [   ]    MULTIPLE DAILY INJECTIONS - # PER DAY: ____________ [   ] OTHER: _______________ <br>
			I certify that I am the physician identified in the “Physician Information” section above and hereby attest that the medical necessity information is true, accurate, and complete to the best of my knowledge.I understand that any falsification, omission, or concealment of material fact may subject me to administrative, civil, or criminal liability. The patient/caregiver is capable and has successfully completed or will be trained on the proper use of the products prescribed on this order.<br>			
		</p>
	</div>
	<div>
		<p>		
			<b>
			
			Physician Signature: _______________________________________________       Date: _____________________ <br>
		</b>	
		</p>
	</div>
	<div>
		<p>
			It is ultimately the responsibility of the healthcare professional/persons associated with the patient’s care to determine and document the appropriate diagnosis(es) and code(s) for the patient’s condition. There is not a guarantee that the use of any information provided in this form will result in coverage or payment by any third-party payer. healthcare provider is ultimately responsible for verifying codes, coverage, and payment policies used to ensure that they are accurate for the services and items provided.
		</p>
		
	</div>
	<div>
		<p style="font-size: 26px; text-align: center;">
			FAX TO:			
		</p>
	</div>
			<?php } ?>

 
</body></html>