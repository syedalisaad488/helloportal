<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">


            <div class="">
            <button class="btn btn-primary">Edit Profile</button>
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">

      </div><!-- az-content-header -->
      <div class="az-content-body">
<?php foreach($data->result() as $key){

	?>

        <div style="background: white; padding: 10px;">
          <form action="<?php echo base_url('admin/Owner/save_agent/'.$key->id);?>" method="post">
                <div class="row">

                    <div class="col-md-4">
						<label>Role</label>

						<select class="form-control" required="required" name="role">
                          <option value="User"<?php $key->id=='User'? 'selected':'';?>>User</option>

                        </select>
                    </div>
                    <div class="col-md-3">
						<label>User Name</label>
                        <input class="form-control" required="required" name="name" type="text" value="<?=isset($key->name) ? $key->name : ''; ?>" placeholder="Agent Name">
                    </div>
                    <div class="col-md-3">
						<label>Password</label>

						<input class="form-control" required="required" name="password" type="text" value="<?=isset($key->password) ? $key->password : ''; ?>" >
                    </div>
					<div class="col-md-3">
						<label>Email</label>

						<input class="form-control"  name="email" type="text" value="<?=isset($key->email) ? $key->email : ''; ?>" placeholder="username@usercompanydomain.com">
					</div>
<!--					<div class="col-md-3">-->
<!--						<input class="form-control" required="required" name="company_name" value="--><?//=isset($key->compnay_name) ? $key->compnay_name : ''; ?><!--" type="text" placeholder="Company Name">-->
<!--					</div>-->
						<div class="col-md-3">
							<label>Receive Number</label>

							<input class="form-control" required="required" name="receive_num" value="<?=isset($key->receive_num) ? $key->receive_num : ''; ?>" type="number" placeholder="No that you reveived fax">
						</div>
						<div class="col-md-3">
							<label>Send Number</label>
							<input class="form-control" required="required" name="sent_name" value="<?=isset($key->sent_num) ? $key->sent_num : ''; ?>" type="number" placeholder="No that your fax will be sent">
						</div>


						<div class="col-md-3">
							<label>Campaign</label>

							<div class="row">
								<div class="col-md-12">
									<label>Mask</label>
									<input class="check"  name="mask" type="checkbox" value='4' <?=isset($key->mask_tab) ? $key->mask_tab=='1' ?  'checked' : '' : '' ; ?> placeholder="Password">
									<label>Heating Pad</label>
									<input class="check"  name="heatpad" type="checkbox" value='3' <?=isset($key->heatpad_tab) ? $key->heatpad_tab=='1' ?  'checked' : '' : '' ; ?> placeholder="Password">
									<label>CGM</label>
									<input class="check" name="cgmdiab" type="checkbox" value='1' <?=isset($key->cgmdiab_tab) ? $key->cgmdiab_tab=='1' ?  'checked' : '' : '' ; ?> placeholder="Password">
								</div>
								<div class="col-md-12">
									<label>Brace</label>
									<input class="check"  name="aurbrac" type="checkbox" value='2' <?=isset($key->aurbrac_tab) ? $key->aurbrac_tab=='1' ?  'checked' : '' : '' ; ?> placeholder="Password">
                                    <label>Hipaa</label>
                                    <input class="check"  name="hipaa" type="checkbox" value='2' <?=isset($key->hipaa_tab) ? $key->hipaa_tab=='1' ?  'checked' : '' : '' ; ?> placeholder="Password">

                                </div>
							</div>



					</div>
                  <?php } ?>
                    <div class="col-md-1">
                        <input class="btn btn-primary" type="submit" value="Edit">
                    </div>
                </div>
            </form>
            <br>

          </div><!-- row -->
      </div><!-- az-content-body -->

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Ucored </span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->



    <script>
      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })

        /* ----------------------------------- */
        /* Dashboard content */

        $.plot('#flotChart1', [{
            data: flotSampleData5,
            color: '#8039f4'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.12 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart2', [{
            data: flotSampleData2,
            color: '#007bff'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart3', [{
            data: flotSampleData5,
            color: '#00cccc'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0.2 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotPie', [
          { label: 'Very Satisfied', data: [[1,25]], color: '#6f42c1'},
          { label: 'Satisfied', data: [[1,38]], color: '#007bff'},
          { label: 'Not Satisfied', data: [[1,20]], color: '#00cccc'},
          { label: 'Very Unsatisfied', data: [[1,15]], color: '#969dab'}
        ], {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 3/4,
                formatter: labelFormatter
              }
            }
          },
          legend: { show: false }
        });

        function labelFormatter(label, series) {
          return '<div style="font-size:11px; font-weight:500; text-align:center; padding:2px; color:white;">' + Math.round(series.percent) + '%</div>';
        }

        var ctx6 = document.getElementById('chartStacked1');
        new Chart(ctx6, {
          type: 'bar',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
              data: [10, 24, 20, 25, 35, 50, 20, 30, 28, 33, 45, 65],
              backgroundColor: '#6610f2',
              borderWidth: 1,
              fill: true
            },{
              data: [20, 30, 28, 33, 45, 65, 25, 35, 50, 20, 30, 28],
              backgroundColor: '#00cccc',
              borderWidth: 1,
              fill: true
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false,
                labels: {
                  display: false
                }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true,
                  fontSize: 11
                }
              }],
              xAxes: [{
                barPercentage: 0.4,
                ticks: {
                  fontSize: 11
                }
              }]
            }
          }
        });
      });
    </script>
  </body>


<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
