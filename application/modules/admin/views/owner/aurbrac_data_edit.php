<?php $this->load->view("include/head"); ?>
      <?php $this->load->view("include/nav"); ?>

    
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
         
           <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href='<?php echo base_url("admin/Dashboard/logout/").$prefix; ?>'> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
          
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
        <h3>Client Entry Form</h3>
      </div><!-- az-content-header -->
      <div class="az-content-body">
      <form method="post" action="<?php echo base_url("admin/Owner/campaign_update_data/").$IDcampaign."/".$prefix; ?>"  enctype="multipart/form-data">
         <?php if($this->session->flashdata("data_saved")){ ?>
                           <div class="alert alert-success">
                    <?php echo $this->session->flashdata("data_saved"); ?>
                              </div>
              <?php } ?>
        <div style="background: white; padding: 10px;">
         <h4>Client Details</h4>
          <div class="row">
            <div class="col-md-3" class="form-group"><br>
              <label>First Name</label>
              <input type="text" value="<?php echo $campaign_data[0]['aurbrac_firstname']; ?>" name="aurbrac_firstname" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>last Name</label>
              <input type="text" value="<?php echo $campaign_data[0]['aurbrac_lastname']; ?>" name="aurbrac_lastname" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>City</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_city']; ?>" name="aurbrac_city" class="form-control">
            </div>
            <div class="col-md-3" class="form-group"><br>
                <label>State</label>
                <input type="text" value="<?php echo $campaign_data[0]['aurbrac_state']; ?>" name="aurbrac_state" class="form-control">
            </div>  
              <div class="col-md-12" class="form-group"><br>
                <label>Address</label>
                <input type="text" value="<?php echo $campaign_data[0]['aurbrac_address']; ?>" name="aurbrac_address" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Zip</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_zip']; ?>" name="aurbrac_zip" class="form-control">
              </div>

              <div style="margin-top:18px;" class="col-md-12">
              <label><h4>Product</h4></label>
                  
              </div>
             
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-2">
                  <label>Left wrist</label>
                  <input style="display:none;" type="checkbox" checked value="2" name="aurbrac_product[]">
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_l_wrist'] == 1 ? 'checked' : ''; ?> value="l_wrist" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>Right wrist</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_r_wrist'] == 1 ? 'checked' : ''; ?> value="r_wrist" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>Right elbow</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_r_elbow'] == 1 ? 'checked' : ''; ?> value="r_elbow" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>Right ankle</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_r_ankle'] == 1 ? 'checked' : ''; ?> value="r_ankle" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>Right knee</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_r_knee'] == 1 ? 'checked' : ''; ?> value="r_knee" name="aurbrac_product[]">
                  </div>
                 
               

                </div>
                <div class="row">
                  <div class="col-md-2">
                  <label>Left elbow</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_l_elbow'] == 1 ? 'checked' : ''; ?> value="l_elbow" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>back</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_back'] == 1 ? 'checked' : ''; ?> value="back" name="aurbrac_product[]">
                  </div>
                
                 
                 
                    <div class="col-md-2">
                  <label>hip</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_hip'] == 1 ? 'checked' : ''; ?> value="hip" name="aurbrac_product[]">
                  </div>
                </div>
                 <div class="row">
                  <div class="col-md-2">
                  <label>Left knee</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_l_knee'] == 1 ? 'checked' : ''; ?> value="l_knee" name="aurbrac_product[]">
                  </div>
                   <div class="col-md-2">
                  <label>Left ankle</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_l_ankle'] == 1 ? 'checked' : ''; ?> value="l_ankle" name="aurbrac_product[]">
                  </div>
                
                    <div class="col-md-2">
                  <label>Left shoulder</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_l_shoulder'] == 1 ? 'checked' : ''; ?> value="l_shoulder" name="aurbrac_product[]">
                  </div>
                    <div class="col-md-2">
                  <label>Right shoulder</label>
                  <input type="checkbox" <?php echo $campaign_data[0]['aurbrac_r_shoulder'] == 1 ? 'checked' : ''; ?> value="r_shoulder" name="aurbrac_product[]">
                  </div>
                </div>
              </div>
             
          </div>
         
              <div class="row"> 
              <div class="col-md-4"><br>
              <label>Doctor First Name</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_firstname']; ?>" name="aurbrac_doc_firstname" class="form-control">
            </div>
            <div class="col-md-4"><br>
                <label>Doctor Last Name</label>
                <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_lastname']; ?>" name="aurbrac_doc_lastname" class="form-control">
            </div>
              <div class="col-md-4"><br>
              <label>Doctor Address</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_address']; ?>" name="aurbrac_doc_address" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor City</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_city']; ?>" name="aurbrac_doc_city" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Doctor State</label>
                <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_state']; ?>" name="aurbrac_doc_state" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor ZIP</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_zip']; ?>" name="aurbrac_doc_zip" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor Phone</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_phone']; ?>" name="aurbrac_doc_phone" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor Fax</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_fax']; ?>" name="aurbrac_doc_fax" class="form-control">
              </div>
              <div class="col-md-4"><br>
                <label>Doctor NPI</label>
                <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_npi']; ?>" name="aurbrac_doc_npi" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Dr. PECOS certificate</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_doc_pecos_cert']; ?>" name="aurbrac_doc_pecos_cert" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>PCP within Past</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_pcp_within_past']; ?>"  name="aurbrac_pcp_within_past" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Recording File</label>
                  <input type="text" value="<?php echo $campaign_data[0]['aurbrac_record_file']; ?>" name="aurbrac_record_file" class="form-control">
              </div>
            
              <input type="hidden" name="IDrepresentative" value="<?php echo $this->session->userdata("id"); ?>">

              <br><br> <br><br><br><br>
              <div class="col-md-12">
              <button  style="float:right;" class="btn btn-success">Submit</button>
              </div>

          </div>
         </form>
          </div><!-- row -->
      </div><!-- az-content-body -->

   <?php $this->load->view('include/footer');?>
    </div><!-- az-content -->


  
    <script>
      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })

        /* ----------------------------------- */
        /* Dashboard content */

        $.plot('#flotChart1', [{
            data: flotSampleData5,
            color: '#8039f4'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.12 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart2', [{
            data: flotSampleData2,
            color: '#007bff'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart3', [{
            data: flotSampleData5,
            color: '#00cccc'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0.2 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotPie', [
          { label: 'Very Satisfied', data: [[1,25]], color: '#6f42c1'},
          { label: 'Satisfied', data: [[1,38]], color: '#007bff'},
          { label: 'Not Satisfied', data: [[1,20]], color: '#00cccc'},
          { label: 'Very Unsatisfied', data: [[1,15]], color: '#969dab'}
        ], {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 3/4,
                formatter: labelFormatter
              }
            }
          },
          legend: { show: false }
        });

        function labelFormatter(label, series) {
          return '<div style="font-size:11px; font-weight:500; text-align:center; padding:2px; color:white;">' + Math.round(series.percent) + '%</div>';
        }

        var ctx6 = document.getElementById('chartStacked1');
        new Chart(ctx6, {
          type: 'bar',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
              data: [10, 24, 20, 25, 35, 50, 20, 30, 28, 33, 45, 65],
              backgroundColor: '#6610f2',
              borderWidth: 1,
              fill: true
            },{
              data: [20, 30, 28, 33, 45, 65, 25, 35, 50, 20, 30, 28],
              backgroundColor: '#00cccc',
              borderWidth: 1,
              fill: true
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false,
                labels: {
                  display: false
                }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true,
                  fontSize: 11
                }
              }],
              xAxes: [{
                barPercentage: 0.4,
                ticks: {
                  fontSize: 11
                }
              }]
            }
          }
        });
      });
    </script>
  </body>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
