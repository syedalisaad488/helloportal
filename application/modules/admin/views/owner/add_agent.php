<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">


            <div class="">
            <button class="btn btn-primary">Edit Profile</button>
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">

      </div><!-- az-content-header -->
      <div class="az-content-body">

        <div style="background: white; padding: 10px;">
          <form action="<?php echo base_url('admin/Owner/save_agent');?>" method="post">
                <div class="row" style="margin-bottom:5px;">
                    <div class="col-md-3">
                        <select class="form-control" required="required" name="role">
                          <option value="User">User</option>
							<option value="QA">QA</option>
							<option value="Viewer">Viewer</option>



						</select>
                    </div>
                    <div class="col-md-3">
                        <input class="form-control" required="required" value="<?php echo set_value('username'); ?>" name="name" type="text" placeholder="User Name">
                        <div class="text-danger" ><?php echo $this->session->flashdata("unique_field"); ?></div>
                    </div>
                    <div class="col-md-3">
                        <input class="form-control" required="required" name="email" type="text" placeholder="username@usercompanydomain.com">
                    </div>
<!--                    <div class="col-md-3">-->
<!--                        <input class="form-control"  required="required" name="company_name" type="text" placeholder="Company Name">-->
<!--                    </div>-->
                    </div>
                    <div class="row" style="margin-bottom:5px;">
                    <div class="col-md-3">
                        <input class="form-control" required="required" name="receive_num" type="number" placeholder="No that you reveived fax">
                    </div>
                    <div class="col-md-3">
                        <input class="form-control" required="required" name="sent_name" type="number" placeholder="No that your fax will be sent">
                    </div>

                    <div class="col-md-3">
                        <input class="form-control" required="required" name="password" type="text" placeholder="Password">
                    </div>
                    <div class="col-md-3">

						<div class="row">
							<div class="col-md-12">
								<label>Mask</label>
								<input class="check"  name="mask" type="checkbox" value='4' placeholder="Password">
								<label>Heating Pad</label>
								<input class="check"  name="heatpad" type="checkbox" value='3' placeholder="Password">
								<label>CGM</label>
								<input class="check" name="cgmdiab" type="checkbox" value='1' placeholder="Password">
							</div>
							<div class="col-md-12">

								<label>Brace</label>
                                <input class="check"  name="aurbrac" type="checkbox" value='2' placeholder="Password">
                                <label>HIPAA</label>
                                <input class="check"  name="hipaa" type="checkbox" value='2' placeholder="Password">
							</div>
						</div>
					</div>

                    <div class="col-md-12 repcode">
                        <?php foreach($rep_code as $key){?>

                        <input class="check" name="repcode[]" type="checkbox" value='<?php echo $key->aurbrac_repcode ?>' placeholder="">
                                                <label><?php echo $key->aurbrac_repcode ?></label>

                        <?php
                        } ?>
                    </div>

                </div>
                <div class="col-md-1">
                        <input class="btn btn-success" type="submit" value="ADD USER">
                    </div>
            </form>
            <br>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Uesr ID</th>
                <th>Full Name</th>
                <th>Password</th>
                <th>Role</th>
                <th>Received No</th>
                <th>Sented No</th>
				<th>Mask</th>
				<th>Heating Pad</th>
				<th>CGM</th>
				<th>Brace</th>
				<th>HIPPA</th>
<!--                <th>Action</th>-->
<!--                <th>Action</th>-->
            </tr>
        </thead>
        <tbody>
          <?php foreach($data->result() as $key){ ?>
            <tr>
                <td><?=$key->id; ?></td>
                <td><?=$key->name; ?></td>
                <td><?=$key->password; ?></td>
                <td><?=$key->role; ?></td>
                <td><?=$key->receive_num; ?></td>
                <td><?=$key->sent_num; ?></td>
				<td><?=($key->mask_tab)=='1' ? 'Yes' : 'No'; ?></td>
				<td><?=($key->heatpad_tab)=='1' ? 'Yes' : 'No'; ?></td>
                <td><?=($key->cgmdiab_tab)=='1' ? 'Yes' : 'No'; ?></td>
                <td><?=($key->aurbrac_tab)=='1' ? 'Yes' : 'No'; ?></td>
                <td><?=($key->hipaa_tab)=='1' ? 'Yes' : 'No'; ?></td>
<!--                <td>--><?//=isset($key->active)!='0'?"<span class='btn btn-sm rounded-pill btn-outline-success'>Active</span>":"<span class='btn btn-sm rounded-pill btn-outline-danger'>Denied</span>" ?><!--</td>-->
<!---->
<!--                <td>-->
<!--                 <a href="--><?php ////echo base_url('admin/Owner/save_agent/'.$key->id);?><!--"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>-->
<!--                    <a href="--><?php //echo base_url('admin/Owner/delete/'.$key->id);?><!--">-->
<!--                    <button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>-->
<!--                </td>-->

            </tr>
            <?php } ?>
        </tbody>
        <tfoot>

        </tfoot>
    </table>
          </div><!-- row -->
      </div><!-- az-content-body -->

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Ucored </span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->

  </body>

  <script>
 $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'lfrtip',

    } );
} );

	if($("select").val()=='Viewer'){
	  		$('.repcode').show();
	}else{
	    	  		$('.repcode').hide();

	}
 $('select').on('change', function(){
 	if($("select").val()=='QA' || $("select").val()=='Viewer'){

		$('input[name=email]').prop( "disabled", true );
		$('input[name=company_name]').prop( "disabled", true );
		$('input[name=receive_num]').prop( "disabled", true );
		$('input[name=sent_name]').prop( "disabled", true );

	}
 	else{
		$('input[name=email]').prop( "disabled", false );
		$('input[name=company_name]').prop( "disabled", false );
		$('input[name=receive_num]').prop( "disabled", false );
		$('input[name=sent_name]').prop( "disabled", false );
	}

	if($("select").val()=='Viewer'){
	  		$('.repcode').show();
	}else{
	    	  		$('.repcode').hide();

	}
 });
  </script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>




