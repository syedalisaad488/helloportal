<style>
	div.dt-buttons {
		float: right;
		margin-left:10px;
	}
	div.dataTables_wrapper {
		width:1045px;
		margin: 0 ;
	}
	th, td { white-space: nowrap; }
	th{
		background:white;
	}
	.DTFC_RightBodyWrapper{
		left: 20px!important;
	}
	.DTFC_RightHeadWrapper{
		left: 20px!important;

	}
	.DTFC_RightBodyLiner::-webkit-scrollbar {
		display: none;
	}
 .dataTables_processing {
        top: 64px !important;
        z-index: 11000 !important;
        font-size : 40px !important;
    }
    	.dataTables_scrollBody{
	      min-height: 200px;

	}

</style>
<?php

$prefix_segment = $this->uri->segment(4);


?>
<div class="az-content az-content-dashboard-five">
	<div class="az-header">
		<div class="container-fluid">
			<div class="az-header-left">
				<a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
			</div><!-- az-header-left -->
			<div class="az-header-right">


				<div class="">
					<button class="btn btn-primary">Edit Profile</button>
					<a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
				</div>
			</div>
		</div><!-- container -->
	</div><!-- az-header -->




	</form>
	<div class="az-content-body">


		<div style="background: white; padding: 10px;">
			<div id="checkboxlist">

					<form action="<?php echo base_url().'admin/Change/changetab/'.$prefix_segment; ?>" method="POST">


							<table id="user_data" class="display stripe row-border order-column" style="width:100%; font-size: 12px;">
								<thead>
								<tr>
									<th class="noExport">Select&nbsp;&nbsp;<input type="checkbox" name="sample" class="selectall"/></th>
<!--									<th>status</th>-->
<!--									<th>Reason</th>-->
									<th class="noExport">Dupe</th>
									<th>Fist name</th>
									<th>Last name</th>
									<th>address</th>
									<th>city</th>
									<th>state</th>
									<th>zip</th>
									<!--									<th>email</th>-->
									<th>dob</th>
									<th>phone</th>
									<th>gender</th>
									<th>Height</th>
									<th>Weight</th>
									<th>Pain Level</th>
									<!--									<th>ssn</th>-->
									<th>Back</th>
									<th>Left Knee</th>
									<th>Right Knee</th>
									<th>Left Wrist</th>
									<th>Right Wrist</th>
									<th>Left Ankle</th>
									<th>Right Ankle</th>
									<th>Left Shoulder</th>
									<th>Right Shoulder</th>
									<th>Left Elbow</th>
									<th>Right Elbow</th>
									<th>Left hip</th>
									<th>Right hip</th>
									<th>Neck</th>
									<th>Dr.First Name</th>
									<th>Dr.Last Name</th>
									<th>Dr.Address</th>
									<th>Dr.city</th>
									<th>Dr.State</th>
									<th>Dr.Zip</th>
									<th>Dr.NPI</th>
									<th>Dr.Phone</th>
									<th>Dr.Fax</th>
									<th>MedicareID#</th>
									<!--									<th>Insurance Company</th>-->
									<!--									<th>ID Member</th>-->
									<!--									<th>ID Group</th>-->
									<th class="noExport">Date Entered</th>
									<!--									<th>ID Track</th>-->
									<!--									<th>ID Site</th>-->
									<th>Call Back Number</th>
									<th>Center Code</th>

<!--									<th>Action</th>-->
								</tr>
								</thead>
								<tbody>
								<div class='row pd-10 px-2'>
									<div class="col-md-6">
										<?php
                                        echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cleaning</p>" : '';
										echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Can't Reach</p>" : '';
										echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
										echo ($this->uri->segment(5))=='Rxrequest' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
										echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
										echo ($this->uri->segment(5))=='Sent' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
										echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
										echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
										echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
										echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
										echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
										echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
										?>
									</div>
<!--									--><?php //if(($this->uri->segment(5))!='Cleaning'){?>
										<div class="col-md-4">
									<select class="form-control" name="status">
										<option value="Cleaning">Leads</option>
										<option value="Good">Ready To Fax</option>
										<option value="Sent">Sent Fax</option>
										<option value="Not_sent">Sent Failure</option>
										<option value="Delete">Delete</option>

									</select>
								</div>
								<div class="col-md-2">
									<input type="submit" value="Move To Tab" class="btn btn-success pull-right" >

								</div>
<!--									--><?php // }  if(($this->uri->segment(5))=='Cleaning') { ?>
<!--									<div class="col-md-4">-->
<!--										<select class="form-control" name="idqa">-->
<!--											--><?php //			foreach($qa_assign as $key1) {
//											 ?>
<!---->
<!--											<option value="--><?php //echo $key1->id;?><!--">--><?php //echo $key1->id.' '. $key1->name;?><!--</option>-->
<!--										--><?php //} ?>
<!---->
<!--										</select>-->
<!--									</div>-->
<!--									<div class="col-md-2">-->
<!--										<input type="submit" value="Assign QA" class="btn btn-success pull-right" >-->
<!---->
<!--									</div>-->
<!--								</div>-->
<!--								--><?php //} ?>

			</div>

            <tr>

				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
<!--                <td></td>-->
<!--                <td></td>-->
<!--                <td></td>-->
<!--                <td></td>-->
<!--                <td></td>-->
<!--                <td></td>-->
<!--                <td></td>-->
            </tr>


			</tbody>
			<tfoot>

			</tfoot>
			</table>
		</div>
		<div >

		</div>

		</form>

	</div><!-- row -->
</div><!-- az-content-body -->

<?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>


<script>
    $('.selectall').click(function() {
        if ($(this).is(':checked')) {
            $('div input').attr('checked', true);
        } else {
            $('div input').attr('checked', false);
        }
    });
    $(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            dom: 'Bfrltip',
            scrollX: true,
            // info: false,
            processing:true,
            serverSide:true,
            order:[],
            // fixedColumns:{
            //     leftColumns: false,
            //     rightColumns: 1
            // },
            columnDefs: [ {
            "targets": 0,
            "orderable": false
            } ],

            ajax:{
                url:"<?php echo base_url() . 'admin/Owner/fetch_camp_data/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>",
                dataType: "json",
                type:"POST",
                error:function()
                {
                   alert("Something went wrong. Please refresh the page."); 
                   location.reload();
                }

            },


            pageLength: 10,
            lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000],




            buttons: [
                {
                    extend:    'copyHtml5',
                    text:      '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}

				},
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}

                },

                {
                    extend:    'csvHtml5',
                    text:      '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}


				},
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}


				}
            ]
        } );
    } );


</script>
<script>
    $("#customFile").change(function () {
        var fileExtension = ['csv'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
        }
    });
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

