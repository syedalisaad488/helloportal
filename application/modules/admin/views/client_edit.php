
 
   <?php $this->load->view("include/head"); ?>
      <?php $this->load->view("include/nav"); ?>

    
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
         
           
          
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
       <h3>Client Edit Form</h3>
      </div><!-- az-content-header -->

      <div class="az-content-body">
        <?php foreach($client_data->result() as $key) { ?>
      <form method="post" action="<?php echo base_url("admin/Repren/update_data/".$key->IDclient) ?>">
        <div style="background: white; padding: 10px;">
         <h4>Client Details</h4>
          <div class="row">
        
            <div class="col-md-3" class="form-group"><br>
              <label>Lead Processing DATE</label>
              <input type="date" value="<?php echo $key->client_lead_process_date; ?>" name="client_lead_process_date" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>LEAD POSTING DATE</label>
              <input type="date" value="<?php echo $key->client_lead_posting_date; ?>" name="client_lead_posting_date" class="form-control">
            </div>
             <div class="col-md-3" class="form-group"><br>
              <label>First Name</label>
                  <input type="text" value="<?php echo $key->client_first_name; ?>" name="client_first_name" class="form-control">
            </div>
            <div class="col-md-3" class="form-group"><br>s
              	<label>Last Name</label>
                <input type="text" value="<?php echo $key->client_last_name; ?>" name="client_last_name" class="form-control">
            </div>  
              <div class="col-md-12" class="form-group"><br>
              	<label>Address</label>
                <input type="text" value="<?php echo $key->client_address; ?>" name="client_address" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>City</label>
                  <input type="text" value="<?php echo $key->client_city; ?>" name="client_city" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>State</label>
                  <input type="text" value="<?php echo $key->client_state; ?>" name="client_state" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Zip Code</label>
                <input type="text" value="<?php echo $key->client_zip_code; ?>" name="client_zip_code" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Phone No</label>
                <input type="text" value="<?php echo $key->client_phone; ?>" name="client_phone" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Date of birth</label>
                <input type="date" value="<?php echo $key->client_dob; ?>" name="client_dob" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Sex</label>
                <select value="" name="client_sex" class="form-control">
                	<option value="Male" <?php echo $key->client_sex=="Male" ? 'Selected' : ''  ?>>Male</option>
                	<option value="Female" <?php echo $key->client_sex=="Female" ? 'Selected' : ''  ?>>Female</option>
                	<option value="Other" <?php echo $key->client_sex=="Other" ? 'Selected' : ''  ?>>Other</option>
                </select>
              </div>
              <div class="col-md-4"><br>
              	<label>diabetes type 1 or 2</label>
                <select value="" name="client_diabetes" class="form-control">
                	<option value=""></option>
                	<option value="abc" <?php echo $key->client_diabetes=="abc" ? 'Selected' : ''  ?>>abc</option>
                	<option value="xyz"<?php echo $key->client_diabetes=="xyz" ? 'Selected' : ''  ?>>xyz</option>
                </select>
              </div>
              <div class="col-md-4"><br>
              	<label>How many times do you use insulin</label>
                <input type="number" value="<?php echo $key->client_times_of_use_insul; ?>" name="client_times_of_use_insul" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>How many times do you check blood sugar</label>
                <input type="number" value="<?php echo $key->client_times_of_check_blood_sugar; ?>" name="client_times_of_check_blood_sugar" class="form-control">
              </div>
          </div>
          <br><br>
              <h4>Doctor Details</h4>
              <div class="row">
              <div class="col-md-4"><br>
              <label>Doctor First Name</label>
                  <input type="text" value="<?php echo $key->client_doc_first_name; ?>" name="client_doc_first_name" class="form-control">
            </div>
            <div class="col-md-4"><br>
              	<label>Doctor Last Name</label>
                <input type="text" value="<?php echo $key->client_doc_last_name; ?>" name="client_doc_last_name" class="form-control">
            </div>
              <div class="col-md-4"><br>
              	<label>Doctor Address</label>
                <input type="text" value="<?php echo $key->client_doc_address; ?>" name="client_doc_address" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor City</label>
                  <input type="text" value="<?php echo $key->client_doc_city; ?>" name="client_doc_city" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor State</label>
                  <input type="text" value="<?php echo $key->client_doc_state; ?>" name="client_doc_state" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Doctor Zip Code</label>
                <input type="text" value="<?php echo $key->client_doc_zip_code; ?>" name="client_doc_zip_code" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Doctor NPI</label>
                  <input type="text" value="<?php echo $key->client_doc_npi; ?>" name="client_doc_npi" class="form-control">
              </div>
              <div class="col-md-4"><br>
              	<label>Phone No</label>
                <input type="text" value="<?php echo $key->client_doc_phone; ?>" name="client_doc_phone" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>FAX</label>
                  <input type="text" value="<?php echo $key->client_doc_fax; ?>" name="client_doc_fax" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Medicare</label>
                  <input type="text" value="<?php echo $key->client_doc_medicare; ?>" name="client_doc_medicare" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Member ID</label>
                  <input type="text" value="<?php echo $key->client_doc_id_member; ?>" name="client_doc_id_member" class="form-control">
              </div>
              <div class="col-md-4"><br>
              <label>Recording</label>
                  <input type="text" value="<?php echo $key->client_doc_recording; ?>" name="client_doc_recording" class="form-control">
              </div>
              <br><br> <br><br><br><br>
              <div class="col-md-12">
              <button  style="float:right;" class="btn btn-success">Submit</button>
              </div>

          </div>
          <?php } ?>
         </form>

          </div><!-- row -->
      </div><!-- az-content-body -->

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Ucored </span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->


  
    <script>
      $(function(){
        'use strict'

        $('.az-sidebar .with-sub').on('click', function(e){
          e.preventDefault();
          $(this).parent().toggleClass('show');
          $(this).parent().siblings().removeClass('show');
        })

        $(document).on('click touchstart', function(e){
          e.stopPropagation();

          // closing of sidebar menu when clicking outside of it
          if(!$(e.target).closest('.az-header-menu-icon').length) {
            var sidebarTarg = $(e.target).closest('.az-sidebar').length;
            if(!sidebarTarg) {
              $('body').removeClass('az-sidebar-show');
            }
          }
        });


        $('#azSidebarToggle').on('click', function(e){
          e.preventDefault();

          if(window.matchMedia('(min-width: 992px)').matches) {
            $('.az-sidebar').toggle();
          } else {
            $('body').toggleClass('az-sidebar-show');
          }
        })

        /* ----------------------------------- */
        /* Dashboard content */

        $.plot('#flotChart1', [{
            data: flotSampleData5,
            color: '#8039f4'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.12 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart2', [{
            data: flotSampleData2,
            color: '#007bff'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotChart3', [{
            data: flotSampleData5,
            color: '#00cccc'
          }], {
          series: {
            shadowSize: 0,
            lines: {
              show: true,
              lineWidth: 2,
              fill: true,
              fillColor: { colors: [ { opacity: 0.2 }, { opacity: 0.5 } ] }
            }
          },
          grid: {
            borderWidth: 0,
            labelMargin: 10,
            markings: [{ color: '#70737c', lineWidth: 1, font: {color: '#000'}, xaxis: { from: 75, to: 75} }]
          },
          yaxis: { show: false },
          xaxis: {
            show: true,
            position: 'top',
            color: 'rgba(102,16,242,.1)',
            reserveSpace: false,
            ticks: [[15,'1h'],[35,'1d'],[55,'1w'],[75,'1m'],[95,'3m'], [115,'1y']],
            font: {
              size: 10,
              weight: '500',
              family: 'Roboto, sans-serif',
              color: '#999'
            }
          }
        });

        $.plot('#flotPie', [
          { label: 'Very Satisfied', data: [[1,25]], color: '#6f42c1'},
          { label: 'Satisfied', data: [[1,38]], color: '#007bff'},
          { label: 'Not Satisfied', data: [[1,20]], color: '#00cccc'},
          { label: 'Very Unsatisfied', data: [[1,15]], color: '#969dab'}
        ], {
          series: {
            pie: {
              show: true,
              radius: 1,
              innerRadius: 0.5,
              label: {
                show: true,
                radius: 3/4,
                formatter: labelFormatter
              }
            }
          },
          legend: { show: false }
        });

        function labelFormatter(label, series) {
          return '<div style="font-size:11px; font-weight:500; text-align:center; padding:2px; color:white;">' + Math.round(series.percent) + '%</div>';
        }

        var ctx6 = document.getElementById('chartStacked1');
        new Chart(ctx6, {
          type: 'bar',
          data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [{
              data: [10, 24, 20, 25, 35, 50, 20, 30, 28, 33, 45, 65],
              backgroundColor: '#6610f2',
              borderWidth: 1,
              fill: true
            },{
              data: [20, 30, 28, 33, 45, 65, 25, 35, 50, 20, 30, 28],
              backgroundColor: '#00cccc',
              borderWidth: 1,
              fill: true
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false,
                labels: {
                  display: false
                }
            },
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero:true,
                  fontSize: 11
                }
              }],
              xAxes: [{
                barPercentage: 0.4,
                ticks: {
                  fontSize: 11
                }
              }]
            }
          }
        });
      });
    </script>
  </body>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
