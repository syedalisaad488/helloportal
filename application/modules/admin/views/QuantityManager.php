<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
       
      </div><!-- az-content-header -->
      <div class="az-content-body">

        <div style="background: white; padding: 10px;">
            <?= $this->session->flashdata("errorMsg"); ?>
          <form action="<?php echo base_url('admin/QuantityManager/save_dollar');?>" method="post">
                <input type="hidden" name="token" value="<?= $token ?>">
                <div class="row">
                   
                    <div class="col-md-3">
                         <label>Amount(in Rupee)</label>
                        <input class="form-control" step="0.01" required="required"   name="dollar_rs_value" type="number" placeholder="Amount(in Rupee)">
                    </div>
                    <div class="col-md-3">
                        <label>Cuurent value of Rupee in 1 dollar</label>
                        <input class="form-control" step="0.01" required="required" name="dollar_value_in_rs" type="number" placeholder="current value of Rupee in 1 dollar.">
                    </div>
                    <div class="col-md-3">
                        <label style="font-size:13px;">Amount twilio charge per page(in dollar)</label>
                        <input class="form-control" step="0.01" required="required" name="dollar_twillio_per_page" value="0.019" readonly type="number" placeholder="Amount twilio charge per page(in dollar)">
                    </div>
                    <div class="col-md-3">
                        <input style="margin-top:28px;" class="btn btn-success" type="submit" value="Add Amount">
                    </div>                
                </div>
            </form>
            <br>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Rupee Value</th>
                <th >Dollar in Rs.</th>
                <th>Date</th>
                <th>Dollar Value</th>
                <th>Total Pages</th>
                <th>Dollar per page(twillio)</th>
                <th>Payment Entered Time</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach($dollar_data->result() as $index => $key){ ?>
            <tr>
                <td><?= $index+1 ; ?></td>
                <td><?=$key->dollar_rs_value; ?></td>
                <td><?=$key->dollar_value_in_rs; ?></td>
                <td><?=$key->dollar_date; ?></td>
                <td><?=$key->dollar_value; ?></td>
                <td><?=$key->dollar_total_pages; ?></td>
                <td><?=$key->dollar_twillio_per_page; ?></td>
                <td><?=$key->dollar_created_at; ?></td>
                <!--<td>-->
                <!--  <a href="<?= base_url().'admin/User/edit_agent/'.$key->id; ?>"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>-->
                <!--   <?php if($key->is_activated == 1){ ?>-->
                <!--    <a href="<?= base_url().'admin/User/Avalibility/de-activate/'.$key->id; ?>">-->
                <!--    <button onclick="return confirm('Are you sure want to de-activate this agent?')" class="btn btn-danger">De-activate</button></a>-->
                <!--    <?php } else { ?>-->
                <!--      <a href="<?= base_url().'admin/User/Avalibility/activate/'.$key->id; ?>">-->
                <!--    <button onclick="return confirm('Are you sure want to activate this agent?')" class="btn btn-success">Activate</button></a>-->
                <!--    <?php } ?>-->
                <!--</td>-->
                
            </tr>
            <?php } ?>
        </tbody>
    </table>
          </div><!-- row -->
      </div><!-- az-content-body -->

 <?php $this->load->view('include/footer');?>
    </div><!-- az-content -->

  </body>
  <script>
 $(document).ready(function() {
    
    $(document).on('mousedown', '.passwordShowHide', function (event) {
        $("#showHidePassword_"+this.id).show();
    });
    $(document).on('mouseup', '.passwordShowHide', function (event) {
        $("#showHidePassword_"+this.id).hide();
    });

                
    $('#example').DataTable( {
    pageLength: 10,
    lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500]
    } );
} );


  </script>

</html>



