<style>
        div.dt-buttons {
            float: right;
            margin-left:10px;
        }
    </style>
    
    <?php echo getcwd();?>
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      
      
      
       
      </form>
      <div class="az-content-body">
          <form method="post" id="import_csv" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-6">
                   <div class="form-group">
                    <label>Select CSV File</label>
                    <input type="file" name="csv_file" id="csv_file"  accept=".csv" />
                   </div>
              </div>
              <div class="col-md-6">
                 <button type="submit" name="import_csv" class="btn btn-info" id="import_csv_btn">Import CSV</button>
              </div>
            </div>
            </form>
        <div style="background: white; padding: 10px;">
        <div id="checkboxlist">
        <form action="<?php echo base_url().'admin/User/send_fax'; ?>" method="POST">
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Select&nbsp;&nbsp;<input type="checkbox" name="sample" class="selectall"/></th>
                <th>Client Name</th>
                <th>Client Phone</th>
                <th>Client lead posting date</th>
                <th>Doctor Name</th>
                <th>Doctor Phone</th>
                <th>Doctor Fax</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <?php 
      
          for($i=0;$i<count($client_data);$i++){
          foreach($client_data[$i] as $key) { ?>
            <tr>
                <td><center><input type="checkbox" name="send_fax_data[]" value="<?php echo $key->IDclient.'@4Glt$@34!2%6&86%43@6@1%-=+++-21'.$key->client_doc_fax.'@4Glt$@34!2%6&86%43@6@1%-=+++-21'.$key->client_doc_phone ?>" ></center></td>
                <td><?php echo $key->client_first_name." ".$key->client_last_name; ?></td>
                <td><?php echo $key->client_phone; ?></td>
                <td><?php echo $key->client_lead_process_date; ?></td>
                <td><?php echo $key->client_doc_first_name." ".$key->client_doc_last_name; ?></td>
                <td><?php echo $key->client_doc_phone ?></td>
                <td><?php echo $key->client_doc_fax ;?></td>

                <td>
                  <a href="<?php echo base_url("admin/User/edit_client_data/".$key->IDclient); ?>" >Edit</a>
                  
                <a href="<?php echo base_url("admin/User/show_client_data/".$key->IDclient); ?>" >Show</a></td>
            </tr>
          <?php }
               } 
                      ?>

        </tbody>
        <tfoot>
      
        </tfoot>
    </table>
    </div>
    <input type="submit" value="submit">
    </form>

          </div><!-- row -->
      </div><!-- az-content-body -->

 <?php $this->load->view('include/footer');?>
    </div><!-- az-content -->


  
  
  </body>
  <script>
  $('.selectall').click(function() {
    if ($(this).is(':checked')) {
        $('div input').attr('checked', true);
    } else {
        $('div input').attr('checked', false);
    }
});
 $(document).ready(function() {
    $('#example').DataTable( {


        dom: 'lBfrtip',
        pageLength: 10,
        lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000],
      
        buttons: [
            {   
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                className: 'btn btn-outline-primary btn-xs' 
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-outline-primary btn-xs' 

            }
        ]
    } );
} );


  </script>
  <script>
$(document).ready(function(){

	load_data();

	$('#import_csv').on('submit', function(event){
		event.preventDefault();
		$.ajax({
			url:"<?php echo base_url(); ?>admin/User/import",
			method:"POST",
			data:new FormData(this),
			contentType:false,
			cache:false,
			processData:false,
			beforeSend:function(){
				$('#import_csv_btn').html('Importing...');
			},
			success:function(data)
			{
				$('#import_csv')[0].reset();
				$('#import_csv_btn').attr('disabled', false);
				$('#import_csv_btn').html('Import Done');
				load_data();
			}
		})
	});
	
});
</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
