
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-body">
        <div style="background: white; padding: 10px;">
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                
                <th>Client Name</th>
                <th>Client Phone</th>
                <th>Client lead posting date</th>
                <th>Doctor Name</th>
                <th>Doctor Phone</th>
                <th>Doctor Fax</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <?php 
      
          for($i=0;$i<count($client_data);$i++){
          foreach($client_data[$i] as $key) { ?>
            <tr>
                <td><?php echo $key->client_first_name." ".$key->client_last_name; ?></td>
                <td><?php echo $key->client_phone; ?></td>
                <td><?php echo $key->client_lead_process_date; ?></td>
                <td><?php echo $key->client_doc_first_name." ".$key->client_doc_last_name; ?></td>
                <td><?php echo $key->client_doc_phone ?></td>
                <td><?php echo $key->client_doc_fax ;?></td>

                <td>
                  <a href="<?php echo base_url("admin/User/edit_client_data/".$key->IDclient); ?>" >Edit</a>
                  
                <a href="<?php echo base_url("admin/User/show_client_data/".$key->IDclient); ?>" >Show</a></td>
            </tr>
          <?php }
               }        ?>
        </tbody>
        <tfoot>
      
        </tfoot>
    </table>
          </div><!-- row -->
      </div><!-- az-content-body -->

      <div class="az-footer ht-40">
        <div class="container-fluid pd-t-0-f ht-100p">
          <span>&copy; 2019 Ucored </span>
        </div><!-- container -->
      </div><!-- az-footer -->
    </div><!-- az-content -->


  
  
  </body>
  <script>
 $(document).ready(function() {
    $('#example').DataTable( {


        dom: 'Bfrtip',
        pageLength: 10,
        lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500],
        buttons: [
            {
                extend:    'copyHtml5',
                text:      '<i class="fa fa-files-o"></i>',
                titleAttr: 'Copy',
                className: 'btn btn-outline-primary btn-xs' 
            },
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o"></i>',
                titleAttr: 'Excel',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'csvHtml5',
                text:      '<i class="fa fa-file-text-o"></i>',
                titleAttr: 'CSV',
                className: 'btn btn-outline-primary btn-xs' 

            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o"></i>',
                titleAttr: 'PDF',
                className: 'btn btn-outline-primary btn-xs' 

            }
        ]
    } );
} );
  </script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
