

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<div id="medicalEntryModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">Medical-Id Entry Panel</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="form-control" name="medicalId" id="medicalId" placeholder="Enter Medical-Id Here " />
                <input type="hidden" name="id" />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
                <div>
                    <input type="text" autocomplete="off" class="form-control" name="firstname" id="firstname" placeholder="Enter First Name" />
                    <a href="#" onclick="get_med_id_from_name()">Get Med ID</a>
                </div>
                <br>
                <div id="retrieve_med_id">

                </div>
            </div>
            <br>
        </div>
    </div>
</div>
<form method="POST" id="ChangeReceive">
    <input type="hidden" id="show_data_time_form" name="show_data_time_form">
    <input type="hidden" id="show_send_no_form" name="show_send_no_form">
    <input type="hidden" id="show_receive_no_form" name="show_receive_no_form">
    <input type="hidden" id="show_is_edited_form" name="show_is_edited_form">
</form>
<div class="az-content az-content-dashboard-five">

    <div class="az-header">
        <div class="container-fluid">
            <div class="az-header-left" >
                <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
            </div><!-- az-header-left -->
            <div class="az-header-right" style="width: 950px!important;">


            </div>
            <div class="">
                <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
            </div>

        </div><!-- container -->
    </div><!-- az-header -->

    <div class="az-content-header d-block d-md-flex">
        <div class="col-md-12">
            <div class="row">
                <form action="<?= base_url().'admin/Get_twillo/getlist/'; ?>" method="post" >
                    <div class="col-md-5">
                        <label>Start Date</label>
                        <input type="date" required name="start_date" placeholder="Start Date" class="form-control">
                    </div>
                    <div class="col-md-5">
                        <label>End Date</label>
                        <input type="date" required name="end_date" placeholder="End Date" class="form-control">
                    </div>
                    <div class="col-md-2" style="margin-top: 5px;">
                        <label></label>
                        <button type="submit" class="form-control"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <?php if(isset($start_date)){ ?>
                <div class="row">
                    <div class="col-md-12">
                        <h5><?= 'Records Filtered Between <b>'.$start_date.'</b> & <b>'.$end_date.'</b>'; ?></h5>
                    </div>
                </div>
            <?php } ?>
        </div>

    </div><!-- az-content-header -->
    <div class="az-content-body">


        <div style="background: white; padding: 10px;">
            <table  id="user_data" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>Medical Id</th>
                    <th>Company Name</th>
                    <th>Fax ID</th>
                    <th>Date/Time</th>
                    <th>Sender No</th>
                    <th>Receiver No</th>
                    <th>Active</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>


                </tbody>
                <tfoot>
                <tr>
                    <th>Medical Id</th>
                    <th>Company Name</th>
                    <th>Fax ID</th>
                    <th>Date/Time</th>
                    <th>Sender No</th>
                    <th>Receiver No</th>
                    <th>Active</th>
                </tr>
                </tfoot>
            </table>
        </div><!-- row -->
    </div><!-- az-content-body -->

    <?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script>
    var start_date = "";
    var end_date = "";
</script>
<?php if(isset($start_date)){ ?>
    <script>
        var start_date = "<?= $start_date ?>";
        var end_date = "<?= $end_date ?>";
    </script>

<?php } ?>
<script>

    var url = "<?= site_url('admin/get/fetchMedicalId'); ?>"//"<?= base_url(); ?>";
    $(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            processing:true,
            dom: 'lfrtip',
            serverSide:true,
            order:[],


            columnDefs: [ {
                "targets": [],
                "orderable": true

            } ],




            ajax:{
                url:"<?php echo base_url() . 'admin/Get_twillo/fetch_fax/' ?>",
                type:"POST",
                dataType: "json",
                data : {

                    start_date : start_date,
                    end_date : end_date

                }

            },
            pageLength: 10,
            lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000]





        } );
        // medicalIdEntry
        $( '#user_data' ).delegate( ".medicalIdEntryBtn", "click", function(e) {
            e.preventDefault();
            _this   = $(this);
            id      = _this.data("id");
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/get/fetchMedicalId'); ?>",
                data: {
                    "id": id
                },
                success: function (data) {
                    $("#medicalEntryModal input[name='medicalId']").val(data)
                    $("#medicalEntryModal input[name='id']").val(id)
                    $("#medicalEntryModal").modal("show");
                }
            });
        });

        $("#medicalEntryModal input[name='medicalId']").keyup(function (e) {
            e.preventDefault();
            _this = $(this);
            _this.val(_this.val().trim());
        });
        $("#medicalEntryModal .btn-success").click(function (e) {
            e.preventDefault();
            var id          = $("#medicalEntryModal input[name='id']").val()
            var medicalId   = $("#medicalEntryModal input[name='medicalId']").val()
            $.ajax({
                type: "POST",
                url: "<?= site_url('admin/get/twillo_medicalIdEntry'); ?>",
                data: {
                    "id": id,
                    "medicalId": medicalId
                },
                success: function (response) {
                    // $("#medicalEntryModal input").val("")
                    location.reload();
                }
            });
        });
    } );

    function get_med_id_from_name()
    {

        var firstname =document.getElementById('firstname').value;
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open('GET',"<?php echo base_url().'admin/Get/'; ?>get_med_id_from_name?firstname="+firstname,false);
        xmlHttp.send(null);
        document.getElementById('retrieve_med_id').innerHTML=xmlHttp.responseText;

    }

    function change_status(id)
    {
        document.getElementById("old_new_"+id).innerHTML =" ";
    }

    function submitChangeReceive($id)
    {
        var is_edited = 0 ;
        var show_data_time = $("#show_data_time"+$id).val();
        var show_send_no = $("#show_send_no"+$id).val();
        var show_receive_no = $("#show_receive_no"+$id).val();
        
        if($('#is_edited'+$id).is(':checked'))
        {
          is_edited = 1;
        }
     

        $("#show_data_time_form").val(show_data_time) ;
        $("#show_send_no_form").val(show_send_no) ;
        $("#show_receive_no_form").val(show_receive_no) ;
        $("#show_is_edited_form").val(is_edited) ;

        $('#ChangeReceive').attr('action',"<?= base_url().'admin/ChangeReceivingFax/ChangeReceiveDetails/'; ?>"+$id);
        $('#ChangeReceive').submit();
    }
</script>

</html>










