<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="az-content az-content-dashboard-five">
    <div class="az-header">
        <div class="container-fluid">
            <div class="az-header-left">
                <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
            </div><!-- az-header-left -->

            <div class="az-header-right">

                <div class="">
                    <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
                </div>

            </div><!-- az-header-right -->
        </div><!-- container -->
        <?php $faxremaing =0; foreach($user as $users){
            if($users->send_month == date('M-Y') OR $users->send_month ==  null){  $faxremaing +=$users->send_static; } }?>

    </div><!-- az-header -->
    <div class="az-content-header d-block d-md-flex">

    </div><!-- az-content-header -->
    <div class="az-content-body">

        <div style="background: white; padding: 10px;">
            <form action="<?= base_url('admin/AgentLimit/assgin_limit_send') ?>" method="post">
                <input type="hidden" name="token" value="<?= $token; ?>">
                <?= $this->session->flashdata("errorMsg"); ?>
                <div class="row">

                    <div class="col-md-2">
                        <label  class="label"> Total pages of faxes</label><br>
                        <label  class="label"> <?= isset($userlimit_total_fax[0]->userlimit_send_static)? $userlimit_total_fax[0]->userlimit_send_static  :'0' ?></label>
                    </div>
                    <div class="col-md-2">
                        <label  class="label"> Pages of faxes assigned</label><br>
                        <label  class="label"> <?= isset($faxremaing)? $faxremaing :'0' ?></label>
                    </div>
                    <div class="col-md-2">
                        <label  class="label"> Pages of faxes remain</label><br>
                        <label  class="label"> <?= isset($userlimit_total_fax[0]->userlimit_send_static)? $userlimit_total_fax[0]->userlimit_send_static -$faxremaing :'0' ?></label>
                    </div>
                    <div class="col-md-2">
                        <label  class="label">No# value to assign</label>

                        <input class="form-control" name="userlimit_send_static" min="1" max="<?= isset($userlimit_total_fax[0]->userlimit_send_static)? $userlimit_total_fax[0]->userlimit_send_static  -$faxremaing :'0' ?>" type="number" placeholder="no# value to assign" required>
                        <input class="form-control" name="userlimit_month" value="<?= date('M-Y') ?>" hidden>
                        <input class="form-control" name="IDpgquan" value="<?= isset($userlimit_total_fax[0]->userID)? $userlimit_total_fax[0]->userID :'0' ?>" hidden>
                    </div>
                    <div class="col-md-2">
                        <label>Assign Name</label>
                        <select class="form-control" name="userID" required>
                            <?php foreach($select_user as $select_users) {?>
                                <option value="<?=$select_users->id ?>"><?=$select_users->name ?></option>
                            <?php  }  ?>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <br>
                        <input class="btn btn-success" type="submit" value="Assign">
                    </div>
                </div>
            </form>
            <br>
            <table id="example" class="display" style="width:100%">
                <thead>
                <tr>
                    <th>ID </th>
                    <th>Assign Name</th>
                    <th>Total Assign</th>
                    <th>total Used</th>
                    <th>Remaining</th>
                </tr>
                </thead>
                <tbody>

                <?php $count =1; foreach($user as $users){ ?>
                    <?php if($users->send_month == date('M-Y') OR $users->send_month ==  null){ ?>
                        <tr>
                            <td><?=$count++; ?></td>
                            <td><?=$users->name; ?></td>
                            <td><?=$users->send_static; ?></td>
                            <td><?=$users->send_dynamic; ?></td>
                            <td><?=$users->send_static-$users->send_dynamic; ?></td>


                        </tr>
                    <?php } }  ?>

                </tbody>

            </table>
        </div><!-- row -->
    </div><!-- az-content-body -->

    <?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script>
    $(document).ready(function() {

        $(document).on('mousedown', '.passwordShowHide', function (event) {
            $("#showHidePassword_"+this.id).show();
        });
        $(document).on('mouseup', '.passwordShowHide', function (event) {
            $("#showHidePassword_"+this.id).hide();
        });


        $('#example').DataTable( {
            pageLength: 10,
            lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500]
        } );
    } );


</script>

</html>
