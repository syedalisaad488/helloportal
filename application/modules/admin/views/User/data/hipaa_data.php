<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
    div.dt-buttons {
        float: right;
        margin-left:10px;
    }
    div.dataTables_wrapper {
        width:1045px;
        margin: 0 ;
    }
    th, td { white-space: nowrap; }
    th{
        background:white;
    }
    .DTFC_RightBodyWrapper{
        left: 20px!important;
    }
    .DTFC_RightHeadWrapper{
        left: 20px!important;

    }
    .DTFC_RightBodyLiner::-webkit-scrollbar {
        display: none;
    }
    .dataTables_processing {
        top: 64px !important;
        z-index: 11000 !important;
        font-size : 40px !important;
    }

    a {
        color: black;
    }

    a:active {
        color: blue;
    }

    a[tabindex]:focus {
        color:blue;
        outline: none;
    }

</style>
<?php

$prefix_segment = $this->uri->segment(4);


?>
<div class="modal" id="SentFaxesModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Sent Faxes</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" style="">

                <div  id="sent_faxes_body" >

                </div>

            </div>



        </div>
    </div>
</div>

<div class="az-content az-content-dashboard-five">
    <div class="az-header">
        <div class="container-fluid">
            <div class="az-header-left">
                <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
            </div><!-- az-header-left -->
            <div class="az-header-right">


                <div class="">
                    <button class="btn btn-primary">Edit Profile</button>
                    <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
                </div>
            </div>
        </div><!-- container -->
    </div><!-- az-header -->




    </form>
    <div class="az-content-body">



        <?php if(($this->uri->segment(5))=='Cleaning'){?>

            <form method='post' action='<?php echo base_url().'admin/Cvs_controller/hipaa'; ?>' enctype="multipart/form-data">
                <div class='row '>
                    <div class="col-md-6">
                        <div class="custom-file mb-3">
                            <input  class="custom-file-input" id="customFile" type='file' name='file' accept=".csv" id="">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <input type='submit' class="btn btn-outline-primary" value='Upload' name='upload'>
                    </div>


                </div>
            </form>

        <?php } ?>

        <div style="background: white; padding: 10px;">
            <div id="checkboxlist">
                <?php if(($this->uri->segment(5))=='Good'){?>
                    <!--<form action="<?php echo base_url().'admin/User/campaign_send_fax/'.$prefix_segment; ?>" method="POST">-->
                <?php  } if(($this->uri->segment(5))!='Good') { ?>
                <form action="<?php echo base_url().'admin/Change/changetab/'.$prefix_segment; ?>" method="POST">


                    <?php } ?>
                    <!--<form action="" method="POST">-->
                    <table id="user_data" class="display stripe row-border order-column" style="width:100%">
                        <thead>
                        <tr>
                            <th class="noExport">Select&nbsp;&nbsp;<input type="checkbox" name="sample" class="selectall"/></th>
                            <!--                                    <th>status</th>-->
                            <!--                                    <th>Reason</th>-->
                            <th class="noExport">Dupe</th>
                            <th class="noExport">Count</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>address</th>
                            <th>city</th>
                            <th>state</th>
                            <th>zip</th>
                            <th>dob</th>
                            <th>phone</th>
                            <th>Dr.First Name</th>
                            <th>Dr.Last Name</th>
                            <th>Dr.Address</th>
                            <th>Dr.city</th>
                            <th>Dr.State</th>
                            <th>Dr.Zip</th>
                            <th>Dr.Phone</th>
                            <th>Dr.Fax</th>
                            <th>Dr NPI</th>
                            <th>MedicareID#</th>
                            <th class="noExport">Date Entered</th>
                            <th class="noExport">Agent Name</th>
                            <th>Call Back Number</th>
                            <th>Call Fax Back</th>
                            <th>Center Code</th>
                            <th class="noExport">Status</th>

                            <!--                                    <th>Action</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <div class='row pd-10 px-2'>
                            <div class="col-md-6">
                                <?php
                                echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Cleaning</p>" : '';
                                echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Can't Reach</p>" : '';
                                echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
                                echo ($this->uri->segment(5))=='Rxrequest' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
                                echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
                                echo ($this->uri->segment(5))=='Sent' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
                                echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
                                echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
                                echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
                                echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
                                echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
                                echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
                                ?>
                            </div>
                            <?php if(($this->uri->segment(5))=='Good'){?>
                                <div class="col-md-6 ">
                                    <select  style="width: 200px; float:left;" onchange="showDemo(this.value)" name="hipaaLayoutType" id="hipaaLayoutType" class="form-control">
                                        <option value="fax_temp_hipaaNew5">Patient Verification 1 </option>
                                        <option value="fax_temp_hipaaNew">Patient Verification 2</option>
                                    </select>

                                    <div style="float:left; margin-top: 10px; margin-left: 5px;" id="hippaDemo"></div>
                                    &nbsp;&nbsp;&nbsp;
                                    <input type="submit" id="sendFax" value="Send FAX" class="btn btn-success pull-right" >
                                </div>
                            <?php  } if(($this->uri->segment(5))!='Good' && $this->uri->segment(5)!='Sent'  && $this->uri->segment(5)!='Delete') { ?>
                            <div class="col-md-4">
                                <select class="form-control" name="status">
                                    <option value="Cleaning">Leads</option>
                                    <option value="Good">Ready To Fax</option>
                                    <option value="Sent">Sent Fax</option>
                                    <option value="Not_sent">Sent Failure</option>
                                    <option value="Delete">Delete</option>

                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Move To Tab" class="btn btn-success pull-right" >

                            </div>
                        </div>
                        <?php } ?>

            </div>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>


            </tr>



            </tbody>
            <tfoot>

            </tfoot>
            </table>
        </div>
        <div >

        </div>

        </form>
        <button type="button" id = "openModal" class="btn btn-info btn-lg" style="display:none;" data-toggle="modal" data-target="#myModal">Open Modal</button>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div style="margin-top:20px;">
                        <center>
                            <h4 >Fax Sender</h4>
                        </center>
                    </div>
                    <div class="modal-body">
                        <div id="ProgressBarMainDiv">
                            <div  class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" id="progressBar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" ></div>
                            </div>
                        </div>
                        <div style="float:right;" id="NumberOfFaxesSent"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" onclick="cancelSending()" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- row -->
</div><!-- az-content-body -->

<?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>

<script>
    $(".selectall").click(function() {
        $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
    });

    $("input[type=checkbox]").click(function() {
        if (!$(this).prop("checked")) {
            $(".selectall").prop("checked", false);
        }
    });

</script>
<script>
    var base_url = "<?= base_url(); ?>";
    let send_url = "<?= base_url().'admin/User/campaign_send_fax/'.$prefix_segment;?>";
    let totalFaxesSent = 1;
    let iterationStopper = 1;
    let indexCounter = 0;
    let faxIDs = [];
    let systemInitializer = 0;
    var stopper = 0;
    
    function showDemo(inputValue)
    {
        if(inputValue == "fax_temp_hipaaNew5")
        {
            $("#hippaDemo").html("<a href='" + base_url + "assets/demo/hipaa5.pdf' target='_blank'>Show</a>");
        }
        else if(inputValue == "fax_temp_hipaaNew")
        {
            $("#hippaDemo").html("<a href='" + base_url + "assets/demo/new.pdf' target='_blank'>Show</a>");
        }


   
    }


    $(document).ready(function() {
        if(document.getElementById("hipaaLayoutType"))
        {
            $("#hippaDemo").html("<a href='"+base_url+"assets/demo/hipaa5.pdf' target='_blank'>Show</a>");
        }


        
        var TooltipInterval = () => setInterval(function() {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            } );
        },1);

        TooltipInterval();
        clearInterval(TooltipInterval);
        
        
        $("#sendFax").click(function(){
            let hipaaLayoutType = "";
            if(document.getElementById("hipaaLayoutType"))
            {
                hipaaLayoutType = document.getElementById("hipaaLayoutType").value;
            }

            $.each($("input[name='send_fax_data[]']:checked"), function(){
                faxIDs.push(this.value);
            });
            if(faxIDs.length>0)
            {

                $("#myModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });

                document.onkeydown = function (e) {
                    return false;
                }
                $("#ProgressBarMainDiv").html("<h5>Initializing System...</h5>");
                $("#openModal").click();



                function sendFaxAjaxFunc(){
                    $.ajax({
                        url:send_url,
                        type:"POST",
                        data:
                            {
                                campaign_id: faxIDs[indexCounter],
                                hipaaLayoutType : hipaaLayoutType,
                            },

                        success:function(data) {
                            stopper = data;
                            if(iterationStopper<=faxIDs.length)
                            {
                                if(systemInitializer==0)
                                {
                                    $("#ProgressBarMainDiv").html("<div  class='progress'><div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' id='progressBar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' ></div></div>");
                                    systemInitializer=1;
                                }
                                if(stopper == 0)
                                {
                                    indexCounter++;
                                    $("#progressBar").width(parseInt((totalFaxesSent/faxIDs.length)*100) + "%");
                                    $("#NumberOfFaxesSent").html("Faxes sent: "+totalFaxesSent+ " out of "+faxIDs.length);
                                    sendFaxAjaxFunc();
                                }
                                else
                                {
                                    alert("You are out of sending limit");
                                    location.reload();
                                }
                                
                                iterationStopper+=1;
                                totalFaxesSent++;

                            }
                            else
                            {
                                location.reload();
                            }
                            document.getElementById("totalFaxesSent").innerHTML = totalFaxesSent;

                        }
                    });
                }
                sendFaxAjaxFunc();
            }
            else
            {
                alert("Please select atleast 1 record");
            }


        });
    });

    function cancelSending()
    {
        location.reload();
    }










    $(document).ready(function() {
        var dataTable = $('#user_data').DataTable({
            dom: 'lBfrtip',
            scrollX: true,
            processing:true,
            "language": {
                processing: '<i style="color:gray !important" class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},

            serverSide:true,
            order:[],
            // fixedColumns:{
            //     leftColumns: false,
            //     rightColumns: 1
            // },
            columnDefs: [ {
                "targets": 0,
                "orderable": false
            } ],


            ajax:{
                url:"<?php echo base_url() . 'admin/List_controller/fetch_camp_data/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>",
                dataType: "json",
                type:"POST",
                
            },


            pageLength: 10,
            lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000],




            buttons: [
                {
                    extend:    'copyHtml5',
                    text:      '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    className: 'btn btn-outline-primary btn-xs',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    className: 'btn btn-outline-primary btn-xs',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }

                },
                {
                    extend:    'csvHtml5',
                    text:      '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    className: 'btn btn-outline-primary btn-xs',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }

                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    className: 'btn btn-outline-primary btn-xs',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }

                }
            ]
        } );
    } );

    $("#customFile").change(function () {
        var fileExtension = ['csv'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : "+fileExtension.join(', '));
        }
    });
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    // 	function show_sent_fax(counter)
    // 	{
    // 		$("#SentFaxesModal").modal("show");
    // 		var IDcampaign_sent = document.getElementById("IDcampaign_sent_"+counter).value;
    // 		var used_products = document.getElementById("used_products_"+counter).value;
    // 		var used_products_array = used_products.split("/");
    // 		var html = "";
    // 		var product_link_name = "";
    // 		for(var i=0;i<used_products_array.length;i++)
    // 		{
    // 			if(used_products_array[i]=="Back"){ product_link_name = "back_brace" }
    // 			if(used_products_array[i]=="Left Knee"){ product_link_name = "left_knee" }
    // 			if(used_products_array[i]=="Right Knee"){ product_link_name = "right_knee" }
    // 			if(used_products_array[i]=="Left Wrist"){ product_link_name = "left_wrist" }
    // 			if(used_products_array[i]=="Right Wrist"){ product_link_name = "right_wrist" }
    // 			if(used_products_array[i]=="Left Ankle"){ product_link_name = "left_ankle" }
    // 			if(used_products_array[i]=="Right Ankle"){ product_link_name = "right_ankle" }
    // 			if(used_products_array[i]=="Left Shoulder"){ product_link_name = "left_shoulder" }
    // 			if(used_products_array[i]=="Right Shoulder"){ product_link_name = "right_shoulder" }
    // 			if(used_products_array[i]=="Left Elbow"){ product_link_name = "left_elbow" }
    // 			if(used_products_array[i]=="Right Elbow"){ product_link_name = "right_elbow" }
    // 			if(used_products_array[i]=="Hip"){ product_link_name = "hip" }
    // 			if(used_products_array[i]!="")
    // 			{
    // 				html += "<a href='"+base_url+"assets/documents/faxes/fax_of_sending_"+product_link_name+"_aurbrac_"+IDcampaign_sent+".pdf' target='_blank' style='margin-top:10px;' class='btn btn-info'>"+used_products_array[i]+"</a>&nbsp;&nbsp;&nbsp;";
    // 			}
    // 		}

    // 		document.getElementById("sent_faxes_body").innerHTML = html;

    // 	}

</script>


<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>














































