<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
	div.dt-buttons {
		float: right;
		margin-left:10px;
	}
	div.dataTables_wrapper {
		width:1045px;
		margin: 0 ;
	}
	th, td { white-space: nowrap; }
	th{
		background:white;
	}
	.DTFC_RightBodyWrapper{
		left: 20px!important;
	}
	.DTFC_RightHeadWrapper{
		left: 20px!important;

	}
 .dataTables_processing {
        top: 64px !important;
        z-index: 11000 !important;
        font-size : 40px !important;
    }a {
    color: black;
}

a:active {
    color: blue;
}

a[tabindex]:focus {
    color:blue;
    outline: none;
}

</style>
<?php

$prefix_segment = $this->uri->segment(4);


?>
<div class="az-content az-content-dashboard-five">
	<div class="az-header">
		<div class="container-fluid">
			<div class="az-header-left">
				<a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
			</div><!-- az-header-left -->
			<div class="az-header-right">


				<div class="">
					<button class="btn btn-primary">Edit Profile</button>
					<a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>
				</div>
			</div>
		</div><!-- container -->
	</div><!-- az-header -->




	</form>
	<div class="az-content-body">
		<?php if(($this->uri->segment(5))=='Cleaning'){?>

		<form method='post' action='<?php echo base_url().'admin/Cvs_controller/cgmdiab'; ?>' enctype="multipart/form-data">
		<div class='row '>
			<div class="col-md-6">
				<div class="custom-file mb-3">
					<input  class="custom-file-input" id="customFile" type='file' name='file' accept=".csv" id="">
					<label class="custom-file-label" for="customFile">Choose file</label>
				</div>

			</div>
			<div class="col-md-6">
				<input type='submit' class="btn btn-outline-primary" value='Upload' name='upload'>
			</div>

		</div>
		</form>

		<?php } ?>

		<div style="background: white; padding: 10px;">
			<div id="checkboxlist">
				<?php if(($this->uri->segment(5))=='Good'){?>
				<!--<form action="<?php echo base_url().'admin/User/campaign_send_fax/'.$prefix_segment; ?>" method="POST">-->
					<?php  } if(($this->uri->segment(5))!='Good') { ?>
					<form action="<?php echo base_url().'admin/Change/changetab/'.$prefix_segment; ?>" method="POST">


						<?php } ?>
						<table id="user_data" class="stripe row-border order-column" style="width:100%">
							<thead>
							<tr>
                                <th class="noExport">Select&nbsp;&nbsp;<input type="checkbox" name="sample" class="selectall"/></th>
<!--                                <th>status</th>-->
<!--                                <th>Reason</th>-->
								<th class="noExport">Dupe</th>
								<th class="noExport">Count</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>address</th>
                                <th>city</th>
                                <th>state</th>
                                <th>zip</th>
                                <th>email</th>
                                <th>dob</th>
                                <th>phone</th>
<!--                                <th>sex</th>-->
<!--                                <th>ssn</th>-->
<!--                                <th>diabetes</th>-->
<!--                                <th>visit_doc</th>-->
<!--                                <th>insulin pump</th>-->
<!--                                <th>blood sugar</th>-->
                                <th>doc firstname</th>
                                <th>doc lastname</th>
                                <th>doc address</th>
                                <th>doc city</th>
                                <th>doc state</th>
                                <th>doc zip</th>
                                <th>doc npi</th>
                                <th>doc phone</th>
                                <th>doc fax</th>

								<th>medicare</th>

								<th>SECONDARY insurance ID</th>
								<th>SECONDARY insurance</th>

                                <th class="noExport">date entered</th>
                                <th class="noExport">Agent Name</th>


								<th>Call Back Number</th>
								<th>Call Fax Back</th>
								<th>Center Code</th>
								<th class="noExport">Status</th>
							</tr>
							</thead>
							<tbody>
							<div class='row pd-10 px-2'>
								<div class="col-md-6">
									<?php
                                    echo ($this->uri->segment(5))=='Cleaning' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>lead</p>" : '';
									echo ($this->uri->segment(5))=='Cannotreach' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Can't Reach</p>" : '';
									echo ($this->uri->segment(5))=='Staging' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Staging</p>" : '';
									echo ($this->uri->segment(5))=='Sent' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Request</p>" : '';
									echo ($this->uri->segment(5))=='Rxadditinal' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>RX Additional</p>" : '';
									echo ($this->uri->segment(5))=='Senttover' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Sent</p>" : '';
									echo ($this->uri->segment(5))=='Pending' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Pending</p>" : '';
									echo ($this->uri->segment(5))=='Rejected' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Rejected</p>" : '';
									echo ($this->uri->segment(5))=='Deleted' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Deleted</p>" : '';
									echo ($this->uri->segment(5))=='Completed' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Completed</p>" : '';
									echo ($this->uri->segment(5))=='Archive' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>Archive</p>" : '';
									echo ($this->uri->segment(5))=='All' ? "<p style='font-size:16px; color:#045BDD; font-weight: bold;'>All</p>" : '';
									?>
								</div>
								<?php if(($this->uri->segment(5))=='Good'){?>
								<div class="col-md-6 ">
									<input type="submit" id="sendFax" value="Send FAX" class="btn btn-success pull-right" >
								</div>
                                <?php  } if(($this->uri->segment(5))!='Good' && $this->uri->segment(5)!='Sent'  && $this->uri->segment(5)!='Delete') { ?>
								<div class="col-md-4">
									<select class="form-control" name="status">
										<option value="Cleaning">Leads</option>
										<option value="Good">Ready To Fax</option>
										<option value="Sent">Sent Fax</option>
										<option value="Not_sent">Sent Failure</option>
										<option value="Delete">Delete</option>

									</select>
								</div>
								<div class="col-md-2">
									<input type="submit" value="Move To Tab" class="btn btn-success pull-right" >

								</div>
							</div>
                                <?php } ?>
							<tr>
							    <td></td>
							    <td></td>
							    <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

							</tr>


							</tbody>
							<tfoot>

							</tfoot>
						</table>
			</div>

			</form>
  <button type="button" id = "openModal" class="btn btn-info btn-lg" style="display:none;" data-toggle="modal" data-target="#myModal">Open Modal</button>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div style="margin-top:20px;">
          <center>
            <h4 >Fax Sender</h4>
          </center>
        </div>
        <div class="modal-body">
            <div id="ProgressBarMainDiv">
                <div  class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" id="progressBar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" ></div>
                </div>
            </div>
             <div style="float:right;" id="NumberOfFaxesSent"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="cancelSending()" data-dismiss="modal">Cancel</button>
        </div>
      </div>

    </div>
  </div>
		</div><!-- row -->
	</div><!-- az-content-body -->

	<?php $this->load->view('include/footer');?>
</div><!-- az-content -->

</body>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>


<script>







	$('.selectall').click(function() {
		if ($(this).is(':checked')) {
			$('div input').attr('checked', true);
		} else {
			$('div input').attr('checked', false);
		}
	});

    $(document).ready(function() {
        
        var TooltipInterval = () => setInterval(function() {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            } );
        },1);

        TooltipInterval();
        clearInterval(TooltipInterval);
        
        
        var dataTable = $('#user_data').DataTable({
            processing:true,
            "language": {
                processing: '<i style="color:gray !important" class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},

            dom: 'lBfrtip',
            scrollX: true,
            serverSide:true,
            order:[],

            // fixedColumns:{
            //     leftColumns: false,
            //     rightColumns: 1
            // },
               columnDefs: [ {
            "targets": 0,
            "orderable": false
            } ],




            ajax:{
                url:"<?php echo base_url() . 'admin/List_controller/fetch_camp_data/'.$this->uri->segment(4).'/'.$this->uri->segment(5); ?>",
                type:"POST",
                dataType: "json",
                error:function()
                {
                   alert("Something went wrong. Please refresh the page.");
                   location.reload();
                }

            },


            pageLength: 10,
            lengthMenu: [ 5, 10, 20, 50, 100, 200, 500, 1000,2000,3000,4000, 50000, 100000],




            buttons: [
                {
                    extend:    'copyHtml5',
                    text:      '<i class="fa fa-files-o"></i>',
                    titleAttr: 'Copy',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}
                },
                {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}

                },
                {
                    extend:    'csvHtml5',
                    text:      '<i class="fa fa-file-text-o"></i>',
                    titleAttr: 'CSV',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}

                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    className: 'btn btn-outline-primary btn-xs',
					exportOptions: {
						columns: "thead th:not(.noExport)"
					}

                }
            ]
        } );
    } );


</script>
<script>
	$("#customFile").change(function () {
		var fileExtension = ['csv'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			alert("Only formats are allowed : "+fileExtension.join(', '));
		}
	});
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});






    let send_url = "<?= base_url().'admin/User/campaign_send_fax/'.$prefix_segment;?>";
    let totalFaxesSent = 1;
    let iterationStopper = 1;
    let indexCounter = 0;
    let faxIDs = [];
    let systemInitializer = 0;
    var stopper = 0;

    $(document).ready(function() {
        $("#sendFax").click(function(){

            $.each($("input[name='send_fax_data[]']:checked"), function(){
                faxIDs.push(this.value);
            });

            if(faxIDs.length>0)
            {

                $("#myModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });

                document.onkeydown = function (e) {
                    return false;
                }
                $("#ProgressBarMainDiv").html("<h5>Initializing System...</h5>");
                $("#openModal").click();



                function sendFaxAjaxFunc(){
                    $.ajax({
                        url:send_url,
                        type:"POST",
                        data:
                            {
                                campaign_id: faxIDs[indexCounter],
                            },

                        success:function(data) {
                            stopper = data;
                            if(iterationStopper<=faxIDs.length)
                            {
                                if(systemInitializer==0)
                                {
                                    $("#ProgressBarMainDiv").html("<div  class='progress'><div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar' id='progressBar' aria-valuenow='75' aria-valuemin='0' aria-valuemax='100' ></div></div>");
                                    systemInitializer=1;
                                }
                                if(stopper == 0)
                                {
                                    indexCounter++;
                                    $("#progressBar").width(parseInt((totalFaxesSent/faxIDs.length)*100) + "%");
                                    $("#NumberOfFaxesSent").html("Faxes sent: "+totalFaxesSent+ " out of "+faxIDs.length);
                                    sendFaxAjaxFunc();
                                }
                                else
                                {
                                    alert("You are out of sending limit");
                                    location.reload();
                                }
                                
                                iterationStopper+=1;
                                totalFaxesSent++;

                            }
                            else
                            {
                                location.reload();
                            }
                            document.getElementById("totalFaxesSent").innerHTML = totalFaxesSent;

                        }
                    });
                }
                sendFaxAjaxFunc();
            }
            else
            {
                alert("Please select atleast 1 record");
            }


        });
    });

    function cancelSending()
    {
        location.reload();
    }


</script>

<!-- Mirrored from www.bootstrapdash.com/demo/azia/v1.0.0/template/dashboard-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 01 Jan 2020 18:18:31 GMT -->
</html>
