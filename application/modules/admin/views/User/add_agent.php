<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
    <div class="az-content az-content-dashboard-five">
      <div class="az-header">
        <div class="container-fluid">
          <div class="az-header-left">
            <a href="#" id="azSidebarToggle" class="az-header-menu-icon"><span></span></a>
          </div><!-- az-header-left -->
        <div class="az-header-right">
          
            
            <div class="">
            <button class="btn btn-primary">Edit Profile</button> 
              <a href="<?php echo base_url("admin/Dashboard/logout"); ?>"> <button class="btn btn-danger">Logout</button>    </a>          
            </div>
          </div>
        </div><!-- container -->
      </div><!-- az-header -->
      <div class="az-content-header d-block d-md-flex">
       
      </div><!-- az-content-header -->
      <div class="az-content-body">

        <div style="background: white; padding: 10px;">
            <?= $this->session->flashdata("msg"); ?>
          <form action="<?php echo base_url('admin/User/save_agent');?>" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <input class="form-control" required="required" value="<?= $this->session->flashdata('name'); ?>" id="agentName" name="name" type="text" placeholder="Agent Name (Use Alphabets and Numbers only without space)">
                    </div>
                    <div class="col-md-5">
                        <input class="form-control" required="required" name="password" type="password" placeholder="Password (Use Alphabets and Numbers only without space)">
                    </div>
                    <div class="col-md-2">
                        <input class="btn btn-success" type="submit" value="Add Agent">
                    </div>                
                </div>
            </form>
            <br>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Full Name</th>
                <th style="width:30%;">Password</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach($agent_data->result() as $index => $key){ ?>
            <tr>
                <td><?= $index+1 ; ?></td>
                <td><?=$key->name; ?></td>
                <td><button id="<?=$key->id; ?>" class="fa fa-eye btn btn-default passwordShowHide"></button><span style="display:none; color:green;" id="showHidePassword_<?=$key->id; ?>"><?=$key->password; ?></span></td>
                <td>
                    <?php if($key->is_activated == 1) {?>
                    <span class="badge badge-success">Activated</span>
                    <?php
                    }
                    else{ ?>
                     <span class="badge badge-danger">De-activated</span>
                    <?php } ?>
</td>
                <td>
                  <a href="<?= base_url().'admin/User/edit_agent/'.$key->id; ?>"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>
                   <?php if($key->is_activated == 1){ ?>
                    <a href="<?= base_url().'admin/User/Avalibility/de-activate/'.$key->id; ?>">
                    <button onclick="return confirm('Are you sure want to de-activate this agent?')" class="btn btn-danger">De-activate</button></a>
                    <?php } else { ?>
                      <a href="<?= base_url().'admin/User/Avalibility/activate/'.$key->id; ?>">
                    <button onclick="return confirm('Are you sure want to activate this agent?')" class="btn btn-success">Activate</button></a>
                    <?php } ?>
                </td>
                
            </tr>
            <?php } ?>
        </tbody>
    </table>
          </div><!-- row -->
      </div><!-- az-content-body -->

 <?php $this->load->view('include/footer');?>
    </div><!-- az-content -->

  </body>
  <script>
 $(document).ready(function() {
    
    $(document).on('mousedown', '.passwordShowHide', function (event) {
        $("#showHidePassword_"+this.id).show();
    });
    $(document).on('mouseup', '.passwordShowHide', function (event) {
        $("#showHidePassword_"+this.id).hide();
    });

                
    $('#example').DataTable( {
    pageLength: 10,
    lengthMenu: [0, 5, 10, 20, 50, 100, 200, 500]
    } );
} );


  </script>

</html>



