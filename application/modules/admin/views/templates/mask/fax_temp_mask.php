<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_005{font-family:"Calibri Bold",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Calibri Bold",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_011{font-family:"Calibri",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:"Calibri",serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_035{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_035{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_036{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_036{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Calibri Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:"Calibri Bold",serif;font-size:12.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:"Calibri Bold",serif;font-size:12.6px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:"Calibri Bold",serif;font-size:11.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:"Calibri Bold",serif;font-size:11.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_021{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_022{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_023{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:"Calibri",serif;font-size:12.4px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:"Calibri",serif;font-size:12.4px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:"Calibri",serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:"Calibri",serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_027{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_037{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_037{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:"Calibri Bold",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_030{font-family:"Calibri Bold",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_032{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_032{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:"Verdana Bold",serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_033{font-family:"Verdana Bold",serif;font-size:13.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_034{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_034{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="3d785720-2a93-11eb-8b25-0cc47a792c0a_id_3d785720-2a93-11eb-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
$date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/mask/cover.jpg" width=612 height=792></div>
<div style="position:absolute;left:72.02px;top:184.58px" class="cls_005"><span class="cls_005">FROM : MEDIX PHARMACY</span></div>
<div style="position:absolute;left:72.02px;top:207.14px" class="cls_006"><span class="cls_006">FAX : <?= $key->mask_fax_back; ?></span></div>
<div style="position:absolute;left:72.02px;top:224.18px" class="cls_006"><span class="cls_006">TELEPHONE : <?= $key->mask_call_back_num; ?></span></div>
<div style="position:absolute;left:72.02px;top:241.37px" class="cls_006"><span class="cls_006">EMAIL : info@medixpharmacy.com</span></div>
<div style="position:absolute;left:72.02px;top:357.05px" class="cls_005"><span class="cls_005">MESSAGE :</span></div>
<div style="position:absolute;left:72.02px;top:380.81px" class="cls_011"><span class="cls_011">When returning the attached Medical necessity to</span><span class="cls_005"> MEDIX PHARMACY </span><span class="cls_011">, use this fax cover sheet</span></div>
<div style="position:absolute;left:72.02px;top:396.53px" class="cls_011"><span class="cls_011">and please fax back to </span><span class="cls_012"><?= $key->mask_fax_back; ?> .</span></div>
<div style="position:absolute;left:72.02px;top:462.79px" class="cls_035"><span class="cls_035">IMPORTANT</span><span class="cls_011">: This facsimile transmission contains confidential information, some or all of</span></div>
<div style="position:absolute;left:72.02px;top:481.27px" class="cls_011"><span class="cls_011">which may be protected health information as defined by the federal Health Insurance</span></div>
<div style="position:absolute;left:72.02px;top:497.11px" class="cls_011"><span class="cls_011">Portability & Accountability Act (HIPPA) Privacy Rule. This transmission is intended for the</span></div>
<div style="position:absolute;left:72.02px;top:512.95px" class="cls_011"><span class="cls_011">exclusive use of the individual or entity to whom it is addressed and may contain information</span></div>
<div style="position:absolute;left:72.02px;top:528.67px" class="cls_011"><span class="cls_011">that is proprietary, privileged, confidential and/or exempt from disclosure under applicable law.</span></div>
<div style="position:absolute;left:72.02px;top:544.51px" class="cls_011"><span class="cls_011">If you are not the intended recipient (or an employee or agent responsible for delivering this</span></div>
<div style="position:absolute;left:72.02px;top:560.35px" class="cls_011"><span class="cls_011">facsimile transmission to the intended recipient), you are hereby notified that any disclosure,</span></div>
<div style="position:absolute;left:72.02px;top:576.22px" class="cls_011"><span class="cls_011">dissemination, distribution or copying of this information is strictly prohibited and may be</span></div>
<div style="position:absolute;left:72.02px;top:591.94px" class="cls_011"><span class="cls_011">subject to legal restriction or sanction. Please notify the sender by telephone.</span></div>
<div style="position:absolute;left:90.02px;top:663.22px" class="cls_014"><span class="cls_014">-</span></div>
<div style="position:absolute;left:108.02px;top:663.22px" class="cls_036"><span class="cls_036">Send back this form within 24 to 48 hours.</span></div>
<div style="position:absolute;left:72.02px;top:738.36px" class="cls_012"><span class="cls_012">CONTACT NO: </span><span class="cls_007"><?= $key->mask_call_back_num; ?></span></div>
<div style="position:absolute;left:310.49px;top:738.36px" class="cls_012"><span class="cls_012">FAX NO: <?= $key->mask_fax_back; ?></span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-306px;top:802px;width:612px;height:792px;border-style:outset;overflow:auto;margin-top: -800px;margin-bottom: 800px;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/mask/mask.jpg" width=612 height=792></div>
<div style="position:absolute;left:163.10px;top:155.85px" class="cls_018"><span class="cls_018">PROTECTIVE FACE MASK DETAILED WRITTEN ORDER</span></div>
<div style="position:absolute;left:214.13px;top:177.78px" class="cls_019"><span class="cls_019">CERTIFICATE OF MEDICAL NECESSITY</span></div>
<div style="position:absolute;left:244.01px;top:196.16px" class="cls_018"><span class="cls_018">SIGNATURE REQUIRED</span></div>
<div style="position:absolute;left:72.02px;top:232.05px" class="cls_021"><span class="cls_021">Please Fax Back to</span><span class="cls_020"> :</span><span class="cls_022">  </span><span class="cls_023"><?= $key->mask_fax_back; ?></span></div>
<div style="position:absolute;left:72.02px;top:251.95px" class="cls_025"><span class="cls_025">Patient’s Information</span><span class="cls_026">:</span></div>
<div style="position:absolute;left:72.02px;top:271.61px" class="cls_027"><span class="cls_027">Patient Name: <?= $key->mask_firstname.'  '.$key->mask_lastname; ?></span></div>
<div style="position:absolute;left:72.02px;top:288.05px" class="cls_027"><span class="cls_027">Date of Birth: <?= $key->mask_dob; ?></span></div>
<div style="position:absolute;left:72.02px;top:304.61px" class="cls_027"><span class="cls_027">MBI #: <?= $key->mask_med_id; ?></span></div>
<div style="position:absolute;left:72.02px;top:321.05px" class="cls_027"><span class="cls_027">Address: <?= $key->mask_address . "," .$key->mask_city . "," .$key->mask_state . "," . $key->mask_zip ; ?></span></div>
<div style="position:absolute;left:72.02px;top:337.61px" class="cls_027"><span class="cls_027">Telephone: <?= $key->mask_phone; ?></span></div>
<div style="position:absolute;left:72.02px;top:370.87px" class="cls_025"><span class="cls_025">Prescriber’s Information:</span></div>
<div style="position:absolute;left:72.02px;top:389.45px" class="cls_027"><span class="cls_027">Doctor Name: <?= $key->mask_doc_firstname.'  '.$key->mask_doc_lastname; ?></span></div>
<div style="position:absolute;left:72.02px;top:406.99px" class="cls_027"><span class="cls_027">Doctor Address: <?= $key->mask_doc_address . "," .$key->mask_doc_city . "," .$key->mask_doc_state . "," . $key->mask_doc_zip ; ?></span></div>
<div style="position:absolute;left:72.02px;top:422.47px" class="cls_027"><span class="cls_027">Doctor Phone: <?= $key->mask_doc_phone; ?></span></div>
<div style="position:absolute;left:72.02px;top:439.99px" class="cls_027"><span class="cls_027">Doctor Fax: <?= $key->mask_doc_fax; ?></span></div>
<div style="position:absolute;left:72.02px;top:451.51px" class="cls_027"><span class="cls_027">Doctor NPI: <?= $key->mask_doc_npi; ?></span></div>
<div style="position:absolute;left:72.02px;top:474.55px" class="cls_037"><span class="cls_037">Product Description</span><span class="cls_027">:</span></div>
<div style="position:absolute;left:72.02px;top:498.67px" class="cls_027"><span class="cls_027">HCPS CODE:</span></div>
<div style="position:absolute;left:156.02px;top:498.67px" class="cls_027"><span class="cls_027">A4298</span></div>
<div style="position:absolute;left:210.41px;top:498.67px" class="cls_027"><span class="cls_027">3M N95 Respirator Mask</span></div>
<div style="position:absolute;left:72.02px;top:521.71px" class="cls_027"><span class="cls_027">Quantity:</span></div>
<div style="position:absolute;left:140.78px;top:521.71px" class="cls_027"><span class="cls_027">1</span></div>
<div style="position:absolute;left:72.02px;top:544.87px" class="cls_027"><span class="cls_027">Diagnosis</span><span class="cls_030">:</span></div>
<div style="position:absolute;left:72.02px;top:584.38px" class="cls_027"><span class="cls_027">Please indicate Length of need:</span></div>
<div style="position:absolute;left:252.05px;top:584.38px" class="cls_032"><span class="cls_032">months</span></div>
<div style="position:absolute;left:72.02px;top:603.94px" class="cls_027"><span class="cls_027">Other: _____________</span></div>
<div style="position:absolute;left:252.05px;top:634.90px" class="cls_027"><span class="cls_027">Prescriber’s Signature: __________________________</span></div>
<div style="position:absolute;left:255.41px;top:657.94px" class="cls_027"><span class="cls_027">Date :</span></div>
<div style="position:absolute;left:72.02px;top:675.10px" class="cls_033"><span class="cls_033">Phone : <?= $key->mask_call_back_num; ?></span></div>
<div style="position:absolute;left:72.02px;top:690.94px" class="cls_033"><span class="cls_033">Fax:  <?= $key->mask_fax_back; ?></span></div>
<div style="position:absolute;left:72.02px;top:721.42px" class="cls_034"><span class="cls_034">info@medixpharmacy.com</span></div>
</div>

</body>
</html>
<?php } ?>