<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <style type="text/css">
        <!--
        span.cls_003{font-family:"Calibri Bold",serif;font-size:12.6px;color:rgb(0,58,130);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_003{font-family:"Calibri Bold",serif;font-size:12.6px;color:rgb(0,58,130);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_005{font-family:"Calibri Bold",serif;font-size:11.5px;color:rgb(0,58,130);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_005{font-family:"Calibri Bold",serif;font-size:11.5px;color:rgb(0,58,130);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_007{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_007{font-family:"Calibri Bold",serif;font-size:18.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_006{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_006{font-family:"Calibri Bold",serif;font-size:14.4px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_011{font-family:"Calibri",serif;font-size:12.4px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_011{font-family:"Calibri",serif;font-size:12.4px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_012{font-family:"Calibri",serif;font-size:11.1px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_012{font-family:"Calibri",serif;font-size:11.1px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_013{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_013{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_021{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
        div.cls_021{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_017{font-family:"Calibri Bold",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_017{font-family:"Calibri Bold",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_019{font-family:Arial,serif;font-size:10.0px;color:rgb(34,31,31);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_019{font-family:Arial,serif;font-size:10.0px;color:rgb(34,31,31);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_020{font-family:"Verdana Bold",serif;font-size:13.0px;color:rgb(118,118,118);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_020{font-family:"Verdana Bold",serif;font-size:13.0px;color:rgb(118,118,118);font-weight:bold;font-style:normal;text-decoration: none}
        -->
    </style>
    <script type="text/javascript" src="afd1ac36-5ff4-11eb-8b25-0cc47a792c0a_id_afd1ac36-5ff4-11eb-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
$date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
    <div style="position:absolute;left:0px;top:0px">
        <img src="assets/images/mask/cover2.jpg" width=612 height=792></div>
    <div style="position:absolute;left:163.10px;top:133.53px" class="cls_003"><span class="cls_003">PROTECTIVE FACE MASK DETAILED WRITTEN ORDER</span></div>
    <div style="position:absolute;left:214.13px;top:155.46px" class="cls_005"><span class="cls_005">CERTIFICATE OF MEDICAL NECESSITY</span></div>
    <div style="position:absolute;left:244.01px;top:173.72px" class="cls_003"><span class="cls_003">SIGNATURE REQUIRED</span></div>
    <div style="position:absolute;left:72.02px;top:209.58px" class="cls_007"><span class="cls_007">Please Fax Back to</span><span class="cls_006"> : <?= $key->mask_fax_back; ?></span></div>
    <div style="position:absolute;left:72.02px;top:246.55px" class="cls_011"><span class="cls_011">Patient’s Information</span><span class="cls_012">: </span></div>
    <div style="position:absolute;left:72.02px;top:266.21px" class="cls_013"><span class="cls_013">Patient Name: <?= $key->mask_firstname.'  '.$key->mask_lastname; ?></span></div>
    <div style="position:absolute;left:72.02px;top:282.65px" class="cls_013"><span class="cls_013">Date of Birth: <?= $key->mask_dob; ?></span></div>
    <div style="position:absolute;left:72.02px;top:299.21px" class="cls_013"><span class="cls_013">MBI #: <?= $key->mask_med_id; ?></span></div>
    <div style="position:absolute;left:72.02px;top:315.65px" class="cls_013"><span class="cls_013">Address: <?= $key->mask_address . "," .$key->mask_city . "," .$key->mask_state . "," . $key->mask_zip ; ?></span></div>
    <div style="position:absolute;left:72.02px;top:332.21px" class="cls_013"><span class="cls_013">Telephone: <?= $key->mask_phone; ?></span></div>
    <div style="position:absolute;left:72.02px;top:365.47px" class="cls_011"><span class="cls_011">Prescriber’s Information:</span></div>
    <div style="position:absolute;left:72.02px;top:384.05px" class="cls_013"><span class="cls_013">Doctor Name: <?= $key->mask_doc_firstname.'  '.$key->mask_doc_lastname; ?></span></div>
    <div style="position:absolute;left:72.02px;top:401.57px" class="cls_013"><span class="cls_013">Practice Address: <?= $key->mask_doc_address . "," .$key->mask_doc_city . "," .$key->mask_doc_state . "," . $key->mask_doc_zip ; ?></span></div>
    <div style="position:absolute;left:72.02px;top:417.07px" class="cls_013"><span class="cls_013">Phone: <?= $key->mask_doc_phone; ?></span></div>
    <div style="position:absolute;left:72.02px;top:434.59px" class="cls_013"><span class="cls_013">Fax: <?= $key->mask_doc_fax; ?></span></div>
    <div style="position:absolute;left:72.02px;top:446.11px" class="cls_013"><span class="cls_013">NPI: <?= $key->mask_doc_npi; ?></span></div>
    <div style="position:absolute;left:72.02px;top:469.15px" class="cls_021"><span class="cls_021">Product Description</span><span class="cls_013">:</span></div>
    <div style="position:absolute;left:72.02px;top:493.27px" class="cls_013"><span class="cls_013">HCPS CODE:</span></div>
    <div style="position:absolute;left:156.02px;top:493.27px" class="cls_013"><span class="cls_013">A4298</span></div>
    <div style="position:absolute;left:210.41px;top:493.27px" class="cls_013"><span class="cls_013">3M N95 Respirator Mask</span></div>
    <div style="position:absolute;left:72.02px;top:516.31px" class="cls_013"><span class="cls_013">Quantity:</span></div>
    <div style="position:absolute;left:140.78px;top:516.31px" class="cls_013"><span class="cls_013">1</span></div>
    <div style="position:absolute;left:72.02px;top:539.47px" class="cls_013"><span class="cls_013">Diagnosis</span><span class="cls_017">:</span></div>
    <div style="position:absolute;left:72.02px;top:578.98px" class="cls_013"><span class="cls_013">Please indicate Length of need:</span></div>
    <div style="position:absolute;left:252.05px;top:578.98px" class="cls_019"><span class="cls_019">months</span></div>
    <div style="position:absolute;left:72.02px;top:598.54px" class="cls_013"><span class="cls_013">Other: _____________</span></div>
    <div style="position:absolute;left:252.05px;top:629.50px" class="cls_013"><span class="cls_013">Prescriber’s Signature: __________________________</span></div>
    <div style="position:absolute;left:255.41px;top:652.54px" class="cls_013"><span class="cls_013">Date  : __________________________</span></div>
    <div style="position:absolute;left:72.02px;top:690.46px" class="cls_020"><span class="cls_020">Phone:  <?= $key->mask_call_back_num; ?></span></div>
    <div style="position:absolute;left:72.02px;top:706.30px" class="cls_020"><span class="cls_020"><?= $key->mask_fax_back; ?></span></div>
    <div style="position:absolute;left:72.02px;top:722.14px" class="cls_020"><span class="cls_020"> </span><A HREF="http://www.cvs.com/">www.cvs.com</A> </div>
</div>

</body>
</html>
<?php } ?>