<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
@media print {
    #breakPage {page-break-after: always;}
}
span.c_cls_002{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.c_cls_002{font-family:Arial,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.c_cls_005{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.c_cls_005{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.c_cls_006{font-family:"Calibri Bold",serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.c_cls_006{font-family:"Calibri Bold",serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.c_cls_007{font-family:"Calibri",serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.c_cls_007{font-family:"Calibri",serif;font-size:16.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}





span.cls_003{font-family:"Arial Bold",serif;font-size:15.7px;font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:"Arial Bold",serif;font-size:15.7px;font-weight:bold;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Arial",serif;font-size:13.4px;font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Arial",serif;font-size:13.4px;font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:"Arial",serif;font-size:12.1px;font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:"Arial",serif;font-size:12.1px;font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:11.0px;font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:11.0px;font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: underline}
div.cls_014{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
span.cls_010{font-family:"Arial Bold",serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
div.cls_010{font-family:"Arial Bold",serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:11.0px;font-weight:bold;font-style:normal;text-decoration: none}
span.cls_013{font-family:"Arial Bold",serif;font-size:15.4px;font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:"Arial Bold",serif;font-size:15.4px;font-weight:bold;font-style:normal;text-decoration: none}
-->
</style>

</head>
<body>
    <?php
 $date = date("m/d/Y");
foreach($fax as $key){ ?>

<!--Cover Letter Starts-->
<div style="height:1584">
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;;width:612px; ">
<div style="position:absolute;left:72px;top:145px">
<img src="assets/images/mask/cover_background.jpg" style="width:300px; margin-left:70px; margin-top:-20px;" ></div>
<div style="position:absolute;left:72.02px;top:74.90px" class="c_cls_002"><span class="c_cls_002">FAX</span></div>
<div style="position:absolute;left:72.02px;top:205.51px" class="c_cls_005"><span class="c_cls_005">From: CVS Caremark</span></div>
<div style="position:absolute;left:449.45px;top:205.51px" class="c_cls_005"><span class="c_cls_005">Date:  <?= $date; ?></span></div>
<div style="position:absolute;left:72.02px;top:219.19px" class="c_cls_005"><span class="c_cls_005">Phone: <?= $key->mask_call_back_num; ?></span></div>
<div style="position:absolute;left:72.02px;top:233.35px" class="c_cls_005"><span class="c_cls_005">Fax: <?= $key->mask_fax_back; ?></span></div>
<div style="position:absolute;left:72.02px;top:276.55px" class="c_cls_005"><span class="c_cls_005">To: <?= $key->mask_doc_firstname.'  '.$key->mask_doc_lastname; ?></span></div>
<div style="position:absolute;left:72.02px;top:290.50px" class="c_cls_005"><span class="c_cls_005">Phone: <?= $key->mask_doc_phone; ?></span></div>
<div style="position:absolute;left:72.02px;top:304.42px" class="c_cls_005"><span class="c_cls_005">Fax: <?= $key->mask_doc_fax; ?></span></div>
<div style="position:absolute;left:72.02px;top:339.70px" class="c_cls_005"><span class="c_cls_005">Subject: Certificate of Medical Necessity Signature Required</span></div>
<div style="position:absolute;left:72.02px;top:397.80px" class="c_cls_006"><span class="c_cls_006">NO of Pages Including Cover: 2</span></div>
<div style="position:absolute;left:72.02px;top:434.04px" class="c_cls_007"><span class="c_cls_007">Important: This facsimile transmission contains confidential</span></div>
<div style="position:absolute;left:72.02px;top:453.50px" class="c_cls_007"><span class="c_cls_007">information, some or all of which may be protected health information</span></div>
<div style="position:absolute;left:72.02px;top:472.94px" class="c_cls_007"><span class="c_cls_007">as defined by the federal Health Insurance Portability & Accountability</span></div>
<div style="position:absolute;left:72.02px;top:492.62px" class="c_cls_007"><span class="c_cls_007">Act (HIPPA) Privacy Rule. This transmission is intended for the exclusive</span></div>
<div style="position:absolute;left:72.02px;top:512.06px" class="c_cls_007"><span class="c_cls_007">use of the individual or entity to which it is addressed and may contain</span></div>
<div style="position:absolute;left:72.02px;top:531.77px" class="c_cls_007"><span class="c_cls_007">information that is proprietary, privileged, confidential and/or exempt</span></div>
<div style="position:absolute;left:72.02px;top:551.21px" class="c_cls_007"><span class="c_cls_007">from disclosure under applicable law. If you are not the intended</span></div>
<div style="position:absolute;left:72.02px;top:570.65px" class="c_cls_007"><span class="c_cls_007">recipient (or an employee or agent responsible for delivering this</span></div>
<div style="position:absolute;left:72.02px;top:590.33px" class="c_cls_007"><span class="c_cls_007">facsimile transmission to the intended recipient), you are hereby</span></div>
<div style="position:absolute;left:72.02px;top:609.79px" class="c_cls_007"><span class="c_cls_007">notified that any disclosure, dissemination, distribution or copying of</span></div>
<div style="position:absolute;left:72.02px;top:629.23px" class="c_cls_007"><span class="c_cls_007">this information is strictly prohibited and may be subject to legal</span></div>
<div style="position:absolute;left:72.02px;top:648.91px" class="c_cls_007"><span class="c_cls_007">restriction or sanction. Please notify the sender by telephone.</span></div>
</div>

<!--Cover Letter Ends-->
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px; border-style:outset;">
<div style="position:absolute;left:0px;top:0px">

<div style="position:absolute;left:162.26px;top:67.69px" class="cls_003"><span class="cls_003">Protective Face Mask Detailed Written Order</span></div>
<div style="position:absolute;left:205.25px;top:95.41px" class="cls_003"><span class="cls_003">Certificate of Medical Necessity</span></div>
<div style="position:absolute;left:244.73px;top:123.13px" class="cls_003"><span class="cls_003">Signature Required</span></div>
<div style="position:absolute;left:72.02px;top:164.48px" class="cls_006" ><span class="cls_006" style="background-color:black !important; color:white !important;">Patient Information</span><span class="cls_007">:</span></div>
<div style="position:absolute;left:72.02px;top:190.74px" class="cls_008"><span class="cls_008">Patient Name:</span><span class="cls_009"> <?= $key->mask_firstname.'  '.$key->mask_lastname; ?></span></div>
<div style="position:absolute;left:393.79px;top:190.74px" class="cls_009"><span class="cls_009">Order Start Date: <?= $date; ?></span></div>
<div style="position:absolute;left:72.02px;top:202.14px" class="cls_008"><span class="cls_008">Date of Birth:</span><span class="cls_009"> <?= $key->mask_dob; ?></span></div>
<div style="position:absolute;left:72.02px;top:213.66px" class="cls_008"><span class="cls_008">MBI #: </span><span class="cls_009"> <?= $key->mask_med_id; ?></span></div>
<div style="position:absolute;left:72.02px;top:225.18px" class="cls_008"><span class="cls_008">Address:</span><span class="cls_009"> <?= $key->mask_address . "," .$key->mask_city . "," .$key->mask_state . "," . $key->mask_zip ; ?></span></div>
<div style="position:absolute;left:72.02px;top:248.25px" class="cls_008"><span class="cls_008">Telephone:</span><span class="cls_009">  <?= $key->mask_phone; ?></span></div>
<div style="position:absolute;left:72.02px;top:281.39px" class="cls_006"><span class="cls_006" style="background-color:black !important; color:white !important;">Prescriber Information</span>:</div>
<div style="position:absolute;left:72.02px;top:307.65px" class="cls_008"><span class="cls_008">Doctor Name:</span><span class="cls_009"> <?= $key->mask_doc_firstname.'  '.$key->mask_doc_lastname; ?></span></div>
<div style="position:absolute;left:72.02px;top:319.17px" class="cls_008"><span class="cls_008">Doctor NPI:</span><span class="cls_009"> <?= $key->mask_doc_npi; ?></span></div>
<div style="position:absolute;left:72.02px;top:330.57px" class="cls_008"><span class="cls_008">Doctor Phone:</span><span class="cls_009"> <?= $key->mask_doc_phone; ?></span></div>
<div style="position:absolute;left:72.02px;top:342.09px" class="cls_008"><span class="cls_008">Doctor Fax:</span><span class="cls_009"> <?= $key->mask_doc_fax; ?></span></div>
<div style="position:absolute;left:72.02px;top:365.13px" class="cls_014"><span class="cls_014">Product Description</span><span class="cls_008">:</span></div>
<div style="position:absolute;left:72.02px;top:389.25px" class="cls_008"><span class="cls_008">HCPS CODE:</span></div>
<div style="position:absolute;left:142.78px;top:386.25px" class="cls_008"><span class="cls_008"><input type="checkbox"></span></div>
<div style="position:absolute;left:166.78px;top:389.25px" class="cls_008"><span class="cls_008"> A4298</span></div>
<div style="position:absolute;left:213.17px;top:389.25px" class="cls_008"><span class="cls_008">3M N95 Respirator Mask</span></div>
<div style="position:absolute;left:72.02px;top:412.31px" class="cls_008"><span class="cls_008">Quantity:</span></div>
<div style="position:absolute;left:140.78px;top:412.31px" class="cls_008"><span class="cls_008">1</span></div>
<div style="position:absolute;left:72.02px;top:435.35px" class="cls_008"><span class="cls_008">Diagnosis: ____________________</span><span class="cls_010"></span></div>
<div style="position:absolute;left:72.02px;top:474.95px" class="cls_008"><span class="cls_008">Please indicate Length of need: _______</span></div>
<div style="position:absolute;left:257.09px;top:474.95px" class="cls_012"><span class="cls_012">months</span></div>
<div style="position:absolute;left:72.02px;top:506.03px" class="cls_008"><span class="cls_008">Other: _____________</span></div>
<div style="position:absolute;left:252.05px;top:559.91px" class="cls_008"><span class="cls_008">Prescriber’s Signature: __________________________</span></div>
<div style="position:absolute;left:252.05px;top:571.43px" class="cls_009"><span class="cls_009"></span></div>
<div style="position:absolute;left:324.07px;top:594.50px" class="cls_008"><span class="cls_008">Date: ______________________________</span></div>
<div style="position:absolute;left:72.02px;top:639.65px" class="cls_013"><span class="cls_013">For more information: <?= $key->mask_call_back_num; ?></span></div>
<div style="position:absolute;left:72.02px;top:659.57px;" class="cls_013"><span class="cls_013">Return Fax: <?= $key->mask_fax_back; ?></span></div>

<div id="breakPage"></div>
</div>

</div>
	<?php } ?>
</body>

