<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:"Calibri",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:"Calibri",serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_006{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:13.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="089ffc1e-af69-11eb-8b25-0cc47a792c0a_id_089ffc1e-af69-11eb-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
date_default_timezone_set("Asia/Karachi");
 $date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/hipaa/background5_hipaa.jpg" width=612 height=792></div>
<div style="position:absolute;left:493.30px;top:3.75px" class="cls_002"><span class="cls_002"><?= $key->hipaa_fax_back; ?></span></div>
<div style="position:absolute;left:40.56px;top:119.30px" class="cls_006"><span class="cls_006">HIPAA COMPLIANT PROVIDER CONFIRMATION FORM</span></div>
<div style="position:absolute;left:43.32px;top:134.18px" class="cls_007"><span class="cls_007">Please Fax Back To : <?= $key->hipaa_fax_back; ?></span></div>
<div style="position:absolute;left:158.54px;top:180.26px" class="cls_009"><span class="cls_009">PATIENT INFORMATION</span></div>
<div style="position:absolute;left:40.92px;top:201.26px" class="cls_010"><span class="cls_010">Name:<?= $key->hipaa_firstname . " " . $key->hipaa_lastname ; ?></span></div>
<div style="position:absolute;left:369.43px;top:201.26px" class="cls_010"><span class="cls_010">Birth Date:<?= $key->hipaa_dob ; ?></span></div>
<div style="position:absolute;left:40.80px;top:219.26px" class="cls_010"><span class="cls_010">Address:<?= $key->hipaa_address . " " .$key->hipaa_city . " " .$key->hipaa_state . " ".$key->hipaa_zip ; ?></span></div>
<div style="position:absolute;left:40.92px;top:237.41px" class="cls_010"><span class="cls_010">Phone :<?= $key->hipaa_phone	 ; ?></span></div>
<div style="position:absolute;left:294.89px;top:237.41px" class="cls_010"><span class="cls_010">MBI :<?= $key->hipaa_med_id ; ?></span></div>
<div style="position:absolute;left:154.22px;top:259.85px" class="cls_010"><span class="cls_010">PROVIDER’S  INFORMATION</span></div>
<div style="position:absolute;left:40.92px;top:276.89px" class="cls_010"><span class="cls_010">Name:<?= $key->hipaa_doc_firstname . " " . $key->hipaa_doc_lastname ; ?></span></div>
<div style="position:absolute;left:367.39px;top:276.89px" class="cls_010"><span class="cls_010">Npi:<?= $key->hipaa_doc_npi ; ?></span></div>
<div style="position:absolute;left:40.92px;top:295.97px" class="cls_010"><span class="cls_010">Practice Address:<?= $key->hipaa_doc_address . " " .$key->hipaa_doc_city . " " .$key->hipaa_doc_state . " ".$key->hipaa_doc_zip ; ?></span></div>
<div style="position:absolute;left:367.39px;top:295.97px" class="cls_010"><span class="cls_010">Phone :<?= $key->hipaa_doc_phone ; ?></span></div>
<div style="position:absolute;left:38.64px;top:312.41px" class="cls_010"><span class="cls_010">Fax :<?= $key->hipaa_doc_fax	 ; ?></span></div>
<div style="position:absolute;left:35.04px;top:356.09px" class="cls_007"><span class="cls_007">I certify that i am the physician identified on the above section</span></div>
<div style="position:absolute;left:35.04px;top:367.61px" class="cls_007"><span class="cls_007">and i confirm that the information contained in this document</span></div>
<div style="position:absolute;left:35.04px;top:379.13px" class="cls_007"><span class="cls_007">is true ,accurate and complete ,to the best of my knowledge.</span></div>
<div style="position:absolute;left:35.04px;top:428.71px" class="cls_015"><span class="cls_015">Doctor’s Confirmation signature :</span></div>
<div style="position:absolute;left:35.04px;top:458.71px" class="cls_016"><span class="cls_016">MESSAGE :</span></div>
<div style="position:absolute;left:35.04px;top:486.31px" class="cls_014"><span class="cls_014">To Whom It May Concern:</span></div>
<div style="position:absolute;left:35.28px;top:527.71px" class="cls_014"><span class="cls_014">We want to follow up with you regarding a prescription but we need to confirm that this patient is an</span></div>
<div style="position:absolute;left:35.04px;top:541.39px" class="cls_014"><span class="cls_014">active patient at your facility so that we can send prior authorization form for a medication which patient</span></div>
<div style="position:absolute;left:35.04px;top:554.95px" class="cls_014"><span class="cls_014">has ordered  .</span></div>
<div style="position:absolute;left:35.40px;top:568.63px" class="cls_014"><span class="cls_014">Please verify and confirm that this patient is still being treated by doctor at this facility and patient’s pcp</span></div>
<div style="position:absolute;left:35.04px;top:582.46px" class="cls_014"><span class="cls_014">has also not changed his / her practice location .</span></div>
<div style="position:absolute;left:35.04px;top:609.58px" class="cls_014"><span class="cls_014">After receiving confirmation from your side , we will follow up with you in 48 hours .</span></div>
<div style="position:absolute;left:35.04px;top:623.38px" class="cls_014"><span class="cls_014">Sincerel</span></div>
<div style="position:absolute;left:35.04px;top:667.18px" class="cls_014"><span class="cls_014">Regina Simmon</span></div>
<div style="position:absolute;left:35.04px;top:680.98px" class="cls_014"><span class="cls_014">Office manager ,</span></div>
<div style="position:absolute;left:35.04px;top:694.78px" class="cls_014"><span class="cls_014">Walgreens Pharmacy</span></div>
<!--<div style="position:absolute;left:35.04px;top:708.58px" class="cls_014"><span class="cls_014"><?= $date; ?></span></div>-->
</div>
<?php } ?>
</body>
</html>
