<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_004{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="fdf10d28-f125-11ea-8b25-0cc47a792c0a_id_fdf10d28-f125-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
            <?php
 $date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/hipaa/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:103.58px;top:72.26px" class="cls_004"><span class="cls_004">HIPAA COMPLIANT PHYSICIAN AUTHORIZATION FORM TO</span></div>
<div style="position:absolute;left:203.21px;top:88.34px" class="cls_004"><span class="cls_004">CONFIRM A ACTIVE PATIENT</span></div>
<div style="position:absolute;left:72.02px;top:132.02px" class="cls_007"><span class="cls_007">TO: </span></div>
<div style="position:absolute;left:120.02px;top:129.98px" class="cls_008"><span class="cls_008"><?= $key->hipaa_doc_firstname . " " . $key->hipaa_doc_lastname ; ?><span style="margin-left:180px">NPI: <?= $key->hipaa_doc_npi ?></span></span></div>
<div style="position:absolute;left:117.02px;top:145.82px" class="cls_002"><span class="cls_002">Name of Healthcare Provider/Physician/Facility/Medicare Contractor</span></div>
<div style="position:absolute;left:115.10px;top:171.62px" class="cls_002"><span class="cls_002">Phone: </span><span class="cls_008"><?= $key->hipaa_doc_phone ; ?></span></div>
<div style="position:absolute;left:361.03px;top:171.62px" class="cls_002"><span class="cls_002">Fax:</span><span class="cls_008"><?= $key->hipaa_doc_fax ; ?></span></div>
<div style="position:absolute;left:114.98px;top:196.94px" class="cls_002"><span class="cls_002">Address: </span><span class="cls_008"><?= $key->hipaa_doc_address . " " .$key->hipaa_doc_city . " " .$key->hipaa_doc_state . " ".$key->hipaa_doc_zip ; ?></span></div>
<div style="position:absolute;left:72.02px;top:238.37px" class="cls_007"><span class="cls_007">RE:</span><span class="cls_002">    Patient Name:</span><span class="cls_008" style="margin-left:20px;"><?= $key->hipaa_firstname . " " . $key->hipaa_lastname ; ?></span></div>
<div style="position:absolute;left:109.34px;top:265.85px" class="cls_002"><span class="cls_002">Date of Birth: </span><span class="cls_008"><?= $key->hipaa_dob ; ?></span></div>
<div style="position:absolute;left:284.09px;top:265.85px" class="cls_002"><span class="cls_002">Member ID:</span></div>
<div style="position:absolute;left:352.63px;top:265.85px" class="cls_008"><span class="cls_008"><?= $key->hipaa_med_id ; ?></span></div>
<div style="position:absolute;left:109.34px;top:302.45px" class="cls_002"><span class="cls_002">Phone: </span><span class="cls_008">(<?= $key->hipaa_phone ; ?>)</span></div>
<div style="position:absolute;left:284.09px;top:302.45px" class="cls_002"><span class="cls_002">Address:  </span><span class="cls_008"><?= $key->hipaa_address . " " .$key->hipaa_city . " " .$key->hipaa_state . " ".$key->hipaa_zip ; ?></span></div>
<div style="position:absolute;left:117.02px;top:340.97px" class="cls_002"><span class="cls_002">This is just a active patient authorization form so no clinical or office visit notes of the</span></div>
<div style="position:absolute;left:117.02px;top:353.09px" class="cls_002"><span class="cls_002">above mentioned is required. office notes, face sheets, history and physical,</span></div>
<div style="position:absolute;left:117.02px;top:365.21px" class="cls_002"><span class="cls_002">consultation notes, inpatient, outpatient and emergency room treatment, all clinical</span></div>
<div style="position:absolute;left:117.02px;top:377.33px" class="cls_002"><span class="cls_002">charts, r ports, order sheets, progress notes, nurse's notes, social worker records, clinic</span></div>
<div style="position:absolute;left:117.02px;top:389.45px" class="cls_002"><span class="cls_002">records, treatment plans, admission records, discharge summaries, requests for and</span></div>
<div style="position:absolute;left:117.02px;top:401.59px" class="cls_002"><span class="cls_002">reports of consultations, documents, correspondence, test results, statements,</span></div>
<div style="position:absolute;left:117.02px;top:413.83px" class="cls_002"><span class="cls_002">questionnaires/histories, are not required.</span></div>
<div style="position:absolute;left:117.02px;top:438.07px" class="cls_002"><span class="cls_002">Please authorize whether the patient is still under the care at this office.</span></div>
<div style="position:absolute;left:117.02px;top:462.43px" class="cls_002"><span class="cls_002">Please fax this form back within 48 hours so that we can follow up with you</span></div>
<div style="position:absolute;left:117.02px;top:474.55px" class="cls_002"><span class="cls_002">accordingly.</span></div>
<div style="position:absolute;left:117.02px;top:498.91px" class="cls_002"><span class="cls_002">If the patient has changed or switched to another Provider please mentioned</span></div>
<div style="position:absolute;left:117.02px;top:511.03px" class="cls_002"><span class="cls_002">below.</span></div>
<div style="position:absolute;left:66.02px;top:535.75px" class="cls_015"><span class="cls_015">➔</span><span class="cls_007">   I undersigned; certify that the above patient is under my care and being treated at our</span></div>
<div style="position:absolute;left:66.02px;top:556.63px" class="cls_007"><span class="cls_007">facility. I certify that this information is true and correct and as per as HIPAA Compliance.</span></div>
<div style="position:absolute;left:66.02px;top:570.46px" class="cls_007"><span class="cls_007">The above mentioned information will strictly remain confidential.</span></div>
<div style="position:absolute;left:72.02px;top:646.90px" class="cls_002"><span class="cls_002">Physician or FNP Signature</span></div>
<div style="position:absolute;left:402.67px;top:646.90px" class="cls_002"><span class="cls_002">Date: </span></div>
<div style="position:absolute;left:402.67px;top:674.58px" class="cls_007"><span class="cls_007">Phone: </span><span class="cls_004"><?= $key->hipaa_call_back_num; ?></span></div>
<div style="position:absolute;left:402.67px;top:686.94px" class="cls_007"><span class="cls_007">Return Fax: </span><span class="cls_004"><?= $key->hipaa_fax_back; ?></span></div>
<div style="position:absolute;left:72.02px;top:666.90px" class="cls_002"><span class="cls_002">If different doctor from above print name: </span></div>
<div style="position:absolute;left:72.02px;top:675.58px" class="cls_007"><span class="cls_007">Name:</span><span class="cls_004">________________________</span></div>
<div style="position:absolute;left:72.02px;top:687.94px" class="cls_007"><span class="cls_007">NPI: </span><span class="cls_004">_________________________</span></div>

</div>
<?php } ?>
</body>
</html>
