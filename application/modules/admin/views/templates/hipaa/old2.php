<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
    <style type="text/css">
        <!--
        span.cls_004{font-family:Times,serif;font-size:16.0px;color:rgb(108,108,108);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_004{font-family:Times,serif;font-size:16.0px;color:rgb(108,108,108);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_021{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: underline}
        div.cls_021{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_011{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_011{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_015{font-family:Arial,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_016{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_016{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_018{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_018{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        span.cls_019{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        div.cls_019{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
        span.cls_020{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        div.cls_020{font-family:Times,serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
        -->
    </style>
    <script type="text/javascript" src="60b19104-1489-11eb-8b25-0cc47a792c0a_id_60b19104-1489-11eb-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
$date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
    <div style="position:absolute;left:0px;top:0px">
        <img src="assets/images/hipaa/background1.jpg" width=612 height=792></div>
    <div style="position:absolute;left:174.14px;top:66.38px" class="cls_004"><span class="cls_004">HIPAA COMPLIANT PHYCISIAN</span></div>
    <div style="position:absolute;left:90.20px;top:78.74px" class="cls_004"><span class="cls_004">AUTHORIZATION FORM TO CONFIRM AN ACTIVE PATIENT</span></div>
    <div style="position:absolute;left:246.29px;top:123.98px" class="cls_021"><span class="cls_021">Patient Information</span></div>
    <div style="position:absolute;left:108.02px;top:153.98px" class="cls_002"><span class="cls_002">Patient Name: <?= $key->hipaa_firstname . " " . $key->hipaa_lastname ; ?></span></div>
    <div style="position:absolute;left:109.34px;top:172.22px" class="cls_002"><span class="cls_002">Date of Birth: <?= $key->hipaa_dob ; ?></span></div>
    <div style="position:absolute;left:109.34px;top:190.58px" class="cls_002"><span class="cls_002">Member ID: <?= $key->hipaa_med_id ; ?></span></div>
    <div style="position:absolute;left:109.34px;top:208.82px" class="cls_002"><span class="cls_002">Address: <?= $key->hipaa_address . " " .$key->hipaa_city . " " .$key->hipaa_state . " ".$key->hipaa_zip ; ?></span></div>
    <div style="position:absolute;left:109.34px;top:227.18px" class="cls_002"><span class="cls_002">Phone: <?= $key->hipaa_phone ; ?></span></div>
    <div style="position:absolute;left:237.53px;top:268.37px" class="cls_021"><span class="cls_021">Physician Information</span></div>
    <div style="position:absolute;left:105.02px;top:298.13px" class="cls_002"><span class="cls_002">Name</span><span class="cls_011">: <?= $key->hipaa_doc_firstname . " " . $key->hipaa_doc_lastname ; ?></span></div>
    <div style="position:absolute;left:105.02px;top:312.41px" class="cls_002"><span class="cls_002">Address: <?= $key->hipaa_doc_address . " " .$key->hipaa_doc_city . " " .$key->hipaa_doc_state . " ".$key->hipaa_doc_zip ; ?></span></div>
    <div style="position:absolute;left:105.02px;top:326.69px" class="cls_002"><span class="cls_002">Phone: <?= $key->hipaa_doc_phone ; ?></span></div>
    <div style="position:absolute;left:105.02px;top:340.85px" class="cls_002"><span class="cls_002">Fax: <?= $key->hipaa_doc_fax ; ?></span></div>
    <div style="position:absolute;left:105.02px;top:355.13px" class="cls_002"><span class="cls_002">NPI: <?= $key->hipaa_doc_npi ?></span></div>
    <div style="position:absolute;left:117.02px;top:395.57px" class="cls_011"><span class="cls_011">This is just a active patient authorization form so no clinical or office visit notes of the above</span></div>
    <div style="position:absolute;left:117.02px;top:406.75px" class="cls_011"><span class="cls_011">mentioned is required. office notes, face sheets, history and physical, consultation notes,</span></div>
    <div style="position:absolute;left:117.02px;top:417.79px" class="cls_011"><span class="cls_011">inpatient, outpatient and emergency room treatment, all clinical charts, reports, order sheets,</span></div>
    <div style="position:absolute;left:117.02px;top:428.95px" class="cls_011"><span class="cls_011">progress notes, nurse's notes, social worker records, clinic records, treatment plans, admission</span></div>
    <div style="position:absolute;left:117.02px;top:440.11px" class="cls_011"><span class="cls_011">records, discharge summaries, requests for and reports of consultations, documents,</span></div>
    <div style="position:absolute;left:117.02px;top:451.15px" class="cls_011"><span class="cls_011">correspondence, test results, statements, questionnaires/histories, are not required.</span></div>
    <div style="position:absolute;left:117.02px;top:474.31px" class="cls_011"><span class="cls_011">Please authorize whether the patient is under the care at this office.</span></div>
    <div style="position:absolute;left:117.02px;top:496.51px" class="cls_011"><span class="cls_011">Please fax this form back within 48 hours so that we can follow up with you accordingly.</span></div>
    <div style="position:absolute;left:117.02px;top:519.31px" class="cls_011"><span class="cls_011">If the patient has changed or switched to another Provider please mention below.</span></div>
    <div style="position:absolute;left:66.02px;top:542.95px" class="cls_015"><span class="cls_015">➔</span><span class="cls_016">   I undersigned; certify that the above patient is under my care and being treated at our facility. I</span></div>
    <div style="position:absolute;left:66.02px;top:561.43px" class="cls_016"><span class="cls_016">certify that this information is true and correct and as per as HIPAA Compliance. The above</span></div>
    <div style="position:absolute;left:66.02px;top:574.18px" class="cls_016"><span class="cls_016">mentioned information will strictly remain confidential.</span></div>
    <div style="position:absolute;left:72.02px;top:640.06px" class="cls_011"><span class="cls_011">Physician or FNP Signature</span></div>
    <div style="position:absolute;left:402.67px;top:639.10px" class="cls_011"><span class="cls_011">Date</span></div>
    <div style="position:absolute;left:66.02px;top:653.02px" class="cls_018"><span class="cls_018">If different doctor from above print name:</span></div>
    <div style="position:absolute;left:66.02px;top:676.06px" class="cls_018"><span class="cls_018">Name:___________________</span></div>
    <div style="position:absolute;left:66.02px;top:698.98px" class="cls_018"><span class="cls_018">NPI: ___________________</span></div>
    <div style="position:absolute;left:215.45px;top:733.56px" class="cls_019"><span class="cls_019">Return Fax</span><span class="cls_020">: <?= $key->hipaa_fax_back; ?></span></div>
</div>

</body>
</html>
<?php } ?>
