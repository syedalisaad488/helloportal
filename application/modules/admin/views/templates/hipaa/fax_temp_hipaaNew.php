<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_004{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Times,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Arial,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_014{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:"Lucida Fax Regular",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:"Lucida Fax Regular",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="04a30bf8-2f42-11eb-8b25-0cc47a792c0a_id_04a30bf8-2f42-11eb-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
        <?php
$date = date("m/d/Y");
foreach($fax as $key){ ?>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/hipaa/pv2.jpg" width=612 height=792></div>
<div style="position:absolute;left:99.26px;top:84.98px" class="cls_004"><span class="cls_004">HIPAA COMPLIANT PHYSICIAN AUTHORIZATION FORM TO</span></div>
<div style="position:absolute;left:198.17px;top:101.06px" class="cls_004"><span class="cls_004">CONFIRM AN ACTIVE PATIENT</span></div>
<div style="position:absolute;left:72.02px;top:142.34px" class="cls_007"><span class="cls_007">TO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $key->hipaa_doc_firstname . " " . $key->hipaa_doc_lastname ; ?></span></div>
<div style="position:absolute;left:117.02px;top:158.18px" class="cls_002"><span class="cls_002">Name of Healthcare Provider/Physician/Facility</span></div>
<div style="position:absolute;left:69.02px;top:183.86px" class="cls_002"><span class="cls_002">Phone: <?= $key->hipaa_doc_phone ; ?></span></div>
<div style="position:absolute;left:361.03px;top:183.86px" class="cls_002"><span class="cls_002">Fax: <?= $key->hipaa_doc_fax ; ?></span></div>
<div style="position:absolute;left:66.02px;top:209.66px" class="cls_002"><span class="cls_002">Address: <?= $key->hipaa_doc_address . " " .$key->hipaa_doc_city . " " .$key->hipaa_doc_state . " ".$key->hipaa_doc_zip ; ?></span></div>
<div style="position:absolute;left:72.02px;top:237.29px" class="cls_007"><span class="cls_007">RE:</span><span class="cls_002">    Patient Name: <?= $key->hipaa_firstname . " " . $key->hipaa_lastname ; ?></span></div>
<div style="position:absolute;left:109.34px;top:264.77px" class="cls_002"><span class="cls_002">Date of Birth: <?= $key->hipaa_dob ; ?></span></div>
<div style="position:absolute;left:284.09px;top:264.77px" class="cls_002"><span class="cls_002">Member ID: <?= $key->hipaa_med_id ; ?></span></div>
<div style="position:absolute;left:109.34px;top:301.37px" class="cls_002"><span class="cls_002">Phone: <?= $key->hipaa_phone ; ?></span></div>
<div style="position:absolute;left:284.09px;top:301.37px" class="cls_002"><span class="cls_002">Address: <?= $key->hipaa_address . " " .$key->hipaa_city . " " .$key->hipaa_state . " ".$key->hipaa_zip ; ?></span></div>
<div style="position:absolute;left:66.02px;top:329.09px" class="cls_007"><span class="cls_007">In accordance with the Health Insurance Portability and Accountability Act of 1996</span></div>
<div style="position:absolute;left:66.02px;top:342.89px" class="cls_007"><span class="cls_007">(HIPAA), I confirm that</span><span class="cls_002">:</span></div>
<div style="position:absolute;left:120.02px;top:379.73px" class="cls_002"><span class="cls_002">Please authorize whether the patient is / was under the care at this office.</span></div>
<div style="position:absolute;left:120.02px;top:404.11px" class="cls_002"><span class="cls_002">Please fax this form back within 48 hours so that we can follow up with you</span></div>
<div style="position:absolute;left:117.02px;top:416.23px" class="cls_002"><span class="cls_002">accordingly.</span></div>
<div style="position:absolute;left:117.02px;top:440.59px" class="cls_002"><span class="cls_002">If the patient has changed or switched to another Provider please mentioned</span></div>
<div style="position:absolute;left:117.02px;top:452.71px" class="cls_002"><span class="cls_002">below.</span></div>
<div style="position:absolute;left:66.02px;top:477.31px" class="cls_013"><span class="cls_013">➔</span><span class="cls_014"> </span><span class="cls_007">I undersigned; certify that the above patient is under my care and being treated at our</span></div>
<div style="position:absolute;left:66.02px;top:498.31px" class="cls_007"><span class="cls_007">facility. I certify that this information is true and correct and as per as HIPAA Compliance.</span></div>
<div style="position:absolute;left:66.02px;top:512.11px" class="cls_007"><span class="cls_007">The above mentioned information will strictly remain confidential.</span></div>
<div style="position:absolute;left:72.02px;top:579.22px" class="cls_002"><span class="cls_002">Physician or FNP Signature</span></div>
<div style="position:absolute;left:402.67px;top:579.22px" class="cls_002"><span class="cls_002">Date</span></div>
<div style="position:absolute;left:75.26px;top:593.02px" class="cls_016"><span class="cls_016">NPI :  <?= $key->hipaa_doc_npi; ?></span></div>
<div style="position:absolute;left:66.02px;top:618.22px" class="cls_016"><span class="cls_016">If different doctor from above print name:</span></div>
<div style="position:absolute;left:362.59px;top:630.70px" class="cls_017"><span class="cls_017">Return Fax</span><span class="cls_018"> </span><span class="cls_019">:   <?= $key->hipaa_fax_back; ?></span></div>
<div style="position:absolute;left:66.02px;top:647.02px" class="cls_016"><span class="cls_016">Name: ___________________</span></div>
<div style="position:absolute;left:66.02px;top:662.86px" class="cls_016"><span class="cls_016">NPI: ___________________</span></div>
<div style="position:absolute;left:362.83px;top:659.86px" class="cls_020"><span class="cls_020">Phone :  <?= $key->hipaa_call_back_num; ?></span></div>
</div>

</body>
</html>
<?php } ?>