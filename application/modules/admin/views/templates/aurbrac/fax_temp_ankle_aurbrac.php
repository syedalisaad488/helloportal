<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}


span.cls_002{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:16.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:16.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_012{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="25e60e0c-e300-11ea-8b25-0cc47a792c0a_id_25e60e0c-e300-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>

<?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    

<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">

<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/ankle/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:351.12px;top:66.75px" class="cls_002"><span class="cls_002">Fax :</span><span class="cls_003"> </span><span class="cls_004"><?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:335.28px;top:84.15px" class="cls_002"><span class="cls_002">Phone : </span><span class="cls_004"><?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:188.64px;top:137.79px" class="cls_005"><span class="cls_005">DOCTOR ORDER FORM FOR</span></div>
<div style="position:absolute;left:204.12px;top:157.11px" class="cls_005"><span class="cls_005">ANKLE FOOT ORTHOSIS</span></div>
<div style="position:absolute;left:118.80px;top:217.71px" class="cls_004"><span class="cls_004">has requested that we send them an adjustable Ankle Brace  for</span></div>
<div style="position:absolute;left:106.32px;top:234.15px" class="cls_004"><span class="cls_004">instability relief.  They indicated that you are the treating physician to</span></div>
<div style="position:absolute;left:227.88px;top:250.35px" class="cls_004"><span class="cls_004">complete the written order.</span></div>
<div style="position:absolute;left:93.60px;top:296.06px" class="cls_006"><span class="cls_006">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:110.52px;top:317.19px" class="cls_006"><span class="cls_006">1. </span><span class="cls_004"> ICD-10</span></div>
<div style="position:absolute;left:110.52px;top:333.63px" class="cls_006"><span class="cls_006">2. </span><span class="cls_004"> Indication of need</span></div>
<div style="position:absolute;left:110.52px;top:349.95px" class="cls_006"><span class="cls_006">3. </span><span class="cls_004"> Please sign and date the form</span></div>
<div style="position:absolute;left:93.60px;top:373.83px" class="cls_004"><span class="cls_004">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:93.60px;top:397.94px" class="cls_007"><span class="cls_007">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:93.60px;top:408.86px" class="cls_007"><span class="cls_007">you, we will be able to ship out your patient’s ANKLE ORTHOSIS. If you have any questions, please</span></div>
<div style="position:absolute;left:93.60px;top:420.26px" class="cls_007"><span class="cls_007">call us at (<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:181.08px;top:546.15px" class="cls_005"><span class="cls_005">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:600.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/ankle/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:93.60px;top:85.83px" class="cls_005"><span class="cls_005">DOCTOR ORDER FORM ANKLE FOOT ORTHOSIS</span></div>
<div style="position:absolute;left:93.60px;top:125.96px" class="cls_003"><span class="cls_003">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:125.96px" class="cls_003"><span class="cls_003">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:139.28px" class="cls_003"><span class="cls_003">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:152.36px" class="cls_003"><span class="cls_003">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:152.36px" class="cls_003"><span class="cls_003">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:165.56px" class="cls_003"><span class="cls_003">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:165.56px" class="cls_003"><span class="cls_003">Date:</span></div>
<div style="position:absolute;left:338.76px;top:165.56px" class="cls_003"><span class="cls_003"><?= $date ; ?></span></div>
<div style="position:absolute;left:93.60px;top:178.64px" class="cls_010"><span class="cls_010">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:189.48px;top:178.64px" class="cls_010"><span class="cls_010">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:287.76px;top:178.72px" class="cls_010"><span class="cls_010">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:93.60px;top:212.00px" class="cls_003"><span class="cls_003">ITEM DESCRIPTION & HCPCS CODE: L1971-Ankle Foot Orthosis, plastic or other material</span></div>
<div style="position:absolute;left:93.60px;top:223.76px" class="cls_003"><span class="cls_003">with ankle joint, prefabricated</span></div>
<div style="position:absolute;left:125.04px;top:249.14px" class="cls_012"><span class="cls_012">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:392.28px;top:249.92px" class="cls_010"><span class="cls_010">Physician information</span></div>
<div style="position:absolute;left:125.04px;top:261.51px" class="cls_011"><span class="cls_011"></span><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png">Pain in right ankle and joints of right foot.</span></div>
<div style="position:absolute;left:333.36px;top:267.92px" class="cls_003"><span class="cls_003">Name: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:125.04px;top:279.08px" class="cls_010"><span class="cls_010">(M25.571)</span></div>
<div style="position:absolute;left:333.36px;top:279.80px" class="cls_003"><span class="cls_003">Phone: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:125.04px;top:287.91px" class="cls_011"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png">Pain in left ankle and joints of left foot.</span></div>
<div style="position:absolute;left:333.36px;top:291.68px" class="cls_003"><span class="cls_003">Fax: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:125.04px;top:305.48px" class="cls_010"><span class="cls_010">(M25.572)</span></div>
<div style="position:absolute;left:333.36px;top:303.56px" class="cls_003"><span class="cls_003">Address: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:125.04px;top:314.91px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"><span class="cls_010">M25.376 Other instability, unspecified foot</span></div>
<div style="position:absolute;left:333.36px;top:325.88px" class="cls_010"><span class="cls_010">Corrections:</span></div>
<div style="position:absolute;left:125.04px;top:331.95px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"><span class="cls_010">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:125.04px;top:349.47px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"><span class="cls_010">Left</span></div>
<div style="position:absolute;left:185.40px;top:349.47px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"><span class="cls_010">Right</span></div>
<div style="position:absolute;left:247.56px;top:349.47px" class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"><span class="cls_010">Bilateral</span></div>
<div style="position:absolute;left:361.32px;top:352.76px" class="cls_003"><span class="cls_003">Date: _________________</span></div>
<div style="position:absolute;left:361.32px;top:370.52px" class="cls_003"><span class="cls_003">Physician Signature:_____________</span></div>
<div style="position:absolute;left:125.04px;top:377.66px" class="cls_012"><span class="cls_012">Select indication of Need:</span></div>
<div style="position:absolute;left:362.11px;top:388.40px" class="cls_003"><span class="cls_003">NPI Number:</span></div>
<div style="position:absolute;left:125.04px;top:390.03px" class="cls_011"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png">To reduce pain by restricting mobility</span></div>
<div style="position:absolute;left:125.04px;top:407.19px" class="cls_002"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px; " src="assets/images/aurbrac/unchecked.png">To restrict and eliminate motion in an injured</span></div>
<div style="position:absolute;left:125.04px;top:424.88px" class="cls_010"><span class="cls_010">body part</span></div>
<div style="position:absolute;left:333.36px;top:431.48px" class="cls_003"><span class="cls_003">If different doctor from above print name:</span></div>
<div style="position:absolute;left:138.36px;top:435.72px" class="cls_010"><span class="cls_010">To Facilitate healing following an injury or</span></div>
<div style="position:absolute;left:125.04px;top:434.19px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px; " src="assets/images/aurbrac/unchecked.png"></div>
<div style="position:absolute;left:333.36px;top:443.36px" class="cls_003"><span class="cls_003">Name: ___________________</span></div>
<div style="position:absolute;left:125.04px;top:451.88px" class="cls_010"><span class="cls_010">surgical procedure</span></div>
<div style="position:absolute;left:333.36px;top:455.24px" class="cls_003"><span class="cls_003">NPI: ___________________</span></div>
<div style="position:absolute;left:138.36px;top:462.84px" class="cls_010"><span class="cls_010">To otherwise support weak or a deformed</span></div>
<div style="position:absolute;left:125.04px;top:461.31px" class="cls_002"><img style="width:9px; height:9px; margin-top:2px; " src="assets/images/aurbrac/unchecked.png"></div>
<div style="position:absolute;left:333.36px;top:475.70px" class="cls_008"><span class="cls_008">By signing above, I attest I am the ordering Physician or</span></div>
<div style="position:absolute;left:125.04px;top:478.88px" class="cls_010"><span class="cls_010">body part</span></div>
<div style="position:absolute;left:333.36px;top:485.06px" class="cls_008"><span class="cls_008">Practitioner of the items listed and have determined items to</span></div>
<div style="position:absolute;left:125.04px;top:489.08px" class="cls_010"><span class="cls_010">Length of need is Lifetime per Medicare</span></div>
<div style="position:absolute;left:333.36px;top:492.44px" class="cls_008"><span class="cls_008">be a medical necessity for condition described</span><span class="cls_010">.</span></div>
<div style="position:absolute;left:125.04px;top:498.80px" class="cls_010"><span class="cls_010">guidelines (unless specified) __99____ months</span></div>
<div style="position:absolute;left:175.68px;top:547.71px" class="cls_005"><span class="cls_005">Please Fax Back to: <?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:600.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
