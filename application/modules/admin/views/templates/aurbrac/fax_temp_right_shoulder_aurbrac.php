<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);


}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:17.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:17.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:17.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:14.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:12.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:12.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:1.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:9.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:9.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-size:9.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_014{font-size:9.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="img/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>
    
    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
    
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_shoulder/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:343.92px;top:68.19px" class="cls_002"><span class="cls_002">Fax: <?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:333.72px;top:86.55px" class="cls_002"><span class="cls_002">Phone: <?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:90.32px;top:139.83px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR SHOULDER IMMOBILIZER</span></div>
<div style="position:absolute;left:225.00px;top:159.15px" class="cls_003"><span class="cls_003"></span></div>
<div style="position:absolute;left:93.60px;top:262.11px" class="cls_006"><span class="cls_006">has requested an adjustable Shoulder brace for instability/painrelief.</span></div>
<div style="position:absolute;left:93.60px;top:278.31px" class="cls_006"><span class="cls_006">They indicated that you are the treating physician to complete the written</span></div>
<div style="position:absolute;left:93.60px;top:294.63px" class="cls_006"><span class="cls_006">order</span></div>
<div style="position:absolute;left:93.60px;top:340.22px" class="cls_005"><span class="cls_005">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:110.52px;top:361.59px" class="cls_005"><span class="cls_005">1. </span><span class="cls_006"> ICD-10</span></div>
<div style="position:absolute;left:110.52px;top:377.91px" class="cls_005"><span class="cls_005">2. </span><span class="cls_006"> Indication of need</span></div>
<div style="position:absolute;left:110.52px;top:394.23px" class="cls_005"><span class="cls_005">3. </span><span class="cls_006"> Please sign and date the form</span></div>
<div style="position:absolute;left:93.60px;top:418.35px" class="cls_006"><span class="cls_006">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:93.60px;top:454.10px" class="cls_007"><span class="cls_007">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:93.60px;top:464.90px" class="cls_007"><span class="cls_007">you, we will be able to ship out your patient’s SHOULDER IMMOBILIZER. If you haveany questions,</span></div>
<div style="position:absolute;left:93.60px;top:476.54px" class="cls_007"><span class="cls_007">please call us at (<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:175.44px;top:582.99px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_shoulder/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:93.60px;top:93.87px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM SHOULDER IMMOBILIZER</span></div>
<div style="position:absolute;left:93.60px;top:141.80px" class="cls_004"><span class="cls_004">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:141.80px" class="cls_004"><span class="cls_004">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:155.24px" class="cls_004"><span class="cls_004">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:168.32px" class="cls_004"><span class="cls_004">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:168.32px" class="cls_004"><span class="cls_004">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:181.52px" class="cls_004"><span class="cls_004">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:181.52px" class="cls_004"><span class="cls_004">Date:</span></div>
<div style="position:absolute;left:338.76px;top:181.52px" class="cls_004"><span class="cls_004"><?= $date ; ?></span></div>
<div style="position:absolute;left:93.60px;top:196.60px" class="cls_010"><span class="cls_010">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:189.44px;top:196.60px" class="cls_010"><span class="cls_010">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:287.68px;top:196.68px" class="cls_010"><span class="cls_010">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:93.60px;top:257.12px" class="cls_004"><span class="cls_004">ITEM DESCRIPTION & HCPCS CODE:</span><span class="cls_007">L3960</span><span class="cls_004">SHOULDER ORTHOSIS, FIGURE OF EIGHT DESIGN </span></div>
<div style="position:absolute;left:93.60px;top:269.60px" class="cls_004"><span class="cls_004">ABDUCTION RESTRAINER, CANVAS AND WEBBING, PREFABRICATED, OFF-THE-SHELF.</span></div>
<div style="position:absolute;left:386.40px;top:294.56px" class="cls_010"><span class="cls_010">Physician information</span></div>
<div style="position:absolute;left:124.80px;top:306.50px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:326.88px;top:312.56px" class="cls_004"><span class="cls_004">NAME: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:127.04px;top:324.86px" class="cls_007"><span class="cls_007"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png">M13.812- Other specified arthritis,</span></div>
<div style="position:absolute;left:326.88px;top:325.28px" class="cls_004"><span class="cls_004">PHONE: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:124.80px;top:337.82px" class="cls_007"><span class="cls_007">left shoulder</span></div>
<div style="position:absolute;left:326.88px;top:337.88px" class="cls_004"><span class="cls_004">FAX: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:124.80px;top:347.55px" class="cls_012"><span class="cls_012"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_007">Pain in Right Shoulder (M25.511)</span></div>
<div style="position:absolute;left:326.88px;top:350.48px" class="cls_004"><span class="cls_004">ADDRESS: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:122.04px;top:373.24px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:326.88px;top:373.64px" class="cls_010"><span class="cls_010">Corrections:</span></div>
<div style="position:absolute;left:123.44px;top:398.36px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png">Left</span></div>
<div style="position:absolute;left:162.24px;top:391.83px" class="cls_012"><span class="cls_012"></span></div>
<div style="position:absolute;left:175.44px;top:398.36px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png">Right</span></div>
<div style="position:absolute;left:354.81px;top:399.68px" class="cls_004"><span class="cls_004">Date: _________________</span></div>
<div style="position:absolute;left:354.81px;top:412.28px" class="cls_004"><span class="cls_004">Physician Signature: ______________</span></div>
<div style="position:absolute;left:124.80px;top:421.82px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:354.81px;top:424.88px" class="cls_004"><span class="cls_004">NPI Number:</span></div>
<div style="position:absolute;left:124.80px;top:434.07px" class="cls_012"><span class="cls_012"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:124.80px;top:451.64px" class="cls_010"><span class="cls_010">affected body part</span></div>
<div style="position:absolute;left:326.88px;top:462.80px" class="cls_004"><span class="cls_004">If different doctor from above print name:</span></div>
<div style="position:absolute;left:124.80px;top:461.07px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">To facilitate healing following an injury or</span></div>
<div style="position:absolute;left:124.80px;top:478.64px" class="cls_010"><span class="cls_010">surgical procedure</span></div>
<div style="position:absolute;left:326.88px;top:475.52px" class="cls_004"><span class="cls_004">Name: ___________________</span></div>
<div style="position:absolute;left:326.88px;top:488.00px" class="cls_004"><span class="cls_004">NPI: ___________________</span></div>
<div style="position:absolute;left:124.80px;top:487.95px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">To apply traction for either correction or</span></div>
<div style="position:absolute;left:124.80px;top:505.76px" class="cls_010"><span class="cls_010">prevention contractures</span></div>
<div style="position:absolute;left:326.88px;top:509.42px" class="cls_011"><span class="cls_011">By signing above, I attest I am the ordering Physician or</span></div>
<div style="position:absolute;left:326.88px;top:519.38px" class="cls_011"><span class="cls_011">Practitioner of the items listed and have determined </span></div>
<div style="position:absolute;left:124.80px;top:515.07px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">To support weak or deformed body part</span></div>
<div style="position:absolute;left:326.88px;top:527.36px" class="cls_011"><span class="cls_011">items to be a medical necessity for condition described</span><span class="cls_010">.</span></div>
<div style="position:absolute;left:124.80px;top:533.12px" class="cls_010"><span class="cls_010">Length of need is Lifetime per Medicare</span></div>
<div style="position:absolute;left:124.80px;top:542.96px" class="cls_010"><span class="cls_010">guidelines (unless specified) ____</span><span class="cls_014">99</span><span class="cls_010">__ months</span></div>
<div style="position:absolute;left:175.44px;top:620.31px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>
</div>
<?php
 } 
?>
</body>
</html>
