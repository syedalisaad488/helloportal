<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="img/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>

    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_wrist/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:345.48px;top:70.83px" class="cls_002"><span class="cls_002">Fax :<?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:334.80px;top:89.19px" class="cls_002"><span class="cls_002">Phone :<?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:72.00px;top:144.75px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR WRISTHAND ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:252.51px" class="cls_006"><span class="cls_006">has requested that we send them an adjustable Wrist Brace for</span></div>
<div style="position:absolute;left:72.00px;top:269.91px" class="cls_006"><span class="cls_006">instability/Pain relief.  They indicated that you are the treating physician to</span></div>
<div style="position:absolute;left:72.00px;top:287.31px" class="cls_006"><span class="cls_006">complete the written order.</span></div>
<div style="position:absolute;left:72.00px;top:335.78px" class="cls_005"><span class="cls_005">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:358.47px" class="cls_005"><span class="cls_005">1. </span><span class="cls_006"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:375.75px" class="cls_005"><span class="cls_005">2. </span><span class="cls_006"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:393.15px" class="cls_005"><span class="cls_005">3. </span><span class="cls_006"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:418.59px" class="cls_006"><span class="cls_006">Please Include any chart notes to support medical necessity.</span></div>
<div style="position:absolute;left:72.00px;top:443.91px" class="cls_006"><span class="cls_006">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:495.14px" class="cls_007"><span class="cls_007">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:506.66px" class="cls_007"><span class="cls_007">you, we will be able to ship out your patient’s Wrist Orthosis. If you haveany questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:518.06px" class="cls_007"><span class="cls_007">(</span><span class="cls_004"><?= $key->aurbrac_call_back_num ; ?></span><span class="cls_007">).</span></div>
<div style="position:absolute;left:169.08px;top:632.43px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_wrist/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:72.00px;top:70.47px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR WRISTHAND ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:122.78px" class="cls_004"><span class="cls_004">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:122.78px" class="cls_004"><span class="cls_004">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:137.18px" class="cls_004"><span class="cls_004">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:151.10px" class="cls_004"><span class="cls_004">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:151.10px" class="cls_004"><span class="cls_004">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:165.02px" class="cls_004"><span class="cls_004">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:165.02px" class="cls_004"><span class="cls_004">Date:</span></div>
<div style="position:absolute;left:332.76px;top:165.02px" class="cls_004"><span class="cls_004"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:189.26px" class="cls_009"><span class="cls_009">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:173.94px;top:189.26px" class="cls_009"><span class="cls_009">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.42px;top:189.26px" class="cls_009"><span class="cls_009">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:240.90px" class="cls_007"><span class="cls_007">ITEM DESCRIPTION & HCPCS CODE: L3916 wrist, hand orthosis, includes one or more non torsion</span></div>
<div style="position:absolute;left:72.00px;top:251.42px" class="cls_007"><span class="cls_007">joint(s), elastic bands, turnbuckles, may include soft interface, straps, prefabricated, off the shelf</span></div>
<div style="position:absolute;left:397.32px;top:284.06px" class="cls_009"><span class="cls_009">Physician information</span></div>
<div style="position:absolute;left:106.56px;top:286.10px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:106.56px;top:299.19px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">M19.042 - Primary Osteoarthritis, Left Hand</span></div>
<div style="position:absolute;left:321.72px;top:303.14px" class="cls_004"><span class="cls_004">Name: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:321.72px;top:315.86px" class="cls_004"><span class="cls_004">Phone: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:106.56px;top:317.43px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">M70.98- Unspecified soft tissue disorder</span></div>
<div style="position:absolute;left:321.72px;top:328.46px" class="cls_004"><span class="cls_004">Fax: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:106.56px;top:336.38px" class="cls_009"><span class="cls_009">related to use, overuse and pressure other</span></div>
<div style="position:absolute;left:321.72px;top:341.18px" class="cls_004"><span class="cls_004">Address: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:106.56px;top:346.11px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">M79.9- Soft tissue disorder, unspecified</span></div>
<div style="position:absolute;left:106.56px;top:364.59px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:321.72px;top:376.94px" class="cls_009"><span class="cls_009">Corrections:</span></div>
<div style="position:absolute;left:106.56px;top:382.95px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">Left</span></div>
<div style="position:absolute;left:150.96px;top:382.95px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Right</span><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">Bilateral</span></div>
<div style="position:absolute;left:106.56px;top:412.46px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:357.75px;top:413.78px" class="cls_004"><span class="cls_004">Date: _________________</span></div>
<div style="position:absolute;left:357.75px;top:426.50px" class="cls_004"><span class="cls_004">Physician Signature: _______________</span></div>
<div style="position:absolute;left:106.56px;top:425.55px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:357.75px;top:439.10px" class="cls_004"><span class="cls_004">NPI Number:</span></div>
<div style="position:absolute;left:106.56px;top:444.38px" class="cls_009"><span class="cls_009">affected body part</span></div>
<div style="position:absolute;left:106.56px;top:454.11px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To facilitate healing following an injury or</span></div>
<div style="position:absolute;left:106.56px;top:473.06px" class="cls_009"><span class="cls_009">surgical procedure</span></div>
<div style="position:absolute;left:321.72px;top:477.14px" class="cls_004"><span class="cls_004">If different doctor from above print name:</span></div>
<div style="position:absolute;left:106.56px;top:482.79px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To apply traction for either correction or</span></div>
<div style="position:absolute;left:321.72px;top:495.86px" class="cls_004"><span class="cls_004">Name: ___________________</span></div>
<div style="position:absolute;left:106.56px;top:501.86px" class="cls_009"><span class="cls_009">prevention contractures</span></div>
<div style="position:absolute;left:106.56px;top:511.59px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To support weak or deformed body part</span></div>
<div style="position:absolute;left:321.72px;top:514.46px" class="cls_004"><span class="cls_004">NPI: ___________________</span></div>
<div style="position:absolute;left:321.72px;top:533.30px" class="cls_012"><span class="cls_012">By signing above, I attest I am the ordering Physician or Practitioner</span></div>
<div style="position:absolute;left:106.56px;top:537.02px" class="cls_009"><span class="cls_009">Length of need is Lifetime per Medicare</span></div>
<div style="position:absolute;left:321.72px;top:543.14px" class="cls_012"><span class="cls_012">of the items listed and have determined items to be a medical</span></div>
<div style="position:absolute;left:106.56px;top:547.46px" class="cls_009"><span class="cls_009">guidelines (unless specified) ________ months</span></div>
<div style="position:absolute;left:321.72px;top:553.10px" class="cls_012"><span class="cls_012">necessity for condition described</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:169.08px;top:632.51px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
