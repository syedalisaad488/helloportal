<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_014{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="9b882f66-e303-11ea-8b25-0cc47a792c0a_id_9b882f66-e303-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>    
    
    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_wrist/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:346.68px;top:72.27px" class="cls_002"><span class="cls_002">Fax:<?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:335.88px;top:91.83px" class="cls_002"><span class="cls_002">Phone:<?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:72.00px;top:148.59px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR WRISTHAND ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:258.87px" class="cls_005"><span class="cls_005">has requested that we send them an adjustable Wrist Brace for</span></div>
<div style="position:absolute;left:72.00px;top:276.27px" class="cls_005"><span class="cls_005">instability/Pain relief.  They indicated that you are the treating physician to</span></div>
<div style="position:absolute;left:72.00px;top:293.55px" class="cls_005"><span class="cls_005">complete the written order.</span></div>
<div style="position:absolute;left:72.00px;top:342.14px" class="cls_004"><span class="cls_004">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:364.71px" class="cls_004"><span class="cls_004">1. </span><span class="cls_005"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:382.11px" class="cls_004"><span class="cls_004">2. </span><span class="cls_005"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:399.51px" class="cls_004"><span class="cls_004">3. </span><span class="cls_005"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:425.31px" class="cls_005"><span class="cls_005">Please Include any chart notes to support medical necessity.</span></div>
<div style="position:absolute;left:72.00px;top:451.83px" class="cls_005"><span class="cls_005">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:504.62px" class="cls_006"><span class="cls_006">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:516.14px" class="cls_006"><span class="cls_006">you, we will be able to ship out your patient’s Wrist Orthosis. If you haveany questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:528.50px" class="cls_006"><span class="cls_006">(<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:177.72px;top:623.07px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_wrist/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:72.00px;top:99.75px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR WRISTHAND ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:142.34px" class="cls_008"><span class="cls_008">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:142.34px" class="cls_008"><span class="cls_008">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:156.62px" class="cls_008"><span class="cls_008">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:170.54px" class="cls_008"><span class="cls_008">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:170.54px" class="cls_008"><span class="cls_008">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:184.58px" class="cls_008"><span class="cls_008">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:184.58px" class="cls_008"><span class="cls_008">Date:</span></div>
<div style="position:absolute;left:332.76px;top:184.58px" class="cls_008"><span class="cls_008"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:198.38px" class="cls_009"><span class="cls_009">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:174.00px;top:198.38px" class="cls_009"><span class="cls_009">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.52px;top:198.38px" class="cls_009"><span class="cls_009">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:249.98px" class="cls_008"><span class="cls_008">ITEM DESCRIPTION & HCPCS CODE:L3916 wrist, hand orthosis, includes one or more non torsion joint(s),</span></div>
<div style="position:absolute;left:72.00px;top:263.42px" class="cls_008"><span class="cls_008">elastic bands, turnbuckles, may include soft interface, straps, prefabricated, off the shelf</span></div>
<div style="position:absolute;left:398.04px;top:294.98px" class="cls_009"><span class="cls_009">Physician information</span></div>
<div style="position:absolute;left:106.56px;top:294.62px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:106.56px;top:308.19px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_006">Primary Osteoarthritis, Right Hand</span></div>
<div style="position:absolute;left:321.72px;top:314.06px" class="cls_006"><span class="cls_006">NAME: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:106.56px;top:327.74px" class="cls_006"><span class="cls_006">(M19.041)</span></div>
<div style="position:absolute;left:321.72px;top:325.46px" class="cls_006"><span class="cls_006">PHONE: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:321.72px;top:336.98px" class="cls_006"><span class="cls_006">FAX: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:106.56px;top:338.67px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">M70.98-</span><span class="cls_006"> Unspecified soft tissue</span></div>
<div style="position:absolute;left:321.72px;top:348.50px" class="cls_006"><span class="cls_006">ADDRESS: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:106.56px;top:358.34px" class="cls_006"><span class="cls_006">disorder related to use, overuse and</span></div>
<div style="position:absolute;left:106.56px;top:370.46px" class="cls_006"><span class="cls_006">pressure other</span></div>
<div style="position:absolute;left:321.72px;top:371.30px" class="cls_009"><span class="cls_009">Corrections:</span></div>
<div style="position:absolute;left:106.56px;top:381.39px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">M79.9- </span><span class="cls_006">Soft tissue disorder,</span></div>
<div style="position:absolute;left:106.56px;top:401.06px" class="cls_006"><span class="cls_006">unspecified</span></div>
<div style="position:absolute;left:106.56px;top:411.87px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:106.56px;top:430.35px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Left</span></div>
<div style="position:absolute;left:146.40px;top:430.35px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">Right</span><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">Bilateral</span></div>
<div style="position:absolute;left:351.57px;top:435.86px" class="cls_008"><span class="cls_008">Date: _________________</span></div>
<div style="position:absolute;left:351.72px;top:449.30px" class="cls_008"><span class="cls_008">Physician Signature: _________________</span></div>
<div style="position:absolute;left:106.56px;top:460.82px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:351.72px;top:462.74px" class="cls_008"><span class="cls_008">NPI Number:</span></div>
<div style="position:absolute;left:106.56px;top:474.39px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:106.56px;top:493.22px" class="cls_009"><span class="cls_009">affected body part</span></div>
<div style="position:absolute;left:321.72px;top:502.94px" class="cls_008"><span class="cls_008">If different doctor from above print name:</span></div>
<div style="position:absolute;left:106.56px;top:502.95px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To facilitate healing following an injury or</span></div>
<div style="position:absolute;left:321.72px;top:516.38px" class="cls_008"><span class="cls_008">Name: ___________________</span></div>
<div style="position:absolute;left:106.56px;top:521.90px" class="cls_009"><span class="cls_009">surgical procedure</span></div>
<div style="position:absolute;left:321.72px;top:529.82px" class="cls_008"><span class="cls_008">NPI: ___________________</span></div>
<div style="position:absolute;left:106.56px;top:531.63px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To apply traction for either correction or</span></div>
<div style="position:absolute;left:106.56px;top:550.70px" class="cls_009"><span class="cls_009">prevention contractures</span></div>
<div style="position:absolute;left:321.72px;top:552.62px" class="cls_012"><span class="cls_012">By signing above, I attest I am the ordering Physician or Practitioner of the</span></div>
<div style="position:absolute;left:321.72px;top:563.06px" class="cls_012"><span class="cls_012">items listed and have determined items to be a medical necessity for</span></div>
<div style="position:absolute;left:106.56px;top:560.43px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To support weak or deformed body part</span></div>
<div style="position:absolute;left:321.72px;top:571.58px" class="cls_012"><span class="cls_012">condition described</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:106.56px;top:579.98px" class="cls_009"><span class="cls_009">Length of need is Lifetime per Medicare guidelines</span></div>
<div style="position:absolute;left:106.56px;top:591.02px" class="cls_009"><span class="cls_009">(unless specified) ___</span><span class="cls_014">99</span><span class="cls_009">_____ months</span></div>
<div style="position:absolute;left:167.28px;top:630.47px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
