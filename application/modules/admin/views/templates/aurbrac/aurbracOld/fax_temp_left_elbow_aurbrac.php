<?php
$date = date("Y/m/d");
foreach($fax as $key){  ?>
<style>
	table {border-style: ridge;
		
	}

	td{
		margin-bottom:-10px;
		font-size:12px;
	}
	p{
		font-size:12px;
	}
	h3{
		font-size:16px;
	}
		.square {
		    height:13px;
		    width: 11px;
		    border: 1px solid black;
		    display: inline-block;
		}
</style>

<html>
<p><strong>PRIOR AUTHORIZATION PRESCRIPTION REQUEST FORM FOR ELBOW ORTHOSIS</strong></p>
<p>Please Send RX Form Pertinent Chart Notes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax No: <?=$this->session->userdata('receive_num'); ?></p>
<p><strong>PLEASE SEND THIS FORM BACK IN 3 BUSINESS DAYS</strong></p>
<table>
	<tbody>
	<tr>
		<td width='150'>
			Date:<?php echo $date; ?> 
		</td>
		<td width='150'>
		</td>
		<td width='150'>
		</td>
	</tr>
	<tr>
		<td width='150'>
			First: <?= $key->aurbrac_firstname; ?>
		</td>
		<td width='150'>
			Last: <?= $key->aurbrac_lastname; ?>
		</td>
		<td width='150'>
			Physician Name: <?= $key->aurbrac_doc_firstname." ".$key->aurbrac_doc_lastname; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			DOB: <?= $key->aurbrac_dob; ?>
		</td>
		<td width='150'>
			Sex: <?= $key->aurbrac_gender; ?>
		</td>
		<td width='150'>
			NPI: <?= $key->aurbrac_doc_npi; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Address: <?= $key->aurbrac_address." ".$key->aurbrac_city." ".$key->aurbrac_state." ".$key->aurbrac_zip; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Address: <?= $key->aurbrac_doc_address; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			City: <?= $key->aurbrac_city; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			City: <?= $key->aurbrac_doc_city; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			State: <?= $key->aurbrac_state; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			State: <?= $key->aurbrac_doc_state; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Postal Code: <?= $key->aurbrac_zip ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Postal code: <?= $key->aurbrac_doc_zip; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Patient Phone Number: <?= $key->aurbrac_phone; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Phone Number: <?= $key->aurbrac_doc_phone; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Primary Ins: <?= $key->aurbrac_med_id; ?>
		</td>
		<td width='150'>
			Policy #:
		</td>
		<td width='150'>
			Fax Number: <?= $key->aurbrac_doc_fax; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Private Ins:
		</td>
		<td width='150'>
			Policy #:
		</td>
		<td width='150'>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Height:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Weight:
		</td>
		<td width='150'>
			Waist:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shoe Size:
		</td>
		<td width='150'>
		</td>
	</tr>
	</tbody>
</table>
<p><strong>This patient is being treated under a comprehensive plan of care for Elbow pain.</strong></p>
<p><strong>I, the undersigned; certify that the prescribed orthosis is medically necessary for the patient&rsquo;s overall well-being. This patient suffered injury, has undergone surgery, or has arthritis. In my opinion, the following elbow orthosis products are both reasonable and necessary in reference to treatment of the patient&rsquo;s condition and/or rehabilitation. My patient has been in my care regarding the diagnosis below. This is the treatment I see fit for this patient at this time. I certify that this information is true &amp; correct.</strong></p>
<h3><strong><u>DIAGNOSIS</u></strong><strong>:Please select the patient&rsquo;s diagnosis</strong>.(&uuml;)</h3>
<p><span class="square"></span> Cubital Tunnel Syndrome (G56.2)</p>
<p><span class="square"></span> Rheumatoid Bursitis, left elbow (M06.222)</p>
<p><span class="square"></span> Pain in left elbow (M25.522)</p>
<p><span class="square"></span> Unspecified sprain of left elbow (S53.402)</p>
<p><span class="square"></span> Disorder of ligament, left elbow (M24.222)</p>
<p><span class="square"></span> Other /Explain (Includecode): <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></p>
<p><strong><u>&nbsp;</u></strong></p>
<p><strong><u>AFFECTEDAREA </u></strong><strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ELBOW:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Left </p>
<p><strong><em>Our evaluation of the above patient has determined that providing the following Elbow orthosis product will benefit this patient:</em></strong></p>
<p><strong><u>DISPENSE </u></strong><strong>:</strong><strong>L3761</strong> - Elbow orthosis (eo), with adjustable position locking joint(s), prefabricated,off-the-shelf</p>
<p>Estimated length of need (#ofmonths):<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>6 - 99 (99=LIFETIME)</p>
<p><strong>Physician Signature:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>Datesigned:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></strong></p>
<p><strong>Physician Name:<?= $key->aurbrac_doc_firstname." ".$key->aurbrac_doc_lastname; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NPI: <?= $key->aurbrac_doc_npi; ?></strong></p>
<?php } ?>
