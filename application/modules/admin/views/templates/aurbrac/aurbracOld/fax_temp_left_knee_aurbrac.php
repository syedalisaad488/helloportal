<?php
$date = date("Y/m/d");
foreach($fax as $key){  ?>
	<style>
		table {border-style: ridge;
	
		}

		td{
			margin-bottom:-10px;
			font-size:12px;
		}
		p{
			font-size:12px;
		}
		h3{
			font-size:16px;
		}
			.square {
		    height:13px;
		    width: 11px;
		    border: 1px solid black;
		    display: inline-block;
		}
	</style>

	<html>
<p><strong>PRIOR AUTHORIZATION PRESCRIPTION REQUEST FORM FOR KNEE ORTHOSIS</strong></p>
<p>Please Send RX Form Pertinent Chart Notes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax No: <?=$this->session->userdata('receive_num'); ?></p>
<p><strong>PLEASE SEND THIS FORM BACK IN 3 BUSINESS DAYS</strong></p>
<table>
	<tbody>
	<tr>
		<td width='150'>
			Date: <?=$date; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
		</td>
	</tr>
	<tr>
		<td width='150'>
			First: <?= $key->aurbrac_firstname; ?>
		</td>
		<td width='150'>
			Last: <?= $key->aurbrac_lastname; ?>
		</td>
		<td width='150'>
			Physician Name: <?= $key->aurbrac_doc_firstname." ".$key->aurbrac_doc_lastname; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			DOB: <?= $key->aurbrac_dob; ?>
		</td>
		<td width='150'>
			Sex: <?= $key->aurbrac_gender; ?>
		</td>
		<td width='150'>
			NPI: <?= $key->aurbrac_doc_npi; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Address: <?= $key->aurbrac_address." ".$key->aurbrac_city." ".$key->aurbrac_state." ".$key->aurbrac_zip; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Address: <?= $key->aurbrac_doc_address; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			City: <?= $key->aurbrac_city; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			City: <?= $key->aurbrac_doc_city; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			State: <?= $key->aurbrac_state; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			State: <?= $key->aurbrac_doc_state; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Postal Code: <?= $key->aurbrac_zip ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Postal code: <?= $key->aurbrac_doc_zip; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Patient Phone Number: <?= $key->aurbrac_phone; ?>
		</td>
		<td width='150'>
		</td>
		<td width='150'>
			Phone Number: <?= $key->aurbrac_doc_phone; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Primary Ins: <?= $key->aurbrac_med_id; ?>
		</td>
		<td width='150'>
			Policy #:
		</td>
		<td width='150'>
			Fax Number: <?= $key->aurbrac_doc_fax; ?>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Private Ins:
		</td>
		<td width='150'>
			Policy #:
		</td>
		<td width='150'>
		</td>
	</tr>
	<tr>
		<td width='150'>
			Height:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Weight:
		</td>
		<td width='150'>
			Waist:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shoe Size:
		</td>
		<td width='150'>
		</td>
	</tr>
	</tbody>
</table>
<p><strong>This patient is being treated under a comprehensive plan of care for knee pain.</strong></p>
<p><strong>I, the undersigned; certify that the prescribed orthosis is medically necessary for the patient&rsquo;s overall well-being. This patient has suffered an injury or undergone knee surgery. In my opinion, the following knee orthosis products are both reasonable and necessary in reference to treatment of the patient&rsquo;s condition and/or rehabilitation. My patient has been in my care regarding the diagnosis below. This is the treatment I see fit for this patient at this time. I certify that this information is true and correct.</strong></p>
<h3><strong><u>&nbsp;</u></strong></h3>
<h3><strong><u>DIAGNOSIS </u>:</strong><strong>Please select the patient&rsquo;</strong><strong>s diagnosis</strong>.(&uuml;)</h3>
<p><span class="square"></span> Rheumatoid Arthritis without rheumatoid factor, left knee (M06.062)</p>
<p><span class="square"></span> Unilateral Primary Osteoarthritis, left knee (M17.12)</p>
<p><span class="square"></span> Bilateral Primary Osteoarthritis (M17.0)</p>
<p><span class="square"></span> Chronic instability of knee, left knee (M23.52)</p>
<p><span class="square"></span> Other/Explain: <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></p>
<p><strong><u>AFFECTED AREA </u></strong><strong>:KNEE:&nbsp; Left Knee</strong></p>
<p><strong><em>Our evaluation of the above patient has determined that providing the following knee orthosis product will benefit this patient:</em></strong></p>
<p><strong><u>DISPENSE </u></strong><strong>:L1851:</strong> Adjustable Flexion &amp; Extension Joint (Unicentric or Polycentric), Medial-Lateral &amp; Rotation Control, +/-Varus/Valgus Adjustment, Prefabricated, Off-The Shelf; includes L2397Suspension Sleeve.</p>
<p>Estimated length of need (#ofmonths):<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>6 - 99 (99= LIFETIME)</p>
<p><strong>Physician Signature:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>Datesigned:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></strong></p>
<p><strong>Physician Name:<?= $key->aurbrac_doc_firstname." ".$key->aurbrac_doc_lastname; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NPI: <?= $key->aurbrac_doc_npi; ?></strong></p>
<?php } ?>
