<?php 
 $date = date("Y/m/d");
foreach($fax as $key){  ?>
   
   <html>
<p><strong>PRIOR AUTHORIZATION PRESCRIPTION REQUEST FORM FOR WRIST ORTHOSIS</strong></p>
<p>Please Send RXForm Pertinent ChartNotes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax No: (111-222-3333)</p>
<p><strong>PLEASE SEND THIS FORM BACK IN 3 BUSINESS DAYS</strong></p>
<table>
<tbody>
<tr>
<td width='166'>
<p>Date:$date</p>
</td>
<td width='195'>
</td>
<td width='336'>
</td>
</tr>
<tr>
<td width='166'>
<p>First: <?= $key->aurbrac_firstname; ?></p>
</td>
<td width='195'>
<p>Last: <?= $key->aurbrac_lastname; ?></p>
</td>
<td width='336'>
<p>Physician Name: <?= $key->aurbrac_doc_firstname; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>DOB: <?= $key->aurbrac_dob; ?></p>
</td>
<td width='195'>
<p>Sex: <?= $key->aurbrac_gender; ?></p>
</td>
<td width='336'>
<p>NPI: <?= $key->aurbrac_doc_npi; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>Address: <?= $key->aurbrac_address; ?></p>
</td>
<td width='195'>
</td>
<td width='336'>
<p>Address: <?= $key->aurbrac_doc_address; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>City: <?= $key->aurbrac_city; ?></p>
</td>
<td width='195'>
</td>
<td width='336'>
<p>City: <?= $key->aurbrac_doc_city; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>State: <?= $key->aurbrac_state; ?></p>
</td>
<td width='195'>
</td>
<td width='336'>
<p>State: <?= $key->aurbrac_doc_state; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>Postal Code: <?= $key->aurbrac_zip ?></p>
</td>
<td width='195'>
</td>
<td width='336'>
<p>Postal code: <?= $key->aurbrac_doc_zip; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>Patient Phone Number: <?= $key->aurbrac_phone; ?></p>
</td>
<td width='195'>
</td>
<td width='336'>
<p>Phone Number: <?= $key->aurbrac_doc_phone; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>Primary Ins:</p>
</td>
<td width='195'>
<p>Policy #:</p>
</td>
<td width='336'>
<p>Fax Number: <?= $key->aurbrac_doc_fax; ?></p>
</td>
</tr>
<tr>
<td width='166'>
<p>Private Ins:</p>
</td>
<td width='195'>
<p>Policy #:</p>
</td>
<td width='336'>
</td>
</tr>
<tr>
<td width='166'>
<p>Height:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Weight:</p>
</td>
<td width='195'>
<p>Waist:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Shoe Size:</p>
</td>
<td width='336'>
</td>
</tr>
</tbody>
</table>
<p><strong>This patient is being treated under a comprehensive plan of care for Elbow pain.</strong></p>
<p><strong>I, the undersigned; certify that the prescribed orthosis is medically necessary for the patient&rsquo;s overall well-being. This patient suffered injury, has undergone surgery, or has arthritis. In my opinion, the following elbow orthosis products are both reasonable and necessary in reference to treatment of the patient&rsquo;s condition and/or rehabilitation. My patient has been in my care regarding the diagnosis below. This is the treatment I see fit for this patient at this time. I certify that this information is true &amp; correct.</strong></p>
<h3><strong><u>DIAGNOSIS</u></strong><strong>:Please select the patient&rsquo;s diagnosis</strong>.(&uuml;)</h3>
<p>rCubital Tunnel Syndrome (G56.2)</p>
<p>rRheumatoid Bursitis, left elbow (M06.222)</p>
<p>rPain in left elbow (M25.522)</p>
<p>rUnspecified sprain of left elbow (S53.402)</p>
<p>rDisorder of ligament, left elbow (M24.222)</p>
<p>rOther /Explain (Includecode):<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></p>
<p><strong><u>&nbsp;</u></strong></p>
<p><strong><u>AFFECTEDAREA</u></strong><strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ELBOW:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Left &thorn;</p>
<p><strong><em>Our evaluation of the above patient has determined that providing the following Elbow orthosis product will benefit this patient:</em></strong></p>
<p><strong><u>DISPENSE</u></strong><strong>:</strong><strong>L3761</strong> - Elbow orthosis (eo), with adjustable position locking joint(s), prefabricated,off-the-shelf</p>
<p>Estimated length of need (#ofmonths):<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>6 - 99 (99=LIFETIME)</p>
<p><strong>PhysicianSignature:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u>Datesigned:<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></strong></p>
<p><strong>PhysicianName:<?= $key->aurbrac_doc_firstname." ".$key->aurbrac_doc_firstname; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NPI: <?= $key->aurbrac_doc_npi; ?></strong></p>
<?php } ?>