<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_014{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_015{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="b1079eea-e302-11ea-8b25-0cc47a792c0a_id_b1079eea-e302-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>

    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_hip/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:347.76px;top:70.83px" class="cls_002"><span class="cls_002">Fax:<?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:336.96px;top:89.19px" class="cls_002"><span class="cls_002">Phone:<?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:114.48px;top:144.75px" class="cls_004"><span class="cls_004">DOCTOR ORDER FORM FOR HIP ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:257.55px" class="cls_006"><span class="cls_006">has requested an adjustable Hip  Brace for instability/pain relief.  They</span></div>
<div style="position:absolute;left:72.00px;top:274.83px" class="cls_006"><span class="cls_006">indicated that you are the treating physicianto complete the written order.</span></div>
<div style="position:absolute;left:82.20px;top:300.27px" class="cls_013"><span class="cls_013">Please return with a copy of the patient’s medical record associated with</span></div>
<div style="position:absolute;left:235.92px;top:317.55px" class="cls_013"><span class="cls_013">the requested product.</span></div>
<div style="position:absolute;left:72.00px;top:366.14px" class="cls_005"><span class="cls_005">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:388.71px" class="cls_005"><span class="cls_005">1. </span><span class="cls_006"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:406.11px" class="cls_005"><span class="cls_005">2. </span><span class="cls_006"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:423.51px" class="cls_005"><span class="cls_005">3. </span><span class="cls_006"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:448.83px" class="cls_006"><span class="cls_006">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:474.74px" class="cls_007"><span class="cls_007">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:486.26px" class="cls_007"><span class="cls_007">you, we will be able to ship out patient’s HIP ORTHOSIS. If you have any questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:497.78px" class="cls_007"><span class="cls_007">(<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:169.08px;top:597.03px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_hip/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:114.48px;top:99.75px" class="cls_004"><span class="cls_004">DOCTOR ORDER FORM FOR HIP ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:133.70px" class="cls_003"><span class="cls_003">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:133.70px" class="cls_003"><span class="cls_003">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:147.98px" class="cls_003"><span class="cls_003">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:161.90px" class="cls_003"><span class="cls_003">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:161.90px" class="cls_003"><span class="cls_003">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:175.94px" class="cls_003"><span class="cls_003">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:175.94px" class="cls_003"><span class="cls_003">Date:</span></div>
<div style="position:absolute;left:332.76px;top:175.94px" class="cls_003"><span class="cls_003"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:189.74px" class="cls_009"><span class="cls_009">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:174.00px;top:189.74px" class="cls_009"><span class="cls_009">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.52px;top:189.74px" class="cls_009"><span class="cls_009">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:248.54px" class="cls_007"><span class="cls_007">L1686/L2624 Hip orthosis, abduction control of hip joint, postoperative hip abduction type,</span></div>
<div style="position:absolute;left:72.00px;top:260.06px" class="cls_007"><span class="cls_007">prefabricated, includes fitting and adjustment. Addition to lower extremity, pelvic control, hip joint</span></div>
<div style="position:absolute;left:72.00px;top:271.58px" class="cls_007"><span class="cls_007">, adjustable flexion, extension, abduction control, each</span></div>
<div style="position:absolute;left:107.40px;top:288.26px" class="cls_014"><span class="cls_014">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:383.16px;top:290.54px" class="cls_009"><span class="cls_009">Physician information</span></div>
<div style="position:absolute;left:107.40px;top:301.35px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_005"> M16.11 Unilateral primary</span></div>
<div style="position:absolute;left:320.40px;top:309.74px" class="cls_003"><span class="cls_003">Name: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:107.40px;top:320.18px" class="cls_005"><span class="cls_005">osteoarthritis right hip</span></div>
<div style="position:absolute;left:320.40px;top:322.34px" class="cls_003"><span class="cls_003">Phone: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:320.40px;top:335.06px" class="cls_003"><span class="cls_003">Fax: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:107.40px;top:334.23px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_005">M16.12 Unilateral primary</span></div>
<div style="position:absolute;left:320.40px;top:347.66px" class="cls_003"><span class="cls_003">Address: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:107.40px;top:353.30px" class="cls_005"><span class="cls_005">osteoarthritis Left hip</span></div>
<div style="position:absolute;left:107.40px;top:367.35px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:320.40px;top:373.10px" class="cls_009"><span class="cls_009">Corrections:</span></div>
<div style="position:absolute;left:107.40px;top:385.71px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">Left</span></div>
<div style="position:absolute;left:171.60px;top:385.71px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_002"> </span><span class="cls_009">Right</span></div>
<div style="position:absolute;left:237.96px;top:385.71px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">Bilateral</span></div>
<div style="position:absolute;left:107.40px;top:425.30px" class="cls_014"><span class="cls_014">Select indication of Need:</span></div>
<div style="position:absolute;left:356.40px;top:437.54px" class="cls_007"><span class="cls_007">Date: _________________</span></div>
<div style="position:absolute;left:107.40px;top:438.39px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span></div>
<div style="position:absolute;left:126.12px;top:440.34px" class="cls_009"><span class="cls_009">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:107.40px;top:450.27px" class="cls_009"><span class="cls_009">trunk</span></div>
<div style="position:absolute;left:356.39px;top:455.06px" class="cls_007"><span class="cls_007">Physician Signature: ______________</span></div>
<div style="position:absolute;left:107.40px;top:466.47px" class="cls_010"><span class="cls_010"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_002"> </span><span class="cls_009">To facilitate healing following an injury to the</span></div>
<div style="position:absolute;left:356.39px;top:472.58px" class="cls_007"><span class="cls_007">NPI Number:</span></div>
<div style="position:absolute;left:107.40px;top:485.30px" class="cls_009"><span class="cls_009">spine or related tissues; or</span></div>
<div style="position:absolute;left:121.44px;top:495.98px" class="cls_009"><span class="cls_009">To Facilitate healing following a surgical</span></div>
<div style="position:absolute;left:107.40px;top:495.03px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span></div>
<div style="position:absolute;left:320.40px;top:502.70px" class="cls_003"><span class="cls_003">If different doctor from above print name:</span></div>
<div style="position:absolute;left:107.40px;top:514.10px" class="cls_009"><span class="cls_009">procedure to the spine or related soft tissues; or</span></div>
<div style="position:absolute;left:320.40px;top:521.30px" class="cls_003"><span class="cls_003">Name: ___________________</span></div>
<div style="position:absolute;left:107.40px;top:530.90px" class="cls_009"><span class="cls_009">Length of need is Lifetime = 99 months, per</span></div>
<div style="position:absolute;left:320.40px;top:540.02px" class="cls_003"><span class="cls_003">NPI: __________________</span></div>
<div style="position:absolute;left:107.40px;top:541.22px" class="cls_009"><span class="cls_009">Medicare guidelines, unless otherwise specified -</span></div>
<div style="position:absolute;left:107.40px;top:551.42px" class="cls_009"><span class="cls_009">____</span><span class="cls_015">99</span><span class="cls_009">____ months</span></div>
<div style="position:absolute;left:320.40px;top:558.74px" class="cls_011"><span class="cls_011">By signing above, I attest I am the ordering Physician or</span></div>
<div style="position:absolute;left:320.40px;top:567.98px" class="cls_011"><span class="cls_011">Practitioner of the items listed and have determined items to</span></div>
<div style="position:absolute;left:320.40px;top:577.22px" class="cls_011"><span class="cls_011">be a medical necessity for condition described</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:334.80px;top:596.54px" class="cls_011"><span class="cls_011">This is a certifying statement of medical necessity</span></div>
<div style="position:absolute;left:178.08px;top:634.23px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
