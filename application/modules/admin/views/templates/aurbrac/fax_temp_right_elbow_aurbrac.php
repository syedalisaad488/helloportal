<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_014{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="7958b894-e302-11ea-8b25-0cc47a792c0a_id_7958b894-e302-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>
    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_elbow/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:354.72px;top:72.27px" class="cls_002"><span class="cls_002">Fax:</span><span class="cls_003"><?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:343.92px;top:91.83px" class="cls_002"><span class="cls_002">Phone:</span><span class="cls_003"><?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:72.00px;top:148.59px" class="cls_004"><span class="cls_004">DOCTOR ORDER FORM FOR ELBOW ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:235.95px" class="cls_003"><span class="cls_003">has requested that we send them an adjustable ELBOW Brace for</span></div>
<div style="position:absolute;left:72.00px;top:253.35px" class="cls_003"><span class="cls_003">instability/Pain relief.  They indicated that you are the treating physician to</span></div>
<div style="position:absolute;left:72.00px;top:270.75px" class="cls_003"><span class="cls_003">complete the written order.</span></div>
<div style="position:absolute;left:72.00px;top:319.22px" class="cls_005"><span class="cls_005">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:341.91px" class="cls_005"><span class="cls_005">1. </span><span class="cls_003"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:359.19px" class="cls_005"><span class="cls_005">2. </span><span class="cls_003"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:376.59px" class="cls_005"><span class="cls_005">3. </span><span class="cls_003"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:402.51px" class="cls_003"><span class="cls_003">Please Include any chart notes to support medical necessity.</span></div>
<div style="position:absolute;left:72.00px;top:428.91px" class="cls_003"><span class="cls_003">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:481.82px" class="cls_006"><span class="cls_006">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:493.34px" class="cls_006"><span class="cls_006">you, we will be able to ship out your patient’s WristOrthosis. If you haveany questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:505.58px" class="cls_006"><span class="cls_006">(</span><span class="cls_007"><?= $key->aurbrac_call_back_num ; ?></span><span class="cls_006">).</span></div>
<div style="position:absolute;left:185.76px;top:621.75px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_003"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/right_elbow/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:72.00px;top:70.59px" class="cls_004"><span class="cls_004">DOCTOR ORDER FORM FOR ELBOWORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:113.18px" class="cls_007"><span class="cls_007">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:113.18px" class="cls_007"><span class="cls_007">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:127.46px" class="cls_007"><span class="cls_007">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:141.38px" class="cls_007"><span class="cls_007">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:141.38px" class="cls_007"><span class="cls_007">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:155.42px" class="cls_007"><span class="cls_007">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:155.42px" class="cls_007"><span class="cls_007">Date:</span></div>
<div style="position:absolute;left:332.76px;top:155.42px" class="cls_007"><span class="cls_007"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:169.22px" class="cls_009"><span class="cls_009">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:174.00px;top:169.22px" class="cls_009"><span class="cls_009">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.52px;top:169.22px" class="cls_009"><span class="cls_009">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:219.98px" class="cls_007"><span class="cls_007">ITEM DESCRIPTION & HCPCS CODE:L3761 - Elbow orthosis (eo), with adjustable position locking joint(s),</span></div>
<div style="position:absolute;left:72.00px;top:233.42px" class="cls_007"><span class="cls_007">prefabricated, off-</span></div>
<div style="position:absolute;left:398.04px;top:264.98px" class="cls_009"><span class="cls_009">Physician information</span></div>
<div style="position:absolute;left:106.56px;top:265.46px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:106.56px;top:279.03px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_005"> Pain in Right elbow (M25.521)</span></div>
<div style="position:absolute;left:321.72px;top:284.18px" class="cls_007"><span class="cls_007">NAME: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:321.72px;top:297.62px" class="cls_007"><span class="cls_007">PHONE: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:106.56px;top:296.79px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_005">   Disorder of ligament, Right elbow</span></div>
<div style="position:absolute;left:321.72px;top:311.06px" class="cls_007"><span class="cls_007">FAX: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:106.56px;top:315.62px" class="cls_005"><span class="cls_005">(M24.221)</span></div>
<div style="position:absolute;left:321.72px;top:324.50px" class="cls_007"><span class="cls_007">ADDRESS: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:106.56px;top:329.67px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:321.72px;top:347.06px" class="cls_009"><span class="cls_009">Corrections:</span></div>
<div style="position:absolute;left:106.56px;top:348.03px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Left</span></div>
<div style="position:absolute;left:146.40px;top:348.03px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">Right</span><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_009">Bilateral</span></div>
<div style="position:absolute;left:106.56px;top:378.50px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:106.56px;top:392.07px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:351.57px;top:405.86px" class="cls_007"><span class="cls_007">Date: _________________</span></div>
<div style="position:absolute;left:106.56px;top:410.90px" class="cls_009"><span class="cls_009">affected body part</span></div>
<div style="position:absolute;left:351.72px;top:419.30px" class="cls_007"><span class="cls_007">Physician Signature: _________________</span></div>
<div style="position:absolute;left:106.56px;top:420.63px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To facilitate healing following an injury or</span></div>
<div style="position:absolute;left:351.72px;top:432.74px" class="cls_007"><span class="cls_007">NPI Number:</span></div>
<div style="position:absolute;left:106.56px;top:439.70px" class="cls_009"><span class="cls_009">surgical procedure</span></div>
<div style="position:absolute;left:106.56px;top:448.95px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">To apply traction for either correction or</span></div>
<div style="position:absolute;left:106.56px;top:467.78px" class="cls_009"><span class="cls_009">prevention contractures</span></div>
<div style="position:absolute;left:321.72px;top:473.06px" class="cls_007"><span class="cls_007">If different doctor from above print name:</span></div>
<div style="position:absolute;left:106.56px;top:477.51px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To support weak or deformed body part</span></div>
<div style="position:absolute;left:321.72px;top:486.50px" class="cls_007"><span class="cls_007">Name: ___________________</span></div>
<div style="position:absolute;left:106.56px;top:497.18px" class="cls_009"><span class="cls_009">Length of need is Lifetime per Medicare guidelines</span></div>
<div style="position:absolute;left:321.72px;top:499.94px" class="cls_007"><span class="cls_007">NPI: ___________________</span></div>
<div style="position:absolute;left:106.56px;top:508.10px" class="cls_009"><span class="cls_009">(unless specified) ___</span><span class="cls_014">99</span><span class="cls_009">_____ months</span></div>
<div style="position:absolute;left:321.72px;top:522.62px" class="cls_012"><span class="cls_012">By signing above, I attest I am the ordering Physician or Practitioner of the</span></div>
<div style="position:absolute;left:321.72px;top:533.18px" class="cls_012"><span class="cls_012">items listed and have determined items to be a medical necessity for</span></div>
<div style="position:absolute;left:321.72px;top:541.70px" class="cls_012"><span class="cls_012">condition described</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:175.44px;top:608.79px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_003"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
