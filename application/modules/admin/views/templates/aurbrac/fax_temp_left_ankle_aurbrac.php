<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="04e16534-e301-11ea-8b25-0cc47a792c0a_id_04e16534-e301-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>    
    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_ankle/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:347.76px;top:70.83px" class="cls_002"><span class="cls_002">Fax:<?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:336.96px;top:89.19px" class="cls_002"><span class="cls_002">Phone:<?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:181.44px;top:144.75px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR</span></div>
<div style="position:absolute;left:198.00px;top:165.39px" class="cls_003"><span class="cls_003">ANKLE FOOT ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:252.87px" class="cls_005"><span class="cls_005">has requested that we send them an adjustable Ankle Brace for instability</span></div>
<div style="position:absolute;left:72.00px;top:270.15px" class="cls_005"><span class="cls_005">relief.  They indicated that you are the treating physician to complete the</span></div>
<div style="position:absolute;left:72.00px;top:287.55px" class="cls_005"><span class="cls_005">written order.</span></div>
<div style="position:absolute;left:72.00px;top:336.02px" class="cls_004"><span class="cls_004">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:358.71px" class="cls_004"><span class="cls_004">1. </span><span class="cls_005"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:376.11px" class="cls_004"><span class="cls_004">2. </span><span class="cls_005"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:393.51px" class="cls_004"><span class="cls_004">3. </span><span class="cls_005"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:418.83px" class="cls_005"><span class="cls_005">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:444.62px" class="cls_006"><span class="cls_006">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:456.14px" class="cls_006"><span class="cls_006">you, we will be able to ship out your patient’s Ankle Orthosis. If you have any questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:467.54px" class="cls_006"><span class="cls_006">(</span><span class="cls_007"><?= $key->aurbrac_call_back_num ; ?></span><span class="cls_006">).</span></div>
<div style="position:absolute;left:169.08px;top:602.31px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_ankle/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:92.52px;top:70.59px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM ANKLE FOOT ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:113.18px" class="cls_007"><span class="cls_007">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:113.18px" class="cls_007"><span class="cls_007">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:127.46px" class="cls_007"><span class="cls_007">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:141.38px" class="cls_007"><span class="cls_007">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:141.38px" class="cls_007"><span class="cls_007">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:155.42px" class="cls_007"><span class="cls_007">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:155.42px" class="cls_007"><span class="cls_007">Date:</span></div>
<div style="position:absolute;left:332.76px;top:155.42px" class="cls_007"><span class="cls_007"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:169.22px" class="cls_010"><span class="cls_010">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:174.00px;top:169.22px" class="cls_010"><span class="cls_010">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.52px;top:169.22px" class="cls_010"><span class="cls_010">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:219.38px" class="cls_007"><span class="cls_007">ITEM DESCRIPTION & HCPCS CODE: L1971-Ankle Foot Orthosis, plastic or other</span></div>
<div style="position:absolute;left:72.00px;top:231.98px" class="cls_007"><span class="cls_007">material with ankle joint, prefabricated</span></div>
<div style="position:absolute;left:380.04px;top:263.06px" class="cls_010"><span class="cls_010">Physician information</span></div>
<div style="position:absolute;left:105.48px;top:272.90px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:314.16px;top:282.38px" class="cls_010"><span class="cls_010">NAME: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:105.48px;top:286.47px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">M25.371- Other instability, right ankle</span></div>
<div style="position:absolute;left:314.16px;top:292.70px" class="cls_010"><span class="cls_010">PHONE: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:314.16px;top:303.02px" class="cls_010"><span class="cls_010">FAX: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:105.48px;top:304.47px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010"> M25.572 - Pain in left ankle and joints of</span></div>
<div style="position:absolute;left:314.16px;top:313.34px" class="cls_010"><span class="cls_010">ADDRESS: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:105.48px;top:323.30px" class="cls_010"><span class="cls_010">left foot</span></div>
<div style="position:absolute;left:105.48px;top:333.03px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">M25.376 - Other instability, unspecified foot</span></div>
<div style="position:absolute;left:314.16px;top:346.70px" class="cls_010"><span class="cls_010">Corrections:</span></div>
<div style="position:absolute;left:105.48px;top:351.39px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:344.16px;top:370.82px" class="cls_007"><span class="cls_007">Date: _________________</span></div>
<div style="position:absolute;left:105.48px;top:369.87px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010">Left</span></div>
<div style="position:absolute;left:149.88px;top:369.87px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">Right</span><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">Bilateral</span></div>
<div style="position:absolute;left:344.16px;top:383.54px" class="cls_007"><span class="cls_007">Physician Signature: _____________</span></div>
<div style="position:absolute;left:344.87px;top:396.14px" class="cls_007"><span class="cls_007">NPI Number:</span></div>
<div style="position:absolute;left:105.48px;top:399.98px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:105.48px;top:413.07px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010">To reduce pain by restricting mobility</span></div>
<div style="position:absolute;left:105.48px;top:431.31px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">To restrict and eliminate motion in an injured</span></div>
<div style="position:absolute;left:314.16px;top:434.30px" class="cls_007"><span class="cls_007">If different doctor from above print name:</span></div>
<div style="position:absolute;left:105.48px;top:450.26px" class="cls_010"><span class="cls_010">body part</span></div>
<div style="position:absolute;left:314.16px;top:446.90px" class="cls_007"><span class="cls_007">Name: ___________________</span></div>
<div style="position:absolute;left:314.16px;top:459.50px" class="cls_007"><span class="cls_007">NPI: ___________________</span></div>
<div style="position:absolute;left:105.48px;top:459.99px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_010">To Facilitate healing following an injury or</span></div>
<div style="position:absolute;left:105.48px;top:479.06px" class="cls_010"><span class="cls_010">surgical procedure</span></div>
<div style="position:absolute;left:314.16px;top:481.34px" class="cls_012"><span class="cls_012">By signing above, I attest I am the ordering Physician or</span></div>
<div style="position:absolute;left:314.16px;top:491.18px" class="cls_012"><span class="cls_012">Practitioner of the items listed and have determined items to be</span></div>
<div style="position:absolute;left:105.48px;top:488.79px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_010">To otherwise support weak or a deformed</span></div>
<div style="position:absolute;left:314.16px;top:499.10px" class="cls_012"><span class="cls_012">a medical necessity for condition described</span><span class="cls_010">.</span></div>
<div style="position:absolute;left:105.48px;top:507.74px" class="cls_010"><span class="cls_010">body part</span></div>
<div style="position:absolute;left:105.48px;top:524.66px" class="cls_010"><span class="cls_010">Length of need is Lifetime per Medicare</span></div>
<div style="position:absolute;left:105.48px;top:534.98px" class="cls_010"><span class="cls_010">guidelines (unless specified) __99____ months</span></div>
<div style="position:absolute;left:169.08px;top:585.15px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div></div>
<?php
 } 
?>
</body>
</html>
