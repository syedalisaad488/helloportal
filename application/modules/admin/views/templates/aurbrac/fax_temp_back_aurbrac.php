<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:10.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:16.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:16.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:13.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-size:9.4px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:7.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:7.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:18.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:18.9px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_014{font-size:11.3px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:15.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_015{font-size:8.5px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="6f2083a4-e300-11ea-8b25-0cc47a792c0a_id_6f2083a4-e300-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>    
    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/back/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:426.12px;top:66.87px" class="cls_002"><span class="cls_002">Fax:</span><span class="cls_003"> <?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:407.52px;top:85.35px" class="cls_002"><span class="cls_002">Phone:</span><span class="cls_003"> <?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:100.44px;top:122.67px" class="cls_004"><span class="cls_004">DOCTOR ORDER FORM FOR LUMBAR ORTHOSIS</span></div>
<div style="position:absolute;left:96.00px;top:188.67px" class="cls_005"><span class="cls_005">has requested an adjustable Back Brace for instability/pain relief.  They</span></div>
<div style="position:absolute;left:108.00px;top:205.95px" class="cls_005"><span class="cls_005">indicated that you are the treating physician to complete the written</span></div>
<div style="position:absolute;left:285.84px;top:223.35px" class="cls_005"><span class="cls_005">order.</span></div>
<div style="position:absolute;left:106.20px;top:276.99px" class="cls_013"><span class="cls_013">Please return with a copy of the patient’s medical record associated</span></div>
<div style="position:absolute;left:223.56px;top:294.39px" class="cls_013"><span class="cls_013">with the requested product.</span></div>
<div style="position:absolute;left:93.60px;top:369.98px" class="cls_006"><span class="cls_006">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:110.52px;top:394.34px" class="cls_006"><span class="cls_006">1.  ICD-10</span></div>
<div style="position:absolute;left:110.52px;top:408.38px" class="cls_006"><span class="cls_006">2.  Indication of need</span></div>
<div style="position:absolute;left:110.52px;top:422.30px" class="cls_006"><span class="cls_006">3.  Please sign and date the form</span></div>
<div style="position:absolute;left:93.60px;top:464.31px" class="cls_005"><span class="cls_005">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:93.60px;top:492.02px" class="cls_007"><span class="cls_007">Thank you in advance for your time and cooperation.  As soon as we receive the completed form</span></div>
<div style="position:absolute;left:93.60px;top:502.94px" class="cls_007"><span class="cls_007">from you, we will be able to ship out patient’s LUMBAR ORTHOSIS. If you haveany questions,</span></div>
<div style="position:absolute;left:93.60px;top:514.46px" class="cls_007"><span class="cls_007">please call us at (<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:184.20px;top:602.43px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>

</div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/back/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:116.64px;top:66.76px" class="cls_009"><span class="cls_009">DOCTOR ORDER FORM FOR LUMBAR ORTHOSIS</span></div>
<div style="position:absolute;left:93.60px;top:103.28px" class="cls_003"><span class="cls_003">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:103.28px" class="cls_003"><span class="cls_003">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:116.72px" class="cls_003"><span class="cls_003">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:129.80px" class="cls_003"><span class="cls_003">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:129.80px" class="cls_003"><span class="cls_003">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:93.60px;top:142.88px" class="cls_003"><span class="cls_003">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:291.24px;top:142.88px" class="cls_003"><span class="cls_003">Date:</span></div>
<div style="position:absolute;left:338.76px;top:142.88px" class="cls_003"><span class="cls_003"><?= $date ; ?></span></div>
<div style="position:absolute;left:93.60px;top:156.08px" class="cls_010"><span class="cls_010">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:189.44px;top:156.08px" class="cls_010"><span class="cls_010">WEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:287.68px;top:156.08px" class="cls_010"><span class="cls_010">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:93.60px;top:215.60px; font-size: 7.25px !important; " class="cls_010"><span class="cls_010">ITEM DESCRIPTION & HCPCS CODE: L0650LUMBAR-SACRAL ORTHOSIS, SAGITTAL-CORONAL CONTROL, WITH RIGID ANTERIOR</span></div>
<div style="position:absolute;left:93.60px;top:225.92px; font-size: 7.25px !important;" class="cls_010"><span class="cls_010">AND POSTERIOR FRAME/PANEL(S), POSTERIOR EXTENDS FROM SACROCOCCYGEAL JUNCTION TO T-9 VERTEBRA, LATERAL</span></div>
<div style="position:absolute;left:93.60px;top:236.24px; font-size: 7.25px !important;" class="cls_010"><span class="cls_010">STRENGTH PROVIDED BY RIGID LATERAL FRAME/PANEL(S), PRODUCES INTRACAVITARY PRESSURE TO REDUCE LOAD ON</span></div>
<div style="position:absolute;left:93.60px;top:246.56px; font-size: 7.25px !important;" class="cls_010"><span class="cls_010">INTERVERTEBRAL DISCS, INCLUDES STRAPS, CLOSURES, MAY INCLUDE PADDING, SHOULDER STRAPS, PENDULOUS ABDOMEN</span></div>
<div style="position:absolute;left:93.60px;top:256.88px; font-size: 7.25px !important;" class="cls_010"><span class="cls_010">DESIGN, PREFABRICATED, OFF-THE-SHELF</span></div>
<div style="position:absolute;left:394.08px;top:277.64px" class="cls_010"><span class="cls_010">Physician information</span></div>
<div style="position:absolute;left:120.84px;top:279.86px" class="cls_014"><span class="cls_014">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:120.84px;top:292.47px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010">M54.5 - </span><span class="cls_007">Low back pain</span></div>
<div style="position:absolute;left:336.84px;top:298.16px" class="cls_003"><span class="cls_003">Name: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:336.84px;top:310.76px" class="cls_003"><span class="cls_003">Phone: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:120.84px;top:309.63px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_010">M53.9 - </span><span class="cls_007">Dorsopathy, unspecified</span></div>
<div style="position:absolute;left:336.90px;top:323.36px" class="cls_003"><span class="cls_003">Fax: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:134.04px;top:327.18px" class="cls_010"><span class="cls_010">M51.36-</span><span class="cls_007">Lumbar/ Lumbosacral</span></div>
<div style="position:absolute;left:120.84px;top:326.55px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span></div>
<div style="position:absolute;left:336.84px;top:336.08px" class="cls_003"><span class="cls_003">Address : <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:120.84px;top:345.02px" class="cls_007"><span class="cls_007">Intervertebral Disc Degeneration.</span></div>
<div style="position:absolute;left:120.84px;top:355.52px" class="cls_010"><span class="cls_010">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:334.56px;top:374.12px" class="cls_010"><span class="cls_010">Corrections:</span></div>
<div style="position:absolute;left:120.84px;top:395.78px" class="cls_014"><span class="cls_014">Select indication of Need:</span></div>
<div style="position:absolute;left:365.56px;top:405.32px" class="cls_003"><span class="cls_003">Date: _________________</span></div>
<div style="position:absolute;left:134.04px;top:410.04px" class="cls_010"><span class="cls_010">To reduce pain by restricting mobility of the</span></div>
<div style="position:absolute;left:120.84px;top:408.51px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span></div>
<div style="position:absolute;left:365.56px;top:417.92px" class="cls_003"><span class="cls_003">Physician Signature: _________________</span></div>
<div style="position:absolute;left:120.84px;top:419.55px" class="cls_010"><span class="cls_010">trunk</span></div>
<div style="position:absolute;left:365.56px;top:430.52px" class="cls_003"><span class="cls_003">NPI Number:</span></div>
<div style="position:absolute;left:134.04px;top:436.92px" class="cls_010"><span class="cls_010">To facilitate healing following an injury to the</span></div>
<div style="position:absolute;left:120.84px;top:435.39px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span></div>
<div style="position:absolute;left:120.84px;top:453.08px" class="cls_010"><span class="cls_010">spine or related tissues; or</span></div>
<div style="position:absolute;left:334.56px;top:455.84px" class="cls_003"><span class="cls_003">If different doctor from above print name:</span></div>
<div style="position:absolute;left:120.84px;top:462.39px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"> </span><span class="cls_010">To Facilitate healing following a surgical</span></div>
<div style="position:absolute;left:334.56px;top:468.44px" class="cls_003"><span class="cls_003">Name: ___________________</span></div>
<div style="position:absolute;left:120.84px;top:480.08px" class="cls_010"><span class="cls_010">procedure to the spine or related soft tissues; or</span></div>
<div style="position:absolute;left:334.56px;top:481.16px" class="cls_003"><span class="cls_003">NPI: __________________</span></div>
<div style="position:absolute;left:120.84px;top:488.91px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_010">To otherwise support weak spinal and/or a</span></div>
<div style="position:absolute;left:120.84px;top:506.48px" class="cls_010"><span class="cls_010">deformed spine.</span></div>
<div style="position:absolute;left:334.56px;top:505.10px" class="cls_008"><span class="cls_008">By signing above, I attest I am the ordering Physician or</span></div>
<div style="position:absolute;left:120.84px;top:516.92px" class="cls_010"><span class="cls_010">Length of need is Lifetime = 99 months, per</span></div>
<div style="position:absolute;left:334.56px;top:515.78px" class="cls_008"><span class="cls_008">Practitioner of the items listed and have determined items to be a</span></div>
<div style="position:absolute;left:120.84px;top:527.24px" class="cls_010"><span class="cls_010">Medicare guidelines, unless otherwise specified -</span></div>
<div style="position:absolute;left:334.56px;top:526.40px" class="cls_008"><span class="cls_008">medical necessity for condition described</span><span class="cls_010">.</span></div>
<div style="position:absolute;left:120.84px;top:537.56px" class="cls_010"><span class="cls_010">____</span><span class="cls_015">99</span><span class="cls_010">____ months</span></div>
<div style="position:absolute;left:348.12px;top:555.86px" class="cls_008"><span class="cls_008">This is a certifying statement of medical necessity</span></div>
<div style="position:absolute;left:175.20px;top:629.67px" class="cls_004"><span class="cls_004">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>
</div>
<?php
 } 
?>
</body>
</html>
