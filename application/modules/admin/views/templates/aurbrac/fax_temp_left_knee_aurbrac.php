<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
input{
	transform : scale(0.6);
	margin: 0px;
	padding: 0px;

}
span.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:18.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:14.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-size:9.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_013{font-size:12.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-size:8.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="35430dea-e301-11ea-8b25-0cc47a792c0a_id_35430dea-e301-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
<?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>

    <?php $this->load->view("admin/templates/aurbrac/fax_temp_cover_aurbrac",$fax); ?>
    
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_knee/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:348.48px;top:72.27px" class="cls_002"><span class="cls_002">Fax:<?= $key->aurbrac_fax_back;  ?></span></div>
<div style="position:absolute;left:337.68px;top:91.83px" class="cls_002"><span class="cls_002">Phone:<?= $key->aurbrac_call_back_num ; ?></span></div>
<div style="position:absolute;left:104.52px;top:148.59px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR KNEE ORTHOSIS</span></div>
<div style="position:absolute;left:84.96px;top:243.15px" class="cls_005"><span class="cls_005">has requested an adjustable Knee Brace for instability/pain relief.  They</span></div>
<div style="position:absolute;left:78.36px;top:260.55px" class="cls_005"><span class="cls_005">indicated that you are the treating physician to complete the written order.</span></div>
<div style="position:absolute;left:72.00px;top:309.02px" class="cls_004"><span class="cls_004">Please complete the following items on the attached document:</span></div>
<div style="position:absolute;left:90.00px;top:331.71px" class="cls_004"><span class="cls_004">1. </span><span class="cls_005"> ICD-10</span></div>
<div style="position:absolute;left:90.00px;top:348.99px" class="cls_004"><span class="cls_004">2. </span><span class="cls_005"> Indication of need</span></div>
<div style="position:absolute;left:90.00px;top:366.39px" class="cls_004"><span class="cls_004">3. </span><span class="cls_005"> Please sign and date the form</span></div>
<div style="position:absolute;left:72.00px;top:392.31px" class="cls_005"><span class="cls_005">Initial and date any changes made to the following prescription.</span></div>
<div style="position:absolute;left:72.00px;top:445.10px" class="cls_006"><span class="cls_006">Thank you in advance for your time and cooperation.  As soon as we receive the completed form from</span></div>
<div style="position:absolute;left:72.00px;top:456.62px" class="cls_006"><span class="cls_006">you, we will be able to ship out your patient’s KneeOrthosis. If you haveany questions, please call us at</span></div>
<div style="position:absolute;left:72.00px;top:468.98px" class="cls_006"><span class="cls_006">(<?= $key->aurbrac_call_back_num ; ?>).</span></div>
<div style="position:absolute;left:179.64px;top:598.47px" class="cls_003"><span class="cls_003">Please Fax Back to: </span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/aurbrac/left_knee/background2.jpg" width=612 height=792></div>
<div style="position:absolute;left:104.52px;top:70.59px" class="cls_003"><span class="cls_003">DOCTOR ORDER FORM FOR KNEE ORTHOSIS</span></div>
<div style="position:absolute;left:72.00px;top:103.94px" class="cls_008"><span class="cls_008">Patient: <span style="margin-left:35px;"><?= $key->aurbrac_firstname . " " . $key->aurbrac_lastname ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:103.94px" class="cls_008"><span class="cls_008">Address:<span style="margin-left:5px;"> <?= $key->aurbrac_address . "," . $key->aurbrac_city . "," .$key->aurbrac_state . "," .$key->aurbrac_zip ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:118.34px" class="cls_008"><span class="cls_008">DOB: <span style="margin-left:40px;"><?= $key->aurbrac_dob ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:132.26px" class="cls_008"><span class="cls_008">Gender: <span style="margin-left:32px;"><?= $key->aurbrac_gender ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:132.26px" class="cls_008"><span class="cls_008">Phone: <span style="margin-left:15px;"><?= $key->aurbrac_phone ; ?></span></span></div>
<div style="position:absolute;left:72.00px;top:146.18px" class="cls_008"><span class="cls_008">Medicare ID: <span style="margin-left:10px;"><?= $key->aurbrac_med_id ; ?></span></span></div>
<div style="position:absolute;left:282.24px;top:146.18px" class="cls_008"><span class="cls_008">Date:</span></div>
<div style="position:absolute;left:332.76px;top:146.18px" class="cls_008"><span class="cls_008"><?= $date ; ?></span></div>
<div style="position:absolute;left:72.00px;top:159.98px" class="cls_009"><span class="cls_009">HEIGHT : <?= $key->aurbrac_height ; ?></span></div>
<div style="position:absolute;left:174.00px;top:159.98px" class="cls_009"><span class="cls_009">WEIGHT : <?= $key->aurbrac_weight ; ?></span></div>
<div style="position:absolute;left:278.52px;top:159.98px" class="cls_009"><span class="cls_009">PAIN LEVEL : <?= $key->aurbrac_pain_level ; ?></span></div>
<div style="position:absolute;left:72.00px;top:217.22px" class="cls_006"><span class="cls_006">ITEM DESCRIPTION & HCPCS CODE:  L1851:Adjustable Flexion & Extension Joint (Unicentric or Polycentric), Medial-</span></div>
<div style="position:absolute;left:72.00px;top:229.46px" class="cls_006"><span class="cls_006">Lateral & Rotation Control, +/- Varus/Valgus Adjustment, Prefabricated, Off-TheShelf; includes L2397 Suspension</span></div>
<div style="position:absolute;left:72.00px;top:241.70px" class="cls_006"><span class="cls_006">Sleeve.</span></div>
<div style="position:absolute;left:397.92px;top:262.82px" class="cls_009"><span class="cls_009">Physician information</span></div>
<div style="position:absolute;left:106.44px;top:277.82px" class="cls_013"><span class="cls_013">Select ICD-10 Diagnosis Code:</span></div>
<div style="position:absolute;left:326.16px;top:282.02px" class="cls_008"><span class="cls_008">NAME: <?= $key->aurbrac_doc_firstname . " " . $key->aurbrac_doc_lastname ; ?></span></div>
<div style="position:absolute;left:106.44px;top:291.39px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_006">Chronic instability of knee, left knee</span></div>
<div style="position:absolute;left:326.16px;top:295.46px" class="cls_008"><span class="cls_008">PHONE: <?= $key->aurbrac_doc_phone ; ?></span></div>
<div style="position:absolute;left:106.44px;top:310.94px" class="cls_006"><span class="cls_006">(M23.52)</span></div>
<div style="position:absolute;left:326.16px;top:308.90px" class="cls_008"><span class="cls_008">FAX: <?= $key->aurbrac_doc_fax ; ?></span></div>
<div style="position:absolute;left:326.16px;top:322.34px" class="cls_008"><span class="cls_008">ADDRESS: <?= $key->aurbrac_doc_address . "," . $key->aurbrac_doc_city . "," .$key->aurbrac_doc_state . "," .$key->aurbrac_doc_zip ; ?></span></div>
<div style="position:absolute;left:106.44px;top:321.75px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_006">M17.11- Unilateral primary</span></div>
<div style="position:absolute;left:106.44px;top:341.54px" class="cls_006"><span class="cls_006">osteoarthritis, right knee</span></div>
<div style="position:absolute;left:326.16px;top:349.46px" class="cls_009"><span class="cls_009">Corrections:</span></div>
<div style="position:absolute;left:106.44px;top:352.35px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_006">M17.0- Bilateral primary osteoarthritis</span></div>
<div style="position:absolute;left:106.44px;top:372.02px" class="cls_006"><span class="cls_006">of knee</span></div>
<div style="position:absolute;left:106.44px;top:382.95px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Other ICD-10 code: _____________</span></div>
<div style="position:absolute;left:356.01px;top:396.98px" class="cls_008"><span class="cls_008">Date: _________________</span></div>
<div style="position:absolute;left:356.16px;top:410.42px" class="cls_008"><span class="cls_008">Physician Signature: _________________</span></div>
<div style="position:absolute;left:106.44px;top:411.63px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">Left</span></div>
<div style="position:absolute;left:150.84px;top:411.63px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Right</span><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">Bilateral</span></div>
<div style="position:absolute;left:356.16px;top:423.74px" class="cls_008"><span class="cls_008">NPI Number:</span></div>
<div style="position:absolute;left:106.44px;top:442.10px" class="cls_013"><span class="cls_013">Select indication of Need:</span></div>
<div style="position:absolute;left:106.44px;top:455.67px" class="cls_011"><span class="cls_011"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/checked.png"></span><span class="cls_009">To reduce pressure on joints</span></div>
<div style="position:absolute;left:326.16px;top:464.06px" class="cls_008"><span class="cls_008">If different doctor from above print name:</span></div>
<div style="position:absolute;left:106.44px;top:473.91px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To reduce knee stiffness and improve mobility</span></div>
<div style="position:absolute;left:326.16px;top:477.50px" class="cls_008"><span class="cls_008">Name: ___________________</span></div>
<div style="position:absolute;left:326.16px;top:490.94px" class="cls_008"><span class="cls_008">NPI: ___________________</span></div>
<div style="position:absolute;left:106.44px;top:492.27px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To restrict and eliminate motion in a diseased or</span></div>
<div style="position:absolute;left:106.44px;top:511.34px" class="cls_009"><span class="cls_009">injured body part</span></div>
<div style="position:absolute;left:326.16px;top:513.62px" class="cls_012"><span class="cls_012">By signing above, I attest I am the ordering Physician or Practitioner of</span></div>
<div style="position:absolute;left:326.16px;top:524.18px" class="cls_012"><span class="cls_012">the items listed and have determined items to be a medical necessity for</span></div>
<div style="position:absolute;left:106.44px;top:521.07px" class="cls_002"><span class="cls_002"><img style="width:9px; height:9px; margin-top:2px;" src="assets/images/aurbrac/unchecked.png"></span><span class="cls_009">To support weak or deformed body part</span></div>
<div style="position:absolute;left:326.16px;top:532.70px" class="cls_012"><span class="cls_012">condition described</span><span class="cls_009">.</span></div>
<div style="position:absolute;left:106.44px;top:540.74px" class="cls_009"><span class="cls_009">Length of need is Lifetime per Medicare guidelines</span></div>
<div style="position:absolute;left:106.44px;top:551.66px" class="cls_009"><span class="cls_009">(unless specified) ___99_____ months</span></div>
<div style="position:absolute;left:171.72px;top:602.55px" class="cls_003"><span class="cls_003">Please Fax Back to:</span><span class="cls_002"><?= $key->aurbrac_fax_back;  ?></span></div>
    <div style="position:absolute;left:98.76px;top:635.78px;width:400px; text-align: center; font-size: 11px !important;" class="cls_008"><span class="cls_008"><p>This communication is intended to be delivered only to the named addressee, and may contain material that is confidential,
            proprietary, or subject to legal privilege or legal protection under applicable. Federal or state law, including without limitation the
            Health Insurance Portability and Accountability Act of 1996 (HIPAA). If you are not the intended recipient, you should immediately
            notify the sender at the address and telephone number set forth herein and obtain instructions as to the disposal of the transmitted
                material. In no event should the attached material be read or retained by anyone other than the named addressee, except by express
                authority of the sender or the named addressee
            </p></span></div>
</div>
<?php
 } 
?>
</body>
</html>
