<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-size:25.1px;color:rgb(0,0,108);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-size:25.1px;color:rgb(0,0,108);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_004{font-size:12.1px;color:rgb(0,0,108);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-size:12.1px;color:rgb(0,0,108);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-size:10.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-size:10.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-size:8.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-size:8.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-size:12.1px;color:rgb(0,0,108);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_007{font-size:12.1px;color:rgb(0,0,108);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-size:13.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-size:11.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-size:10.1px;color:rgb(0,0,108);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-size:10.1px;color:rgb(0,0,108);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-size:9.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-size:9.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-size:9.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-size:8.6px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}

span.cls_122002{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122002{font-family:Arial,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122004{font-family:"Century Schoolbook Bold",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122004{font-family:"Century Schoolbook Bold",serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122006{font-family:"Century Schoolbook Bold",serif;font-size:18.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122006{font-family:"Century Schoolbook Bold",serif;font-size:18.1px;color:rgb(255,255,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122007{font-family:Tahoma,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122007{font-family:Tahoma,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122008{font-family:Tahoma,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_122008{font-family:Tahoma,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_122009{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122009{font-family:Tahoma,serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122012{font-family:"Century Schoolbook Bold",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122012{font-family:"Century Schoolbook Bold",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_122013{font-family:"Century",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_122013{font-family:"Century",serif;font-size:16.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_122018{font-family:Times,serif;font-size:18.1px;color:rgb(34,82,123);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_122018{font-family:Times,serif;font-size:18.1px;color:rgb(34,82,123);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_122016{font-family:"Century Schoolbook Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_122016{font-family:"Century Schoolbook Bold",serif;font-size:14.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}

-->
</style>
<script type="text/javascript" src="6004517e-de51-11ea-8b25-0cc47a792c0a_id_6004517e-de51-11ea-8b25-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>
            <?php
 $date = date("m/d/Y");
 foreach($fax as $key){
?>

<!--cover start-->
<div style="position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:841px;border-style:outset;overflow:auto;page-break-after: always;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/cgmdiab/cover_cgmdiab.jpg" width=595 height=841></div>
<div style="position:absolute;left:18.00px;top:111.06px;font-size:16px !important;" class="cls_122004"><span class="cls_122004">FOR REVIEW</span></div>
<div style="position:absolute;left:148.22px;top:111.06px" class="cls_122006"><span class="cls_122006">HEALTHWAYS MEDICAL EQUIPMENT ®</span></div>
<div style="position:absolute;left:72.38px;top:191.22px" class="cls_122007"><span class="cls_122007">From</span><span class="cls_122008">: </span><span class="cls_122009">HEALTHWAYS MEDICAL EQUIPMENT®</span></div>
<div style="position:absolute;left:72.38px;top:247.26px" class="cls_122007"><span class="cls_122007">Phone</span><span class="cls_122008">: <?= $key->cgmdiab_doc_phone ; ?></span></div>
<div style="position:absolute;left:72.38px;top:296.97px" class="cls_122007"><span class="cls_122007">Fax</span><span class="cls_122008">: <?= $key->cgmdiab_doc_fax ; ?></span></div>
<div style="position:absolute;left:72.02px;top:338.73px" class="cls_122012"><span class="cls_122012">TO WHOM IT MAY CONCERN :</span></div>
<div style="position:absolute;left:72.02px;top:376.77px" class="cls_122013"><span class="cls_122013">Your patient has requested for a  </span><A HREF="https://www.freestylelibre.us/about-system-overview">Free Style Libre</A> </span></div>
<div style="position:absolute;left:72.02px;top:407.85px" class="cls_122018"><span class="cls_122018"> </span><A HREF="https://www.freestylelibre.us/about-system-overview">diabetic monitor</A> <span class="cls_122013">. Patient indicated that you are the</span></div>
<div style="position:absolute;left:72.02px;top:440.15px" class="cls_122013"><span class="cls_122013">treating physician to review the written order. Please</span></div>
<div style="position:absolute;left:72.02px;top:469.55px" class="cls_122013"><span class="cls_122013">review and sign the attached form and send us back with </span></div><br>
<div style="position:absolute;left:72.02px;top:469.55px" class="cls_122013"><span class="cls_122013"></span></div><br>
<div style="position:absolute;left:72.02px;top:499.07px" class="cls_122013"><span class="cls_122013">patient's chart and medical records so that we can complete the request .</span></div>
<div style="position:absolute;left:72.02px;top:567.83px" class="cls_122013"><span class="cls_122013">Thank you in advance for your time and cooperation. If</span></div>
<div style="position:absolute;left:72.02px;top:597.23px" class="cls_122013"><span class="cls_122013">you have any questions ,please call us at between 9 am</span></div>
<div style="position:absolute;left:72.02px;top:626.66px" class="cls_122013"><span class="cls_122013">to 5pm ( mon-fri ) .</span></div>
<div style="position:absolute;left:72.02px;top:676.46px" class="cls_122012"><span class="cls_122012">Return fax :  <?= $key->cgmdiab_fax_back; ?></span></div>
<div style="position:absolute;left:205.37px;top:745.58px" class="cls_122016"><span class="cls_122016">info@imperialmedical.com</span></div>
</div>
<!--cover end-->
<div style="position:absolute;left:50%;margin-left:-306px;top:0px;width:612px;height:792px;border-style:outset;overflow:auto;">
<div style="position:absolute;left:0px;top:0px">
<img src="assets/images/cgmdiab/background1.jpg" width=612 height=792></div>
<div style="position:absolute;left:56.04px;top:48.48px" class="cls_002"><span class="cls_002">Medicare Detailed Written Order <img style="height:100px; width:150px; float:right; margin-top:-40px; margin-left:60px;" src="assets/images/cgmdiab/fsl-logo.jpg"></span></div>
<div style="position:absolute;left:36.00px;top:96.02px" class="cls_004"><span class="cls_004">Instructions</span></div>
<div style="position:absolute;left:36.00px;top:115.70px" class="cls_005"><span class="cls_005">1.C</span><span class="cls_006">OMPLETE ALL FIELDS ON THIS </span><span class="cls_005">D</span><span class="cls_006">ETAILED </span><span class="cls_005">W</span><span class="cls_006">RITTEN </span><span class="cls_005">O</span><span class="cls_006">RDER</span><span class="cls_005">.</span></div>
<div style="position:absolute;left:36.00px;top:128.78px" class="cls_005"><span class="cls_005">2. U</span><span class="cls_006">SE THE </span><span class="cls_005">N</span><span class="cls_006">ORIDIAN </span><span class="cls_005">N</span><span class="cls_006">OVEMBER </span><span class="cls_005">2017</span><span class="cls_006"> </span><span class="cls_005">P</span><span class="cls_006">HYSICIAN </span><span class="cls_005">R</span><span class="cls_006">ESOURCE </span><span class="cls_005">L</span><span class="cls_006">ETTER </span><span class="cls_005">(C</span><span class="cls_006">ONTINUOUS </span><span class="cls_005">G</span><span class="cls_006">LUCOSE </span><span class="cls_005">M</span><span class="cls_006">ONITORS</span><span class="cls_005">)</span><span class="cls_006"> TO</span></div>
<div style="position:absolute;left:47.04px;top:140.30px" class="cls_006"><span class="cls_006">CONFIRM COVERAGE CRITERIA AND MEDICAL NECESSITY DOCUMENTATION REQUIREMENTS ARE MET</span><span class="cls_005">.</span></div>
<div style="position:absolute;left:36.00px;top:152.54px" class="cls_005"><span class="cls_005">3. F</span><span class="cls_006">AX BOTH THIS ORDER AND THE PATIENT</span><span class="cls_005">’</span><span class="cls_006">S MOST RECENT MEDICAL RECORDS THAT DEMONSTRATE COVERAGE</span></div>
<div style="position:absolute;left:47.04px;top:164.06px" class="cls_006"><span class="cls_006">CRITERIA ARE MET TO A </span><span class="cls_005">DME</span><span class="cls_006"> SUPPLIER THAT PROVIDES THE </span><span class="cls_005">F</span><span class="cls_006">REE </span><span class="cls_005">S</span><span class="cls_006">TYLE </span><span class="cls_005">L</span><span class="cls_006">IBRE </span><span class="cls_005">14</span><span class="cls_006"> DAY SYSTEM</span><span class="cls_005">.</span></div>
<div style="position:absolute;left:36.00px;top:183.38px" class="cls_007"><span class="cls_007">Patient Information</span></div>
<div style="position:absolute;left:36.00px;top:201.98px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">ATIENT </span><span class="cls_005">N</span><span class="cls_006">AME</span><span >:<?= $key->cgmdiab_firstname . " " . $key->cgmdiab_lastname ; ?></span></div>
<div style="position:absolute;left:399.07px;top:201.98px" class="cls_005"><span class="cls_005">D</span><span class="cls_006">ATE OF </span><span class="cls_005">B</span><span class="cls_006">IRTH</span><span class="cls_005">:<?= $key->cgmdiab_dob ; ?></span></div>
<div style="position:absolute;left:36.00px;top:218.30px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">HONE</span><span class="cls_005">:<?= $key->cgmdiab_phone ; ?></span></div>
<div style="position:absolute;left:288.05px;top:214.58px" class="cls_005"><span class="cls_005">E</span><span class="cls_006">MAIL</span><span class="cls_005">:<?= $key->cgmdiab_email ; ?> </span><span class="cls_003">-</span></div>
<div style="position:absolute;left:36.00px;top:233.30px" class="cls_005"><span class="cls_005">A</span><span class="cls_006">DDRESS</span><span class="cls_005">:<?= $key->cgmdiab_address ; ?></span></div>
<div style="position:absolute;left:288.05px;top:233.30px" class="cls_005"><span class="cls_005">C</span><span class="cls_006">ITY</span><span class="cls_005">:<?= $key->cgmdiab_city ; ?></span></div>
<div style="position:absolute;left:432.07px;top:233.30px" class="cls_005"><span class="cls_005">S</span><span class="cls_006">TATE</span><span class="cls_005">:<?= $key->cgmdiab_state ; ?></span></div>
<div style="position:absolute;left:504.10px;top:233.30px" class="cls_005"><span class="cls_005">ZIP:<?= $key->cgmdiab_zip ; ?></span></div>
<div style="position:absolute;left:36.00px;top:247.37px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">RIMARY </span><span class="cls_005">I</span><span class="cls_006">NSURANCE</span><span class="cls_005">: </span><span class="cls_011">:MEDICARE</span></div>
<div style="position:absolute;left:288.05px;top:248.57px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">RIMARY </span><span class="cls_005">I</span><span class="cls_006">NSURANCE </span><span class="cls_005">M</span><span class="cls_006">EMBER </span><span class="cls_005">ID:<?= $key->cgmdiab_medicare ; ?></span></div>
<div style="position:absolute;left:36.00px;top:265.97px" class="cls_005"><span class="cls_005">S</span><span class="cls_006">ECONDARY </span><span class="cls_005">I</span><span class="cls_006">NSURANCE</span><span class="cls_005">:<?= $key->cgmdiab_insurance_com ; ?></span></div>
<div style="position:absolute;left:134.06px;top:262.37px" class="cls_003"><span class="cls_003">-</span></div>
<div style="position:absolute;left:288.05px;top:265.97px" class="cls_005"><span class="cls_005">S</span><span class="cls_006">ECONDARY </span><span class="cls_005">I</span><span class="cls_006">NSURANCE </span><span class="cls_005">M</span><span class="cls_006">EMBER </span><span class="cls_005">ID:<?= $key->cgmdiab_insurance_com_id ; ?></span></div>
<div style="position:absolute;left:432.07px;top:262.37px" class="cls_003"><span class="cls_003">-</span></div>
<div style="position:absolute;left:36.00px;top:281.69px" class="cls_005"><span class="cls_005">N</span><span class="cls_006">OTES</span><span class="cls_005">: </span><span class="cls_010">-</span></div>
<div style="position:absolute;left:36.00px;top:322.25px" class="cls_007"><span class="cls_007">Physician Information</span></div>
<div style="position:absolute;left:36.00px;top:342.05px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">HYSICIAN </span><span class="cls_005">N</span><span class="cls_006">AME</span><span class="cls_005">:<?= $key->cgmdiab_doc_firstname . " " . $key->cgmdiab_doc_lastname ; ?></span></div>
<div style="position:absolute;left:360.07px;top:342.05px" class="cls_005"><span class="cls_005">P</span><span class="cls_006">HONE</span><span class="cls_005">:<?= $key->cgmdiab_doc_phone ; ?></span></div>
<div style="position:absolute;left:36.05px;top:355.49px" class="cls_005"><span class="cls_005"></span><span class="cls_006">NPI</span><span class="cls_005">:<?= $key->cgmdiab_doc_npi ; ?></span></div>
<div style="position:absolute;left:288.05px;top:355.49px" class="cls_005"><span class="cls_005">F</span><span class="cls_006">AX</span><span class="cls_005">:<?= $key->cgmdiab_doc_fax ; ?></span></div>
<div style="position:absolute;left:36.00px;top:373.97px" class="cls_005"><span class="cls_005">A</span><span class="cls_006">DDRESS</span><span class="cls_005">:<?= $key->cgmdiab_doc_address	 ; ?></span></div>
<div style="position:absolute;left:288.05px;top:373.97px" class="cls_005"><span class="cls_005">C</span><span class="cls_006">ITY</span><span class="cls_005">:<?= $key->cgmdiab_doc_city ; ?></span></div>
<div style="position:absolute;left:432.07px;top:373.97px" class="cls_005"><span class="cls_005">S</span><span class="cls_006">TATE</span><span class="cls_005">:<?= $key->cgmdiab_doc_state ; ?></span></div>
<div style="position:absolute;left:504.10px;top:373.97px" class="cls_005"><span class="cls_005">ZIP:<?= $key->cgmdiab_doc_zip ; ?></span></div>
<div style="position:absolute;left:36.00px;top:402.43px" class="cls_007"><span class="cls_007">O</span><span class="cls_013">RDER </span><span class="cls_007">D</span><span class="cls_013">ETAIL</span></div>
<div style="position:absolute;left:39.96px;top:424.27px" class="cls_007"><span class="cls_007">O</span><span class="cls_013">RDER </span><span class="cls_007">D</span><span class="cls_013">ATE</span><span class="cls_007">:</span><span class="cls_013"> </span><span class="cls_004"><?= $date; ?></span></div>
<div style="position:absolute;left:40.44px;top:449.95px" class="cls_010"><span class="cls_010">K0554</span><span class="cls_014"> </span><span class="cls_010">(F</span><span class="cls_014">REE </span><span class="cls_010">S</span><span class="cls_014">TYLE </span><span class="cls_010">L</span><span class="cls_014">IBRE </span><span class="cls_010">14</span><span class="cls_014"> DAY </span><span class="cls_010">R</span><span class="cls_014">EADER</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:310.01px;top:449.95px" class="cls_010"><span class="cls_010">K0553</span><span class="cls_014"> </span><span class="cls_010">(F</span><span class="cls_014">REE </span><span class="cls_010">S</span><span class="cls_014">TYLE </span><span class="cls_010">L</span><span class="cls_014">IBRE </span><span class="cls_010">14</span><span class="cls_014"> DAY </span><span class="cls_010">S</span><span class="cls_014">ENSORS</span><span class="cls_010">)</span></div>
<div style="position:absolute;left:40.44px;top:467.23px" class="cls_005"><span class="cls_005">1</span><span class="cls_006"> </span><span class="cls_005">R</span><span class="cls_006">EADER</span><span class="cls_005">/1095</span><span class="cls_006"> </span><span class="cls_005">D</span><span class="cls_006">AYS</span></div>
<div style="position:absolute;left:310.01px;top:467.23px" class="cls_005"><span class="cls_005">1</span><span class="cls_006"> </span><span class="cls_005">U</span><span class="cls_006">NIT</span><span class="cls_005">/30</span><span class="cls_006"> </span><span class="cls_005">D</span><span class="cls_006">AYS </span><span class="cls_005">(1</span><span class="cls_006"> </span><span class="cls_005">U</span><span class="cls_006">NIT </span><span class="cls_005">=</span><span class="cls_006"> </span><span class="cls_005">1</span><span class="cls_006"> MONTH OF SENSORS AND SUPPLIES</span><span class="cls_005">)</span></div>
<div style="position:absolute;left:40.44px;top:482.23px" class="cls_005"><span class="cls_005">L</span><span class="cls_006">ENGTH OF </span><span class="cls_005">N</span><span class="cls_006">EED</span><span class="cls_005">:</span><span class="cls_006"> </span><span class="cls_005">L</span><span class="cls_006">IFETIME</span><span class="cls_005">-</span><span class="cls_006">UNLESS SPECIFIED OTHERWISE</span><span class="cls_005">:</span></div>
<div style="position:absolute;left:310.01px;top:482.23px" class="cls_005"><span class="cls_005">L</span><span class="cls_006">ENGTH OF </span><span class="cls_005">N</span><span class="cls_006">EED</span><span class="cls_005">:</span><span class="cls_006"> </span><span class="cls_005">L</span><span class="cls_006">IFETIME</span><span class="cls_005">-</span><span class="cls_006">UNLESS SPECIFIED OTHERWISE</span><span class="cls_005">:</span></div>
<div style="position:absolute;left:36.00px;top:524.47px" class="cls_004"><span class="cls_004">Diagnosis (ICD10):</span></div>
<div style="position:absolute;left:51.96px;top:544.75px" class="cls_005"><span class="cls_005">E10.9</span></div>
<div style="position:absolute;left:114.02px;top:545.17px" class="cls_017"><span class="cls_017">E11.65</span></div>
<div style="position:absolute;left:176.06px;top:544.75px" class="cls_005"><span class="cls_005">E10.65</span></div>
<div style="position:absolute;left:238.13px;top:544.75px" class="cls_005"><span class="cls_005">E11.8</span></div>
<div style="position:absolute;left:301.01px;top:544.75px" class="cls_005"><span class="cls_005">E11.9</span></div>
<div style="position:absolute;left:361.15px;top:544.75px" class="cls_005"><span class="cls_005">Other:</span></div>
<div style="position:absolute;left:36.00px;top:559.87px" class="cls_004"><span class="cls_004">Prescribed Number of Glucose Tests Per Day:</span></div>
<div style="position:absolute;left:289.01px;top:558.55px" class="cls_003"><span class="cls_003">4</span></div>
<div style="position:absolute;left:36.00px;top:583.06px" class="cls_004"><span class="cls_004">Current Insulin Regimen:</span></div>
<div style="position:absolute;left:51.96px;top:597.82px" class="cls_005"><span class="cls_005">Insulin Pump</span></div>
<div style="position:absolute;left:141.98px;top:601.54px" class="cls_005"><span class="cls_005">Multiple Daily Injections-Number Per Day:</span></div>
<div style="position:absolute;left:344.11px;top:597.82px" class="cls_003"><span class="cls_003">3</span></div>
<div style="position:absolute;left:423.07px;top:601.54px" class="cls_005"><span class="cls_005">Other:</span></div>
<div style="position:absolute;left:36.00px;top:625.30px" class="cls_005"><span class="cls_005">I certify that I am the physician identified in the “Physician Information” section above and hereby attest that the medical necessity</span></div>
<div style="position:absolute;left:36.00px;top:636.34px" class="cls_005"><span class="cls_005">information is true, accurate, and complete to the best of my knowledge. I understand that any falsification, omission, or concealment of</span></div>
<div style="position:absolute;left:36.00px;top:647.26px" class="cls_005"><span class="cls_005">material fact may subject me to administrative, civil, or criminal liability. The patient/caregiver is capable and has successfully</span></div>
<div style="position:absolute;left:36.00px;top:658.30px" class="cls_005"><span class="cls_005">completed or will be trained on the proper use of the products prescribed on this order.</span></div>
<div style="position:absolute;left:36.00px;top:686.02px" class="cls_010"><span class="cls_010">Physician Signature:</span></div>
<div style="position:absolute;left:395.11px;top:684.94px" class="cls_011"><span class="cls_011">Date:</span></div>
<div style="position:absolute;left:36.00px;top:710.26px" class="cls_014"><span class="cls_014">It is ultimately the responsibility of the healthcare professional/persons associated with the patient’s care to determine and document the appropriate</span></div>
<div style="position:absolute;left:36.00px;top:719.98px" class="cls_014"><span class="cls_014">diagnosis (es) and code(s) for the patient’s condition. Abbott does not guarantee that the use of any information provided in this form will result in</span></div>
<div style="position:absolute;left:36.00px;top:729.70px" class="cls_014"><span class="cls_014">coverage or payment by any third-party payer. Each healthcare provider is ultimately responsible for verifying codes, coverage, and payment policies</span></div>
<div style="position:absolute;left:36.00px;top:739.42px" class="cls_014"><span class="cls_014">used to ensure that they are accurate for the services and items provided.</span></div>
<div style="position:absolute;left:36.00px;top:753.60px" class="cls_019"><span class="cls_019">See reverse for Indications and Important Safety Information.</span></div>
</div>
<?php
 } 
?>
</body>
</html>
