
<body class="az-body">

    <div class="az-signin-wrapper">
      <div class="az-card-signin">
            <img src="<?php echo base_url();?>img/PandaProject.png" alt="" width="280px" style="margin-top:30px;">
        <div class="az-signin-header">
          <h2>Welcome back!</h2>
          <h4>Please sign in to continue</h4>
          <p style="color:red"><?php echo $this->session->flashdata("authentication_error"); ?></p>
          <form action="<?php echo base_url('admin/dashboard/login_process');?>" method="post">
            <div class="form-group">
              <label>User Name</label>
              <input type="text" name="username" class="form-control" placeholder="Enter username" value="">
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" placeholder="Enter password" value="">
            </div>
            <button class="btn btn-az-primary btn-block">Login</button>
          </form>
        </div>       
      </div>
    </div>

  </body>

</html>
