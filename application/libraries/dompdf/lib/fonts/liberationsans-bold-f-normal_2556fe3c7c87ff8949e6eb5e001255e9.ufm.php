<?php return array (
  'codeToName' => 
  array (
    32 => 'uni0020',
    35 => 'uni0023',
    49 => 'uni0031',
    50 => 'uni0032',
    51 => 'uni0033',
    52 => 'uni0034',
    53 => 'uni0035',
    56 => 'uni0038',
    57 => 'uni0039',
    58 => 'uni003a',
    65 => 'uni0041',
    66 => 'uni0042',
    67 => 'uni0043',
    68 => 'uni0044',
    69 => 'uni0045',
    70 => 'uni0046',
    72 => 'uni0048',
    73 => 'uni0049',
    76 => 'uni004c',
    77 => 'uni004d',
    78 => 'uni004e',
    79 => 'uni004f',
    80 => 'uni0050',
    81 => 'uni0051',
    82 => 'uni0052',
    83 => 'uni0053',
    84 => 'uni0054',
    95 => 'uni005f',
    97 => 'uni0061',
    98 => 'uni0062',
    99 => 'uni0063',
    100 => 'uni0064',
    101 => 'uni0065',
    102 => 'uni0066',
    103 => 'uni0067',
    104 => 'uni0068',
    105 => 'uni0069',
    107 => 'uni006b',
    108 => 'uni006c',
    109 => 'uni006d',
    110 => 'uni006e',
    111 => 'uni006f',
    112 => 'uni0070',
    114 => 'uni0072',
    115 => 'uni0073',
    116 => 'uni0074',
    117 => 'uni0075',
    120 => 'uni0078',
    121 => 'uni0079',
    8217 => 'uni2019',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Liberation Sans',
  'FullName' => 'Liberation Sans Bold',
  'Version' => 'Version 2.00.1',
  'PostScriptName' => 'LiberationSans-Bold',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '105',
  'UnderlinePosition' => '-106',
  'FontHeightOffset' => '0',
  'Ascender' => '800',
  'Descender' => '-200',
  'FontBBox' => 
  array (
    0 => '-10',
    1 => '-212',
    2 => '827',
    3 => '725',
  ),
  'StartCharMetrics' => '51',
  'C' => 
  array (
    32 => 278.0,
    35 => 556.0,
    49 => 556.0,
    50 => 556.0,
    51 => 556.0,
    52 => 556.0,
    53 => 556.0,
    56 => 556.0,
    57 => 556.0,
    58 => 333.0,
    65 => 722.0,
    66 => 722.0,
    67 => 722.0,
    68 => 722.0,
    69 => 667.0,
    70 => 611.0,
    72 => 722.0,
    73 => 278.0,
    76 => 611.0,
    77 => 833.0,
    78 => 722.0,
    79 => 778.0,
    80 => 667.0,
    81 => 778.0,
    82 => 722.0,
    83 => 667.0,
    84 => 611.0,
    95 => 556.0,
    97 => 556.0,
    98 => 611.0,
    99 => 556.0,
    100 => 611.0,
    101 => 556.0,
    102 => 333.0,
    103 => 611.0,
    104 => 611.0,
    105 => 278.0,
    107 => 556.0,
    108 => 278.0,
    109 => 889.0,
    110 => 611.0,
    111 => 611.0,
    112 => 611.0,
    114 => 389.0,
    115 => 556.0,
    116 => 333.0,
    117 => 611.0,
    120 => 556.0,
    121 => 556.0,
    8217 => 278.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJztz8dKbkEQRtFtzjnn7L3m8P4P58+ZCeJAB4qsBdXwVQe66pvGhnXjkxMPXfavi/4P6bqr5t/tb7bcSQuddzBKx60P3afWmu2s8e666bSdD99+HtVEL+231XRLPbba1Kh3225zzbTY3iittN1kR8ONw+6/PiwAAAAAAAAAAAAAAAAAAAD8Gq8//QEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOBvegPx+QT8',
  '_version_' => 6,
);