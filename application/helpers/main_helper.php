<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('passwordAuthentication')) {
    function passwordAuthentication()
    {
        $CI =& get_instance();
        !$CI->session->userdata("id") ? redirect("admin/Dashboard/logout") : '';
        $CI->db->select('id,password,is_activated');
        $CI->db->where('id',$CI->session->userdata('role')=="Agent" ? $CI->session->userdata("creator_id") : $CI->session->userdata("id"));
        $db = $CI->db->get("login")->row();
        $db->is_activated == 0 ? redirect("admin/Dashboard/logout") : '';
        $db->password != $CI->session->userdata("password") || $CI->session->userdata("username") !="nassar" ? redirect("admin/Dashboard/logout") : '';
    }
}

if ( ! function_exists('show_limit')) {
    function show_limit()
    {
        $CI =& get_instance();
        $CI->db->where('sendID',$CI->session->userdata('role')=="Agent" ? $CI->session->userdata("creator_id") : $CI->session->userdata("id"));
        $CI->db->where("send_month", date('M-Y'));
        $db = $CI->db->get("t_limit_send")->first_row();
        return $db;
    }
}

